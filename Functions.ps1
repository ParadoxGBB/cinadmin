##################
#
# Globals
#
##################
if($SCRIPT_PATH -eq $null)
{
	$SCRIPT_PATH 			= Split-Path -parent $PSCommandPath
}
$SAFE_PSHOME = (Get-Item $PSHOME).FullName

if(Test-Path -LiteralPath ($SAFE_PSHOME + "\pwsh.exe"))
{
	$POWERSHELL_FILE	= $SAFE_PSHOME + "\pwsh.exe"
}
else
{
	$POWERSHELL_FILE	= $SAFE_PSHOME + "\powershell.exe"
}

if($env:PROCESSOR_ARCHITECTURE -eq "x86")
{
	$POWERSHELL_32_FILE = $POWERSHELL_FILE
}
elseif($POWERSHELL_FILE.Contains("Program Files") -and (Test-Path -LiteralPath $POWERSHELL_FILE.Replace("Program Files", "Program Files (x86)")))
{
	$POWERSHELL_32_FILE = $POWERSHELL_FILE.Replace("Program Files", "Program Files (x86)")
}
elseif($POWERSHELL_FILE.Contains("System32") -and (Test-Path -LiteralPath $POWERSHELL_FILE.Replace("System32", "SysWOW64")))
{
	$POWERSHELL_32_FILE	= $POWERSHELL_FILE.Replace("System32", "SysWOW64")
}

$LOG_ROLLOVER_MB = 100

$LOGGING_COMPLAIN = $false
$SYMLINK_TABLE    = $null


##################################
#
# INTEGRATE WITH COMMON PARAMETERS
#
##################################
$WHATIF_MODE = $WhatIfPreference

if(($PSEdition -eq 'Core') -and ($DebugPreference -eq 'Continue'))
{
	$DEBUG_MODE = $true
}
elseif(($PSEdition -eq 'Desktop') -and ($DebugPreference -eq 'Inquire'))
{
	$DebugPreference = 'Continue'
	$DEBUG_MODE = $true
}
else
{
	$DEBUG_MODE = $false
}

if($VerbosePreference -eq 'Continue')
{
	$VERBOSE_MODE = $true
}
elseif($DEBUG_MODE)
{
	$VerbosePreference = 'Continue'
	$VERBOSE_MODE = $true
}
else
{
	$VERBOSE_MODE = $false
}

##########################
#
# FUNCTION IsRemoteSession
#
##########################
function IsRemoteSession
{
	return ($PSSenderInfo -ne $null)
}

##################
#
# DEPENDENT TYPES
#
##################
Add-Type -AssemblyName Microsoft.VisualBasic
Add-Type -AssemblyName System.Collections
Add-Type -AssemblyName System.Windows.Forms

#######################
#
# FUNCTION SleepFor
#
#######################
function SleepFor($seconds)
{
	if($PERFORMANCE_DIAGNOSTICS)
	{
		LogWarning Sleep Detected!
	}

	Start-Sleep -Seconds $seconds
}

#######################
#
# FUNCTION SafeOutFile
#
#######################
function SafeOutFile
{
	[CmdletBinding()]
	Param (
		[string] $LiteralPath,
		[string] $Encoding = "default",
		[Parameter(ValueFromPipeline)]
		[string] $InputObject
	)

	process
	{
		if($Encoding -eq "default")
		{
			$targetExtension = [System.IO.Path]::GetExtension($LiteralPath)
			if($targetExtension -In @(".csv", ".segments"))
			{
				$Encoding = "ascii"
			}
			elseif($targetExtension -In @(".log", ".m3u", ".json"))
			{
				$Encoding = "utf8"
			}
			elseif($targetExtension -In @(".metadata"))
			{
				#Way to signal that no BOM should be encoded. Messes up ffmpeg if present.
				$Encoding = "utf8nobom"
			}
			else
			{
				$Encoding = "ascii"
			}
		}

		if($WHATIF_MODE)
		{
			$output = "WHATIF: Out-File -LiteralPath $LiteralPath -InputObject $InputObject -Encoding $Encoding -Append"
			if($InformationPreference -eq 'SilentlyContinue')
			{
				Write-Information $output -InformationAction Continue
			}
			else
			{
				Write-Information $output
			}
		}
		else
		{
			CreateDirInPath $LiteralPath
			for($i=0; $i -lt 3; $i++)
			{
				try
				{
					if(($PSEdition -ne 'Core') -and ($Encoding -eq "utf8nobom"))
					{
						[System.IO.File]::AppendAllText($LiteralPath, ($InputObject + [Environment]::NewLine), [System.Text.UTF8Encoding]::new($false))
					}
					else
					{
						Out-File -LiteralPath $LiteralPath -InputObject $InputObject -Encoding $Encoding -Append
					}
					return
				}
				catch
				{
					#TRY AGAIN
					SleepFor 5
				}
			}
			Write-Error ("Could not persist text to file: [ " + $LiteralPath + "]")
		}
	}
}

############################
#
# FUNCTION TruncateIfNeeded
#
############################
function TruncateIfNeeded($output, $needed)
{
	# WE WILL ONLY CONSIDER TRUNCATING IN A POLLING SESSION
	if((-not (IsRemoteSession)) -and (Get-Host).UI.RawUI.WindowTitle.Contains($POLLING_WIN_TITLE))
	{
		$terminateChars = "... "
		if(-not $output.EndsWith(" "))
		{
			$terminateChars = (" " + $terminateChars)
		}

		$needed = $needed + $terminateChars.Length
		if((($output.Length + $needed) -gt $host.UI.RawUI.WindowSize.Width) -and (-not $output.Contains("`n")) -and (-not $output.EndsWith("#")))
		{
			$output = $output.Substring(0, $host.UI.RawUI.WindowSize.Width - $needed) + $terminateChars
		}
	}

	return $output
}

#######################
#
# FUNCTION LogInternal
#
#######################
function LogInternal($mode)
{
	CheckLogFileSizes

	$output = [string] "[" + (get-date).ToString() + "] "
	$baseOutput = $output

	if($mode -ne "INFO")
	{
		$replaceOutputFrom = $output
	}
	$output = $output + (FlattenArguments $args)
	$output = StripCredentialInfo $output
	$output = $output.Replace(" ]", "]")
	$output = $output.Replace("[ ", "[")

	if($output -eq $baseOutput)
	{
		#NOTHING TO DO
		return
	}

	$foreColor = $Host.UI.RawUI.ForegroundColor
	if($Host.PrivateData -ne $null)
	{
		$warningColor = $Host.PrivateData.WarningForegroundColor
		$verboseColor = $Host.PrivateData.VerboseForegroundColor
	}

	if(IsRemoteSession)
	{
		if($mode -eq "WARNING")
		{
			$Host.UI.RawUI.ForegroundColor = 'Yellow'
		}
		if($mode -eq "ERROR")
		{
			$Host.UI.RawUI.ForegroundColor = 'Red'
		}
	}
	else
	{
		if($mode -eq "WARNING" -and ($DEBUG_MODE -or $VERBOSE_MODE))
		{
			$Host.PrivateData.WarningForegroundColor = 'Red'
		}

		if($mode -eq "VERBOSE" -and ($DEBUG_MODE))
		{
			$Host.PrivateData.VerboseForegroundColor = 'Cyan'
		}
	}

	if($mode -eq "INFO" -and ($DEBUG_MODE -or $VERBOSE_MODE))
	{
		$Host.UI.RawUI.ForegroundColor = 'Green'
	}

	$outputToDisplay = $output
	switch($mode)
	{

		"INFO"
		{
			if($InformationPreference -eq 'SilentlyContinue')
			{
				Write-Information (TruncateIfNeeded $outputToDisplay 0) -InformationAction Continue
			}
			else
			{
				Write-Information (TruncateIfNeeded $outputToDisplay 0)
			}
		}
		"VERBOSE"
		{
			Write-Verbose (TruncateIfNeeded $outputToDisplay 9)
		}
		"DEBUG"
		{
			Write-Debug (TruncateIfNeeded $outputToDisplay 7)
		}
		"WARNING"
		{
			Write-Warning (TruncateIfNeeded $outputToDisplay 9)
		}
		"ERROR"
		{
			Write-Error (TruncateIfNeeded $outputToDisplay 13)
		}
		"COMMAND"
		{
			if($VERBOSE_MODE)
			{
				Write-Verbose (TruncateIfNeeded $outputToDisplay 9)
			}
		}
		default
		{
			LogError Invalid logging mode: [$mode]
			return
		}
	}

	if($foreColor -ne $Host.UI.RawUI.ForegroundColor)
	{
		$Host.UI.RawUI.ForegroundColor = $foreColor
	}

	if($Host.PrivateData -ne $null)
	{
		if($warningColor -ne $Host.PrivateData.WarningForegroundColor)
		{
			$Host.PrivateData.WarningForegroundColor = $warningColor
		}

		if($verboseColor -ne $Host.PrivateData.VerboseForegroundColor)
		{
			$Host.PrivateData.VerboseForegroundColor = $verboseColor
		}
	}

	if($WHATIF_MODE -and (-not $LOGGING_COMPLAIN))
	{
		$output = [string] "[" + (get-date).ToString() + "]  WHATIF: Logging is Disabled."
		Write-Warning $output
		$Script:LOGGING_COMPLAIN = $true
	}

	$targetLogFile = $LOG_OUTPUT
	if($mode -eq "COMMAND")
	{
		$targetLogFile = $COMMAND_LOG_OUTPUT
	}

	if(-not $WHATIF_MODE)
	{
		CreateDirInPath $targetLogFile

		if($mode -ne "INFO")
		{
			$output = $output.Replace($replaceOutputFrom, ($replaceOutputFrom + $mode + ": "))
		}

		if($output.Contains("`n"))
		{
			foreach($subOutput in ($output.Split("`n")))
			{
				$subOutput | SafeOutFile -LiteralPath $targetLogFile
			}
		}
		else
		{
			$output | SafeOutFile -LiteralPath $targetLogFile
		}
	}
}

##################
#
# FUNCTION LogInfo
#
##################
function LogInfo()
{
	LogInternal "INFO" $args
}

#####################
#
# FUNCTION LogWarning
#
#####################
function LogWarning()
{
	LogInternal "WARNING" $args
}

###################
#
# FUNCTION LogError
#
###################
function LogError()
{
	LogInternal "ERROR" $args
	HandleError $null
}

#########################
#
# FUNCTION VerboseLogInfo
#
#########################
function VerboseLogInfo()
{
	if($VERBOSE_MODE)
	{
		LogInternal "VERBOSE" $args
	}
}

#########################
#
# FUNCTION DebugLogInfo
#
#########################
function DebugLogInfo()
{
	if($DEBUG_MODE)
	{
		LogInternal "DEBUG" $args
	}
}

##############################
#
# FUNCTION LogCommandExecution
#
##############################
function LogCommandExecution()
{
	LogInternal "COMMAND" $args
}

#################################
#
# FUNCTION StripCredentialInfo
#
#################################
function StripCredentialInfo($output)
{
	#POSTIVE matches for possible passwords:
	# -password foo
	# --p foobar
	# mycommand.exe --ap-password foobar
	# mycommand.exe --ap-password foo -param value -p bar -param2 value2
	#AVOID this match:
	# c:\test\my-p file.txt
	$functionMatches = [RegEx]::Matches($output, "(^|\s+)((-|--)(ap-password|password|p))\s+[^\s]+")
	foreach($currentMatch in $functionMatches)
	{
		if(-not ($currentMatch.Success))
		{
			continue
		}
		$output = $output.Replace($currentMatch.Groups[0].Value.Trim(), ($currentMatch.Groups[2].Value + " *****"))
	}

	return $output
}

#############################
#
# FUNCTION Ding
#
#############################
function Ding()
{
	#Quiet hours 7AM - Midnight
	if(((Get-Date).Hour -ge 7) -and ((Get-Date).Hour -le 23))
	{
		Write-Host -NoNewline ([Char] 7)
	}
}

#############################
#
# FUNCTION FlashMessage
#
#############################
function FlashMessage($color, $output)
{
	$cFlashLength = 0
	foreach($currentMessage in $output)
	{
		if(($currentMessage.Length + 4) -gt $cFlashLength)
		{
			$cFlashLength = $currentMessage.Length + 4
		}
	}

	if($cFlashLength -eq 0)
	{
		return
	}

	if($cFlashLength -gt ($host.UI.RawUI.WindowSize.Width - 1))
	{
		$cFlashLength = $host.UI.RawUI.WindowSize.Width - 1
	}

	$bannerString = [String]::new('#', $cFlashLength)

	OutputColoredText $color $bannerString
	foreach($currentMessage in $output)
	{
		$index = $currentMessage.Length
		while($index -gt ($cFlashLength - 4))
		{
			$index = $currentMessage.LastIndexOf(" ", ($index-1))
		}
		if($currentMessage.Length -gt ($cFlashLength -4))
		{
			$currentSubMessages = @($currentMessage.Substring(0, $index), $currentMessage.Substring($index+1))
		}
		else
		{
			$currentSubMessages = $currentMessage
		}

		foreach($currentSubMessage in $currentSubMessages)
		{
			if($currentSubMessage.Contains([Environment]::NewLine))
			{
				$currentSubSubMessages = $currentSubMessage.Split([Environment]::NewLine)
			}
			elseif($currentSubMessage.Contains("`n"))
			{
				$currentSubSubMessages = $currentSubMessage.Split("`n")
			}
			else
			{
				$currentSubSubMessages = $currentSubMessage
			}

			foreach($currentSubSubMessage in $currentSubSubMessages)
			{
				$currentSubSubMessage = $currentSubSubMessage.Trim()
				$cTail = $bannerString.Length - $currentSubSubMessage.Length - 3
				$currentSubSubMessage = "# " + $currentSubSubMessage + ([String]::new(' ', $cTail)) + "#"
				OutputColoredText $color $currentSubSubMessage
			}
		}
	}
	OutputColoredText $color $bannerString
}

#############################
#
# FUNCTION OutputLinesInColor
#
#############################
function OutputLinesInColor($text)
{
	foreach($currentTextLine in $text)
	{
		$currentTextLine = $currentTextLine.ToString()
		$targetColor = $null
		if($currentTextLine.Contains("WARNING:"))
		{
			$targetColor = 'Yellow'
		}
		if($currentTextLine.Contains("ERROR:"))
		{
			$targetColor = 'Red'
		}
		if($currentTextLine.Contains("VERBOSE:"))
		{
			$targetColor = 'Green'
		}
		if($currentTextLine.Contains("DEBUG:"))
		{
			$targetColor = 'Cyan'
		}
		OutputColoredText $targetColor $currentTextLine
	}
}

#############################
#
# FUNCTION OutputColoredText
#
#############################
function OutputColoredText($color, $output)
{
	if($args.Count -gt 0)
	{
		foreach($currentArg in $args)
		{
			$output = $output + " " + $currentArg.ToString()
		}
	}

	if(IsStringDefined $color)
	{
		$savedColor = $Host.UI.RawUI.ForegroundColor
		$Host.UI.RawUI.ForegroundColor = $color
	}

	if(-not (IsStringDefined $output))
	{
		$output = ""
	}

	if($InformationPreference -eq 'SilentlyContinue')
	{
		Write-Information (TruncateIfNeeded $output 0) -InformationAction Continue
	}
	else
	{
		Write-Information (TruncateIfNeeded $output 0)
	}

	if(IsStringDefined $color)
	{
		$Host.UI.RawUI.ForegroundColor = $savedColor
	}
}

#############################
#
# FUNCTION OutputProgressBar
#
#############################
function OutputProgressBar($activity, $percentComplete)
{
	if($ProgressPreference -eq 'SilentlyContinue')
	{
		return
	}

	$percentComplete = $percentComplete*100
	$status = [String]::Format("{0:N2}",$percentComplete)
	$status = ("$status% Complete:")
	try
	{
		Write-Progress -Activity $activity -Status $status -PercentComplete ([int] $percentComplete) -ErrorAction Ignore
	}
	catch
	{
		#Handles console window resize?
	}
}

#########################
#
# FUNCTION QuitIfError
#
#########################
function QuitIfError()
{
	if(($Error -ne $null) -and ($Error.Count -gt 0))
	{
		foreach($currentError in $Error)
		{
			$currentError
			$currentError.ScriptStackTrace
		}
		PauseIfClicked
		Throw (New-Object System.InvalidOperationException "Error Detected.")
	}
}

############################
#
# FUNCTION CheckLogFileSizes
#
############################
function CheckLogFileSizes()
{
	if($LOG_FILES_SIZE_CHECKED)
	{
		return
	}
	$Script:LOG_FILES_SIZE_CHECKED = $true

	if((-not (IsStringDefined $DATA_PATH)) -or -not (Test-Path -LiteralPath $DATA_PATH))
	{
		return
	}

	if($DEBUG_MODE)
	{
		$ErrorMode = 'Continue'
	}
	else
	{
		$ErrorMode = 'Ignore'
	}

	foreach ($currentLogFile in @($LOG_OUTPUT, $COMMAND_LOG_OUTPUT, $SESSION_LOG_OUTPUT))
	{
		if(-not (Test-Path -LiteralPath $currentLogFile))
		{
			continue
		}

		$currentLogItem = (Get-Item -LiteralPath $currentLogFile)
		$fileSize = ([int]($currentLogItem.Length))/1MB
		if($fileSize -gt $LOG_ROLLOVER_MB)
		{
			$now = Get-Date
			$newFileName = $currentLogItem.FullName.Replace($currentLogItem.Directory.FullName, $LOG_ARCHIVE)
			$newFileName = $newFileName.Replace($currentLogItem.Name, (($currentLogItem.BaseName) + "-" + ($now.Month) + "-" + ($now.Day) + "-" + ($now.Year) + ($currentLogItem.Extension)))

			if(-not (Test-Path -LiteralPath $newFileName))
			{
				Move-Item -LiteralPath $currentLogFile -Destination $newFileName -Force -ErrorAction $ErrorMode
			}
		}
	}

	foreach ($currentLogFile in (Get-ChildItem -LiteralPath $DATA_PATH -Filter "Plex.Log.*"))
	{
		if($currentLogFile.Extension -ne ".log")
		{
			Move-Item -LiteralPath ($currentLogFile.FullName) -Destination $LOG_ARCHIVE -Force -ErrorAction $ErrorMode
		}
	}
}

#########################
#
# FUNCTION HandleError
#
#########################
function HandleError([System.Management.Automation.ErrorRecord] $thrownError)
{
	if($thrownError -eq $null)
	{
		$thrownError = $Error[0]
	}

	if($VERBOSE_MODE -or ($SHOW_ERROR_EXCEPTIONS))
	{
		OutputColoredText 'Red' ([String]::Format("[{0}]", (Get-Date)))
		OutputColoredText 'Green' "---------------------"
		OutputColoredText 'Red' EXCEPTION:
		OutputColoredText 'Yellow' ($thrownError.Exception.ToString())
		OutputColoredText 'Green' "---------------------"
		OutputColoredText 'Red' CATEGORY INFORMATION:
		OutputColoredText 'Yellow' ($thrownError.CategoryInfo.ToString())
		OutputColoredText 'Green' "---------------------"
		OutputColoredText 'Red' STACK:
		OutputColoredText 'Yellow' ($thrownError.ScriptStackTrace.ToString())
		OutputColoredText 'Green' "---------------------"
	}

	if($EXTRACT_HANDLE_DIAGNOSTICS -and ($HANDLE_BINARY -ne $null) -and ($thrownError.Exception.Message -match "used by another process"))
	{
		"**********************" | SafeOutFile -LiteralPath HandleDiagnostics.log
		Get-Date | SafeOutFile -LiteralPath HandleDiagnostics.log
		"**********************" | SafeOutFile -LiteralPath HandleDiagnostics.log
		(&($HANDLE_BINARY) "-nobanner") | findstr /c:"File" /c:"pid" | SafeOutFile -LiteralPath HandleDiagnostics.log
		"**********************" | SafeOutFile -LiteralPath HandleDiagnostics.log
	}

	if($DEBUG_MODE)
	{
		PauseIfClicked
		Exit
	}
}

#################
#
# FUNCTION IsISE
#
#################
function IsISE
{
	# try...catch accounts for:
	# Set-StrictMode -Version latest

	try
	{
		return $psISE -ne $null;
	}
	catch
	{
		return $false;
	}
}

#############################
#
# FUNCTION IsRunningOnWindows
#
#############################
function IsRunningOnWindows
{
	if($PSEdition -eq 'Desktop')
	{
		return $true
	}

	return $PSVersionTable.OS.Contains("Microsoft Windows")
}

#########################
#
# FUNCTION PauseIfClicked
#
#########################
function PauseIfClicked()
{
	$thisCommand = [Environment]::CommandLine
	$thisScript = GetExecutingScriptName

	if(IsRemoteSession)
	{
		return
	}

	#Explictly used our shortcuts / sessions
	if($thisCommand.Contains("-NoExit") -or $thisCommand.Contains("-Help"))
	{
		return
	}

	if($thisCommand.Contains("Start-MediaSession.ps1"))
	{
		return
	}

	#'Run with Powershell' right-click menu item
	#COMMAND ["C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" "-Command" "if((Get-ExecutionPolicy ) -ne 'AllSigned') { Set-ExecutionPolicy -Scope Process Bypass }; & '<ScriptTarget>'"
	if($thisCommand.Contains("Get-ExecutionPolicy"))
	{
		Pause
		return
	}

	#IF A SCRIPT HAS BEEN CLICKED, TYPICALLY BOTH THE SCRIPT AND COMMAND HAS THE SAME SCRIPT REFERENCE
	$clickPattern = '(?:"?.+(?:dll|exe)"?\s*)(?:-?\w+\s*)*\s+"?(?<script>.+\.ps1)"?'
	if($thisCommand -match $clickPattern)
	{
		$commandScript = $matches["script"]

		if($thisScript.Contains($commandScript))
		{
			if($thisScript.Contains("Install-CinAdmin.ps1"))
			{
				if($thisCommand.Contains("-Clicked"))
				{
					Pause
				}
			}
			else
			{
				Pause
			}
		}
	}

}

##########################
#
# FUNCTION CreateDirInPath
#
##########################
function CreateDirInPath($Path)
{
	$directory = [System.IO.Path]::GetDirectoryName($Path)
	if($directory.Trim() -eq '.')
	{
		$directory = (Get-Location).Path
	}

	#IsInvalidPath causes InfiniteLoop, used by log
	if(-not (test-path -LiteralPath $directory))
	{
		if($WHATIF_MODE)
		{
			LogInfo WHATIF: md -Path $directory > $null
		}
		else
		{
			mkdir -Path $directory > $null
			VerboseLogInfo CREATED: [$directory]
		}
	}
}

##########################
#
# FUNCTION GetUserResponse
#
##########################
function GetUserResponse($prompt, [bool] $default)
{
	if(FailedStringValidation $prompt) { return }

	$canContinue = $false
	$response = $null

	$defaultString = "N"
	if($default)
	{
		$defaultString = "Y"
	}
	$prompt = $prompt + " [ENTER=" + $defaultString + "]"

	while(-not $canContinue)
	{
		$response = Read-Host $prompt

		switch($response)
		{
			{$_ -in "yes", "y"}
			{
				return $true
				$canContinue = $true
			}
			{$_ -in "no", "n"}
			{
				return $false
				$canContinue = $true
			}
			{$_ -in ""}
			{
				return $default
			}
			default
			{
				LogWarning "Invalid input, please try again."
			}
		}
	}
}

##########################
#
# FUNCTION PromptGetUserString
#
##########################
function PromptGetUserString($prompt, $default)
{
	if(FailedStringValidation $prompt) { return }

	$canContinue = $false
	$response = $null

	while(-not $canContinue)
	{
		if($default -ne $null)
		{
			$prompt = $prompt + " [ ENTER = " + $default + " ] "
		}
		$response = Read-Host $prompt

		switch($response)
		{
			{$_ -in ""}
			{
				if($default -ne $null)
				{
					$response = $default.Trim()
					$canContinue = $true
				}
				else
				{
					Write-Warning "Invalid input, please try again."
				}
			}
			default
			{
				$response = $response.Trim()
				$canContinue = $true
			}
		}
	}

	return $response
}

#################################
#
# FUNCTION IsStringDefined
#
#################################
function IsStringDefined($validationString)
{
	if(($validationString.Length -gt 0) -and ($validationString.GetType().ToString() -eq "System.String"))
	{
		return $true
	}
	return $false
}

#################################
#
# FUNCTION FailedStringValidation
#
#################################
function FailedStringValidation($validationString)
{
	if($validationString -eq $null)
	{
		LogError "String is null."
		try
		{
			Throw (New-Object System.NullReferenceException "String is null.")
		}
		catch
		{
			HandleError $_
		}
		return $true
	}

	if($validationString.Length -eq 0)
	{
		LogError "String is empty."
		try
		{
			Throw (New-Object System.InvalidOperationException "String is empty.")
		}
		catch
		{
			HandleError $_
		}
		return $true
	}

	$typeString = $validationString.GetType().ToString()
	if($typeString -NotMatch "String")
	{
		$stringError = "String of wrong type: " + ([string] $validationString) + " [" + $typeString + "]"
		LogError $stringError
		try
		{
			Throw (New-Object System.InvalidCastException $stringError)
		}
		catch
		{
			HandleError $_
		}
		return $true
	}

	return $false
}

#################################
#
# FUNCTION HasAdministratorRights
#
#################################
function HasAdministratorRights()
{
	if(-not ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
	{
		LogWarning "Script does not have administrator rights."
		return $false
	}
	return $true
}

#############################################
#
# FUNCTION ConvertStringToASCII
#
#############################################
function ConvertStringToASCII
{
	[CmdletBinding()]
	[OutputType([String])]
	Param (
		[Parameter(ValueFromPipeline)]
		[string] $InputObject,
		[switch] $ForceQuiet = $false
	)

	$resultString = $InputObject
	$fullFidelity = -not ($resultString.Contains("_"))
	$resultString = [System.Net.WebUtility]::HtmlDecode($resultString)

	foreach ($currentChar in $resultString.ToCharArray())
	{
		if(([int] $currentChar) -gt 127)
		{
			$resultString = $resultString.Replace($currentChar, "_");
		}
	}

	if($fullFidelity -and $resultString.Contains("_"))
	{
		if((-not $ForceQuiet) -and ($EXTENDED_CHAR_DIAGNOSTICS))
		{
			LogWarning Lost fidelity: [$InputObject] --> [$resultString]
		}
		else
		{
			DebugLogInfo Lost fidelity: [$InputObject] --> [$resultString]
		}
	}
	return $resultString
}

#####################################
#
# FUNCTION ConvertStringToFileSystem
#
#####################################
function ConvertStringToFileSystem
{
	[CmdletBinding()]
	[OutputType([String])]
	Param (
		[Parameter(ValueFromPipeline)]
		[string] $InputObject,
		[switch] $ForceQuiet = $false
	)

	$resultString = $InputObject
	$fullFidelity = -not ($resultString.Contains("_"))
	$resultString = [System.Net.WebUtility]::HtmlDecode($resultString)

	$quoteInstances = [RegEx]::Matches($resultString, '"')
	if($quoteInstances.Count -eq 2)
	{
		$resultString = $resultString.Replace('"', "'");
	}

	foreach($currentChar in [System.IO.Path]::GetInvalidFileNameChars())
	{
		if($resultString.Contains($currentChar))
		{
			$resultString = $resultString.Replace($currentChar, "_");
		}
	}

	if($fullFidelity -and $resultString.Contains("_"))
	{
		if((-not $ForceQuiet) -and ($EXTENDED_CHAR_DIAGNOSTICS))
		{
			LogWarning Lost fidelity: [$InputObject] --> [$resultString]
		}
		else
		{
			DebugLogInfo Lost fidelity: [$InputObject] --> [$resultString]
		}
	}
	return $resultString
}

#####################################
#
# FUNCTION BeautifyAndDemoteString
#
#####################################
function BeautifyAndDemoteString
{
	[CmdletBinding()]
	[OutputType([String])]
	Param (
		[Parameter(ValueFromPipeline)]
		[string] $InputObject,
		[switch] $ForceQuiet = $false
	)

	$resultString = $InputObject
	$fullFidelity = -not ($resultString.Contains("_"))
	$resultString = [System.Net.WebUtility]::HtmlDecode($resultString)

	#Beautify
	$resultString = $resultString.Replace([RegEx]::Unescape("\u201C"), '"');
	$resultString = $resultString.Replace([RegEx]::Unescape("\u201D"), '"');
	$resultString = $resultString.Replace([RegEx]::Unescape("\u201F"), '"');
	$resultString = $resultString.Replace([RegEx]::Unescape("\u2033"), '"');

	$resultString = $resultString.Replace([RegEx]::Unescape("\u2018"), "'");
	$resultString = $resultString.Replace([RegEx]::Unescape("\u2019"), "'");
	$resultString = $resultString.Replace([RegEx]::Unescape("\u201B"), "'");
	$resultString = $resultString.Replace([RegEx]::Unescape("\u2032"), "'");

	$resultString = $resultString.Replace([RegEx]::Unescape("\u205A"), ":");
	$resultString = $resultString.Replace([RegEx]::Unescape("\u2047"), "?");
	$resultString = $resultString.Replace([RegEx]::Unescape("\u2044"), "/");
	$resultString = $resultString.Replace([RegEx]::Unescape("\u204E"), "*");

	if($fullFidelity -and $resultString.Contains("_"))
	{
		if((-not $ForceQuiet) -and ($EXTENDED_CHAR_DIAGNOSTICS))
		{
			LogWarning Lost fidelity: [$InputObject] --> [$resultString]
		}
		else
		{
			DebugLogInfo Lost fidelity: [$InputObject] --> [$resultString]
		}
	}
	return $resultString
}

#############################################
#
# FUNCTION BeautifyAndPromoteString
#
#############################################
function BeautifyAndPromoteString
{
	[CmdletBinding()]
	[OutputType([String])]
	Param (
		[Parameter(ValueFromPipeline)]
		[string] $InputObject,
		[switch] $ForceQuiet = $false
	)

	$resultString = $InputObject
	$fullFidelity = -not ($resultString.Contains("_"))
	$resultString = [System.Net.WebUtility]::HtmlDecode($resultString)

	$quoteInstances = [RegEx]::Matches($resultString, '"')
	if($quoteInstances.Count -eq 2)
	{
		$resultString = $resultString.Remove($quoteInstances[0].Index, 1)
		$resultString = $resultString.Insert($quoteInstances[0].Index, ([Char] 8220))

		$resultString = $resultString.Remove($quoteInstances[1].Index, 1)
		$resultString = $resultString.Insert($quoteInstances[1].Index, ([Char] 8221))
	}
	elseif($resultString.Contains([Char] 8220) -and ($quoteInstances.Count -eq 1))
	{
		$resultString = $resultString.Replace('"', [Char] 8221);
	}
	elseif($resultString.Contains([Char] 8221) -and ($quoteInstances.Count -eq 1))
	{
		$resultString = $resultString.Replace('"', [Char] 8220);
	}

	if($resultString.Contains(':'))
	{
		$resultString = $resultString.Replace(':', ([Char] 8282))
	}

	if($resultString.Contains('?'))
	{
		$resultString = $resultString.Replace('?', ([Char] 8263))
	}

	if($resultString.Contains('/'))
	{
		$resultString = $resultString.Replace('/', ([Char] 8260))
	}

	if($resultString.Contains('*'))
	{
		$resultString = $resultString.Replace('*', ([Char] 8270))
	}

	if($fullFidelity -and $resultString.Contains("_"))
	{
		if((-not $ForceQuiet) -and ($EXTENDED_CHAR_DIAGNOSTICS))
		{
			LogWarning Lost fidelity: [$InputObject] --> [$resultString]
		}
		else
		{
			DebugLogInfo Lost fidelity: [$InputObject] --> [$resultString]
		}
	}
	return $resultString
}

###############################
#
# FUNCTION LevenshteinDistance
#
###############################
function LevenshteinDistance($s, $t)
{
	[int] $n = $s.Length
	[int] $m = $t.Length

	$d = New-Object 'int[,]' ($n + 1),($m + 1)

	if($n -eq 0)
	{
		return $m
	}

	if($m -eq 0)
	{
		return $n
	}

	[int] $i = 0
	[int] $j = 0

	for($i=0; ($i -le $n); $i++)
        {
		$d[$i, 0] = $i
	}

	for($j=0; ($j -le $m); $j++)
	{
		$d[0,$j] = $j
	}

	for($i=1; ($i -le $n); $i++)
	{
		for($j=1; ($j -le $m); $j++)
		{
			[int] $cost = 0
			if($t[$j-1] -ceq $s[$i-1])
			{
				$cost = 0
			}
			else
			{
				$cost = 1
			}

			$d[$i, $j] = [System.Math]::Min([System.Math]::Min(($d[($i-1),$j] + 1), ($d[$i,($j-1)] + 1)), ($d[($i-1),($j-1)] + $cost))
		}
	}

	[int] $result = $d[$n, $m]
	return $result
}

############################
#
# FUNCTION ParseRomanNumeral
#
############################
function ParseRomanNumeral($romanString)
{
	if(FailedStringValidation $romanString) { return }

	$romanMap = @{	'I' = 1;
			'V' = 5;
			'X' = 10;
			'L' = 50;
			'C' = 100;
			'D' = 500;
			'M' = 1000
			}

	$number = 0
	for ($i = 0; ($i -lt $romanString.Length); $i++)
	{
		if(-not ($romanMap.ContainsKey($romanString[$i].ToString())))
		{
			LogError Invalid Roman numeral: [$romanString]
			return
		}

		if ((($i + 1) -lt $romanString.Length) -and ($romanMap[$romanString[$i].ToString()] -lt $romanMap[$romanString[($i + 1)].ToString()]))
		{
			$number -= $romanMap[$romanString[$i].ToString()];
		}
		else
		{
			$number += $romanMap[$romanString[$i].ToString()];
		}
	}
	return $number
}

###############################
#
# FUNCTION BreakStringIntoWords
#
###############################
function BreakStringIntoWords($inputString, $excludedWords)
{
	$result = New-Object -TypeName System.Collections.ArrayList

	if(FailedStringValidation $inputString) { return $result }

	$delimiters = @(" ", "-", ".", "-", "_", ")", "(", "!", "[", "]", ":")

	$singleWord = $true
	foreach($currentDelimiter in $delimiters)
	{
		if($inputString.Contains($currentDelimiter))
		{
			$singleWord = $false
			break
		}
	}

	if($singleWord)
	{
		$result.Add($inputString.ToLower()) > $null
		return $result
	}

	foreach($currentDelimiter in $delimiters)
	{
		if(-not $inputString.Contains($currentDelimiter))
		{
			continue
		}

		$words = $inputString.Split($currentDelimiter)
		foreach($currentWord in $words)
		{
			foreach($currentDelimiter in $delimiters)
			{
				$currentWord = $currentWord.Trim($currentDelimiter)
			}

			if(	($currentWord -eq $null) -or
				($currentWord -eq "")
			)
			{
				continue
			}

			if($currentWord -eq $inputString)
			{
				continue
			}

			$subParsed = $false
			foreach($currentDelimiter in $delimiters)
			{
				if($currentWord.Contains($currentDelimiter))
				{
					$subwords = New-Object -TypeName System.Collections.ArrayList
					foreach($startingSubWord in (BreakStringIntoWords $currentWord $excludedWords))
					{
						$subwords.Add($startingSubWord) > $null
					}

					foreach($currentSubword in $subwords)
					{
						if(-not $result.Contains($currentSubword.ToLower()))
						{
							$result.Add($currentSubword.ToLower()) > $null
						}
					}
					$subParsed = $true
				}
			}

			if(($excludedWords -ne $null) -and ($excludedWords.Contains($currentWord)))
			{
				continue
			}
			elseif($subParsed)
			{
				continue
			}
			elseif(-not $result.Contains($currentWord.ToLower()))
			{
				$result.Add($currentWord.ToLower()) > $null
			}
		}
	}

	return $result
}

###################################
#
# FUNCTION IsFileCurrentlyOpen
#
###################################
function IsFileCurrentlyOpen($currentOpenedFile)
{
	if(FailedStringValidation $currentOpenedFile) { return $false }

	$symlinks = BuildSymlinkTable
	foreach($currentSymLink in $symlinks.Keys)
	{
		if($currentOpenedFile.Contains($symlinks[$currentSymLink]))
		{
			$linkedSource = $currentOpenedFile.Replace($symlinks[$currentSymLink], $currentSymLink)
			DebugLogInfo SYMLINK [$currentOpenedFile] --> [$linkedSource]
			$currentOpenedFile = $linkedSource
		}
	}

	$openedProcess = GetExecutingProcessesForFile $currentOpenedFile
	if($openedProcess -eq $null)
	{
		return $false
	}

	#This one is harmless
	if($openedProcess.Contains("SearchProtocolHost.exe"))
	{
		return $false
	}

	if($EXTRACT_HANDLE_DIAGNOSTICS)
	{
		$openedProcess = GetExecutingProcessesForMedia

		"----------------------" | SafeOutFile -LiteralPath HandleDiagnostics.log
		Get-Date | SafeOutFile -LiteralPath HandleDiagnostics.log
		"**********************" | SafeOutFile -LiteralPath HandleDiagnostics.log
		$currentOpenedFile | SafeOutFile -LiteralPath HandleDiagnostics.log
		"**********************" | SafeOutFile -LiteralPath HandleDiagnostics.log
		foreach($currentOpenedMedia in $openedProcess)
		{
			($currentOpenedMedia + " " + ($openedProcess[$currentOpenedMedia])) | SafeOutFile -LiteralPath HandleDiagnostics.log
		}
		"**********************" | SafeOutFile -LiteralPath HandleDiagnostics.log
		if($Error.Count -gt 0)
		{
			$clearErrors = $true
		}
		try
		{
			throw
		}
		catch
		{
			$_.ScriptStackTrace | SafeOutFile -LiteralPath HandleDiagnostics.log
			if($clearErrors)
			{
				$Error.Clear()
			}
		}
		"----------------------" | SafeOutFile -LiteralPath HandleDiagnostics.log
	}
	else
	{
		VerboseLogInfo 'A file was detected as being accessed by another process. If you want to see what it is, enable "$EXTRACT_HANDLE_DIAGNOSTICS" in the configuration file.'
	}

	return $true
}

#####################
#
# FUNCTION RenameFile
#
#####################
function RenameFile($source, $destination)
{
	RenameFile $source $destination $false
}

#####################
#
# FUNCTION RenameFile
#
#####################
function RenameFile($source, $destination, [bool] $quiet)
{
	if(FailedStringValidation $source) { return }
	if(FailedStringValidation $destination) { return }

	if(IsInvalidPath $source)
	{
		VerboseLogInfo NOT FOUND: [$source]
		return
	}

	$sourceDirectoryName = [System.IO.Path]::GetDirectoryName($source)
	if($sourceDirectoryName.Contains('$') -or $sourceDirectoryName.Contains('%'))
	{
		LogError Invalid source: [$source]
		return
	}

	$destinationDirectoryName = [System.IO.Path]::GetDirectoryName($destination)
	if($destinationDirectoryName.Contains('$') -or $destinationDirectoryName.Contains('%'))
	{
		LogError Invalid destination: [$destination]
		return
	}

	if(($source -eq $destination) -and ($source -cne $destination))
	{
		#RENAME ANYWAY
	}
	elseif(IsValidPath $destination)
	{
		LogWarning FILE ALREADY PRESENT, SKIPPING RENAME: [$destination]
		return
	}

	$sourceDirectory = [System.IO.Path]::GetDirectoryName($source)
	$destinationDirectory = [System.IO.Path]::GetDirectoryName($destination)
	if($sourceDirectory -ne $destinationDirectory)
	{
		LogError Source and Destination are different locations. Move first. [$source] --> [$destination]
		return
	}

	if(-not $quiet)
	{
		if(((-not $VERBOSE_MODE) -and (-not $WHATIF_MODE)) -and (IsUnderPath $destination $DATA_PATH))
		{
			$quiet = $true
		}
	}

	if($WHATIF_MODE)
	{
		LogInfo WHATIF: [Rename-Item -LiteralPath $source -NewName $destination]
	}
	else
	{
		if(IsFileCurrentlyOpen $source)
		{
			LogWarning File is currently open. Waiting a bit... [$source]
			SleepFor 5
		}
		if(IsFileCurrentlyOpen $source)
		{
			LogWarning File is currently open. Skipping Rename... [$source]
			return
		}

		$ErrorActionPreference = 'Stop'
		try
		{
			Rename-Item -LiteralPath $source -NewName $destination
			$succeeded = $true
		}
		catch
		{
			$succeeded = $false
			HandleError $_
		}
		$ErrorActionPreference = 'Continue'

		if($succeeded)
		{
			if($quiet)
			{
				VerboseLogInfo [$source] --> [$destination]
			}
			else
			{
				LogInfo [$source] --> [$destination]
			}
		}
		else
		{
			LogWarning [$source] --> [$destination] [SUCCEEDED: $succeeded]
		}

		if($succeeded)
		{
			if(IsInvalidPath $destination)
			{
				LogError RENAMED FILE MISSING: [$destination]
			}
		}
	}
}

###################
#
# FUNCTION MoveFile
#
###################
function MoveFile($source, $destination)
{
	MoveFile $source $destination $false
}

###################
#
# FUNCTION MoveFile
#
###################
function MoveFile($source, $destination, [bool] $quiet)
{
	if(FailedStringValidation $source) { return }
	if(FailedStringValidation $destination) { return }

	if(IsInvalidPath $source)
	{
		VerboseLogInfo NOT FOUND: [$source]
		return
	}

	if(($destination.Contains('$') -and (-not $source.Contains('$'))) -or ($destination.Contains('%') -and (-not $source.Contains('%'))))
	{
		LogError Invalid destination: [$destination]
		return
	}

	$sourceDir = [System.IO.Path]::GetDirectoryName($source)
	if($sourceDir -eq $destination)
	{
		if($quiet)
		{
			VerboseLogInfo NO OP: [$source]
		}
		else
		{
			LogInfo NO OP: [$source]
		}
		return
	}

	if(-not $quiet)
	{
		if((-not $VERBOSE_MODE) -and (-not $WHATIF_MODE))
		{
			if((IsUnderPath $destination $DATA_PATH))
			{
				$quiet = $true
			}
			elseif((IsUnderPath $source $DATA_PATH) -and (IsUnderPath $destination $PURGE_PATH))
			{
				$quiet = $true
			}
			elseif((IsUnderPath $source $AD_REMOVE_LOGS) -and (IsUnderPath $destination $PURGE_PATH))
			{
				$quiet = $true
			}
		}
	}

	#CREATE TARGET DIRECTORY
	if(IsInvalidPath $destination)
	{
		if($WHATIF_MODE)
		{
			LogInfo WHATIF: [md $destination]
		}
		else
		{
			VerboseLogInfo CREATING: [$destination]
			mkdir -Path $destination > $null
		}
	}

	$projectedTarget = [System.IO.Path]::Combine($destination, [System.IO.Path]::GetFileName($source))
	if(IsValidPath $projectedTarget)
	{
		if(IsUnderPath $destination $PURGE_PATH)
		{
			DeleteItem $projectedTarget
		}
		else
		{
			$cNumber = 0
			$renamedOriginal = [System.IO.Path]::Combine($destination, ([System.IO.Path]::GetFileNameWithoutExtension($source) + "." + $cNumber.ToString() + [System.IO.Path]::GetExtension($source)))
			while(IsValidPath $renamedOriginal)
			{
				$cNumber++
				$renamedOriginal = [System.IO.Path]::Combine($destination, ([System.IO.Path]::GetFileNameWithoutExtension($source) + "." + $cNumber.ToString() + [System.IO.Path]::GetExtension($source)))
			}
			RenameFile $projectedTarget $renamedOriginal
		}
	}

	if($WHATIF_MODE)
	{
		LogInfo WHATIF: [Move-Item -LiteralPath $source -Destination $destination]
	}
	else
	{
		$cTries = 3
		$canContinue = $false
		while(-not $canContinue)
		{
			if(IsFileCurrentlyOpen $source)
			{
				$cTries--
				if($cTries -eq 0)
				{
					LogWarning File is currently open. Skipping move. [$source]
					DeleteItem $projectedTarget
					return
				}
				else
				{
					LogWarning File is currently open. Waiting a bit... [$source]
					SleepFor 5
				}
			}
			else
			{
				$canContinue = $true
			}
		}

		$ErrorActionPreference = 'Stop'
		try
		{
			Move-Item -LiteralPath $source -Destination $destination -ErrorAction $ErrorActionPreference
			$succeeded = $true
		}
		catch
		{
			$succeeded = $false
			HandleError $_
		}
		$ErrorActionPreference = 'Continue'

		if($succeeded)
		{
			if($quiet)
			{
				VerboseLogInfo [$source] --> [$destination]
			}
			else
			{
				LogInfo [$source] --> [$destination]
			}
		}
		else
		{
			LogWarning [$source] --> [$destination] [SUCCEEDED: $succeeded]
		}

		if($succeeded)
		{
			if(IsInvalidPath $projectedTarget)
			{
				LogError MOVED FILE MISSING: [$projectedTarget]
			}
		}
	}
}

######################
#
# FUNCTION DeleteItem
#
######################
function DeleteItem($file)
{
	if(FailedStringValidation $file) { return }

	if(IsInvalidPath $file)
	{
		return
	}

	if($WHATIF_MODE)
	{
		LogInfo WHATIF: DELETING: [$file]
		return
	}

	if((Get-Item -LiteralPath $file).Attributes -Match 'Directory')
	{
		PermanentlyDeleteItem $file
	}
	else
	{
		if($DEBUG_MODE)
		{
			RecycleItem (Get-Item -LiteralPath $file)
		}
		else
		{
			$extension = [System.IO.Path]::GetExtension($file)
			if($extension -In $MEDIA_EXTENSIONS)
			{
				RecycleItem (Get-Item -LiteralPath $file)
			}
			else
			{
				PermanentlyDeleteItem $file
			}
		}
	}
}

#################################
#
# FUNCTION PermanentlyDeleteItem
#
#################################
function PermanentlyDeleteItem($file)
{
	if(IsValidPath $file)
	{
		if($WHATIF_MODE)
		{
			LogInfo WHATIF: DELETING: [$file]
		}
		else
		{
			VerboseLogInfo DELETING: [$file]
			Remove-Item -LiteralPath $file -Force -Recurse
		}
	}
}

######################
#
# FUNCTION RecycleItem
#
######################
function RecycleItem([System.IO.FileSystemInfo] $item)
{
	if($WHATIF_MODE)
	{
		LogInfo WHATIF: RECYCLING: [($item.FullName)]
	}
	else
	{
		if(IsInvalidPath $item.FullName)
		{
			return
		}

		$start = Get-Date
		$canContinue = $false
		while(-not $canContinue)
		{
			if(IsFileCurrentlyOpen $item.FullName)
			{
				VerboseLogInfo File is currently open. Deferring recycle... [($item.FullName)]
				SleepFor 5
			}
			elseif(((Get-Date)-$start).TotalSeconds -ge 30)
			{
				break
			}
			else
			{
				$canContinue = $true
			}
		}

		if(IsFileCurrentlyOpen $item.FullName)
		{
			VerboseLogInfo File is currently open. Attempting recycle... [($item.FullName)]
		}

		VerboseLogInfo RECYCLING: [($item.FullName)]
		[Microsoft.VisualBasic.FileIO.FileSystem]::DeleteFile($item.FullName,'OnlyErrorDialogs','SendToRecycleBin')
	}
}

##############################
#
# FUNCTION UnzipFile
#
##############################
function UnzipFile($sourceFile, $targetFile)
{
	if(FailedStringValidation $sourceFile) { return }
	if(FailedStringValidation $targetFile) { return }

	if($WHATIF_MODE)
	{
		LogInfo WHATIF: Unzip [$sourceFile] to [$targetFile]
		return
	}

	if(IsInvalidPath $sourceFile)
	{
		LogError Source is not valid: [$sourceFile]
		return
	}

	$tmpExtractionPoint = $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
	$targetFileName = [System.IO.Path]::GetFileName($targetFile)
	$targetFileExtension = [System.IO.Path]::GetExtension($targetFile)
	$targetTemporaryExtractedFile = [System.IO.Path]::Combine($tmpExtractionPoint, $targetFileName)

	CreateDirInPath $targetTemporaryExtractedFile

	$shell = New-Object -ComObject Shell.Application
	$zip = $shell.NameSpace($sourceFile)

	$selectedCompressedFile = $null
	foreach($zippedFile in $zip.Items())
	{
		DebugLogInfo Considering: [($zippedFile.Path)]
		if([System.IO.Path]::GetFileName($zippedFile.Path) -eq $targetFileName -or ($zippedFile.Name -eq $targetFileName))
		{
			$selectedCompressedFile = $zippedFile
		}
	}

	if($selectedCompressedFile -ne $null)
	{
		VerboseLogInfo EXTRACTING: [($selectedCompressedFile.Path)] --> [$tmpExtractionPoint]
		$shell.Namespace($tmpExtractionPoint).CopyHere($selectedCompressedFile) > $null
	}
	else
	{
		foreach($zippedFile in $zip.Items())
		{
			DebugLogInfo Considering Zipped File: [($zippedFile.Path)]
			if([System.IO.Path]::GetExtension($zippedFile.Path) -eq $targetFileExtension)
			{
				VerboseLogInfo EXTRACTING: [($zippedFile.Path)] --> [$tmpExtractionPoint]
				$shell.Namespace($tmpExtractionPoint).CopyHere($zippedFile) > $null
				$extractedFileName = [System.IO.Path]::GetFileName($zippedFile.Path)
				$extractedFileName = [System.IO.Path]::Combine($tmpExtractionPoint, $extractedFileName)
				RenameFile $extractedFileName $targetTemporaryExtractedFile
			}
		}
	}

	if(IsInvalidPath $targetTemporaryExtractedFile)
	{
		LogError Target not found or extracted: [$targetTemporaryExtractedFile]
		return
	}

	if(IsValidPath $targetFileName)
	{
		LogWarning Overwriting: [$targetFile]...
		DeleteItem $targetFile
	}
	MoveFile ($targetTemporaryExtractedFile) ([System.IO.Path]::GetDirectoryName($targetFile))

	if(IsInvalidPath $targetFile)
	{
		LogError Target not found or extracted: [$targetFile]
		return
	}

	DeleteItem $sourceFile
	PurgeEmptyDirectories $CONVERSION_TMP
}

##############################
#
# FUNCTION GenerateTmpFileName
#
##############################
function GenerateTmpFileName($file)
{
	if(FailedStringValidation $file) { return }
	return ($file + ".tmp")
}

##############################
#
# FUNCTION SwapFileWithTmp
#
##############################
function SwapFileWithTmp($file)
{
	if(FailedStringValidation $file) { return }

	if(IsInvalidPath $file)
	{
		LogWarning File not found: [$file]
		return
	}

	$tmpFile = GenerateTmpFileName $file
	if(IsValidPath $tmpFile)
	{
		if($WHATIF_MODE)
		{
			LogInfo WHATIF: [Move-Item -LiteralPath $tmpFile -Destination $file -Force]
		}
		else
		{
			VerboseLogInfo $tmpFile --> $file
			Move-Item -LiteralPath $tmpFile -Destination $file -Force
		}
	}
	else
	{
		DeleteItem $file
	}

	DeleteItem $tmpFile
}

##############################
#
# FUNCTION PurgeItem
#
##############################
function PurgeItem($file)
{
	MoveFile $file $PURGE_PATH $false
}

##############################
#
# FUNCTION PurgeItem
#
##############################
function PurgeItem($file, [bool] $quiet)
{
	MoveFile $file $PURGE_PATH $quiet
}

#############################
#
# FUNCTION GetCurrentUserName
#
#############################
function GetCurrentUserName()
{
	$localInfo = Get-LocalUser ($env:USERNAME)
	if($localInfo.PrincipalSource -ne "MicrosoftAccount")
	{
		return ($localInfo.Name)
	}

	$registryInfo = Get-ChildItem ("Registry::HKEY_USERS\" + $localInfo.SID + "\Software\Microsoft\IdentityCRL\UserExtendedProperties\*")
	return ($registryInfo.PSChildName)
}

#############################
#
# FUNCTION GetCredential
#
#############################
function GetCredential($id)
{
	if(FailedStringValidation $id) { return }

	if(-not (test-path $CREDENTIAL_FILE))
	{
		LogError No credentials are currently saved.
		return
	}

	$accounts = Import-Csv $CREDENTIAL_FILE
	foreach($currentAccount in $accounts)
	{
		if($currentAccount.Id -eq $id)
		{
			$ErrorActionPreference = 'SilentlyContinue'
			$secureStringPw = ($currentAccount.Password | ConvertTo-SecureString -Force)
			$ErrorActionPreference = 'Continue'

			if($secureStringPw -eq $null)
			{
				continue
			}

			$username = $currentAccount.UserName

			$secureStringBSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secureStringPw)
			$pwdPlain = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($secureStringBSTR)

			return @($username, $pwdPlain)
		}
	}

	if($WHATIF_MODE)
	{
		LogWarning Credential not found: [$id]
	}
	else
	{
		LogError Credential not found: [$id]
	}
}

###############################
#
# FUNCTION DisplayNotification
#
###############################
function DisplayNotification($title, $txt)
{
	if(FailedStringValidation $title) { return }
	if(FailedStringValidation $txt) { return }

	Remove-Event BalloonClicked_event -ErrorAction Ignore
	Unregister-Event -SourceIdentifier BalloonClicked_event -ErrorAction Ignore
	Remove-Event BalloonClosed_event -ErrorAction Ignore
	Unregister-Event -SourceIdentifier BalloonClosed_event -ErrorAction Ignore

	$notification = New-Object System.Windows.Forms.NotifyIcon

	$notification.Icon = [System.Drawing.SystemIcons]::Information
	$notification.BalloonTipTitle = $title
	$notification.BalloonTipIcon = "Info"
	$notification.BalloonTipText = $txt
	$notification.Visible = $True

	$notification.ShowBalloonTip(60*1000)
	$block = { param($txt, $title) Add-Type -AssemblyName System.Windows.Forms; [System.Windows.Forms.MessageBox]::Show($txt,$title) }
	(Start-Job -ScriptBlock $block -ArgumentList $txt,$title) > $null
}

#################################
#
# FUNCTION ExecutePowerShellScript
#
#################################
function ExecutePowerShellScript($scriptName)
{
	if(IsInvalidPath $scriptName)
	{
		LogWarning Skipping missing script: [$scriptName]
		return
	}

	VerboseLogInfo EXECUTING: [($scriptName) -WhatIf:($WHATIF_MODE) -Verbose:($VERBOSE_MODE) -Debug:($DEBUG_MODE)]

	$start = get-date
	&($scriptName) -WhatIf:($WHATIF_MODE) -Verbose:($VERBOSE_MODE) -Debug:($DEBUG_MODE)
	$end = get-date
	LogCommandExecution [($scriptName) -WhatIf:($WHATIF_MODE) -Verbose:($VERBOSE_MODE) -Debug:($DEBUG_MODE)] [($end-$start)]
}

#################################
#
# FUNCTION FlattenArguments
#
#################################
function FlattenArguments($commandArgs)
{
	if($commandArgs.Count -eq 0)
	{
		return $null
	}

	#Explicit
	$tmpArgs = New-Object -TypeName System.Collections.ArrayList
	foreach($currentArg in $commandArgs)
	{
		if(($currentArg.Count -gt 1) -and ($currentArg.GetType().ToString() -eq 'System.Collections.Hashtable'))
		{
			foreach($currentSubArg in (FlattenArguments $currentArg.Keys))
			{
				$tmpArgs.Add((FlattenArguments $currentSubArg ($currentArg[$currentSubArg]))) > $null
			}
		}
		elseif($currentArg.Count -gt 1)
		{
			foreach($currentSubArg in (FlattenArguments $currentArg))
			{
				$tmpArgs.Add($currentSubArg.ToString()) > $null
			}
		}
		elseif(($currentArg.Count -eq 1) -and ($currentArg.GetType().ToString() -eq 'System.Object[]'))
		{
			foreach($currentSubArg in (FlattenArguments $currentArg[0]))
			{
				$tmpArgs.Add($currentSubArg.ToString()) > $null
			}
		}
		else
		{
			if($currentArg -eq $null)
			{
				$tmpArgs.Add("NULL") > $null
				continue
			}
			else
			{
				$tmpArgs.Add($currentArg.ToString()) > $null
			}
		}
	}

	#Implicit
	if($args -ne $null)
	{
		foreach($currentArg in (FlattenArguments $args))
		{
			$tmpArgs.Add($currentArg) > $null
		}
	}

	#CANNOT DEBUG LOG, CALLED IN DEBUGLOGINFO
	$toReturn = New-Object -TypeName System.Collections.ArrayList
	for($i=0; $i -lt $tmpArgs.Count; $i++)
	{
		if($tmpArgs[$i] -eq ".")
		{
			$toReturn[$i-1] = ($toReturn[$i-1] + ".")
			continue
		}
		$toReturn.Add($tmpArgs[$i]) > $null
	}
	return ($toReturn.ToArray())
}

##########################################
#
# FUNCTION ShouldExcludeErrorOutputCheck
#
##########################################
function ShouldExcludeErrorOutputCheck($command)
{
	# Some applications like ffprobe.exe appear to redirect 100% of the output to Standard Error even
	# when it contains benign warnings, causing all lines to be wrapped as a ErrorRecord and causing
	# clients of this function to not be able to parse by default. So, we need to expand explicitly.
	# Normally we'd want to output some error here but we have to special case.
	foreach($currrentExclusion in $ERROR_EXCLUSION_BINARY)
	{
		if($command.StartsWith($currrentExclusion))
		{
			return $true
		}
	}
	return $false
}

##########################################
#
# FUNCTION ExecuteCommandInCustomDirectory
#
##########################################
function ExecuteCommandInCustomDirectory($dir, $command)
{
	if(IsInvalidPath $dir)
	{
		LogError Invalid Directory: [$dir]
		return
	}

	$savedWorkingDir = (Get-Location).Path
	try
	{
		Set-Location $dir
		ExecuteCommand $command $args > $null
	}
	finally
	{
		Set-Location $savedWorkingDir
	}
}

#################################
#
# FUNCTION ExecuteCommand
#
#################################
function ExecuteCommand($command)
{
	if(($command -ne $null) -and ($command.Count -gt 1))
	{
		$implicitArgs = FlattenArguments $command $args
		$command = $implicitArgs[0]
		$implicitArgs = $implicitArgs[1..($implicitArgs.Length - 1)]
	}
	else
	{
		$implicitArgs = FlattenArguments $args
	}

	$diagnosticsPath = [System.IO.Path]::Combine($DATA_PATH, "cmd")

	# Assume arguments staring and ending with braces are scriptblocks.
	# To execute correctly, it needs to have them stripped and the type
	# must be converted from a string.
	$index = 0
	foreach($currentArg in $implicitArgs)
	{
		if(($currentArg -eq "NULL") -or (-not (IsStringDefined $currentArg)))
		{
			LogError Empty argument found in arguments: [$implicitArgs]
		}

		if($currentArg.StartsWith('{') -and $currentArg.EndsWith('}'))
		{
			DebugLogInfo String ScriptBlock Detected. Converting...
			$currentArg = $currentArg.Trim('{', '}', ' ')
			$currentArg = [scriptblock]::Create($currentArg)
			$implicitArgs[$index] = $currentArg
		}
		$index++
	}

	if($command.Contains(' ') -and ($implicitArgs.Count -eq 0))
	{
		$executable = $command.Substring(0, $command.IndexOf(" "))
	}
	else
	{
		$executable = $command
	}

	if(IsValidPath $executable)
	{
		$executable = [System.IO.Path]::GetFileName($executable)
	}
	else
	{
		$executable = $executable.Replace(' ', '_').Trim()
		foreach ($currentInvalidChar in [System.IO.Path]::GetInvalidFileNameChars())
		{
			$executable = $executable.Replace($currentInvalidChar, '_')
		}
	}

	if($command -match 'python')
	{
		if(IsInvalidPath $implicitArgs[0])
		{
			LogError Skipping missing script: [($implicitArgs[0])] [($implicitArgs)]
			return
		}
		else
		{
			$executable = [System.IO.Path]::GetFileName($implicitArgs[0])
		}
	}

	if($command -match 'powershell')
	{
		if(IsInvalidPath $implicitArgs[0])
		{
			if(-not ($implicitArgs[0] -match "-command"))
			{
				LogError Skipping missing script: [($implicitArgs[0])]
				return
			}
		}
		else
		{
			$executable = [System.IO.Path]::GetFileName($implicitArgs[0])
		}

		VerboseLogInfo EXECUTING: [($command) -ExecutionPolicy Bypass -NoProfile $implicitArgs -WhatIf:("$" + $WHATIF_MODE) -Verbose:("$" + $VERBOSE_MODE) -Debug:("$" + $DEBUG_MODE)]
		$start = get-date
		$output = &($command) -ExecutionPolicy Bypass -NoProfile $implicitArgs -WhatIf:("$" + $WHATIF_MODE) -Verbose:("$" + $VERBOSE_MODE) -Debug:("$" + $DEBUG_MODE)
		$end = get-date
		LogCommandExecution [($command) -ExecutionPolicy Bypass -NoProfile $implicitArgs -WhatIf:("$" + $WHATIF_MODE) -Verbose:("$" + $VERBOSE_MODE) -Debug:("$" + $DEBUG_MODE)] [($end-$start)]
	}
	else
	{
		$savedLastErrorCode = 0
		VerboseLogInfo EXECUTING: [($command) $implicitArgs]
		$start = get-date
		$stdErrFileName = [String]::Format("{0}\{1}.{2}.err.txt", $diagnosticsPath, $executable, $start.TimeOfDay.Ticks)
		CreateDirInPath $stdErrFileName
		$oldEncoding = [Console]::OutputEncoding
		[Console]::OutputEncoding = [System.Text.UTF8Encoding]::new()
		$output = (&($command) $implicitArgs) 2> $stdErrFileName
		$savedLastErrorCode = $LASTEXITCODE
		$LASTEXITCODE = $null
		[Console]::OutputEncoding = $oldEncoding

		if(((Get-Item $stdErrFileName -ErrorAction Ignore).Length -ne 0) -and ($DEBUG_MODE -or ($executable -NotIn $ERROR_EXCLUSION_BINARY)))
		{
			LogWarning Detected error output when executing: [$executable $implicitArgs]
			LogWarning See output: [$stdErrFileName]
		}
		LogCommandExecution [($command) $implicitArgs] [($end-$start)]
	}

	#Searching for a file handle that is not currently open generates an error code of 1
	# "No matching handles found."
	if(($command -eq $HANDLE_BINARY) -and ($savedLastErrorCode -eq 1))
	{
		$savedLastErrorCode = 0
	}

	if($DEBUG_MODE -or ((IsStringDefined $savedLastErrorCode) -and ($savedLastErrorCode -ne 0)))
	{
		if(($output -eq $null) -or ($output.ToString().Trim().Count -eq 0))
		{
			DebugLogInfo Returning no output for command: [$executable] [$implicitArgs]
		}
		else
		{
			$stdOutFileName = [String]::Format("{0}\{1}.{2}.out.txt", $diagnosticsPath, $executable, $start.TimeOfDay.Ticks)
			$output | SafeOutFile $stdOutFileName
			LogWarning See output: [$stdOutFileName]
		}
	}

	# Delete ALL zero length error outputs - Sometimes handles can be held by previous executers
	foreach($currentStdErrFileName in (Get-ChildItem -LiteralPath $diagnosticsPath -ErrorAction Ignore))
	{
		if($currentStdErrFileName.Length -eq 0)
		{
			DeleteItem $currentStdErrFileName.FullName
		}
	}

	if(IsValidPath $stdErrFileName)
	{
		$errorOutput = Get-Content -LiteralPath $stdErrFileName -Raw
		if(IsStringDefined $errorOutput)
		{
			if($Error -ne $null)
			{
				$Error[0] = $errorOutput
			}
			else
			{
				$Error.Add($errorOutput) > $null
			}
			$errorDescription = $command + " : " + $errorOutput.ToString()
			Throw (New-Object System.InvalidOperationException $errorDescription)
		}
	}

	if(((IsStringDefined $savedLastErrorCode) -and ($savedLastErrorCode -ne 0)))
	{
		$errorDescription = "[($command)]: Process Exited with error level [$savedLastErrorCode]"
		if($Error -ne $null)
		{
			$Error[0] = $errorDescription
		}
		else
		{
			$Error.Add($errorDescription) > $null
		}
		Throw (New-Object System.InvalidOperationException $errorDescription)
	}

	return $output
}
