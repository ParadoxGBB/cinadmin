[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [Parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true, Position=0)]
    [Alias('FullName')]
    [String[]] $Path,
    [switch] $Force
)

#########
#
# MAIN
#
#########

Begin
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1
	trap { ReleaseScriptConcurrency; break }

	LockScriptConcurrency

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

	#Prepare work

	if(-not (IsStringDefined($DOWNLOADED_PODCASTS_PATH)))
	{
		LogError Audio Extraction Path not defined.
		return
	}

	if(IsInvalidPath $FFMPEG_BINARY)
	{
		LogError "FFMpeg is either not installed or not properly configured. Please fix."
		return
	}
}
Process
{
	try
	{
		if($Help)
		{
			return
		}

		if(-not (IsStringDefined($DOWNLOADED_PODCASTS_PATH)))
		{
			LogError Audio Extraction Path not defined.
			return
		}

		if(IsInvalidPath $FFMPEG_BINARY)
		{
			LogError "FFMpeg is either not installed or not properly configured. Please fix."
			return
		}

		#CLEAR/LOAD SUBSCRIPTION
		. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1

		if($Path -eq $null)
		{
			$Path = $DOWNLOADED_TV_PATH

			#
			# If no path is specified, also purge and replace video in audio path.
			#
			ExtractAndReplaceMediaAudio
		}

		foreach($Target in $Path)
		{
			$Target = LoadPath $Target
			if(IsInvalidPath $Target) { continue }

			$files =  Get-ChildItem -LiteralPath $Target -Recurse -FollowSymlink -File
			$cCurrentProgress = 0
			foreach($currentOpenedFile in $files)
			{
				$cCurrentProgress++
				$currentOpenedFile = Get-Item -LiteralPath (TranslatePathToSymLink ($currentOpenedFile.FullName))
				if(-not $Force)
				{
					if($currentOpenedFile.Extension -NotIn $MEDIA_EXTENSIONS)
					{
						VerboseLogInfo EXCLUDED FILE EXTENSION: [($currentOpenedFile.FullName)]
						continue
					}

					if(IsWatched $currentOpenedFile.FullName)
					{
						VerboseLogInfo EXCLUDED CONSUMED: [($currentOpenedFile.FullName)]
						continue
					}

					if(IsExcludedPath $currentOpenedFile.FullName)
					{
						VerboseLogInfo EXCLUDED DIR: [($currentOpenedFile.FullName)]
						continue
					}

					$audioOverride = LoadSubscriptionOverrideVariable $currentOpenedFile "ACQUISITION_EXTRACT_AUDIO"
					if(($audioOverride -ne $null) -and (-not $audioOverride))
					{
						VerboseLogInfo EXCLUDED VIA SUBSCRIPTION: [($currentOpenedFile.FullName)]
						continue
					}

					if(IsMovie $currentOpenedFile.FullName)
					{
						VerboseLogInfo EXCLUDED MOVIE: [($currentOpenedFile.FullName)]
						continue
					}

					if(IsFileCurrentlyRecording ($currentOpenedFile.FullName))
					{
						VerboseLogInfo FILE IS CURRENTLY RECORDING: [($currentOpenedFile.FullName)]
						continue
					}

					if(-not (HasFileBeenRenamed ($currentOpenedFile.FullName)))
					{
						VerboseLogInfo FILE NOT YET RENAMED: [($currentOpenedFile.FullName)]
						continue
					}
				}

				OutputProgressBar ("Extracting Media Audio [$Target]...") ($cCurrentProgress/$files.Count)

				if(IsAudio $currentOpenedFile.FullName)
				{
					VerboseLogInfo EXCLUDED AUDIO: [($currentOpenedFile.FullName)]
					continue
				}

				if($Force -or $audioOverride)
				{
					ExtractMediaAudio ($currentOpenedFile.FullName)
				}
			}
		}
	}
	catch
	{
		ReleaseScriptConcurrency
		throw
	}
}
End
{
	ReleaseScriptConcurrency
	PauseIfClicked
}