[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [Parameter(Position=0)]
    [string] $Subscription,
    [switch] $BuildCache,
    [switch] $ClearSemaphore,
    [switch] $ShowSource,
    [switch] $Force
)

try
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1

	if($ClearSemaphore)
	{
		RemoveSemaphore
		return
	}
	LockScriptConcurrency

	#########
	#
	# MAIN
	#
	#########

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

	if($BuildCache -or $Force)
	{
		#Handled Inside Functions
	}

	if(IsPlayOnInstalled -and -not ($ShowSource -or $BuildCache -or $ClearSemaphore))
	{
		DisplayPlayOnSubscriptionProblems
	}

	if(IsStringDefined $Subscription)
	{
		if(IsInvalidPath $Subscription)
		{
			LogError Subscription not found: [$Subscription]
			return
		}

		if(IsNotUnderPath ((Get-Item -LiteralPath $Subscription).Directory.FullName) $SUBSCRIPTION_INSTALL_PATH)
		{
			LogError Subscription not in install location: [$Subscription]
			return
		}

		if($ShowSource)
		{
			#CLEAR/LOAD SUBSCRIPTION
			. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
			. $Subscription
			if(-not (IsStringDefined $SUBSCRIPTION_URI))
			{
				LogWarning Subscription has no source defined: [$Subscription]
			}
			else
			{
				if(IsRemoteSession)
				{
					LogWarning Cannot execute show subscription source in a remote session [$SUBSCRIPTION_URI].
				}
				else
				{
					Start-Process -FilePath $SUBSCRIPTION_URI
				}
			}
		}
		else
		{
			try
			{
				LogWarning Forcing full check of subscription: [$Subscription]
				$Force = $true
				CheckMediaSubscription $Subscription
			}
			catch
			{
				LogWarning "Error while executing " $Subscription ":"
				HandleError $_
				continue
			}
		}
	}
	else
	{
		$cCurrentProgress = 0
		$subscriptions = Get-ChildItem -Recurse -LiteralPath $SUBSCRIPTION_INSTALL_PATH -Filter "*.ps1" | Where-Object { (Get-Content -LiteralPath $_.FullName | Select-String '\$SUBSCRIPTION_POLLABLE\s*\=\s*\$true') -ne $null } | Sort-Object Name
		foreach($currentSubscription in $subscriptions)
		{
			$cCurrentProgress++
			OutputProgressBar ("Checking Subscription [" + $currentSubscription.Name + "]...") ($cCurrentProgress/$subscriptions.Count)

			$startSub = Get-Date

			try
			{
				CheckMediaSubscription ($currentSubscription.FullName)
			}
			catch
			{
				LogWarning "Error while executing " $Subscription ":"
				HandleError $_
				continue
			}
			finally
			{
				$Error.Clear()
			}

			if($VERBOSE_MODE)
			{
				LogInfo Checking Subscription [($currentSubscription.Name)] ... [((Get-Date) - $startSub)]
			}
		}
	}
}
finally
{
	PurgeEmptyDirectories $CONVERSION_TMP $true
	PurgeEmptyDirectories $SUBSCRIPTION_TMP $true
	ReleaseScriptConcurrency
	PauseIfClicked
}