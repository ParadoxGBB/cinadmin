[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [switch] $BuildCache,
    [switch] $CleanCache,
    [switch] $Force = $false,
    [Parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true, Position=0)]
    [Alias('FullName')]
    [String[]] $Path
)

#########
#
# MAIN
#
#########

Begin
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1
	trap { ReleaseScriptConcurrency; break }

	LockScriptConcurrency

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

	CleanSubtitleCache

	#Prepare work
	if($Force)
	{
		$currentSubtitledMedia = @{}
	}
	else
	{
		$currentSubtitledMedia = LoadSubtitledMedia
	}

	if($Path.Count -eq 0)
	{
		if(-Not (IsStringDefined $DOWNLOADED_TV_PATH))
		{
			LogError No valid paths to process.
			return
		}
		else
		{
			$Path = $DOWNLOADED_TV_PATH
		}
	}
}
Process
{
	try
	{
		if($Help)
		{
			return
		}

		CleanSubtitleCache $CleanCache
		if($CleanCache)
		{
			return
		}

		elseif(IsStringDefined($OPENSUBTITLES_CREDENTIAL))
		{
			#
			# Download subtitles from OpenSubtitles.org
			#
			foreach($Target in $Path)
			{
				$Target = LoadPath $Target
				if(IsInvalidPath $Target) { continue }
				$complainedQuota = $false

				$files =  Get-ChildItem -LiteralPath $Target -Recurse -FollowSymlink -File
				$cCurrentProgress = 0
				foreach($currentOpenedFile in $files)
				{
					$cCurrentProgress++

					if(-not $Force)
					{
						if($currentSubtitledMedia.ContainsKey([System.IO.Path]::GetFileNameWithoutExtension($currentOpenedFile.FullName)))
						{
							VerboseLogInfo FILE ALREADY PROCESSED: [($currentOpenedFile.FullName)]
							continue
						}

						if($currentOpenedFile.Extension -NotIn $MEDIA_EXTENSIONS)
						{
							VerboseLogInfo EXCLUDED EXTENSION: [($currentOpenedFile.FullName)]
							continue
						}

						if(IsAudio $currentOpenedFile.FullName)
						{
							VerboseLogInfo EXCLUDED AUDIO: [($currentOpenedFile.FullName)]
							continue
						}

						if(IsExcludedPath $currentOpenedFile.FullName)
						{
							VerboseLogInfo EXCLUDED DIR: [($currentOpenedFile.FullName)]
							continue
						}

						if(IsUnderPath $currentOpenedFile.FullName $PURGE_PATH)
						{
							VerboseLogInfo EXCLUDED PURGE: [($currentOpenedFile.FullName)]
							continue
						}

						if(IsWatched $currentOpenedFile.FullName)
						{
							VerboseLogInfo EXCLUDED CONSUMED: [($currentOpenedFile.FullName)]
							continue
						}

						if(IsUnderPath $currentOpenedFile.FullName $DOWNLOADED_MOVIES_PATH)
						{
							VerboseLogInfo EXCLUDED MOVIE: [($currentOpenedFile.FullName)]
							continue
						}

						$subscriptionOverride = LoadSubscriptionOverrideVariable $currentOpenedFile "ACQUISITION_SUBTITLES_DAYS"
						if(($subscriptionOverride -ne $null) -and (-not $subscriptionOverride))
						{
							VerboseLogInfo EXCLUDED VIA SUBSCRIPTION: [($currentOpenedFile.FullName)]
							continue
						}

						if(IsFileCurrentlyRecording ($currentOpenedFile.FullName))
						{
							VerboseLogInfo FILE IS CURRENTLY RECORDING: [($currentOpenedFile.FullName)]
							continue
						}
					}

					if($BuildCache)
					{
						OutputProgressBar ("Building Cache [$Target]...") ($cCurrentProgress/$files.Count)

						if(-not $EMBED_SUBTITLES)
						{
							TrackSubtitledMedia ($currentOpenedFile.FullName)
						}
					}
					else
					{
						if(IsValidPath $SUBTITLE_QUOTA)
						{
							$quotaReset = [System.DateTime]::Parse((Get-Content -LiteralPath $SUBTITLE_QUOTA).Trim())
							if((Get-Date) -lt $quotaReset)
							{
								if(-not $complainedQuota)
								{
									LogWarning "Over OpenSubtitle quota until:" [($quotaReset.ToString())]
									$complainedQuota = $true
								}
								continue
							}
							else
							{
								DeleteItem $SUBTITLE_QUOTA
							}
						}

						OutputProgressBar ("Acquiring Subtitles [$Target]...") ($cCurrentProgress/$files.Count)

						if((IsUnderPath $currentOpenedFile.FullName $DOWNLOADED_TV_PATH) -and -not $Force)
						{
							if(-not (HasFileBeenRenamed ($currentOpenedFile.FullName)))
							{
								#Wait until renamed.
								VerboseLogInfo WAIT FOR RENAME: [($currentOpenedFile.FullName)]
								continue
							}
						}

						if($currentOpenedFile.Extension -In $SUBTITLE_EXTENSIONS)
						{
							VerboseLogInfo ALREADY ACQUIRED: [($currentOpenedFile.FullName)]
							continue
						}

						if(IsValidPath $currentOpenedFile.FullName.Replace($currentOpenedFile.Extension, ".srt"))
						{
							VerboseLogInfo ALREADY ACQUIRED SRT: [($currentOpenedFile.FullName)]
							continue
						}

						$trackSubtitle = $false

						if(-not $Force)
						{
							if(MediaHasSubtitles $currentOpenedFile.FullName)
							{
								VerboseLogInfo Media file already has subtitles: [($currentOpenedFile.FullName)]
								$trackSubtitle = $true
							}
						}

						if(-not $EMBED_SUBTITLES)
						{
							foreach($currentRelatedFile in (Get-ChildItem -LiteralPath ($currentOpenedFile.Directory.FullName) -File))
							{
								if(($currentRelatedFile.BaseName -eq $currentOpenedFile.BaseName) -and ($currentRelatedFile.Extension -In $SUBTITLE_EXTENSIONS))
								{
									LogWarning Media file already has subtitle file acquired: [($currentOpenedFile.FullName)]
									$trackSubtitle = $true
									break
								}
							}
						}

						if($trackSubtitle)
						{
							TrackSubtitledMedia ($currentOpenedFile.FullName) (Get-Date)
							continue
						}

						$showName = ExtractShowName $currentOpenedFile
						if(-not (IsStringDefined $showName) -and -not (IsUnderPath $currentOpenedFile.FullName $DOWNLOADED_MOVIES_PATH))
						{
							LogError Show series name not detected. Skipping subtitle acquisition. [($currentOpenedFile.FullName)]
							if(HasFileBeenRenamed ($currentOpenedFile.FullName))
							{
								# MARK MEDIA SUBTITLED SPECIALLY SO NOT ATTEMPTED AGAIN
								TrackSubtitledMedia ($currentOpenedFile.FullName) ([DateTime]::MinValue)
							}
							continue
						}

						$seasonResults = ParseSeason $currentOpenedFile
						$movieHash = $null
						$seasonNumber = $null
						$episodeNumber = $null
						if(($seasonResults -eq $null) -or ($seasonResults[0] -eq $null) -or ($seasonResults[2] -eq $null))
						{
							$movieHash = MovieHash ($currentOpenedFile.FullName)
						}
						else
						{
							$seasonNumber = $seasonResults[0].TrimStart("0")
							if(-not (IsStringDefined $seasonNumber))
							{
								$seasonNumber = "0"
							}
							$episodeNumber = $seasonResults[2].TrimStart("0")
							if(-not (IsStringDefined $episodeNumber))
							{
								$episodeNumber = "0"
							}
						}

						LogInfo SUBTITLE SEARCH: [($currentOpenedFile.FullName)] [ Query: ($showName)] [ S ($seasonNumber) E ($episodeNumber)] [ Hash: $movieHash ] [ Language: ($OPENSUBTITLES_LANGUAGE_CODE)]

						$token = GetOpenSubtitlesToken
						if($token -eq $null)
						{
							LogError Could not acquire token. Exiting...
							return
						}
						$searchCriteria = @{}
						$searchCriteria.Add('languages', $OPENSUBTITLES_LANGUAGE_CODE) > $null
						if($seasonNumber -ne $null)
						{
							$searchCriteria.Add('season_number', $seasonNumber) > $null
						}
						if($episodeNumber -ne $null)
						{
							$searchCriteria.Add('episode_number', $episodeNumber) > $null
						}
						if($movieHash -ne $null)
						{
							$searchCriteria.Add('moviehash', $movieHash) > $null
						}
						$searchCriteria.Add('page', "1") > $null

						$results = SearchForSubtitles ([System.Net.WebUtility]::HtmlEncode($showName)) $searchCriteria

						$viableResults = @{}
						foreach($currentResult in $results.data)
						{
							$fileName = $currentResult.attributes.release
							if($fileName.Count -gt 1)
							{
								$fileName = $fileName[0]
							}
							$language = $currentResult.attributes.language
							$fileId = $currentResult.attributes.files[0].file_id

							if($language -ne $OPENSUBTITLES_LANGUAGE_CODE)
							{
								continue
							}

							$viableResults.Add($fileId, $currentResult)
							VerboseLogInfo Viable OpenSubtitles Result: [$fileName] [$fileId]
						}

						if($viableResults.Count -eq 0)
						{
							LogWarning No suitable subtitle found for media: [($currentOpenedFile.FullName)]
							VerboseLogInfo Deferring marking subtitle as completed: [($subscriptionOverride)] [(((Get-Date) - $currentOpenedFile.CreationTime).TotalDays)]
							if(((Get-Date) - $currentOpenedFile.CreationTime).TotalDays -gt $subscriptionOverride)
							{
								# MARK MEDIA SUBTITLED SPECIALLY SO NOT ATTEMPTED AGAIN
								TrackSubtitledMedia ($currentOpenedFile.FullName) ([DateTime]::MinValue)
							}
						}
						else
						{
							$currentResult = $viableResults.Values[0]
							$fileName = $currentResult.attributes.release
							if($fileName.Count -gt 1)
							{
								$fileName = $fileName[0]
							}
							$fileId = $currentResult.attributes.files[0].file_id
							LogInfo DOWNLOAD FROM OPENSUBTITLES [$fileId] [$fileName] --> [($currentOpenedFile.Name)]

							DownloadSubtitleResult ($currentOpenedFile.FullName) $result

							if(IsValidPath $SUBTITLE_QUOTA)
							{
								LogWarning Currently over quota, can not acquire: [$fileId] [$fileName]
								continue
							}

							$projectedSubtitle = $currentOpenedFile.FullName.Replace($currentOpenedFile.Extension, ".srt")
							if(IsInvalidPath $projectedSubtitle)
							{
								LogError Subtitle not acquired: [$projectedSubtitle]
								continue
							}

							if(-not $EMBED_SUBTITLES)
							{
								TrackSubtitledMedia ($currentOpenedFile.FullName) (Get-Date)
							}
						}
					}
				}
			}
		}

		if(-not $EMBED_SUBTITLES)
		{
			return
		}

		#
		# Embed existing SRTs into media
		#
		foreach($Target in $Path)
		{
			$Target = LoadPath $Target
			if(IsInvalidPath $Target) { continue }

			if([System.IO.Path]::GetExtension($Target) -In $MEDIA_EXTENSIONS)
			{
				$files =  Get-ChildItem -LiteralPath ([System.IO.Path]::GetDirectoryName($Target)) -FollowSymlink -File | Where-Object { $_.BaseName -eq ([System.IO.Path]::GetFileNameWithoutExtension($Target)) -and ($_.Extension -In ($SUBTITLE_EXTENSIONS)) } | Sort-Object -Property FullName
			}
			else
			{
				$files =  Get-ChildItem -LiteralPath $Target -Recurse -FollowSymlink -File | Where-Object { $_.Extension -In ($SUBTITLE_EXTENSIONS) } | Sort-Object -Property FullName
			}

			$cCurrentProgress = 0
			foreach($currentOpenedFile in $files)
			{
				$cCurrentProgress++

				if(-not $Force)
				{
					if($currentSubtitledMedia.ContainsKey([System.IO.Path]::GetFileNameWithoutExtension($currentOpenedFile.FullName)))
					{
						VerboseLogInfo FILE ALREADY PROCESSED: [($currentOpenedFile.FullName)]
						continue
					}

					if(IsExcludedPath $currentOpenedFile.FullName)
					{
						VerboseLogInfo EXCLUDED DIR: [($currentOpenedFile.FullName)]
						continue
					}

					if(IsWatched $currentOpenedFile.FullName)
					{
						VerboseLogInfo EXCLUDED CONSUMED: [($currentOpenedFile.FullName)]
						continue
					}

					if(IsMovie $currentOpenedFile.FullName)
					{
						VerboseLogInfo EXCLUDED MOVIE: [($currentOpenedFile.FullName)]
						continue
					}
				}

				if($BuildCache)
				{
					OutputProgressBar ("Building Cache [$Target]...") ($cCurrentProgress/$files.Count)
					TrackSubtitledMedia ($currentOpenedFile.FullName) (Get-Date)
				}
				else
				{
					OutputProgressBar ("Embedding Subtitles [$Target]...") ($cCurrentProgress/$files.Count)

					$targetMediaInput = $null
					foreach($currentRelatedFile in (Get-ChildItem -LiteralPath ($currentOpenedFile.Directory.FullName) -File))
					{
						if($currentRelatedFile.Extension -NotIn $MEDIA_EXTENSIONS)
						{
							continue
						}

						if($currentRelatedFile.BaseName -eq $currentOpenedFile.BaseName)
						{
							$targetMediaInput = $currentRelatedFile.FullName
							break
						}
					}

					if($targetMediaInput -eq $null)
					{
						LogWarning No matching media found for subtitle file: [($currentOpenedFile.FullName)]
						continue
					}

					if(-not $Force)
					{
						if($currentSubtitledMedia.ContainsKey([System.IO.Path]::GetFileNameWithoutExtension($targetMediaInput)))
						{
							VerboseLogInfo FILE ALREADY PROCESSED: [($targetMediaInput)]
							continue
						}

						if(MediaHasSubtitles $targetMediaInput)
						{
							VerboseLogInfo Media file already has subtitles: [($targetMediaInput)]
							TrackSubtitledMedia ($targetMediaInput) (Get-Date)
							continue
						}
					}

					if($EMBED_SUBTITLES -and -not (CanEmbedSubtitles $targetMediaInput))
					{
						LogWarning Media cannot have subtitles embedded. Converting: [($targetMediaInput)]
						$sourceExtension = (Get-Item -LiteralPath $targetMediaInput).Extension
						$convertedFile = $targetMediaInput.Replace($sourceExtension, ".mp4")
						if((IsInvalidPath $convertedFile) -and ($sourceExtension -ne ".mp4"))
						{
							ConvertMediaToMpX $targetMediaInput
						}
						if(IsValidPath $convertedFile)
						{
							$targetMediaInput = $convertedFile
							while(IsFileCurrentlyOpen $targetMediaInput)
							{
								SleepFor 5
							}
						}
						else
						{
							continue
						}
					}

					$tmpLocation =  $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
					$convertedTmpTarget = $targetMediaInput.Replace($currentOpenedFile.Directory.FullName, $tmpLocation)
					LogInfo EMBED SUBTITLE: [($currentOpenedFile.FullName)] --> [$targetMediaInput]
					$renameSubExtension = ".NoSubs"
					if($Force -and (MediaHasSubtitles $targetMediaInput))
					{
						LogWarning MEDIA WILL HAVE ORIGINAL SUBTITLES OVERRIDDEN: [$targetMediaInput]
						$renameSubExtension = ".Subs"
					}

					CreateDirInPath $convertedTmpTarget

					if($WHATIF_MODE)
					{
						LogInfo WHATIF: Convert [$targetMediaInput] --> [$convertedTmpTarget]
						continue
					}
					else
					{
						#Force flag should strip existing subtitles in media
						ConvertMedia $targetMediaInput $convertedTmpTarget $Force
					}

					$canContinue = $false
					$start = Get-Date
					while((-not $canContinue) -and (((Get-Date) - $start)).TotalSeconds -le 30)
					{
						if(IsFileCurrentlyOpen $targetMediaInput)
						{
							SleepFor 5
						}
						else
						{
							$canContinue = $true
						}
					}

					if(-not $canContinue)
					{
						LogWarning Target media still open. Attempting to replace anyway... [$targetMediaInput]
					}

					if(IsValidPath $convertedTmpTarget)
					{
						$targetMediaInputRenamedExtension = [System.IO.Path]::GetExtension($targetMediaInput)
						$targetMediaInputRenamed = $targetMediaInput.Replace($targetMediaInputRenamedExtension, $renameSubExtension + $targetMediaInputRenamedExtension)

						RenameFile $targetMediaInput $targetMediaInputRenamed
						if(IsValidPath $targetMediaInputRenamed)
						{
							PurgeItem $targetMediaInputRenamed
							PurgeItem $currentOpenedFile.FullName
							MoveFile $convertedTmpTarget $currentOpenedFile.Directory.FullName

							TrackSubtitledMedia $targetMediaInput (Get-Date)
						}
					}
				}
			}
		}
	}
	catch
	{
		ReleaseScriptConcurrency
		throw
	}
}
End
{
	PurgeEmptyDirectories $CONVERSION_TMP $true
	ReleaseScriptConcurrency
	PauseIfClicked
}