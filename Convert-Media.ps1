[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [Parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true, Position=0)]
    [Alias('FullName')]
    [String[]] $Path
)

#########
#
# MAIN
#
#########

Begin
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1
	trap { ReleaseScriptConcurrency; break }

	LockScriptConcurrency

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

	if(($Global:LAST_MAKEMKV -eq $null) -or (((Get-Date) - $Global:LAST_MAKEMKV).TotalDays -gt 1))
	{
		DebugLogInfo Checking MakeMKV Expiration...
		$Global:LAST_MAKEMKV = Get-Date
		# Double check makemkv is still functional and not expired
		foreach($currentMakeMKVOutput in (ExecuteCommand $MAKEMKV_BINARY "mkv"))
		{
			$currentMakeMKVOutput = $currentMakeMKVOutput.ToString()
			if($currentMakeMKVOutput.Contains("This application version is too old."))
			{
				LogWarning "It appears that MAKEMKV has expired."
				LogWarning [$currentMakeMKVOutput]
			}
		}
	}

	#REFRESHING ISO PNGs
	if( ($Path.Count -eq 0) -and ( ($Global:LAST_PNG_REFRESH -eq $null) -or (((Get-Date) - $Global:LAST_PNG_REFRESH).TotalHours -ge $ISO_PNG_REFRESH_HOURS) ) )
	{
		if(-not (IsPlexConfigured))
		{
			return
		}

		if(IsInvalidPath $MAKEMKV_BINARY)
		{
			LogWarning "$MAKEMKV_BINARY not configured."
			return
		}

		if(($Path.Count -eq 0) -and (IsInvalidPath $ISO_PNG_PATH))
		{
			LogWarning "$ISO_PNG_PATH not configured."
			return
		}

		RefreshISOPNGs
		$Global:LAST_PNG_REFRESH = Get-Date
	}
}
Process
{
	try
	{
		if($Help)
		{
			return
		}
		$mediaToConvert = New-Object -TypeName System.Collections.ArrayList

		if($Path.Count -eq 0)
		{
			$mediaToConvert = ScanForViewedISOPNG
		}
		else
		{
			foreach($Target in $Path)
			{
				$Target = LoadPath $Target
				if(IsInvalidPath $Target) { continue }

				$files =  Get-ChildItem -LiteralPath $Target -Recurse -FollowSymlink -File
				foreach($currentOpenedFile in $files)
				{
					if($currentOpenedFile.Extension -NotIn $MEDIA_EXTENSIONS)
					{
						LogWarning Cannot be converted: [($currentOpenedFile.FullName)]
						continue
					}

					if($currentOpenedFile.Extension -In @(".mp4", ".mp3"))
					{
						LogWarning Target is already converted: [($currentOpenedFile.FullName)]
						continue
					}

					$currentOpenedFile = Get-Item -LiteralPath (TranslatePathToSymLink ($currentOpenedFile.FullName))
					VerboseLogInfo FOUND MEDIA TARGET [($currentOpenedFile.FullName)]
					$mediaToConvert.Add($currentOpenedFile.FullName) > $null
				}
			}
		}

		#
		# CONVERT MEDIA
		#
		foreach($targetIsoFileName in $mediaToConvert)
		{
			if((Get-Item $targetIsoFileName).Extension -In ".iso")
			{
				LogInfo Converting to MKV: $targetIsoFileName
				ConvertIsoToMkv $targetIsoFileName
			}
			else
			{
				LogInfo Converting to MP4 or MP3: $targetIsoFileName
				ConvertMediaToMpX $targetIsoFileName
			}
		}
	}
	catch
	{
		ReleaseScriptConcurrency
		throw
	}
}
End
{
	try
	{
		if($Help)
		{
			return
		}
	}
	finally
	{
		ReleaseScriptConcurrency
		PauseIfClicked
	}
}