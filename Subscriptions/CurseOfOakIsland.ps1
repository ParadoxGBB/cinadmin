###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
$SUBSCRIPTION_NAME = "The Curse of Oak Island"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_AUTH_PROVIDER = "Verizon"
$ACQUISITION_CREDENTIAL_ID = "Verizon"
$ACQUISITION_SUBTITLES_DAYS = 0

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://play.history.com/shows/the-curse-of-oak-island"
$SUBSCRIPTION_EPISODE_BEGIN = '"Episode:\d+":{"__typename":"Episode",'
$SUBSCRIPTION_EPISODE_END 	= '}}},'

$SUBSCRIPTION_EXTRACTORS = 	@{
					'"title":"(?<title>([^"]|\\")+)",' = $null;
					'"canonical":"(?<href>[^"]+)",' = $null;
					'"pubDateMsg":"\w+\son\s(?<pubDate>[^"]+)",' = $null;
					'"tvSeasonNumber":(?<season>\d+),' = $null;
					'"tvSeasonEpisodeNumber":(?<episode>\d+),' = $null;
				}

$SUBSCRIPTION_ACQUIRE_ACTION =	{
						if($pubDate -ne $null) { $tooEarly = (((Get-Date) - [System.DateTime]::Parse($pubDate)).TotalHours -le 24); };
						$EPISODE_EXCLUSION = ($tooEarly -or ($episode -eq $null) -or -not ($href.Contains("shows/the-curse-of-oak-island/season-")));
						$EPISODE_CONTENT_URI = "https://play.history.com" + $href;
						if($pubDate -ne $null) { $EPISODE_PUBLISH_DATE = FormatAirDate $pubDate; };
						$title = $title.Replace($SUBSCRIPTION_NAME, "").Trim('_');
						if($episode -ne $null) { $EPISODE_CONTENT_RENAME_TO = (FormatSeasonEpisode $season $episode) + " - " + $title; };
					}
