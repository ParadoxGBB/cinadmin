###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\CBS.ps1
$SUBSCRIPTION_NAME = "The Late Show with Stephen Colbert"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.cbs.com/shows/the-late-show-with-stephen-colbert/"

$SUBSCRIPTION_EPISODE_BEGIN = '<section id="latest-episodes"'
$SUBSCRIPTION_EPISODE_END = '<section class="js-le-carousel'

$SUBSCRIPTION_EXTRACTORS = 	@(
					'href="(?<href>/shows/[^"]+)"',
					('data-tracking="(?:\|+)' + $SUBSCRIPTION_NAME + '(?:\|+)(?<seasonNumber>S\d+)(?:\|+)Ep(?<episodeNumber>\d+)(?:\|+)(?<aireddate>[^\|]+)(?:\|+\d+/\d+/\d+\s+)\((?<title>[^\|]+)\)(?:\|+)(?<vidtype>free|pay)(?:[\|\w]+)">')
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					$SUBSCRIPTION_ACQUIRE_ACTION,
					{
						$EPISODE_EXCLUSION = ($vidtype -match "pay");
						$EPISODE_CONTENT_RENAME_TO = (FormatSeasonEpisode $seasonNumber $episodeNumber) + " - " + (FormatAirDate $aireddate) + " - " + $title
					}
				)