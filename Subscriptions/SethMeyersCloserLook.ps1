###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\YouTube.ps1
$SUBSCRIPTION_NAME = "Late Night with Seth Meyers: A Closer Look"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_ALLOW_RENAME = $false

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.youtube.com/feeds/videos.xml?playlist_id=PLJaq64dKJZoqsh7PGGUi-SARV4wUz_lVa"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					{
						# Content just published do not correctly extract subtitles
						$EPISODE_EXCLUSION = ((((Get-Date)-[DateTime]::Parse($pubDate)).TotalDays -lt 1) -or (-not $title.Contains("A Closer Look")));
						$title = $title.Replace(": A Closer Look", "");
					},
					$SUBSCRIPTION_ACQUIRE_ACTION;
				)

#############
# OVERRIDES #
#############
$PURGE_ON_WATCHED = $true
$ARCHIVE_AGE_DAYS_OVERRIDE          = 3*30
$PURGE_TOPICAL_AGE_DAYS_OVERRIDE    = 6*30
