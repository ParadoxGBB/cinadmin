###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\TBS.ps1
$SUBSCRIPTION_NAME = "Full Frontal with Samantha Bee"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_EXTRACT_AUDIO = $true

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "http://www.tbs.com/shows/full-frontal-with-samantha-bee"

$SUBSCRIPTION_EXTRACTORS = 	@(
					'<a href="(?<href>/shows/full-frontal-with-samantha-bee/season-(?<season>\d+)/episode-(?<episode>\d+)[^"]+)" class="content-tile-eplink">(?<title>[^<]+)</a>'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					$SUBSCRIPTION_ACQUIRE_ACTION,
					{
						$EPISODE_EXCLUSION = ($title -eq "..More");
						$EPISODE_CONTENT_RENAME_TO = (FormatSeasonEpisode $season $episode) + " - " + $title;
					}
				)

#############
# OVERRIDES #
#############
$ALLOW_STALE_TVDB_OVERRIDE        = $true