###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\CBS.ps1
$SUBSCRIPTION_NAME = "The Big Bang Theory"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.cbs.com/shows/big_bang_theory/xhr/episodes/page/0/size/12/xs/0/season/" + (GetLatestShowSeason $SUBSCRIPTION_NAME) + "/"