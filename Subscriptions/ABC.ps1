#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_AUTH_PROVIDER = "Verizon"
$ACQUISITION_CREDENTIAL_ID = "Verizon"

################
# CHECK / SCAN #
################
$SUBSCRIPTION_EPISODE_BEGIN = '<section class="tilegroup tilegroup--shows tilegroup--carousel tilegroup--landscape">'
$SUBSCRIPTION_EPISODE_END = '<section class="tilegroup tilegroup--marketing_banner--slim tilegroup--landscape">'

$SUBSCRIPTION_EXTRACTORS = 	@(
					'class="AnchorLink" tabindex="\d+" as="li" data-carousel-id="\d+" aria-selected="\w+" aria-label="[^"]+" href="(?<href>[^"]+)"',
					'<span>S(?<seasonNumber>\d+)\sE(?<episodeNumber>\d+)[^<]+</span>',
					'data-track-video_episode_title="(?<title>[^"]+)"',
					'class="tile__details-date-duration"\>[^\s]+\s\|\s(?<pubDate>[^\<]+)'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$href = "https://abc.com" + $href;
					$EPISODE_CONTENT_URI = $href;
					$EPISODE_CONTENT_RENAME_TO = (FormatSeasonEpisode $seasonNumber $episodeNumber) + " - " + ($title.Trim().Replace('-', ' '));
					if($pubDate -ne $null) { $EPISODE_PUBLISH_DATE = $pubDate; };
				}
