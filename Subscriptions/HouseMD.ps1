#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\NBC.ps1
$SUBSCRIPTION_NAME = "House MD"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_AUTH_PROVIDER = "Verizon"
$ACQUISITION_CREDENTIAL_ID = "Verizon"

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.nbc.com/house"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
        $SUBSCRIPTION_ACQUIRE_ACTION,
        {
            $EPISODE_EXCLUSION = $null;
            $EPISODE_CONTENT_RENAME_TO = (FormatSeasonEpisode $seasonNumber $episodeNumber) + " - " + $title;
        }
    )
