###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Land of the Giants"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################
$RENAME_TEMPLATE_OVERRIDE = "{SeriesName} - {SeasonAndEpisode} - {EpisodeTitle}"

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://feeds.megaphone.fm/landofthegiants"
