###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "The Dropout"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_ALLOW_RENAME = $false

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://feeds.castfire.com/abc-news-radio/the_dropout"

$SUBSCRIPTION_EPISODE_END = '<image>'

$SUBSCRIPTION_EXTRACTORS = 	@{
					'<itunes:episodeType>(?<eType>[^<]+)</itunes:episodeType>' = {$season = "1"; return $true};
					$SUBSCRIPTION_EXTRACTORS = $null;
					'<item>' = {	$EXTRACTOR_MATCH_OVERRIDE = [RegEx]::Matches("S1", 'S(?<season>\d+)');
							};
				}

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					$SUBSCRIPTION_ACQUIRE_ACTION,
					{
						$EPISODE_EXCLUSION = (($EPISODE_EXCLUSION) -or ($eType -notmatch "full"));
					}
				)
