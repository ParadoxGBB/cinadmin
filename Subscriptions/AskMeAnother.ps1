###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Ask Me Another"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.npr.org/rss/podcast.php?id=510299"

#############
# OVERRIDES #
#############
$ARCHIVE_AGE_DAYS_OVERRIDE 		= 3*30
$PURGE_TOPICAL_AGE_DAYS_OVERRIDE 	= 6*30