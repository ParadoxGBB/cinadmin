###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Up First"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_ALLOW_RENAME = $true

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.npr.org/rss/podcast.php?id=510318"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
    $SUBSCRIPTION_ACQUIRE_ACTION,
    {
        $EPISODE_EXCLUSION 	= 	(($EPISODE_EXCLUSION) -or ([DateTime]::Parse((FormatAirDate $pubDate)).DayOfWeek -eq 'Sunday'));
        #Sometimes re-published with a new GUID --- use URI instead to track dupes
        $EPISODE_CONTENT_ID = $href;
    }
)

