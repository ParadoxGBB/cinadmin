###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\TheCW.ps1
$SUBSCRIPTION_NAME = "Penn & Teller: Fool Us"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "http://www.cwtv.com/shows/penn-teller-fool-us/"

$SUBSCRIPTION_EXTRACTORS = 	@(
					'href="(?<href>/shows(?:(/penn-teller-fool-us)|(/more-video))/[^"]+)"',
					'<p class="eptitle">\s*(?<title>[^<]+)\s*</p>',
					'<p class="videoinfo">\s*S(?<season>\d+)\s+\:\s+E(?<episode>\d+)\s+[\w\s|-]+Aired:\s+(?<pubDate>[^<]+)</p>'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					$SUBSCRIPTION_ACQUIRE_ACTION,
					{
						$seasonEpisode = (FormatSeasonEpisode ($season) ($episode));
						$EPISODE_PUBLISH_DATE = FormatAirDate ($pubDate.Replace('.','/'));
						$EPISODE_EXCLUSION = $href -match 'trailer';
						$EPISODE_CONTENT_RENAME_TO = $seasonEpisode + " - " + $title
					}
				)
