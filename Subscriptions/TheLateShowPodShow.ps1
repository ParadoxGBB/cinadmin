###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "The Late Show Pod Show"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://feeds.megaphone.fm/colbert"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
    $SUBSCRIPTION_ACQUIRE_ACTION,
    {
        $title = $title.Replace("|", "/")
    }
)

#############
# OVERRIDES #
#############
$ARCHIVE_AGE_DAYS_OVERRIDE        = 1*30
$PURGE_TOPICAL_AGE_DAYS_OVERRIDE  = 60
