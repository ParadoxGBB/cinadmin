###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\CBS.ps1
$SUBSCRIPTION_NAME = "Young Sheldon"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_REMOVE_ADS = $false

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.cbs.com/shows/young-sheldon/video/xhr/episodes/page/0/size/18/xs/0/season/" + (GetLatestShowSeason $SUBSCRIPTION_NAME) + "/"
