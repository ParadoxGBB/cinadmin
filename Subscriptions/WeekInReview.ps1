#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Week in Review"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################
$SUBSCRIPTION_EXTRACTORS = 	@(
		$SUBSCRIPTION_EXTRACTORS,
		'<itunes:subtitle>(?<subtitle>[^<]+)</itunes:subtitle>'
	)

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.kuow.org/feeds/podcasts/week-in-review/podcasts/rss.xml"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					{
						$title = $title.Replace(($SUBSCRIPTION_NAME + ": "), "");
					},
					$SUBSCRIPTION_ACQUIRE_ACTION,
					{
						$EPISODE_EXCLUSION = (($EPISODE_EXCLUSION) -or ($title -match 'Words In Review') -or ($subtitle -match 'Words In Review'));
					}
				)