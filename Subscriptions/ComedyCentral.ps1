#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_AUTH_PROVIDER = "Verizon"
$ACQUISITION_CREDENTIAL_ID = "Verizon"

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.cc.com/episodes"

$SUBSCRIPTION_EPISODE_BEGIN = '<div>Full Episodes</div>'
$SUBSCRIPTION_EPISODE_END = '<script>'

$SUBSCRIPTION_EXTRACTORS = 	@(
					'<(?:span|h3) class="text[^"]+">(?<title>[^<]+)</(?:span|h3)>',
					'<(?:span|h2) role="heading"[^>]+>S(?<season>\d+) • E(?<episode>\d+)</(?:span|h2)>',
					'<span>(?<pubDate>\d{2}/\d{2}/\d{4})</span>',
					'<a href="(?<href>/episodes/[^"]+)"'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					if($season -ne $null) { $seasonEpisode = (FormatSeasonEpisode ($season) ($episode)); $EPISODE_CONTENT_RENAME_TO = $seasonEpisode + " - " + $title; };
					$href = "https://www.cc.com" + $href;
					$EPISODE_CONTENT_URI = $href;
					$EPISODE_PUBLISH_DATE = (FormatAirDate $pubDate)
				}
