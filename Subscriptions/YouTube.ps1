###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_ADD_PARAMS = "--no-playlist"

################
# CHECK / SCAN #
################

#
# NOTE: RSS feeds are more reliable than parsing the HTML.
#
# For a YouTube channel, get the id from the source and use the following
#$SUBSCRIPTION_URI = "https://www.youtube.com/feeds/videos.xml?channel_id=<id>"
# For a YouTube playlist, translate the URL from e.g. https://www.youtube.com/playlist?list=PLJaq64dKJZoqsh7PGGUi-SARV4wUz_lVa to
#$SUBSCRIPTION_URI = "https://www.youtube.com/feeds/videos.xml?playlist_id=PLJaq64dKJZoqsh7PGGUi-SARV4wUz_lVa"

$SUBSCRIPTION_EPISODE_BEGIN = '<entry>'

$SUBSCRIPTION_EXTRACTORS = 	@(
					'<media:content(?:\s*.*) url="(?<href>[^"\?]+)[^"]*"',
					'<media:title>(<!\[CDATA\[)?(?<title>[^\]<]+)(\]\]>)?<',
					'<published>(?<pubDate>[^<]+)<',
					'<id>(?<epId>[^<]+)<'
					)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$EPISODE_CONTENT_RENAME_TO = (FormatAirDate $pubDate) + " - " + $title;
					$EPISODE_CONTENT_URI = $href;
					$EPISODE_CONTENT_ID = $epId;
					if($pubDate -ne $null) { $EPISODE_PUBLISH_DATE = $pubDate; };
				}


