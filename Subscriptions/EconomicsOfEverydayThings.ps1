#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Economics of Everyday Things"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://feeds.simplecast.com/ob9OSBIN"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					{
						if(($title -match "^(\d+)\.\s") -and ($episode -eq "0")) { $episode = $Matches[1]; };
						$title = $title -replace "^\d+(\s|\.)";
					},
					$SUBSCRIPTION_ACQUIRE_ACTION
				)


