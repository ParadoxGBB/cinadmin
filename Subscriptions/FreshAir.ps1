#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Fresh Air"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################
$RENAME_TEMPLATE_OVERRIDE = "{SeriesName} - {AiredDate} - {EpisodeTitle}"

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.npr.org/rss/podcast.php?id=381444908"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					$SUBSCRIPTION_ACQUIRE_ACTION,
					{
						$EPISODE_EXCLUSION = (($EPISODE_EXCLUSION) -or $title.Contains("Best Of: "));
					}
				)


#############
# OVERRIDES #
#############
$ARCHIVE_AGE_DAYS_OVERRIDE        = 6*30
$PURGE_TOPICAL_AGE_DAYS_OVERRIDE  = 365
