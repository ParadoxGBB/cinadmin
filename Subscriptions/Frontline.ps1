###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
$SUBSCRIPTION_NAME = "Frontline"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.pbs.org/wgbh/frontline/films/"

$SUBSCRIPTION_EPISODE_BEGIN = '<h2 class="section-hed">All Films</h2>'

$SUBSCRIPTION_EXTRACTORS = 	@(
					'<a class="black-playhead-outer" href="(?<href>[^"]+)">(?<title>[^<]+)</a>',
					'<div class="list__copy small-caps-copy">(?<pubDate>[^<]+)</div>'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
									$EPISODE_CONTENT_URI = $href;
									$EPISODE_CONTENT_RENAME_TO = (FormatAirDate $pubDate) + " - " + $title;
								}
