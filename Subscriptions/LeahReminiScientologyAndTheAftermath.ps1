###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\AETv.ps1
$SUBSCRIPTION_NAME = "Leah Remini: Scientology and the Aftermath"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.aetv.com/shows/leah-remini-scientology-and-the-aftermath"
$SUBSCRIPTION_EPISODE_BEGIN = '<h2 class="list-title">Full Episodes</h2>'
$SUBSCRIPTION_EPISODE_END = '<div class="content-playlists">'

$SUBSCRIPTION_EXTRACTORS = 	@(
					'data-canonical="(?<href>/shows/leah-remini-scientology-and-the-aftermath[^"]+)"',
					'<h3 class="title">(?<title>([^<]+))</h3>',
					'<span aetn-coll="programs" aetn-key="tvSeasonNumber" class="season-number">(?<season>([^<]+))</span>',
					'<span aetn-coll="programs" aetn-key="tvSeasonEpisodeNumber" class="episode-number">(?<episode>([^<]+))</span>',
					'data-episodetype="(?<vidType>[^"]+)'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					$SUBSCRIPTION_ACQUIRE_ACTION,
					{
						$EPISODE_EXCLUSION = $vidType -NotMatch 'Free';
						$EPISODE_CONTENT_RENAME_TO = (FormatSeasonEpisode $season $episode) + " - " + $title;
					}
				)
