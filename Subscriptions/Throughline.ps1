###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Throughline"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_ALLOW_RENAME = $false

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.npr.org/rss/podcast.php?id=510333"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
    $SUBSCRIPTION_ACQUIRE_ACTION,
    {
        # Episodes with a (Year) at the end are rebroadcasts and can be skipped.
        $EPISODE_EXCLUSION 	= 	($EPISODE_EXCLUSION -or ($title -match "Bonus") -or ($title -match "(Throwback)") -or ($title -match "\(\d{4}\)$"));
    }
)