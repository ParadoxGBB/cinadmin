###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Fall Of Civilizations"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "http://feeds.soundcloud.com/users/soundcloud:users:572119410/sounds.rss"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					{
						$title = $title -replace "^\d+\.";
						$title = $title.Trim(' ', '.');
					},
					$SUBSCRIPTION_ACQUIRE_ACTION;
				)