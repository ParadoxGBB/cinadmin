###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Pod Save America"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://feeds.simplecast.com/dxZsm5kX"

#############
# OVERRIDES #
#############
$SUBSCRIPTION_ACQUIRE_ACTION =	@(
    $SUBSCRIPTION_ACQUIRE_ACTION,
    {
        # Wilderness episodes have additional episode info
        $EPISODE_EXCLUSION 	= 	($EPISODE_EXCLUSION -or ($title -match "\(Ep\.\s\d\)"));
    }
)