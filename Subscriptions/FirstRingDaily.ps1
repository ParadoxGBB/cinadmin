###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\YouTube.ps1
$SUBSCRIPTION_NAME = "First Ring Daily"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.youtube.com/feeds/videos.xml?channel_id=UCPVwH9vqT1Jfza37d69V0lg"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					$SUBSCRIPTION_ACQUIRE_ACTION,
					{
						$EPISODE_EXCLUSION = ($title -match "Live Stream") -or ($title -match "Daily Live");
					}
				)
