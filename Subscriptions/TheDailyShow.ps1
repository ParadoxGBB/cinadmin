###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
$SUBSCRIPTION_NAME = "The Daily Show"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_SUBTITLES_DAYS = 0
$ALLOW_STALE_TVDB_OVERRIDE = $false

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.paramountplus.com/shows/the-daily-show/"

$SUBSCRIPTION_EPISODE_BEGIN = '<section id="latest-episodes"'

$SUBSCRIPTION_EXTRACTORS = 	@(
					'data-tracking="\|The Daily Show\|S(?<seasonNumber>\d+)\|Ep(?<episodeNumber>\d+)\|(?<pubDate>[^\|]+)\|',
					'href="(?<href>/shows/video/[^"]+)"'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					if($href -match '"/shows/video/([^/]+)/?$"') { $EPISODE_CONTENT_ID = $Matches[1]; };
					$href = $href.Replace("\/", "/");
					$href = "https://www.paramountplus.com" + $href;
					$EPISODE_CONTENT_RENAME_TO = ( (FormatSeasonEpisode $seasonNumber $episodeNumber) + " - " + (FormatAirDate $pubDate) );
					$EPISODE_PUBLISH_DATE = $pubDate;
					$EPISODE_CONTENT_URI = $href;
				}

$SUBSCRIPTION_ACQUIRE_CUSTOM =	$SUBSCRIPTION_ACQUIRE_MECH

#############
# OVERRIDES #
#############


