#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_SUBTITLES_DAYS = 0 #Only supports TTML

################
# CHECK / SCAN #
################
$SUBSCRIPTION_EPISODE_BEGIN = '"component":"VideoTile"'
$SUBSCRIPTION_EPISODE_END = '"component":"(Slide|FeatureTile|Shelf)"'

$SUBSCRIPTION_EXTRACTORS = 	@{
					'"episodeNumber":"(?<episodeNumber>\d+)"' = { $episodeNumber -eq $null };
					'"seasonNumber":"(?<seasonNumber>\d+)"' = { $seasonNumber -eq $null };
					'"secondaryTitle":"?(?<title>([^"]+)|(null))"?,' = $null;
					'"locked":(?<locked>true|false)' = { $locked -eq $null };
					'"programmingType":"(?<programmingType>[^"]+)"' = { $programmingType -eq $null };
					'"airDate":"(?<airDate>[^"]+)"' = $null;
					'"(permalink|destination)":"(?<href>[^"]+)"' = { $href -eq $null };
				}

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$EPISODE_EXCLUSION = (($title -eq 'null') -or ($locked -eq 'true') -or ($programmingType -ne "Full Episode"));
					$EPISODE_CONTENT_URI = "https://www.nbc.com" + $href;
					$EPISODE_CONTENT_RENAME_TO = (FormatAirDate $airdate) + " - " + $title;
				}