###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\CBS.ps1
$SUBSCRIPTION_NAME = "Life In Pieces"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.cbs.com/shows/life-in-pieces/xhr/episodes/page/0/size/12/xs/0/season/" + (GetLatestShowSeason $SUBSCRIPTION_NAME) + "/"
