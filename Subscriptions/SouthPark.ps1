###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
$SUBSCRIPTION_NAME = "South Park"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_OUTPUT_FORMAT = "%(playlist_title)s.S%(playlist_index)s.%(ext)s"

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://southpark.cc.com/seasons/south-park"
$SUBSCRIPTION_EPISODE_BEGIN = 'aria-labelledby="tab-full-episodes-season"'
$SUBSCRIPTION_EPISODE_END = "<h1>Latest News</h1>"

$SUBSCRIPTION_EXTRACTORS = 	@(
					'<h3 class="text css-[^"]+">(?<title>[^<]+)</h3>',
					'<h2 role="heading" aria-level="2">S(?<seasonNumber>\d+) • E(?<episodeNumber>\d+)</h2>',
					'<a href="(?<href>/episodes/[^"]+)"'
					'<span>(?<pubDate>\d{2}/\d{2}/\d{4})</span>'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
						$EPISODE_EXCLUSION = ((IsStringDefined $pubDate) -and (((Get-Date)-[DateTime]::Parse($pubDate)).TotalDays -le 7));
						if(IsStringDefined $seasonNumber) { $EPISODE_CONTENT_RENAME_TO = (FormatSeasonEpisode $seasonNumber $episodeNumber) + " - " + $title; } else { $EPISODE_CONTENT_RENAME_TO = $title; };
						if($pubDate -ne $null) { $EPISODE_PUBLISH_DATE = $pubDate; };
						$EPISODE_CONTENT_URI = "https://southpark.cc.com" + $href;
					}
