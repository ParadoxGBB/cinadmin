###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "WWDTM"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.npr.org/rss/podcast.php?id=344098539"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
    $SUBSCRIPTION_ACQUIRE_ACTION,
    {
        $EPISODE_EXCLUSION = ($EPISODE_EXCLUSION -or $title.StartsWith("Everyone"));
    }
)