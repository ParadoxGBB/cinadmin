#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
$SUBSCRIPTION_NAME = "MLB Game Recap"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_CREDENTIAL_ID = "MLB"

#***TEAM IDs***#
# '108' =  'Los Angeles Angels';
# '109' =  'Arizona Diamondbacks';
# '110' =  'Baltimore Orioles';
# '111' =  'Boston Red Sox';
# '112' =  'Chicago Cubs';
# '113' =  'Cincinnati Reds';
# '114' =  'Cleveland Indians';
# '115' =  'Colorado Rockies';
# '116' =  'Detroit Tigers';
# '117' =  'Houston Astros';
# '118' =  'Kansas City Royals';
# '119' =  'Los Angeles Dodgers';
# '120' =  'Washington Nationals';
# '121' =  'New York Mets';
# '133' =  'Oakland Athletics';
# '134' =  'Pittsburgh Pirates';
# '135' =  'San Diego Padres';
# '136' =  'Seattle Mariners';
# '137' =  'San Francisco Giants';
# '138' =  'St. Louis Cardinals';
# '139' =  'Tampa Bay Rays';
# '140' =  'Texas Rangers';
# '141' =  'Toronto Blue Jays';
# '142' =  'Minnesota Twins';
# '143' =  'Philadelphia Phillies';
# '144' =  'Atlanta Braves';
# '145' =  'Chicago White Sox';
# '146' =  'Miami Marlins';
# '147' =  'New York Yankees';
# '158' =  'Milwaukee Brewers';

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.mlb.com/data-service/en/search?tags.slug=game-recap&page=1" #Filter by teams later
#$SUBSCRIPTION_URI = "https://www.mlb.com/data-service/en/search?tags.slug=game-recap&tags.slug=teamid-136&page=1" #Only Mariners

$SUBSCRIPTION_EPISODE_BEGIN = '"content_id":'

$SUBSCRIPTION_EXTRACTORS = 	@{
					'^"title":\s*"(?<title>[^"]+)",' 		= $null;
					'^"url":\s*"(?<uri>[^"]+\.mp4)",'		= { $uri -eq $null; };
					'^"date":\s*"(?<date>[^"]+)",'			= $null;
					'^"slug":\s*"teamid-(?<teamId>\d\d\d)",'= { $teamId -eq $null; };
					'^"state":'								= { if($teamId -eq $null)
																{ $EXTRACTOR_MATCH_OVERRIDE = [RegEx]::Matches("0", '(?<teamId>\d+)'); }
																return $true;
															};
				}

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$EPISODE_EXCLUSION = (($teamId -ne "143") -and ($teamId -ne "135") -and ($teamId -ne "117") -and ($teamId -ne "147"));
					$EPISODE_CONTENT_URI = $uri;
					$title = $title.Replace("Recap:", "");
					$title = $title.Replace("  ", " ");
					$EPISODE_CONTENT_RENAME_TO = (FormatAirDate $date) + " - " + $title;
				}

