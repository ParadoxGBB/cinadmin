###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Invisibilia"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################
#$ACQUISITION_ALLOW_RENAME = $true
#$RENAME_TEMPLATE_OVERRIDE = "{SeriesName} - {SeasonAndEpisode} - {AiredDate} - {EpisodeTitle}"

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.npr.org/rss/podcast.php?id=510307"