###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
$SUBSCRIPTION_NAME = "The Assembly Line"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_IS_AUDIO = $true

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://soundcloud.com/nesassemblyline"

$SUBSCRIPTION_REQUEST_HEADERS = @{
					"User-Agent"	=	"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0"
				}

$SUBSCRIPTION_EXTRACTORS = 	@(
					'<a itemprop="url" href="(?<href>[^"]+)">[e|E](?<episodeNumber>\d+)\s*:\s*(?<title>[^"]+)</a>'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$EPISODE_EXCLUSION = $title -eq "The Assembly Line";
					$episodeNumber = [String]::Format("S01E{0}", $episodeNumber);
					$EPISODE_CONTENT_RENAME_TO =  (FormatSeasonEpisode $episodeNumber) + " - " + $title;
					$EPISODE_CONTENT_URI = "https://soundcloud.com" + $href;
				}
