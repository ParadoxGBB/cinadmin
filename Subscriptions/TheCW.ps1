#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_EXTRACT_METADATA = $false

################
# CHECK / SCAN #
################
$SUBSCRIPTION_EPISODE_BEGIN = '<span>Full Episodes</span>'
$SUBSCRIPTION_EPISODE_END = '<h2 class="sh playlist-title\s">EXTRAS'

$SUBSCRIPTION_EXTRACTORS = 	@(
					'href="(?<href>/shows/[^"]+)"'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$href = "https://www.cwtv.com" + $href;
					$EPISODE_CONTENT_URI = $href
				}
