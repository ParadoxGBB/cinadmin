###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\YouTube.ps1
$SUBSCRIPTION_NAME = "Virtual Runs"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.youtube.com/feeds/videos.xml?channel_id=UCq5O2ZfzRQChMdahnpKpGVg"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					{
						$title = $title.Replace("- Virtual Run -", "").Trim();
						$title = $title.Replace("- Virtual Run", "").Trim();
						$title = $title.Replace("Virtual Run - ", "").Trim();
						$title = $title.Replace("Virtual Run", "").Trim();
						$title = $title.Trim('-');
						$title = $title.Trim();
					},
					$SUBSCRIPTION_ACQUIRE_ACTION,
					{
						$EPISODE_EXCLUSION = (($title -match " VR") -or ($title -match " 360") -or ($title -match "Hike"));
					}
				)
