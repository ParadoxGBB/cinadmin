###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
$SUBSCRIPTION_NAME = @("Peacock", "Primetime in Paris")
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_REMOVE_ADS = $true
$ACQUISITION_ALLOW_RENAME = $true
$ACQUISITION_SUBTITLES_DAYS = 0

################
# CHECK / SCAN #
################

#############
# OVERRIDES #
#############
$RENAME_TEMPLATE_OVERRIDE = "Primetime in Paris - {EpisodeTitle}"
$PURGE_ON_WATCHED = $true