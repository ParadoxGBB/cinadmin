#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_AUTH_PROVIDER = "Verizon"
$ACQUISITION_CREDENTIAL_ID = "Verizon"

################
# CHECK / SCAN #
################
$SUBSCRIPTION_EPISODE_BEGIN = 'class="program-tiles-section"'
$SUBSCRIPTION_EPISODE_END = 'data-m-name="static_cast_list"'

$SUBSCRIPTION_EXTRACTORS = 	@(
					'\s+href="(?<href>/shows/[^"]+)"'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$href = "https://www.aetv.com" + $href;
					$EPISODE_CONTENT_URI = $href
				}
