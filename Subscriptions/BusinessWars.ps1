###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Business Wars"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://rss.art19.com/business-wars"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					{
                        $title =  $title -replace "\|\s\d$";
                        $title =  $title.Replace(" | ", ": ");
                        $title =  $title.Replace("| ", ": ");
                        if($pubDate -ne $null) { $EPISODE_PUBLISH_DATE = $pubDate; };
                        if($episode -ne '0') { $pubDate = $null; };
					},
					$SUBSCRIPTION_ACQUIRE_ACTION;
				)

#############
# OVERRIDES #
#############

