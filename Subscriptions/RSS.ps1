###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_ALLOW_RENAME = $false

################
# CHECK / SCAN #
################
$SUBSCRIPTION_EPISODE_BEGIN = '<item>'

$SUBSCRIPTION_EXTRACTORS = 	@{
					'<enclosure(?:\s*.*) url="(?<href>[^"\?]+)[^"]*"'	= $null;
					'<title>(<!\[CDATA\[)?(?<title>[^\]<]+)(\]\]>)?<'	= $null;
					'<pubDate>(?<pubDate>[^<]+)<'				= $null;
					'<guid\s?((isPermaLink|isPermalink)="(true|false)")?>(<!\[CDATA\[)?(?<epId>[^\]<]+)(\]\]>)?<'			= $null;
					'<itunes:season>(?<season>[^<]+)</itunes:season>'	= $null;
					'<itunes:episode>(?<episode>[^<]+)</itunes:episode>'	= $null;
					'</item>' = {
								if($season -eq $null) { $season = "0" }
								if($episode -eq $null) { $episode = "0" }
							};
				}

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$EPISODE_EXCLUSION = ($title -match ($SUBSCRIPTION_NAME + " Introduces")) -or ($title -match "Introducing") -or ($title -match ($SUBSCRIPTION_NAME + " Presents")) -or ($title -match "Presenting") -or ($title.StartsWith("Announcing")) -or ($title -match "Coming Soon") -or ($title -match "Trailer") -or ($title -match "Preview") -or ($title -match [RegEx]::Escape("Replay)"));
					if(($title -match "[Ss](\d+)(\s|:|\||_|$)") -and ($season -eq "0")) { $season = $Matches[1]; };
					if(($title -match "[Ee](\d+)(\s|:|\||_|$)") -and ($episode -eq "0")) { $episode = $Matches[1]; };
					if(($title -match "[Ee]pisode\s(\d+)(\s|:|\||_|$)") -and ($episode -eq "0")) { $episode = $Matches[1]; };
					if(($title -match "[Ee]p\.\s*(\d+)(\s|:|\||_|$)") -and ($episode -eq "0")) { $episode = $Matches[1]; };
					$tmpTitle =  $title -replace "[Ss]\d+(\s|:|\||_|$)";
					$tmpTitle = $tmpTitle -replace "[Ee]\d+(\s|:|\||_|$)";
					$tmpTitle = $tmpTitle -replace "[Ee]pisode\s\d+(\s|:|\||_|$)";
					$tmpTitle = $tmpTitle -replace "[Ee]p\.\s*(\d+)(\s|:|\||_|$)";
					$tmpTitle = $tmpTitle.Trim(' ', ':', '|', "_" );
					$tmpTemplate = (ReturnAppropriateFileNameTemplate);
					if( ($season -eq "0") -and ($episode -ne "0") ) { $season = $null };
					if( ($episode -ne "0") -and ($tmpTemplate.Contains("{SeasonAndEpisode}") -or (($pubDate -eq $null) -and $tmpTemplate.Contains("{AiredDate}"))) ) { $tmpTitle = (FormatSeasonEpisode $season $episode) + " - " + $tmpTitle; };
					if( ($pubDate -ne $null) -and ($tmpTemplate.Contains("{AiredDate}") -or (($episode -eq "0") -and $tmpTemplate.Contains("{SeasonAndEpisode}"))) ) { $tmpTitle = (FormatAirDate $pubDate) + " - " + $tmpTitle; };
					if( $pubDate -ne $null ) { $EPISODE_PUBLISH_DATE = $pubDate; };
					if( $tmptitle.Contains(' - - ') ) { $tmptitle = $tmptitle.Replace(' - - ', " - ") };
					$EPISODE_CONTENT_RENAME_TO = $tmpTitle;
					$EPISODE_CONTENT_URI = $href;
					$EPISODE_CONTENT_ID = $epId;
				}

