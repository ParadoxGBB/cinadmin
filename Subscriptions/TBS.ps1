#########
#GENERAL
#########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_AUTH_PROVIDER = "Verizon"
$ACQUISITION_CREDENTIAL_ID = "Verizon"
$ACQUISITION_ADD_PARAMS = "--hls-prefer-native"

################
# CHECK / SCAN #
################
$SUBSCRIPTION_EPISODE_BEGIN = '<h2>Available episodes</h2>'
$SUBSCRIPTION_EPISODE_END = '<h2>LATEST CLIPS</h2>'

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$href = "https://www.tbs.com" + $href;
					$EPISODE_CONTENT_URI = $href
				}
