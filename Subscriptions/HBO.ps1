###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################
$ALLOW_STALE_TVDB_OVERRIDE = $true
$ACQUISITION_REMOVE_ADS = $true

################
# CHECK / SCAN #
################

#############
# OVERRIDES #
#############
$AD_REMOVE_ADS_BOOKENDED = $true