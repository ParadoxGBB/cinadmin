###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\YouTube.ps1
$SUBSCRIPTION_NAME = "Real Time With Bill Maher (Overtime)"
$SUBSCRIPTION_POLLABLE = $true

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "https://www.youtube.com/feeds/videos.xml?channel_id=UCy6kyFxaMqGtpE3pQTflK8A"

$SUBSCRIPTION_ACQUIRE_ACTION =	@(
					{
						$EPISODE_EXCLUSION = (($title -notmatch "Overtime") -or ($title -match "Watch"));
						$title = $title.Replace("Real Time with Bill Maher: Overtime", "").Trim();
						$title = $title.Replace("Overtime with Bill Maher:", "").Trim();
						$title = $title.Replace("Overtime with Bill Maher -", "").Trim();
						$title = $title.Replace("Overtime with Bill Maher", "").Trim();
						$title = $title.Replace("Overtime:", "").Trim();
						$title = $title.Replace("Overtime", "").Trim();
						$title = $title.Replace("Real Time with Bill Maher", "").Trim();
						$title = $title.Replace("Bill Maher", "").Trim();
						$title = $title.Replace("Real Time", "").Trim();
						$title = $title.Replace("(HBO)", "").Trim();
						$title = $title.Replace("|", "").Trim();
					},
					$SUBSCRIPTION_ACQUIRE_ACTION
				)
