###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\HBO.ps1
$SUBSCRIPTION_POLLABLE = $false
$SUBSCRIPTION_NAME = "Last Week Tonight with John Oliver"

#####################
# ACQUIRE / EXTRACT #
#####################
$ALLOW_STALE_TVDB_OVERRIDE = $false

################
# CHECK / SCAN #
################

#############
# OVERRIDES #
#############
$TVDB_SELECT_OVERRIDE = "SE"