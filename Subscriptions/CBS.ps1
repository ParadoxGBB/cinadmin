###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1

#####################
# ACQUIRE / EXTRACT #
#####################
# New CBS content is now DRM'd and cannot be extracted via tools like YouTube-DL.
#  (https://github.com/yt-dlp/yt-dlp/issues/5550)
$ACQUISITION_REMOVE_ADS = $true
$ALLOW_STALE_TVDB_OVERRIDE = $true

################
# CHECK / SCAN #
################
$SUBSCRIPTION_EPISODE_BEGIN = '"type":"Full Episode"'

$SUBSCRIPTION_EXTRACTORS = 	@{
					'"title":\s*"(?<title>[^"]+)"'					= { $title -eq $null; };
					'"airdate_iso":\s*"(?<aireddate>[^"]+)"'		= { $aireddate -eq $null; };
					'"season_number":\s*"(?<seasonNumber>[^"]+)"'	= $null
					'"episode_number":\s*"(?<episodeNumber>[^"]+)"'	= $null
					'"url":\s*"(?<href>[^"]+)"'						= {$href -eq $null; };
				}

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$href = $href.Replace("\/", "/");
					$href = "https://www.cbs.com" + $href;
					$EPISODE_CONTENT_RENAME_TO = $title.Replace($SUBSCRIPTION_NAME, $SUBSCRIPTION_NAME + " - " + (FormatSeasonEpisode $seasonNumber $episodeNumber));
					$EPISODE_PUBLISH_DATE = $aireddate;
					$EPISODE_CONTENT_URI = $href;
				}

$SUBSCRIPTION_ACQUIRE_CUSTOM =	$SUBSCRIPTION_ACQUIRE_MECH
