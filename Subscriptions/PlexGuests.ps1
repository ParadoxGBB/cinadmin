###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
$SUBSCRIPTION_POLLABLE = $false

$SUBSCRIPTION_NAME = $PLEX_GUEST_SERIES

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_REMOVE_ADS = $false

################
# CHECK / SCAN #
################

#############
# OVERRIDES #
#############
$PURGE_ON_WATCHED = $true
$GUEST_WATCHERS = $PLEX_GUESTS
