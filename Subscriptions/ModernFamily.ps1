###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\ABC.ps1
$SUBSCRIPTION_NAME = "Modern Family"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "http://abc.go.com/shows/modern-family"

$SUBSCRIPTION_EXTRACTORS = 	@(
					'href="(?<href>/shows/[^"]+/season-(?<season>\d+)/(?<episode>\d+)-(?<title>[^"]+))"',
					'data-track-video_episode_release_date="(?<pubDate>[^"]+)"'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$EPISODE_EXCLUSION = ($href -notmatch "/shows/modern-family");
					$href = "http://abc.go.com" + $href;
					$EPISODE_CONTENT_URI = $href;
					$EPISODE_CONTENT_RENAME_TO = (FormatSeasonEpisode $season $episode) + " - " + ($title.Trim().Replace('-', ' '))
				}
