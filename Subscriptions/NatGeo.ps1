###########
# GENERAL #
###########
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################
$ACQUISITION_AUTH_PROVIDER = "Cookies"
$ACQUISITION_CREDENTIAL_ID = $DATA_PATH + "\Cookies.txt"
$ACQUISITION_REMOVE_ADS = $true

################
# CHECK / SCAN #
################
$SUBSCRIPTION_IGNORE_DOWNLOAD_URL_ERRORS = $true

$SUBSCRIPTION_EXTRACTORS = 	@(
					'<a class="Tile_title_(?:\w+) videoTiles_title_(?:\w+)" href="(?<href>/tv/watch/[^"]+)">',
					'<span>\s*S(?<season>\d+)\s+E(?<episode>\d+)\s+(?<title>[^<]+)<'
				)

$SUBSCRIPTION_ACQUIRE_ACTION =	{
					$EPISODE_CONTENT_URI = "https://www.nationalgeographic.com" + $href;
					$EPISODE_CONTENT_RENAME_TO = (FormatSeasonEpisode $season $episode) + " - " + $title;
				}
