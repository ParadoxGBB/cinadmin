###########
# GENERAL #
###########
. $SUBSCRIPTION_INSTALL_PATH\RSS.ps1
$SUBSCRIPTION_NAME = "Recode Decode"
$SUBSCRIPTION_POLLABLE = $false

#####################
# ACQUIRE / EXTRACT #
#####################

################
# CHECK / SCAN #
################
$SUBSCRIPTION_URI = "http://feeds.feedburner.com/Recode-Decode"

#############
# OVERRIDES #
#############
$PURGE_ON_WATCHED = $true

$ARCHIVE_AGE_DAYS_OVERRIDE        = 6*30
$PURGE_TOPICAL_AGE_DAYS_OVERRIDE  = 365
