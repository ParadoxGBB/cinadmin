##################
#
# Paths
#
##################
$CONSUMED_FOLDER_NAME    = "Z Consumed"
$DOWNLOADED_TV_PATH     = "C:\Sample\Media\TV"
$DOWNLOADED_PODCASTS_PATH  = "C:\Sample\Media\Podcasts"
$DOWNLOADED_MOVIES_PATH = "C:\Sample\Media\Movies"
$ISO_CONVERT_PATH       = $DOWNLOADED_MOVIES_PATH + "\Z Converted"
$PURGE_PATH             = "C:\Sample\Media\ToPurge"
$DAILY_FOLDER_NAME      = "DAILY BRIEFING"
$ARCHIVE_FOLDER_NAME    = "Archive"
$DATA_PATH              = "C:\CinAdmin\data"

##################
#
# Files
#
##################
$MEDIA_EXTENSIONS = @(".mkv", ".mp4", ".avi", ".mpg", ".m4v", ".iso", ".mp3", ".m4a", ".ogg", ".wav", ".wma")

$PURGE_PATTERNS   = @("*.txt", "thumbs.db", "*.nfo", "*.png", "*.sfv", "*.jpg")
$WATCHING_HOSTS   = @("wmplayer.exe", "vlc.exe", "System", "Microsoft.Media.Player.exe", "Zune.exe", "Plex Media Server.exe", "KODICLIENT - LOCALHOST\MediaUser")
$RECORDING_HOSTS  = @("Azureus.exe", "comskip.exe", "ffmpeg.exe", "python.exe", "youtube-dl.exe", "yt-dlp.exe", "MediaMallServer.exe", "PLAYONSERVER - LOCALHOST\MediaUser")

##################
#
# Misc
#
##################
$STARTUP_VERBOSE_ALIASES      = $true
$ARCHIVE_AGE_DAYS             = 15
$PURGE_TOPICAL_AGE_DAYS       = $ARCHIVE_AGE_DAYS*2
$TRACK_AGE_HOURS              = 4
$NEW_SEASON_LEAD_NOTICE_DAYS  = 10
$MAX_CACHE_AGE_DAYS           = 30
$EMBED_SUBTITLES              = $true
$OPENSUBTITLES_CREDENTIAL     = "OpenSubtitles"
$OPENSUBTITLES_LANGUAGE_CODE  = "en"

#################################
#
# Downloaded Media Mapping Paths
#
#################################
$ORGANIZE_PATH_OVERRIDE =  @{
#    'Sample'                        =    '$DOWNLOADED_TV_PATH\Sample'; #EXAMPLE
        }

#################################
#
# Consumed Media Mapping Paths
#
#################################
$CONSUMED_PATH_OVERRIDE =  @{
#    'Sample'                        =    '$DOWNLOADED_TV_PATH\$CONSUMED_FOLDER_NAME\Sample'; #EXAMPLE
        }

#################################
#
# Topical Shows
#
#################################
$TOPICAL_SHOWS =  @(
            'The Daily Show' #EXAMPLE
        )

#################################
#
# Daily Shows
#
#################################
$DAILY_SHOWS =  @(
            'Up First' #EXAMPLE
            )

#################################
#
# DOWNLOAD PATH EXCLUSIONS
#
#################################
$DOWNLOADED_PATH_EXCLUSIONS =  @(
#                    '$DOWNLOADED_TV_PATH\Exclude' #EXAMPLE
                    )

#################################
#
# POLLING SCRIPTS
#
#################################
$POLLING_SCRIPTS =  @{
    # CORE
        ($SCRIPT_PATH + "\Audit-MediaLibraries.ps1")    =  "7.00:00:00";
        ($SCRIPT_PATH + "\Check-MediaSubscription.ps1") =    "04:00:00";
        ($SCRIPT_PATH + "\Convert-Media.ps1")           =    "00:02:00";
        ($SCRIPT_PATH + "\Copy-MediaAudio.ps1")         =    "01:00:00";
        ($SCRIPT_PATH + "\Get-MediaSubtitles.ps1")      =    "03:00:00";
        ($SCRIPT_PATH + "\Notify-MediaSeason.ps1")      =  "7.00:00:00";
        ($SCRIPT_PATH + "\Organize-Media.ps1")          =    "01:00:00";
        ($SCRIPT_PATH + "\Remove-MediaAds.ps1")         =    "01:00:00";
        ($SCRIPT_PATH + "\Track-Media.ps1")             =    "00:10:00";
    # EXTENSIONS
#       ($EXTENSION_INSTALL_PATH + "\JohnOliverDuplicates.ps1")      =   "06:00:00";
        ($EXTENSION_INSTALL_PATH + "\PlayOnVersion.ps1")             = "1.00:00:00";
        ($EXTENSION_INSTALL_PATH + "\RefreshClientLibraries.ps1")    =   "04:00:00";
#       ($EXTENSION_INSTALL_PATH + "\ReRecordPartialRecordings.ps1") =   "01:00:00";
#       ($EXTENSION_INSTALL_PATH + "\SouthParkDuplicates.ps1")       =   "06:00:00";
    # SUBSCRIPTION OVERRIDES
    }


#####################
#
# Library Integration
#
#####################
#Only clusters by folder when the unconsumed number of show episodes equals or exceeds
# this number. Note that this is convenient  when using video library interfaces that
# display as if a share to minimize navigation and make new shows pop, but this gets in
# the way of certain clients like Kodi that have problems scraping shows in the root
# folder of a library.
$EPISODES_AGGREGATE_THRESHOLD = 1

$KODI_INTEGRATION       = $false
$KODI_SERVERS = @{  'http://127.0.0.1:8080/jsonrpc' = 'kodi'; #EXAMPLE
                    'http://127.0.0.2:8080/jsonrpc' = 'kodi'; #EXAMPLE
                }
$KODI_FOLDER_TRANSLATE = @{ 'smb://127.0.0.1/'  = 'C:\Sample\Media\' }

$PLAYON_WARN_SUBSCRIPTION_PROBLEMS = $true

$PLEX_INTEGRATION      = $false
#$PLEX_SERVERNAME       = "LOCALHOST" #OPTIONAL IF LOCALHOST
# TO LOGIN VIA TOKEN, WHICH IS QUICKER:
# - Set username in credentials (via Store-MediaCredential.ps1) to "localhost"
# - Set password to token, see https://support.plex.tv/articles/204059436-finding-an-authentication-token-x-plex-token/
$PLEX_CREDENTIALS      = "plex"
$PLEX_GUESTS           = @("guest1@plex.tv", "guest2@plex.tv")
$PLEX_GUEST_SERIES     = @( "Show1", "Show2")

##################
#
# RENAME
#
##################
$USE_TVDB                   = $true
$RENAME_TEMPLATE            = "{SeriesName} - {SeasonAndEpisode} - {EpisodeTitle}"
$TOPICAL_RENAME_TEMPLATE    = "{SeriesName} - {SeasonAndEpisode} - {AiredDate} - {EpisodeTitle}"


#####################################
#
# Audit (Audit-MediaLibraries.ps1)
#
#####################################
$AUDIT_EXCLUSIONS         = @($PURGE_PATH, $ISO_CONVERT_PATH, $DOWNLOADED_PATH_EXCLUSIONS )
$AUDIT_DUPLICATES         = $false
$AUDIT_EPISODE_NAMES      = $false
$AUDIT_SEASON_DIRECTORIES = $false
$AUDIT_SIZES              = $false
$AUDIT_EPISODE_CONTINUITY = $false
$AUDIT_MOVIES             = $false
$AUDIT_PLEX               = $PLEX_INTEGRATION
$AUDIT_KODI               = $KODI_INTEGRATION

##################
#
# Dependencies
#
##################
$COMSKIP_BINARY       = "C:\tools\ComSkip\comskip.exe"
$COMSKIP_PROFILE_PATH = "C:\tools\ComSkip\profiles"
$FFMPEG_BINARY        = "C:\tools\ffmpeg\ffmpeg.exe"
$FFPROBE_BINARY       = "C:\tools\ffmpeg\ffprobe.exe"
$HANDLE_BINARY        = "C:\tools\handle\handle.exe"
$MAKEMKV_BINARY       = "C:\tools\MakeMKV\makemkvcon.exe"
$PLEX_API             = "C:\tools\python\lib\site-packages\plexapi"
$PYTHON_BINARY        = "C:\tools\python\python.exe"
$YOUTUBE_DL_BINARY    = "C:\tools\yt-dlp"
$ERROR_EXCLUSION_BINARY = @( ([System.IO.Path]::GetFileName($COMSKIP_BINARY)) )

####################
#
# Media Transfer
#
####################
# NOTE: You can use a volume name instead if an assigned drive letter shifts too much
#  i.e. D:\USB --> [FLASH_DRIVE]:\USB
$MEDIA_TRANSFER_PROFILES =  @{
    'usb'     =  @{
                'Destination'    = 'D:\USB';
            };
    'phone'   =  @{
                'podcasts:Destination' = 'SD card\Podcasts\'; #EXAMPLE
                'music:Destination'    = 'SD card\Music\'; #EXAMPLE
            };
    'podcasts'=  @{
                'Source'    = 'C:\Sample\Media\Podcasts'; #EXAMPLE
                'Exclude'   = 'C:\Sample\Media\Podcasts\Exclude1\', 'C:\Sample\Media\Podcasts\Exclude2\'; #EXAMPLE
                'Clobber'   = $true
            };
    'music'   =  @{
                'Source'                  = 'C:\Sample\Media\Music\'; #EXAMPLE
                'phone:GeneratePlaylists' = $true
            };
    }

##################
#
# EXTENSIONS
#
##################
$EXT_PLAYPASS_EXE   = "C:\tools\PlayPass\PlayPass.exe"
$EXT_PLAYPASS_SKIPS = "C:\tools\PlayPass\skips"

##################
#
# Diagnostics
#
##################
$AD_REMOVE_DIAGNOSTICS      = $false
$EXTRACT_HANDLE_DIAGNOSTICS = $false
$EXTENDED_CHAR_DIAGNOSTICS  = $false
$PERFORMANCE_DIAGNOSTICS    = $false
$SHOW_ERROR_EXCEPTIONS      = $false
$TASK_SCHEDULING            = $false
