[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [Parameter(Position=0)]
    [string] $Id
)

$SCRIPT_PATH = Split-Path -parent $PSCommandPath
. $SCRIPT_PATH\CinAdmin.Functions.ps1

#########
#
# MAIN
#
#########

if($Help)
{
	ShowSyntax
	PauseIfClicked
	return
}
ShowConfiguration
TestConfiguration

if(-not (IsStringDefined $Id))
{
	$Id = PromptGetUserString "Please specify an identifier for this credential" $null
}

$cred = get-credential

if(test-path $CREDENTIAL_FILE)
{
	$accounts = Import-Csv -Path $CREDENTIAL_FILE
	foreach($currentAccount in $accounts)
	{
		if($currentAccount.UserName -eq $cred.UserName -and ($currentAccount.Id -eq $Id))
		{
			LogWarning Account already added: [($Id)] [($cred.UserName)]
		}
	}
}

if((IsStringDefined $cred.UserName))
{
	$secureStringHash = (($cred.Password) | ConvertFrom-SecureString)
	$csvEntry = New-Object -TypeName PSObject -Property @{"Id" = ($Id); "UserName" = ($cred.UserName); "Password" = ($secureStringHash); }
	AddCsvEntry $csvEntry $CREDENTIAL_FILE
	LogInfo Added credential ($Id): [($cred.UserName)] [([string]::new("*", $cred.Password.Length))]
}
