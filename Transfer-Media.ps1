[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [Parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true)]
    [Alias('Source')]
    [String] $Path,
    [String] $Destination,
    [Parameter(Position=0)]
    [String[]] $Profile,
    [String[]] $Exclude,
    [switch] $Clobber,
    [switch] $StripExtendedChars,
    [switch] $CopyPlaylists
)

#########
#
# MAIN
#
#########

Begin
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1
	trap { ReleaseScriptConcurrency; break }

	LockScriptConcurrency

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

	$isError = $false

	if($Profile.Count -gt 0)
	{
		foreach($currentProfile in $Profile)
		{
			if($MEDIA_TRANSFER_PROFILES[$currentProfile] -eq $null)
			{
				LogError Media Transfer Profile Not Found: [$currentProfile]
				$isError = $true
				return
			}

			$currentSelectedProfile = $MEDIA_TRANSFER_PROFILES[$currentProfile]
			foreach($currentProfileVariable in $currentSelectedProfile.Keys)
			{
				$variableName = $currentProfileVariable
				$variableValue = $currentSelectedProfile[$currentProfileVariable]

				if($variableName.Contains(":"))
				{
					$variableTargetProfile = $variableName.SubString(0, $variableName.IndexOf(":"))
					$variableName = $variableName.SubString($variableName.IndexOf(":")+1)

					if($variableTargetProfile -NotIn $Profile)
					{
						VerboseLogInfo Skipping profile variable: [$variableName]
						continue
					}
				}

				if($variableName -Match "Source")
				{
					$variableName = "Path"
				}

				$isVariableAlreadyDefined = $true
				$currentlyDefinedVariable = (Get-Variable -Name $variableName -ErrorAction SilentlyContinue).Value
				if(($currentlyDefinedVariable -eq $false) -and ($currentlyDefinedVariable.GetType().ToString() -eq 'System.Management.Automation.SwitchParameter'))
				{
					$isVariableAlreadyDefined = $false
				}
				elseif(($currentlyDefinedVariable -eq "") -and ($currentlyDefinedVariable.GetType().ToString() -eq 'System.String'))
				{
					$isVariableAlreadyDefined = $false
				}
				elseif($currentlyDefinedVariable -eq $null)
				{
					$isVariableAlreadyDefined = $false
				}

				if($isVariableAlreadyDefined)
				{
					VerboseLogInfo Variable is already defined. Skipping... [$variableName]
					continue
				}

				if(-not $WHATIF_MODE)
				{
					Set-Variable -Name $variableName -Value $variableValue -Scope Script
				}
				LogInfo [$variableName] --> [$variableValue]
			}
		}
	}

	if(-not (IsStringDefined($Destination)))
	{
		LogError Destination path not defined.
		$isError = $true
		return
	}

	if(-not (IsStringDefined($Path)))
	{
		LogError Source path not defined.
		$isError = $true
		return
	}

	$convertedExclusionList = New-Object -TypeName System.Collections.ArrayList
	foreach($currentExclusion in $Exclude)
	{
		if(IsProbablyPattern $currentExclusion)
		{
			$convertedExclusionList.Add($currentExclusion) > $null
		}
		else
		{
			$convertedExclusionList.Add((ExpandAndFixPath $currentExclusion)) > $null
		}
	}
	$Exclude = $convertedExclusionList

	$shell = New-Object -ComObject Shell.Application

	if(IsInvalidPath($Destination))
	{
		if(IsValidPath(LoadPath $Destination))
		{
			$Destination = LoadPath $Destination
		}
		elseif($Destination -match "\[(?<volName>\w*)\]\:\.*")
		{
			$originalDestination = $Destination
			$volName = $Matches["volName"]
			foreach($currentVolume in (Get-Volume))
			{
				if($currentVolume.FileSystemLabel -match $volName)
				{
					$Destination = $Destination.Replace("[" + $volName + "]", $currentVolume.DriveLetter)
					LogInfo Translated Volume: [$originalDestination] --> [$Destination]
					$Destination = LoadPath $Destination
					break
				}
			}

			if(IsInvalidPath $Destination)
			{
				LogError Destination could not be mapped to a volume name: [($originalDestination)]
				$isError = $true
				return
			}
		}
		else
		{
			LogWarning Destination path not a valid location. Checking for connected MTP devices...

			$devices = $shell.NameSpace(17).self.GetFolder.items() | Where-Object { ($_.Type -eq 'Portable Device') -or ($_.Type -eq 'Mobile Phone') }

			if($devices -eq $null)
			{
				LogError "No MTP connected devices detected."
				$isError = $true
				return
			}

			if($devices.Count -ge 2)
			{
				LogError "Multiple MTP devices detected."
				$isError = $true
				return
			}

			$mtpDevice = $devices
			LogInfo Found MTP Device: [($mtpDevice.Name)] [($mtpDevice.Path)]
			$Script:MTP_DEVICE_PATH = $mtpDevice.Path
			$Script:MTP_DEVICE_NAME = $mtpDevice.Name
			$IsMtpDevice = $true

			if(-not ($Destination.StartsWith('\')))
			{
				$Destination = "\" + $Destination
			}

			$Destination = ($mtpDevice.Path.ToString() + $Destination)
			$destinationRoot = $shell.NameSpace($Destination).self
			if(($destinationRoot -eq $null) -or ($destinationRoot.Type -ne 'File Folder'))
			{
				LogError Root MTP path not accessible or valid: [($Destination)]
				$isError = $true
				return
			}
		}
	}
}
Process
{
	try
	{
		if($Help)
		{
			return
		}

		if($isError)
		{
			return
		}

		if($Path -eq $null)
		{
			$Path = $DOWNLOADED_TV_PATH
		}

		if($Clobber)
		{
			if($IsMtpDevice)
			{
				$destinationDirectory = $Destination.Trim('\')
				$filesToClobber =  ListMtpDirectoryRecursively $destinationDirectory
			}
			else
			{
				$destinationDirectory = (Get-Item -LiteralPath $Destination).FullName.Trim('\')
				$filesToClobber =  (Get-ChildItem -LiteralPath $destinationDirectory -Recurse -FollowSymlink -File | Where-Object { ($_.Extension -In (($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))) }).FullName
			}
			LogInfo Scanning media to clobber: [(FormatFriendlyTransferPath $destinationDirectory)] ...

			$cCurrentProgress = 0
			foreach($currentFileToClobber in $filesToClobber)
			{
				$cCurrentProgress++
				OutputProgressBar ("Clobbering...") ($cCurrentProgress/$filesToClobber.Count)

				$fileFound = $false
				foreach($Target in $Path)
				{
					$Target = LoadPath $Target
					$projectedSourceFile = $currentFileToClobber.Replace($destinationDirectory.Trim('\'), $Target.Trim('\'))
					if($StripExtendedChars)
					{
						$projectedSourceFile = MapHandleFileOutput $projectedSourceFile
						if(-not (IsStringDefined $projectedSourceFile))
						{
							continue
						}
					}

					if(IsExcludedFromTransfer $projectedSourceFile)
					{
						VerboseLogInfo EXCLUDED: [$currentFileToClobber]
						$fileFound = $true
					}

					DebugLogInfo CONSIDERING CLOBBER: [$currentFileToClobber] --> [$projectedSourceFile]
					if(IsValidPath($projectedSourceFile))
					{
						VerboseLogInfo SOURCE MATCHED NO CLOBBER: [$currentFileToClobber]
						$fileFound = $true
					}
				}

				if(-not $fileFound)
				{
					LogInfo [(FormatFriendlyTransferPath $currentFileToClobber)] --> [X]

					if($IsMtpDevice)
					{
						DeleteMtpFile ($currentFileToClobber)
					}
					else
					{
						DeleteItem ($currentFileToClobber)
					}
				}
			}

			if($IsMtpDevice)
			{
				DeleteEmptyMtpDirectoryRecursively $destinationDirectory $CopyPlaylists $false
			}
			else
			{
				PurgeEmptyDirectories $destinationDirectory
			}
		}

		foreach($Target in $Path)
		{
			$Target = LoadPath $Target
			if(IsInvalidPath $Target) { continue }

			$Target = $Target.TrimEnd("\")
			$Destination = $Destination.TrimEnd("\")
			$targetPlaylistFolder = $null
			$directories =  Get-ChildItem -LiteralPath $Target -Recurse -FollowSymlink -Directory | Sort-Object -Property FullName
			$cCurrentProgress = 0
			foreach($currentDirectory in $directories)
			{
				$cCurrentProgress++
				OutputProgressBar ("Transferring Media [" + $currentDirectory.FullName + "]...") ($cCurrentProgress/$directories.Count)
				$destinationDirectory = $currentDirectory.FullName.Replace($Target, $Destination)
				TransferMediaDirectory $Target ($currentDirectory.FullName) $destinationDirectory $Exclude $CopyPlaylists $IsMtpDevice $targetPlaylistFolder
			}

			if((Get-Item -LiteralPath $Target).PSIsContainer)
			{
				$destinationDirectory = (Get-Item -LiteralPath $Target).FullName.Replace($Target, $Destination)
				TransferMediaDirectory $Target $Target $destinationDirectory $Exclude $false $IsMtpDevice $null
			}
			else
			{
				$filesToTransfer = Get-Item -LiteralPath $Target | Where-Object { ($_.Extension -In ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS)) }
				foreach($currentFileToTransfer in $filesToTransfer)
				{
					$source = $currentFileToTransfer.FullName
					$destinationFile = $source.Replace($currentFileToTransfer.Directory.FullName, $Destination)

					if(IsExcludedFromTransfer $source)
					{
						VerboseLogInfo EXCLUDED: [($source)]
					}
					else
					{
						TransferMediaFile $source $destinationFile $destinationIsAudioDevice
					}
				}
			}
		}
	}
	catch
	{
		ReleaseScriptConcurrency
		throw
	}
}
End
{
	if($StripExtendedChars)
	{
		PurgeEmptyDirectories $CONVERSION_TMP
	}

	if(-not ($Help -or $IsMtpDevice -or $isError))
	{
		PurgeEmptyDirectories $destination
	}

	ReleaseScriptConcurrency
	PauseIfClicked
}