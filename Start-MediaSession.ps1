[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [switch] $Remote,
    [string] $RemoteUserName,
    [string] $RemoteHostName,
    [string] $RemoteInstallPath,
    [switch] $Poll,
    [switch] $TerminatePoll,
    [switch] $Task
)

##########################
#
# FUNCTION PromptGetUserString
#
##########################
function PromptGetUserString($prompt, $default)
{
	$canContinue = $false
	$response = $null

	while(-not $canContinue)
	{
		if($default -ne $null)
		{
			$prompt = $prompt + " [ ENTER = " + $default + " ] "
		}
		$response = Read-Host $prompt

		switch($response)
		{
			{$_ -in ""}
			{
				if($default -ne $null)
				{
					$response = $default.Trim()
					$canContinue = $true
				}
				else
				{
					Write-Warning "Invalid input, please try again."
				}
			}
			default
			{
				$response = $response.Trim()
				$canContinue = $true
			}
		}
	}

	return $response
}

if($Remote -and ($Poll -or $Task))
{
	Write-Error "Choose either to poll or a remote session, please."
	return
}

if($Poll -and $Task)
{
	Write-Error "Choose either to poll or task, please."
	return
}

if(($RemoteUserName.Length -gt 0) -or ($RemoteHostName.Length -gt 0) -or ($RemoteInstallPath.Length -gt 0))
{
	$Remote = $true
}

switch($MyInvocation.InvocationName.ToLower().Trim())
{
	{ $_ -in  	"start-remotemediasession",
				"startrms",
				"enter-remotemediasession",
				"new-remotemediasession",
				"start-remotemediasession" }
	{
		$Remote = $true
	}
	{ $_ -in  	"stop-mediasessionpolling",
				"stop-mediapolling",
				"terminate-mediasessionpolling",
				"terminate-mediapolling",
				"termmp",
				"stopmp" }
	{
		$TerminatePoll = $true
	}
	{ $_ -in  	"start-mediapollingtask",
				"start-mediasessiontask",
				"start-mediatask",
				"startmt" }
	{
		$Task = $true
	}
}

if($Remote)
{
	$remoteInstallFileVersion = (Get-Item -LiteralPath ($MyInvocation.MyCommand.Path)).Length

	if($Debug)
	{
		Write-Debug ("EXECUTING: [" + $MyInvocation.Line + "]")
	}

	if(-not (Test-Path -LiteralPath ($PSHOME + "\pwsh.exe")))
	{
		Write-Error ($PROJECT_NAME + " remote sessions requires PowerShell Core. (Get it at: https://github.com/PowerShell/PowerShell/releases)")
		return
	}

	if($PSSenderInfo -ne $null)
	{
		Write-Warning "You are already in a remote session."
		return
	}

	if($RemoteUserName.Length -le 0)
	{
		$RemoteUserName = PromptGetUserString "Remote username:" ($env:USERNAME)
	}
	$RemoteUserName = $RemoteUserName.Trim()

	if($RemoteHostName.Length -le 0)
	{
		$RemoteHostName = PromptGetUserString "Remote host target:" ($env:USERDOMAIN)
	}
	$RemoteHostName = $RemoteHostName.Trim()

	if($RemoteInstallPath.Length -le 0)
	{
		$RemoteInstallPath = PromptGetUserString ("Remote installation path:") (Split-Path -parent $PSCommandPath)
	}
	$RemoteInstallPath = $RemoteInstallPath.Trim()

	$remoteSession = New-PSSession -HostName $RemoteHostName -UserName $RemoteUserName
	if($remoteSession -eq $null)
	{
		return
	}

	$setupBlock = { param($passedRemoteInstallPath, $passedInRemoteInstallFileVersion)
			$passedRemoteInstallPath = $passedRemoteInstallPath.Trim()

			$startupScript = $passedRemoteInstallPath + "\Start-MediaSession.ps1"
			if(-not(Test-Path -LiteralPath $startupScript))
			{
				Write-Error ("Startup Script not found: [" + $startupScript + "]")
				return
			}

			$localInstallFileVersion = (Get-Item -LiteralPath $startupScript).Length
			if($localInstallFileVersion -ne $passedInRemoteInstallFileVersion)
			{
				Write-Error ("The remote startup script is different than what is installed on this machine. Please re-install with the -Remote parameter again. [" + $startupScript + "]")
				return
			}

			&($startupScript)

			Set-Location ($passedRemoteInstallPath)

			if($CONFIG_FILE -eq $null)
			{
				$configFile = $passedRemoteInstallPath + "\CinAdmin.Configuration.ps1"
				. $configFile
			}

			New-Alias -Description $PROJECT_NAME -Name Exit Exit-PSSession -Scope Script
			Write-Information -InformationAction Continue -MessageData "*** You are in a remote PS session. Execute `"exit`" to terminate this session and return to the parent session. ****"
		}

	Write-Information -InformationAction Continue -MessageData ""
	Invoke-Command -Session $remoteSession -ArgumentList ($RemoteInstallPath, $remoteInstallFileVersion) -ScriptBlock $setupBlock
	Write-Information -InformationAction Continue -MessageData ""

	Enter-PSSession -Session $remoteSession
}
else
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}

	if($Poll)
	{
		if(IsRemoteSession)
		{
			Write-Error "You can not poll or task within a remote session."
			return
		}
		$ProgressPreference = 'SilentlyContinue'
		HasAdministratorRights > $null
		if(IsValidPath $CONCURRENCY_FILE)
		{
			Write-Warning "Clearing Semaphore [($CONCURRENCY_FILE)]..."
			DeleteItem $CONCURRENCY_FILE
		}

		if(IsValidPath $TERMINATION_REQUEST)
		{
			LogWarning Poll Termination Request detected.
			DeleteItem ($TERMINATION_REQUEST)
		}
		elseif(IsCurrentlyPolling)
		{
			LogWarning CinAdmin is already polling.
		}
		else
		{
			StartMediaPolling
		}
	}
	elseif($TerminatePoll)
	{
		if(IsCurrentlyPolling)
		{
			if(-not (IsValidPath $TERMINATION_REQUEST))
			{
				GetExecutingScriptName | SafeOutFile -LiteralPath $TERMINATION_REQUEST
				LogInfo Termination request written.
			}
			LogInfo Waiting for termination...

			while(IsValidPath $TERMINATION_REQUEST)
			{
				SleepFor 5
			}

			if(-not (IsCurrentlyPolling))
			{
				if(IsValidPath $RECORDING_CSV)
				{
					LogWarning Recording media pending: [$RECORDING_CSV]
				}
				LogInfo ...Terminated.
			}
			else
			{
				SleepFor 5
				if(IsCurrentlyPolling)
				{
					LogError Polling is still commencing.
				}
			}
		}
		else
		{
			LogInfo Not currently polling.
		}
	}
	elseif($Task)
	{
		if(IsRemoteSession)
		{
			Write-Error "You can not poll or task within a remote session."
			return
		}
		$ProgressPreference = 'SilentlyContinue'
		if(IsCurrentlyPolling)
		{
			LogWarning "Detected that the polling command [$POLL_COMMAND] is currently already executing (or an executing PowerShell window title contains '$POLLING_WIN_TITLE'). Please close it."
			return
		}
		else
		{
			if(IsValidPath $CONCURRENCY_FILE)
			{
				Write-Warning "Clearing Semaphore [($CONCURRENCY_FILE)]..."
				DeleteItem $CONCURRENCY_FILE
			}
			StartScheduledTask
		}
	}
	else
	{
		Set-Location -Path $SCRIPT_PATH

		#CREATE ALIASES
		foreach($currentScript in $ALIASES.Keys)
		{
			if(IsValidPath $currentScript)
			{
				$aliasName = [System.IO.Path]::GetFileNameWithoutExtension($currentScript)
				New-Alias -Description $PROJECT_NAME -Name $aliasName $currentScript -Scope Global

				foreach($currentAlias in ($ALIASES[$currentScript]))
				{
					New-Alias -Description $PROJECT_NAME -Name $currentAlias $currentScript -Scope Global
				}
			}
			else
			{
				LogWarning Script not found: [$currentScript]
			}
		}

		FlashMessage 'Green' @(	('Welcome to ' + $PROJECT_NAME + '!  (v. ' + $PROJECT_VERSION + ')') )
		OutputColoredText $null ""
		OutputColoredText $null 'To get started, you can run any of the scripts with -WhatIf or -Help.'
		OutputColoredText $null ""
		OutputColoredText 'Yellow' 'Example:'
		OutputColoredText $null '  .\Organize-Media.ps1 [-WhatIf] [-Help]'
		OutputColoredText $null ""
		OutputColoredText 'Yellow' 'Questions?'
		OutputColoredText $null '  Leave me a message at this BitBucket repository (https://bitbucket.org/ParadoxGBB/cinadmin).'
		OutputColoredText $null ""

		FlashMessage 'Magenta' @(	($PROJECT_NAME + " Copyright (C) 2015-" + ((Get-Date).Year) + " Gregory Beitler"),
									"This program comes with ABSOLUTELY NO WARRANTY; for details consult the included file 'license.txt'.",
									"This is free software, and you are welcome to redistribute it under certain conditions; see 'license.txt' for details." )


		OutputColoredText $null ""
		if(-not $STARTUP_VERBOSE_ALIASES)
		{
			OutputColoredText 'Green' 'Basic commands (aliases are also configured):'
		}
		else
		{
			OutputColoredText 'Green' 'Commands:'
		}
		Get-Alias | Where-Object {($_.Description -match $PROJECT_NAME) -and (Test-Path ($_.Name + ".ps1"))} | Format-Table -Property Name, Definition

		if($VERBOSE_MODE -or $STARTUP_VERBOSE_ALIASES)
		{
			OutputColoredText 'Green' 'Additional command aliases (Too verbose? Set $STARTUP_VERBOSE_ALIASES to $false):'
			Get-Alias | Where-Object {($_.Description -match $PROJECT_NAME) -and -not (Test-Path ($_.Name + ".ps1"))} | Format-Table -Property Name, Definition
		}

		if($PSEdition -eq 'Desktop')
		{
			FlashMessage 'DarkYellow' @(	("You're running this session under Windows PowerShell."),
											("For better support and performance, please run " + $PROJECT_NAME + " with PowerShell Core instead.") )
			OutputColoredText $null ""
		}

		OutputColoredText 'Cyan' '[$DOWNLOADED_TV_PATH]:       [' $DOWNLOADED_TV_PATH ']'
		OutputColoredText 'Cyan' '[$DOWNLOADED_PODCASTS_PATH]: [' $DOWNLOADED_PODCASTS_PATH ']'
		OutputColoredText 'Cyan' '[$DOWNLOADED_MOVIES_PATH]:   [' $DOWNLOADED_MOVIES_PATH ']'
		OutputColoredText $null ""

		Start-Transcript -Path $SESSION_LOG_OUTPUT -Append -NoClobber > $null

		if((-not (IsRemoteSession)) -and (-not (IsCurrentlyPolling)))
		{
			if(GetUserResponse ("It looks like " + $PROJECT_NAME + " is currently not polling for media changes. Would you like to start the polling task?") $true)
			{
				StartScheduledTask
			}
		}
	}
	PauseIfClicked
}
