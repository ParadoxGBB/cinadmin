- [CINADMIN SCRIPTS README](#cinadmin-scripts-readme)
	- [What is this repository for?](#what-is-this-repository-for)
		- [Core Features](#core-features)
		- [How does it work?](#how-does-it-work)
		- [How do I get set up?](#how-do-i-get-set-up)
	- [Configuration](#configuration)
	- [Dependencies](#dependencies)
	- [Diagnostics](#diagnostics)
	- [How to run tests](#how-to-run-tests)
	- [Deployment instructions](#deployment-instructions)
	- [Description of scripts](#description-of-scripts)
	- [Description of configuration parameters](#description-of-configuration-parameters)
	- [Extensions](#extensions)
	- [Subscriptions](#subscriptions)
		- [A simple subscription example: The Big Bang Theory (CBS)](#a-simple-subscription-example-the-big-bang-theory-cbs)
		- [A more advanced example: Saturday Night Live (NBC)](#a-more-advanced-example-saturday-night-live-nbc)
		- [A few additional notes regarding subscriptions](#a-few-additional-notes-regarding-subscriptions)
		- [Making your own Subscriptions](#making-your-own-subscriptions)
	- [Media on the Go](#media-on-the-go)
	- [Library Integration](#library-integration)
		- [Directory Structure](#directory-structure)
		- [PlayOn](#playon)
		- [Movies](#movies)
		- [Episodic Content (TV, Podcasts)](#episodic-content-tv-podcasts)
		- [Kodi](#kodi)
		- [Plex](#plex)
		- [Exceptions](#exceptions)
	- [Handles](#handles)
	- [Regarding YouTube-DL / YT-DLP](#regarding-youtube-dl--yt-dlp)
	- [Licensing](#licensing)
	- [Known issues and considerations](#known-issues-and-considerations)
		- [Who do I talk to if I have problems with CinAdmin?](#who-do-i-talk-to-if-i-have-problems-with-cinadmin)
	- [History and Versioning](#history-and-versioning)


# CINADMIN SCRIPTS README

## What is this repository for? ##

The Cinematic Administrator, or CinAdmin, is a collection of PowerShell scripts that allows those running on Windows to manage and grow their media libraries, focused on TV, movies, and podcasts. They allow you to define show name / path mappings to organize, archive and purge content automatically after they have been recorded or consumed. It can also normalize file names with information from online databases and notify you if new seasons for your shows are available. It can also utilize open-source tools to remove media advertisements, add subtitles, and acquire content from many different content hosting sites on the internet. Rather than building up this functionality by scratch, CinAdmin leverages other tools when possible to do a lot of its work.

More broadly, the goal here is to have a completely hands-off autopilot internet-powered DVR on your computer, one that handles your library consistently no matter how you get your files and what you use to consume them. It is designed to integrate well with other common recording / watching programs like PlayOn, Kodi and Plex, as well as play nicely with your existing collections.


### Core Features ###

* Organize your library with minimal clicks to access the content you want to see. Standardizes name and metadata presentation of your library.
* Manage your to-watch queue with archiving, series clustering, and topical show expiration. Groups specific media as part of your daily briefing.
* Archive or purge show videos you have consumed, independent on what program you use to view them, so your watch queue is up to date.
* Integrate with popular media consumption and recording tools like PlayOn, Kodi, Plex, and VLC.
* Integrate with popular tools and services to consistently name, acquire content, remove commercial advertisements, and embed subtitles.
* Provide flexible consumption options like transferring media to other devices or streaming from solutions like Plex, allowing your watched/consumed and to watch queue to be accessible.
* Notify you when new seasons of content are available.
* Flexible framework encourages hacking and tweaking to twist the scripts to your needs either direct editing or through adding extension scripts.


### How does it work? ###

In the configuration file (*CinAdmin.Configuration.ps1*), you can specify downloaded root paths for audio, TV, and movies, as well as a purge root path. You can also optionally specify specific show path mappings.

Video files found under $DOWNLOADED_TV_PATH are moved according to $DOWNLOADED_TV_PATHS (or the mappings) if they're not being recorded.

Video files that have been flagged as consumed and are not defined as a topical show are moved under the subfolder name defined by $CONSUMED_FOLDER_NAME where they are archived indefinitely and have the override mappings in $CONSUMED_PATH_OVERRIDE applied to them. This is useful for shows that you may want to re-watch in the future or wish to keep around, but not be listed in your queue of shows to actively watch.

Shows and podcasts that are defined as topical (optionally listed in $TOPICAL_SHOWS) will have episodes older than $ARCHIVE_AGE_DAYS moved to a subfolder for that show named $ARCHIVE_FOLDER_NAME if it is defined. Shows that are older than $PURGE_TOPICAL_AGE_DAYS are moved to the purge folder defined in $PURGE_PATH. Shows that are older than $PURGE_TOPICAL_AGE_DAYS in the purge folder are recycled (moved to the Recycle Bin) where they can be eventually deleted by the user. This setting is intended for those shows that air frequently and you watch when you can, but you are fine with missing if you fall too far behind and you do not wish to archive older episodes. When topical shows are consumed are moved to the purged directory directly and eventual recycling. This is appropriate for shows such as talk shows or news shows.

Videos found under $DOWNLOADED_MOVIES_PATH are moved to the subfolder name defined by $CONSUMED_FOLDER_NAME if they are not being recorded and are not given special consideration to air date, episode, show folders, or anything relating to TV and are not renamed according to THETVDB database information.

Video files under $DOWNLOADED_PODCASTS_PATH will have their audio extracted and the original file deleted. You can configure video files to automatically extract and copy their audio under $DOWNLOADED_PODCASTS_PATH. This is useful for listening to TV shows as if they were podcasts. Audio files are tracked and organized under $DOWNLOADED_PODCASTS_PATH like video files under $DOWNLOADED_TV_PATH are, so it is intended to organize podcasts and shows, not music.

In the configuration file, you can also configure paths to common needed tools like FFMPEG, Python, Handle, and other libraries like in Kodi and in Plex. You can also tweak and configure other options described later in this document.

Once you have everything configured and test that configuration, you can either run the scripts yourself manually (for example, *Organize-Media.ps1* to organize your library, or *Track-Media.ps1* to scan for newly open files that are being watched), or you can instead install and execute a scheduled task that will run some of these scripts at scheduled intervals (set up via *InstallCinAdmin.bat*).


### How do I get set up? ###

These scripts can either be run manually in a CinAdmin PowerShell session, can be run from a parent script (*Start-MediaSession.ps1 -Poll*) or can be run from within a scheduled task to poll for changes on your behalf (which basically runs *Start-MediaSession.ps1 -Task* on login). You can also do remote installations to run these scripts from other computers on your home network.

Before you get started, you might need to configure remote signing in PowerShell to allow your computer to execute these scripts:

	Set-ExecutionPolicy RemoteSigned

To set up, download all the files to a directory on your computer (these are conveniently zipped for you at https://bitbucket.org/ParadoxGBB/cinadmin/downloads/), and execute the installation launching script (*InstallCinAdmin.bat*). To do so, you can execute it in a Command Prompt or PowerShell session yourself, or you can double-click it. Note that all of the CinAdmin scripts should pause when double-clicked to let you read output before the window terminates.

If you wish to run the installation script yourself directly in a PowerShell session, you can execute:

	.\Install-CinAdmin.ps1 -Target <Path to installation directory>

Installing will copy the files to that target location, prompt you to edit the configuration file, create a shortcut you can pin elsewhere in your system, run a few tests on the scripts to check for errors, and finally give you the option to add a new scheduled task on your system to process your libraries from that point forward.

After you install CinAdmin, you can re-execute *InstallCinAdmin.bat* (or *.\Install-CinAdmin.ps1 -Remote*) on other machines to allow you to run CinAdmin from that installation remotely utilizing PowerShell remoting. This is useful if you have multiple computers on your network you wish to execute these scripts on.


## Configuration ##
-------------------

To configure the scripts, you can run the installation scripts (*InstallCinAdmin.bat* or *Install-CinAdmin.ps1*) which will eventually prompt you to edit the configuration file (*CinAdmin.Configuration.ps1*). You can also simply open the configuration file with your text editor of choice to reflect your preferences directly.

For a description on the configuration parameters in the file and what they do, you can use the *-Help* parameter (or scroll down in this content):

	.\Start-MediaSession.ps1 -Help

To see syntax, command examples, parameter definitions, and related configuration settings, you can use the *-Help* parameter for any of the other scripts as well. This provides more information than using the built in PowerShell *-?* parameter.


## Dependencies ##
------------------

To work fully, these scripts leverage several existing tools and programs:

*ComSkip* (http://www.comskip.org/): This is an open-source tool that scans video files, and, using various algorithms, can detect segments within that are most likely advertisements.

*cURL* (https://curl.se//): This tool can download and transfer web data such as HTML, XML, and JSON. Starting with CinAdmin version 2.4.0 subscriptions use cURL to parse sources for new content, rather than PowerShell native web commands. Although cURL has historically started as a Linux-based tool, starting with Windows 10, cURL also natively ships on that platform as well.

*FFMPEG / FFPROBE* (http://http://ffmpeg.org/): These are open-source video manipulation tools that allow splicing, concatenating, and metadata transferal. These are used for a lot of CinAdmin's functionality, for example ad removal and subtitle embedding.

*Handle.exe* (https://docs.microsoft.com/en-us/sysinternals/downloads/handle): This is a simple command line tool that allows the scripts to detect what processes have video files open, and from there determine if a file is being recorded or watched. Since a lot of these programs run as services, typically this needs to run with full administrator privileges to "see" them. *Handle.exe* is used by the *Track-Media.ps1* script.

*Kodi* (https://www.kodi.tv/): Kodi, originally XBMC, or the XBox Media Console, is a powerful media player that can play a wide range of content formats and is supported by an arsenal of plugins to customize and enhance functionality.

*MakeMKV* (http://www.makemkv.com/): MakeMKV converts all titles within a DVD to MKV files, including subtitles. *Convert-Media.ps1* leverages this tool to allow streaming the content within ISO files.

*OpenSubtitles* (http://www.opensubtitles.com / http://www.opensubtitles.org): OpenSubtitles provides crowdsourced subtitle files that can either be downloaded beside your target media or they can be directly embedded into the target by *Get-MediaSubtitles.ps1*. Unlike subtitles that are rendered directly (or "burned") into the video frames, embedded subtitles can be turned on or off by most video viewing clients like Kodi or VNC.

*PlayOn* (https://www.playon.tv/): PlayOn allows you to record content from many internet channels and services many which are premium such as HBO, Hulu, Disney+, and Netflix. CinAdmin can monitor the hosting process and can tell which shows are being actively recorded by querying PlayOn's configuration.

*Plex* (https://www.plex.tv/): Plex is a powerful media streamer that allows you to access your library via many different devices from anywhere, though at the expense of some of the formats supported by Kodi. For example, Plex cannot play ISO files, whereas Kodi can.

*PowerShell* https://github.com/PowerShell/PowerShell/releases): All these scripts require PowerShell to execute. CinAdmin should work on both Windows PowerShell (based on the .NET Framework) and PowerShell Core (which is based on .NET Core). However, it is recommended for a better experience to use PowerShell Core as it is more performant and is being actively developed. CinAdmin currently requires PowerShell to be installed on Microsoft Windows but may one day support other platforms.

*Python 3x* (multiple sources): If you opt to use the Python built version of YouTube-DL or wish to integrate with Plex via PlexAPI, you will also need to install and specify where Python is installed to execute those tools. This should not be required if you are using the self-contained Windows exe build of YouTube-DL or if you have no need to integrate with Plex.

*Python PlexAPI* (https://python-plexapi.readthedocs.io/en/latest/introduction.html): If you use Plex, this Python-based API allows these scripts to mark watched content watched in those libraries. These APIs also trigger library refreshes to update how your library is updated.

*StreamFab* (https://www.dvdfab.cn/downloader.htm?trackid=headmenu2): StreamFab is a Chinese-based media downloading product that can download content from a wide range of sources. Whereas PlayOn is a stream recorder, StreamFab directly downloads and decrypts media files.

*TVDB* (http://www.thetvdb.com/): The TVDB is an open database that provides series and episode information for almost all TV shows out there. If you have $USE_TVDB configured, *Organize-Media.ps1* will attempt to use this information to normalize and rename media (that are not movies) as well as update metadata.

*YOUTUBE-DL / YT-DLP* (https://github.com/rg3/youtube-dl / https://github.com/yt-dlp/yt-dlp): This tool can extract video from many different internet sources, expanding extensively from the original target of YouTube. It can handle most nonencrypted content from networks such as TBS, Comedy Central, CBS, ABC, AE, and The CW. To access content from traditional cable networks, you may need a cable provider set of credentials. Given that YOUTUBE-DL has fallen behind in active development and maintenance, it's recommended that you use a popular fork of the project, YT-DLP, instead. 

*Windows Terminal* (https://github.com/microsoft/terminal/releases): Windows Terminal is a Microsoft open-source project that allows you to group all console / command line session environments such as PowerShell, the Command Prompt, and Windows Subsystem for Linux (WSL) in a beautiful multi-tabbed environment. Starting with Windows 11 it has also been included with the base operating system. CinAdmin detects and integrates with the Preview version of Terminal if that is detected first.


## Diagnostics ##
-----------------

All output for the scripts is redirected to the file defined by the $LOG_OUTPUT configuration parameter (unless you are executing with the *-WhatIf* parameter specified). You can get additional information by passing in *-Verbose* and *-Debug* parameters as well. An additional set of logs are generated when specific tools like COMSKIP is executed, as well as an execution log that shows when and what was executed so you can test, reproduce, and fix any errors you might encounter.

Specifying *-WhatIf* to a script also runs additional configuration checks on *CinAdmin.Configuration.ps1* and will show warnings or errors found.

Additional advanced output on each script can be outputted via the *-Verbose* and *-Debug* parameters. There are also advanced diagnostic parameters you can configure in the configuration file ($TASK_SCHEDULING, $SHOW_ERROR_EXCEPTIONS, $EXTRACT_HANDLE_DIAGNOSTICS, $AD_REMOVE_DIAGNOSTICS).

CinAdmin also comes with an auditing script that can run targeted checks on your libraries (*Audit-MediaLibraries.ps1*) that is useful for if you are trying to understand why content is not scraped correctly.


## How to run tests ##
----------------------

All of CinAdmin's scripts can be run in "test" or "what if" mode, which displays information without modifying any files. For example:

	.\Organize-Media.ps1 -WhatIf

Specifying *-WhatIf* to a script also runs additional configuration checks on *CinAdmin.Configuration.ps1* and will show warnings or errors found.

As part of the installation process, CinAdmin will attempt to run each of the polling scripts (configured as $POLLING_SCRIPTS) under test mode.

These scripts were tested on a x64 Windows 11 system with PowerShell Core, but they should work on any Windows system that supports PowerShell 4.0 or higher and will run on both Windows PowerShell and PowerShell Core. Windows 7+ systems allow an upgrade to PowerShell 4.0. If you encounter issues or unexpected errors, please do log an issue via BitBucket.org's tracking system.


## Deployment instructions ##
-----------------------------

As indicated before, to set up execute the installation launching script:

	.\InstallCinAdmin.bat

After you do so, you will be prompted to enter in the installation target directory, then to edit the configuration file, and let the script test each of the polling scripts with the *-WhatIf* test parameter.


## Description of scripts ##
----------------------------

*Audit-MediaLibraries.ps1:* Performs extensive checks on your media libraries and reports back the results.

*Check-MediaSubscription.ps1:* Checks a subscription whether there is any new media content to acquire. If no subscription is specified, all pollable subscriptions under $SUBSCRIPTION_INSTALL_PATH are checked. If previously unacquired, this script will use *Get-Media.ps1* to grab it. Subscription files provide instructions on how to extract content from RSS and web pages.

*CinAdmin.Configuration.ps1:* This file contains the settings that can be customized by you and isn't intended to be executed by itself.

*CinAdmin.Functions.ps1:* This file contains most of the media-specific logic the other scripts use, and isn't intended to be executed by itself.

*Convert-Media.ps1:* Detects and converts ISO files into streamable formats via MakeMKV. This script is designed to assist Plex clients access disc images from your library.

*Copy-MediaAudio.ps1:* Extracts the audio from video media to the path configured by $DOWNLOADED_PODCASTS_PATH. Subscriptions can indicate that audio should be cloned by the $ACQUISITION_EXTRACT_AUDIO setting. Note that this script supports pipelining.

*Functions.ps1:* This file contains most of the generic logic the other scripts use, and isn't intended to be executed by itself.

*Get-Media.ps1:* Given a URI, acquires media content from that location while optionally using configuration from a subscription file to process it.

*Get-MediaSubtitles.ps1:* If supporting configuration exists, search, match, download, and/or embed subtitles from OpenSubtitles.org into media files.

*Install-CinAdmin.ps1:* Copies, configures, tests, and enables the CinAdmin scripts. To install CinAdmin, it is recommended that you launch the wrapping script *InstallCinAdmin.bat* instead.

*Notify-MediaSeason.ps1:* This script queries TVDB.com to look for new seasons for the shows defined in $TVDB_SERIES_CSV and stores the results in $CURRENT_SEASONS_CSV and will notify you via a popup window and notification if any are found that will air $NEW_SEASON_LEAD_NOTICE_DAYS in the future. It is recommended you run this script every week or so. If you run this via *Start-MediaSession.ps1* *-Poll*, it runs every Sunday at 3AM.

*Organize-Media.ps1:* This script reads the directory mappings you specify via settings like $DOWNLOADED_TV_PATH and moves them according to their consumed status when the tracking threshold is up. The script can be smart about how it clusters content into subfolders minimize the amount of surfing you need to do to get to your content (see the $EPISODES_AGGREGATE_THRESHOLD configuration setting). If you have $USE_TVDB configured, TVDB information is also gathered to attempt to rename each of the files (but deferred if multiple/no results are found --- to configure those, run this script with the *-BuildCache* parameter). Shows that are matched as a "Topical Show" (set via $TOPICAL_SHOWS) have their older shows moved to an archive location if they are not watched, and those files are eventually purged. Consumed topical shows are moved directly to the purge folder and are not mapped to a watched folder. It is recommended that you can run this script every hour or so. Note that now renaming is supported with TVDB information, this can take a bit of time to complete depending on the size of your library, so please be patient if you see delays when executing this script. Note that this script supports pipelining.

*Remove-MediaAds.ps1:* Utilizes tools like COMSKIP and FFMPEG to detect and strip advertising from media. Currently, only matched extensions that are downloaded TV in non-excluded locations are considered for ad removal. Note that this script supports pipelining. It is recommended that you can run this script every hour or so.

*Show-MediaLogs.ps1:* Displays the script and execution logs, or optionally search for a pattern within them.

*Start-MediaSession.ps1:* Starts a PowerShell session to start using CinAdmin, either interactively or an automated polling session via a scheduled task. Interactive sessions can be either started locally or through remote PowerShell (via the *-Remote* parameter).

*Store-MediaCredential.ps1:* Stores credentials in an encrypted form to be used for acquisition of content in secure form. After executing, you will be prompted for a username and password. Stored credentials are referenced and retrieved by a unique identifier.

*Track-Media.ps1:* Scan via handle information to determine whether new media should be tracked as consumed. If a path is specified, manually add or remove media to be tracked as consumed after $TRACK_AGE_HOURS has passed. Media marked as watched is shifted to their mapped consumed directory. *Track-Media.ps1* *-Remove* also either forgets media that is currently being tracked, or resets watched media by moving them back into the to watch downloaded paths. Note that this script supports pipelining and requires administrator rights.

*Transfer-Media.ps1:* Recursively searches, sorts, and copies media to a new location. Note that this script supports pipelining.


## Description of configuration parameters ##
---------------------------------------------

You can also get this output if you execute:

	.\Start-MediaSession.ps1 -Help


*$AD_REMOVE_DIAGNOSTICS [ True ]:* If toggled, exports videoclips marked as ads into the purge folder for debugging purposes.

*$ARCHIVE_AGE_DAYS [ 15 ]:* Age threshold of media in $TOPICAL_SHOWS that are transferred to a dedicated subfolder specified in $ARCHIVE_FOLDER_NAME, if defined.

*$ARCHIVE_FOLDER_NAME [ ]:* Name of the folder that older, unconsumed topical shows are moved within before they are purged. If is not set, archiving is effectively turned off.

*$AUDIT_DUPLICATES [ True ]:* Check all library items for duplicates.

*$AUDIT_EPISODE_CONTINUITY [ False ]:* Analyze episodic library items for possible missing episodes.

*$AUDIT_EPISODE_NAMES [ True ]:* Check all episodic library items for incorrect naming.

*$AUDIT_EXCLUSIONS:* Ignore items in these paths when auditing libraries.

*$AUDIT_KODI [ True ]:* Scan Kodi library items for missing content and incorrect watch state.

*$AUDIT_MOVIES [ True ]:* Check all library movies for possible metadata and title mismatches.

*$AUDIT_PLEX [ True ]:* Scan Plex library items for missing content and incorrect watch state.

*$AUDIT_SEASON_DIRECTORIES [ True ]:* Check directory names for incorrect season format.

*$AUDIT_SIZES [ True ]:* Check all library items a very small size, 5KB. This is useful for flagging corrupted or incorrectly converted media.

*$COMSKIP_BINARY [ C:\tools\ComSkip\comskip.exe ]:* Location where COMSKIP tool is located. This is used to transfer metadata for renamed media and to stitch together media after ads have been identified to remove them.

*$COMSKIP_PROFILE_PATH [ C:\tools\ComSkip\profiles ]:* Location where individual ComSkip profiles are located. You can configure individual profile files via the $COMSKIP_PROFILE setting on a per-subscription basis.

*$CONSUMED_FOLDER_NAME [ Z Consumed ]:* Folder name under downloaded paths to move consumed media that is not purged.

*$CONSUMED_PATH_OVERRIDE:* Directory Mapping to apply to watched files. Note you may include folder variables like $DOWNLOADED_TV_PATH and they will be expanded.

*$DAILY_FOLDER_NAME [ DAILY BRIEFING ]:* Name of the folder that daily shows are moved to. Daily shows are clustered together in this folder to provide a briefing location and are purged after a day if unwatched. If is not set, archiving is effectively turned off.

*$DAILY_SHOWS:* List of daily show names. Media that matches these names are moved to $DAILY_FOLDER_NAME and are directly moved to $PURGE_PATH when watched or within 24 hours. This is ideal for daily news media.

*$DATA_PATH [ C:\CinAdmin\data ]:* Root Path where logs, cache/state CSV files, and temporary media files are stored.

*$DOWNLOADED_MOVIES_PATH [ C:\Sample\Media\Movies ]:* Root folder location for longer media like movies to manage. Media managed here do not get organized or aggregated or mapped in folders per show, undergo renaming with TV database information, or attempt to parse season, air, and episode information from the name.

*$DOWNLOADED_PATH_EXCLUSIONS:* List of paths that will not have their children media organized or marked as consumed. Note you may include folder variables like $DOWNLOADED_TV_PATH and they will be expanded.

*$DOWNLOADED_PODCASTS_PATH [ C:\Sample\Media\Podcasts ]:* Root path where audio media is copied or extracted. Note that this is intended to contain episodic content, not music.

*$DOWNLOADED_TV_PATH [ C:\Sample\Media\TV ]:* Root folder location for newly downloaded media to manage. Shows that cannot be mapped to a folder default to being copied in the root, to help minimize folder traversal.

*$EMBED_SUBTITLES [ True ]:* Embed acquired subtitle files into the media.

*$EPISODES_AGGREGATE_THRESHOLD [ 1 ]:* If new media is not mapped, the show name is extracted. If it can, if there is more than this number of shows matching that name, those shows will be moved into its own dedicated folder. This is convenient for minimizing navigation to new media however may confuse clients that require media to be in its own folder.

*$ERROR_EXCLUSION_BINARY [ comskip.exe ]:* These binaries are excluded from checks for standard error output when executing them.

*$EXTENDED_CHAR_DIAGNOSTICS [ True ]:* Advanced diagnostic. Displays considerations around extended characters when processing media.

*$EXTRACT_HANDLE_DIAGNOSTICS [ False ]:* Advanced diagnostic. If a file conflict is detected, handle.exe outputs process information in a separate file.

*$FFMPEG_BINARY [ C:\tools\ffmpeg\ffmpeg.exe ]:* Location where FFMPEG tool is located. This is used to transfer metadata for renamed media and to stitch together media after ads have been identified to remove them.

*$FFPROBE_BINARY [ C:\tools\ffmpeg\ffprobe.exe ]:* Location where $FFPROBE tool is located.

*$HANDLE_BINARY [ C:\tools\handle\5.0\handle64.exe ]:* Location of handle.exe.

*$ISO_CONVERT_PATH [ C:\Sample\Media\Movies\Z Converted ]:* When an ISO file is converted, its resulting files are copied to a folder in this path. The intention here is that this is shared via Plex for streaming to clients.

*$KODI_FOLDER_TRANSLATE:* Specifies one or more folders configured in Kodi libraries (such as SMB shares) that need to be translated to local library paths.

*$KODI_INTEGRATION [ True ]:* When set to $true, toggles library integration with Kodi if configured. When configured, consumed state is synchronized and its library is periodically refreshed.

*$KODI_SERVERS:* A dictionary containing the JSON-RPC endpoints and credential IDs for configured Kodi servers.

*$MAKEMKV_BINARY [ C:\Program Files (x86)\MakeMKV\makemkvcon.exe ]:* Location of MakeMKV, which is used to convert ISO files.

*$MAX_CACHE_AGE_DAYS [ 30 ]:* After this time, the caches will be cleaned and pruned by the script that uses them.

*$MEDIA_EXTENSIONS [ .mkv .mp4 .avi .mpg .m4v .iso .mp3 .m4a .ogg .wav .wma ]:* File extensions that are considered when organizing media.

*$MEDIA_TRANSFER_PROFILES:* Configures profiles that *Transfer-Media.ps1* can use to conveniently expand parameters. See the readme file for more details.

*$NEW_SEASON_LEAD_NOTICE_DAYS [ 10 ]:* If a new season is found with an episode starting this many days in the future, the *Notify-NewSeason.ps1* script will notify you.

*$OPENSUBTITLES_CREDENTIAL [ OpenSubtitles ]:* Credential ID for a configured account to OpenSubtitles.org. This is needed to download and embed subtitles.

*$OPENSUBTITLES_LANGUAGE_CODE [ en ]:* Default ISO 639 code to search for subtitles. This is needed to download and embed subtitles.

*$ORGANIZE_PATH_OVERRIDE:* Directory Mapping to apply to newly downloaded files. Note you may include folder variables like $DOWNLOADED_TV_PATH and they will be expanded.

*$PERFORMANCE_DIAGNOSTICS [ False ]:* Advanced diagnostic. Exports last media used cache usage file to diagnose unused cache entries and throws when misused.

*$PLAYON_WARN_SUBSCRIPTION_PROBLEMS [ True ]:* If PlayOn server is installed, periodically display errors to the console if a subscription is having problems.

*$PLEX_API [ C:\python\lib\site-packages\plexapi ]:* Location of Python PlexAPI. Needed for Plex library integration.

*$PLEX_CREDENTIALS [ plex ]:* Credential ID for a configured Plex server. Note that this can be a username and password, or a direct server access token. See the comments in the configuration file.

*$PLEX_GUEST_SERIES [ ]:* Media titles that will trigger a watch and purge on media consumption. Can be fine tuned in the *PlexGuests.ps1* subscription file.

*$PLEX_GUESTS [ guest1@plex.tv guest2@plex.tv ]:* Plex library guest users that will trigger a watch and purge on media consumption. Can be fine tuned in the *PlexGuests.ps1* subscription file.

*$PLEX_INTEGRATION [ True ]:* When set to $true, toggles library integration with Plex if configured. When configured, consumed state is synchronized.

*$PLEX_SERVERNAME [ ]:* Name of a configured Plex server.

*$POLLING_SCRIPTS:* Registry of scripts and what regularity they should be executed. Note that this can be edited to remove scripts you do not want to execute, as well as add scripts to extend functionality.

*$PURGE_PATH [ C:\Sample\Media\ToPurge ]:* Root folder location to transfer consumed or discarded media that has expired. This folder is regularly culled with the oldest files being transferred to the recycle bin.

*$PURGE_PATTERNS [ *.txt thumbs.db *.nfo *.png *.sfv *.jpg ]:* As media is organized and empty folders are purged, this setting keeps track of files that are deleted when doing so. This is useful for getting rid of files such as TXT and NFO that come with much downloaded media.

*$PURGE_TOPICAL_AGE_DAYS [ 30 ]:* Age threshold of media in archive folders that is culled to the folder specified in $PURGE_PATH, and threshold that media in $PURGE_PATH is culled into the recycle bin.

*$PYTHON_BINARY [ C:\python\python.exe ]:* Location of Python. Needed if YouTube-DL is not a standalone executable and for Plex integration.

*$RECORDING_HOSTS [ Azureus.exe comskip.exe ffmpeg.exe python.exe youtube-dl.exe yt-dlp.exe MediaMallServer.exe RECORDINGSERVER - LOCALHOST\administrator ]:* These are executables that trigger that a given media is being recorded, utilized by handle.exe.  You can also specify SMB access information in the format "CLIENTMACHINE - SERVER\UserName".

*$RENAME_TEMPLATE [ {SeriesName} - {SeasonAndEpisode} - {EpisodeTitle} ]:* The template that is expanded to derive a new name from the data within if the file is not mapped as a topical show. Valid expansions are {SeriesName}, {AiredDate}, {SeasonAndEpisode}, {EpisodeTitle}.

*$SHOW_ERROR_EXCEPTIONS [ True ]:* Advanced diagnostic. Displays full error exceptions to console while executing.

*$STARTUP_VERBOSE_ALIASES [ False ]:* When starting a new session, show additional alias options available to execute.

*$TASK_SCHEDULING [ False ]:* Advanced diagnostic. Causes polling script to output more information about which script will run next at what time.

*$TOPICAL_RENAME_TEMPLATE [ {SeriesName} - {SeasonAndEpisode} - {AiredDate} - {EpisodeTitle} ]:* The template that is expanded to derive a new name from the data within if the file is mapped as a topical show. Valid expansions are {SeriesName}, {AiredDate}, {SeasonAndEpisode}, {EpisodeTitle}.

*$TOPICAL_SHOWS:* List of topical show names. Media that matches these names have their older shows archived and eventually purged and are directly moved to $PURGE_PATH when watched.

*$TRACK_AGE_HOURS [ 4 ]:* Time after media flagged as watched has their watched mappings applied.

*$USE_TVDB [ True ]:* When renaming media, use information from TheTVDB.

*$WATCHING_HOSTS [ wmplayer.exe vlc.exe System Plex Media Server.exe KODISERVER - LOCALHOST\mediaaccount ]:* These are executables that trigger that a given media is being watched by handle.exe. Note that "System" can specify any file that is accessed via a remote computer via a shared network folder. You can also specify SMB access information in the format "CLIENTMACHINE - SERVER\UserName".

*$YOUTUBE_DL_BINARY [ C:\tools\yt-dlp ]:* Location of the YouTube-DL tool. Used to acquire media content from the sources specified in the subscription files.


## Extensions ##
----------------

When you configure CinAdmin to poll and monitor for media changes ($POLLING_SCRIPTS), you specify a collection of references and the frequency they are executed by *Start-MediaSession.ps1 -Poll*. For example:

	$POLLING_SCRIPTS =  @{
			($SCRIPT_PATH + "\Audit-MediaLibraries.ps1")    =  "5.00:00:00";
			($SCRIPT_PATH + "\Check-MediaSubscription.ps1") =    "04:00:00";
			($SCRIPT_PATH + "\Convert-Media.ps1")           =    "00:02:00";
			($SCRIPT_PATH + "\Copy-MediaAudio.ps1")         =    "01:00:00";
			($SCRIPT_PATH + "\Get-MediaSubtitles.ps1")      =    "01:00:00";
			($SCRIPT_PATH + "\Notify-MediaSeason.ps1")      =  "7.00:00:00";
			($SCRIPT_PATH + "\Organize-Media.ps1")          =    "01:00:00";
			($SCRIPT_PATH + "\Remove-MediaAds.ps1")         =    "01:00:00";
			($SCRIPT_PATH + "\Track-Media.ps1")             =    "00:10:00";

These entries can be added or removed to tweak how the tool works for you. By default, extension scripts executed along these poll scripts are stored in a subfolder called "Extensions". Extension scripts use existing libraries and functions to leverage logging and other functionality. Note also that some of these scripts are commented out of the default polling scripts configuration so do not execute by default and may require additional configuration if enabled.

Extensions are particularly useful if you need to do unique processing for a specific show, product, service, or set of circumstances. Here are a few examples of ones I have created so far that are included:

1. Periodically, leverage different APIs to make sure that PlayOn, Plex, and Kodi representations of the library folders are up to date.

2. Incomplete partial PlayOn recordings, if detected, are automatically deleted and re-queued for recording by leveraging a tool called PlayPass (https://github.com/CodePenguin/PlayPass)

3. The script that refreshes client libraries and their watch states (Kodi, Plex) is also currently listed as an extension script.

You can easily think of additional examples. For example, if you prefer to be notified for specific conditions via email or some other mechanism, you can leverage those easily with PowerShell today and hooked into these scripts. So, if you enjoy writing automation and are familiar with Windows PowerShell, give it a try.


## Subscriptions ##
-------------------

CinAdmin supports the concept of content subscriptions. Not to be confused with subscriptions that are exposed in other products like PlayOn, these subscriptions allow you to define configurations that let you check sites and save video and audio using a tool like YouTube-DL or by directly downloading files like podcasts via RSS feeds.

Here's an overview how it works.

There are subscription configuration files stored under CinAdmin's "Subscriptions" folder. Periodically, a script called *Check-MediaSubscription.ps1* checks that folder for those files that are "pollable" and considers how to use the information within to acquire content. First, *defaults.ps1* is loaded and sets the basic configuration. Then when loading the subscription, it can override and extend that configuration, which stores common configuration useful by source or network or website (for example, generic information is stored for ABC shows in *ABC.ps1*). Finally, the configuration in the Subscription file itself in turn overrides and extends that configuration.

The script then uses that information to parse the URI of the subscription and uses a set of rules (regular expression "extractors") to find new episodes of content. Each subscription remembers past content it saw by storing each as a CSV file under the $DATA_PATH folder.

When a new episode URI is found, the content is extracted via the *Acquire-Media.ps1* script, which also leverages configuration stored in the subscription. It is first stored in a temporary location, a bit of renaming and post processing is done, then it is moved to $DOWNLOADED_TV_PATH or $DOWNLOADED_PODCASTS_PATH to be with your other content that's ready to consume. There it can be processed similarly to your other files as far as how it is organized, renamed, has its ads removed, and how it is tracked when you consume it.


### A simple subscription example: The Big Bang Theory (CBS) ###
----------------------------------------------------------------

The Big Bang Theory's file is not set as poll-able because the series has ended, but for the purpose of this example, let's pretend that it is still an active series. The file also sets the proper URI to one that exposes the JSON used to populate the show webpage (see *TheBigBangTheory.ps1*). Note that you could (and normally will) hardcode the full URI for a series, but in this case the subscription uses a helper function to get the latest season number.

	###########
	# GENERAL #
	###########
	. $SUBSCRIPTION_INSTALL_PATH\CBS.ps1
	$SUBSCRIPTION_NAME = "The Big Bang Theory"
	$SUBSCRIPTION_POLLABLE = $false

	#####################
	# ACQUIRE / EXTRACT #
	#####################

	################
	# CHECK / SCAN #
	################
	$SUBSCRIPTION_URI = "https://www.cbs.com/shows/big_bang_theory/xhr/episodes/page/0/size/12/xs/0/season/" + (GetLatestShowSeason $SUBSCRIPTION_NAME) + "/"

The rest of the configuration to be specified by *CBS.ps1*, which in turn loads from *defaults.ps1*. If you want to see documentation on what each of these options do, you can consult the informational subscription *Sample.ps1.info*.

Here's *CBS.ps1*.

	###########
	# GENERAL #
	###########
	. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1

	#####################
	# ACQUIRE / EXTRACT #
	#####################

	################
	# CHECK / SCAN #
	################
	$SUBSCRIPTION_EPISODE_BEGIN = '"type":"Full Episode"'

	$SUBSCRIPTION_EXTRACTORS = 	@(
						'"url":\s*"(?<href>[^"]+)"',
						'"title":\s*"(?<title>[^"]+)"'
					)

	$SUBSCRIPTION_ACQUIRE_ACTION =	{
						$href = $href.Replace("\/", "/");
						$href = "https://www.cbs.com" + $href;
						$EPISODE_CONTENT_RENAME_TO = $title;
						$EPISODE_CONTENT_URI = $href;
					}

Here's what happens. When *Check-MediaSubscription.ps1* loads *TheBigBangTheory.ps1*, assuming it was set to poll-able, it first loads the configuration. It grabs the content from the URI and splits it up into more manageable lines. It then skips every line that does not match the $SUBSCRIPTION_EPISODE_BEGIN pattern. When it finds that pattern it will then attempt to extract the variables defined in the $SUBSCRIPTION_EXTRACTORS (href, title), using regular expressions. Each time a repeat variable is extracted (for example, the second href) the subscription attempts to acquire the episode with what it has so far by executing the block in $SUBSCRIPTION_ACQUIRE_ACTION, then executing *Get-Media.ps1*. Once that content is acquired, it's marked in the subscription's CSV file so it's not acquired again, and the scripts do other steps that might be specified in the subscription, like renaming the file, stitching together segments into a full episode, and embedding subtitles and metadata.

From that point forward, that content is a part of your library and other scripts will also operate on it when they execute, such as *Organize-Media.ps1*.


### A more advanced example: Saturday Night Live (NBC) ###
----------------------------------------------------------

*SNL.ps1* contains:

	#########
	#GENERAL
	#########
	. $SUBSCRIPTION_INSTALL_PATH\NBC.ps1
	$SUBSCRIPTION_NAME = "Saturday Night Live"
	$SUBSCRIPTION_POLLABLE = $true

	#####################
	# ACQUIRE / EXTRACT #
	#####################

	################
	# CHECK / SCAN #
	################
	$SUBSCRIPTION_URI = "https://www.nbc.com/saturday-night-live"

And *NBC.ps1* contains:

	#########
	#GENERAL
	#########
	. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
	$SUBSCRIPTION_POLLABLE = $false

	#####################
	# ACQUIRE / EXTRACT #
	#####################
	$ACQUISITION_SUBTITLES_DAYS = 0

	################
	# CHECK / SCAN #
	################
	$SUBSCRIPTION_EPISODE_BEGIN = '"component":"VideoTile"'
	$SUBSCRIPTION_EPISODE_END = '"component":"(Slide|FeatureTile|Shelf)"'

	$SUBSCRIPTION_EXTRACTORS = 	@{
						'"episodeNumber":"(?<episodeNumber>\d+)"' = { $episodeNumber -eq $null };
						'"seasonNumber":"(?<seasonNumber>\d+)"' = { $seasonNumber -eq $null };
						'"secondaryTitle":"?(?<title>([^"]+)|(null))"?,' = $null;
						'"locked":(?<locked>true|false)' = { $locked -eq $null };
						'"programmingType":"(?<programmingType>[^"]+)"' = { $programmingType -eq $null };
						'"airDate":"(?<airDate>[^"]+)"' = $null;
						'"(permalink|destination)":"(?<href>[^"]+)"' = { $href -eq $null };
					}

	$SUBSCRIPTION_ACQUIRE_ACTION =	{
						$EPISODE_EXCLUSION = (($title -eq 'null') -or ($locked -eq 'true') -or ($programmingType -ne "Full Episode"));
						$EPISODE_CONTENT_URI = $href;
						$EPISODE_CONTENT_RENAME_TO = (FormatAirDate $airdate) + " - " + $title;
					}

Here you should notice that the extractors are not stored in a list, but in a two-dimensional dictionary instead --- this contains the extractors as keys and the conditions for those extractors as values. This is helpful when you need fine control in how extractor variables are set and utilizes a three-step process for evaluation.

After a line in the URI matches $SUBSCRIPTION_EPISODE_BEGIN, each of the extractors are first tested to see if they match the line. If it does, the extractor condition, the block stored in the value of the dictionary row, is executed. This can set variables that will be persisted for further extractors, or it can simply be set to $null which means to do nothing. If the block returns $null or $true, the extractor stored in the key is then executed and its variables are also extracted. If it is set to $false, that extractor is skipped. If the block sets the variable $EXTRACTOR_MATCH_OVERRIDE to another regular expression object, those matches are used instead of those in the extractor.

In the case for Saturday Night Live, the embedded JSON in the webpage for each episode has duplicate fields that the subscription needs to extract, so the subscription uses extractor conditions to only match and extract the first one per episode if each hasn't been set already.

As with the previous example, when a duplicate extractor variable is detected, each episode is acquired after executing the $SUBSCRIPTION_ACQUIRE_ACTION block of code. Here, the subscription has a set of conditions that signal that the content should be skipped and not downloaded ($EPISODE_EXCLUSION).

Again, please consult *Sample.ps1.info* for further explanations on what options you can use in subscriptions.


### A few additional notes regarding subscriptions ###
------------------------------------------------------

1. As with any of the CinAdmin scripts, these files can be edited and tweaked to meet your needs. Compared to other files in the project, subscription files are more susceptible to break as the sites they target shift beneath them from time to time. It is my hope that the framework is flexible enough to handle most changes that are needed.

2. As should be apparent from the descriptions, having a basic understanding of PowerShell syntax and .NET regular expressions are both essential when making changes to subscription files or creating new ones.

3. A subscription may also "chain" into other subscriptions by returning a redirect URL (via $EPISODE_REDIRECT_URI) instead of a content URL ($EPISODE_CONTENT_URI) and setting $SUBSCRIPTION_REDIRECT to point to another, most likely non-pollable, subscription file. When you do this, the process repeats with that file. This is useful for cases where you need to do multiple steps on multiple sites before being able to find episode information.

4. You have the option to specify multiple URIs in a subscription as a array or dictionary. You can also inherit and build fields from base subscriptions when specifying $SUBSCRIPTION_ACUIRE_ACTION. For example, if A subscription file defines:

		. $SUBSCRIPTION_INSTALL_PATH\base.ps1
		$SUBSCRIPTION_ACQUIRE_ACTION =	@(	{
								$title = $title.Replace("My Show Title", "");
							},
							$SUBSCRIPTION_ACQUIRE_ACTION
						)

And *base.ps1* defines:

	$SUBSCRIPTION_ACQUIRE_ACTION =	{
						$EPISODE_CONTENT_URI = $href;
						$EPISODE_CONTENT_RENAME_TO = (FormatAirDate $airdate) + " - " + $title;
					}

Then the resulting action block is the following, and you'll need to take care that there aren't unintended side effects:

		$SUBSCRIPTION_ACQUIRE_ACTION =	{
							$title = $title.Replace("My Show Title", "");
							$EPISODE_CONTENT_URI = $href;
							$EPISODE_CONTENT_RENAME_TO = (FormatAirDate $airdate) + " - " + $title;
						}

5. Subscriptions contain additional configuration that can be used to tweak how collections of content are processed by the other CinAdmin scripts. For example, the *HBO.ps1* subscription looks like this:

		. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
		$SUBSCRIPTION_POLLABLE = $false

		$SUBSCRIPTION_NAME = @( "Last Week Tonight with John Oliver",
								"Real Time With Bill Maher",
								"Westworld"
								)

		$ACQUISITION_REMOVE_ADS = $true

		$AD_REMOVE_ADS_BOOKENDED = $true

Here although the script is marked as non-pollable (In my case, the content is acquired via PlayOn, not this CinAdmin subscription), any of the content that matches the show names are marked as requiring processing by ComSkip to remove ads, in a special way that indicates that any ads you see will be bookended, or not in the middle of the content.


### Making your own Subscriptions ###
-------------------------------------

If you wish to attempt to create your own subscriptions, try these basic steps:

1. Make sure that you are relatively comfortable with PowerShell scripting conventions, basic .NET syntax, and regular expressions.

2. Make sure that YouTube-DL (or preferably, YT-DLP) supports getting content from the site of your show, or that your content can be directly downloaded (not streamed). You can list all the extractors supported by YouTube-DL by executing:
		
	youtube-dl.exe --list-extractors

3. Copy the sample subscription file (*.\Subscriptions\Sample.ps1.info*) and edit it, looking at the source HTML of your target website as a guide. You might want to base or reference your new subscription on an existing file (for example, leverage *ABC.ps1* for a show that is provided by ABC). To start with, make sure $SUBSCRIPTION_POLLABLE is set to $false so that it is not accidentally picked up by active polling. You can also enable advanced diagnostics by setting $SUBSCRIPTION_DIAGNOSTICS to $true or running with the *-Debug* parameter, which will generate helpful additional log files.

4. Test the subscription manually by running it with the *-Force* parameter (to override that it's not poll-able). You can also use the *-Verbose* and *-Debug* parameters to output more information directly to the console.

	.\Check-MediaSubscription.ps1 -Subscription .\Subscriptions\<YourSubscriptionFile>.ps1 -Force -Debug -Verbose

5. After you get it working, make sure you leverage overlapping configuration by removing it from your file and referencing other subscription files if one already exists, especially *defaults.ps1*.

6. Finally, consider sending me a pull request with your changes. The project license encourages you to pay forward what you develop. If you find that your changes are useful, chances are someone else may feel the same.

Note that it is relatively common for things to change and subscriptions to break as their hosted websites evolve. Currently it is my hope that these files are small and lightweight enough that it allows people to flexibly modify and fix them when that happens.


## Media on the Go ##
---------------------

A big focus on this project has shifted from management to include being more flexible in how you can consume media that are managed by these tools, rather than assume you will always use the same program in the same location each time. Since there are number of different options provided by CinAdmin to consider, it is worth highlighting the differences between them and the use cases each is designed to handle.

More generally, the project has slowly expanded to include audio media as well as video--- in how they are organized, tracked, and post-processed. Specific features have also been added that blend the distinction between the two mediums, allowing subscriptions to automatically clone a copy of a video's audio so you can consume TV as if they were podcasts, which can be useful for talk shows.

*Transfer-Media.ps1 / Track-Media.ps1:* You can make a copy of media you wish to consume on a separate device, like a phone, thumb drive, a SD card, or a MP3 player. While you could copy the files directly, *Transfer-Media.ps1* does attempt to maintain directory ordering (which some car audio systems care about) and allows easier transfer to Android devices. It can also automatically generate playlists for audio files on a per-folder basis. Files that are consumed this way can be marked as watched later via *Track-Media.ps1*, which marks the file the same as if it were consumed on the local network.

*Transfer-Media.ps1* can also transfer media not directly managed by CinAdmin. For example, although CinAdmin doesn't currently organize and normalize digital music albums, I use *Transfer-Media.ps1* on my music libraries anyway.

*Transfer-Media.ps1* also supports the concept of profiles, which allows you to define configuration that allows you to automatically expand parameters to save typing and conveniently persist preferences for different devices and scenarios. For example, in the configuration file you can see:

	$MEDIA_TRANSFER_PROFILES =  @{
		'usb'     =  @{
					'Destination'    = 'D:\USB';
				};
		'phone'   =  @{
					'podcasts:Destination' = 'SD card\Podcasts\'; #EXAMPLE
					'music:Destination'    = 'SD card\Music\'; #EXAMPLE
				};
		'podcasts'=  @{
					'Source'    = 'C:\Sample\Media\Podcasts'; #EXAMPLE
					'Exclude'   = 'C:\Sample\Media\Podcasts\Exclude1\', 'C:\Sample\Media\Podcasts\Exclude2\'; #EXAMPLE
					'Clobber'   = $true
				};
		'music'   =  @{
					'Source'                  = 'C:\Sample\Media\Music\'; #EXAMPLE
					'phone:GeneratePlaylists' = $true
				};
		}

Now, when you execute:

	.\Transfer-Media.ps1 -Profile "usb","podcasts"

It expands to:

	.\Transfer-Media.ps1 -Path 'C:\Sample\Media\Podcasts' -Destination 'G:\' -Exclude 'C:\Sample\Media\Podcasts\Exclude1\','C:\Sample\Media\Podcasts\Exclude2\' -Clobber

Finally, a great option to consuming your media on the go is to leverage Plex. Plex lets you stream your library through several supported apps on several supported devices. When you stream items from your library this way, CinAdmin can automatically detect that media as being consumed without intervention.


## Library Integration ##
-------------------------

CinAdmin works very well with existing media watching clients (such as Plex and Kodi) if you configure it to do so. While initially the project was more focused on presenting your library as a tree of files and folders, best represented as "video" libraries in Plex and Kodi, now library integration has gotten much more rich and full fidelity. Some of CinAdmin's functions may be incompatible with a specific client or cause quirks in how it presents your content to you.

Currently CinAdmin can only synchronize the watch state for movies, videos, and TV with Kodi and Plex --- not music or podcasts.

### Directory Structure ###
---------------------------

Client programs like Kodi and Plex have special considerations for how they see and display media in folders. While CinAdmin provides a lot of flexibility in how they cluster shows in a series (for example, see $EPISODES_AGGREGATE_THRESHOLD), some of those options can cause problems for these clients, which I tried to describe below. In general, your libraries should look something similar to below, with *Organize-Media.ps1* enforcing some of this structure.

CinAdmin allows you to list folders (or patterns) that should be excluded from management and tracking, as well as path overrides to CinAdmin's default folder locations ($DOWNLOADED_PATH_EXCLUSIONS, $ORGANIZE_PATH_OVERRIDE, $CONSUMED_PATH_OVERRIDE).

	$DOWNLOADED_TV_PATH
		|- $DAILY_FOLDER_NAME ("DAILY BRIEFING")
		|- Series 1
		|	|- S01
		|		|- Series 1 - S01E01 - Title 1.mp4
		|		|- ...
		|- $CONSUMED_FOLDER_NAME ("Z Consumed")
	$DOWNLOADED_PODCASTS_PATH
		|- $DAILY_FOLDER_NAME ("DAILY BRIEFING")
		|- Podcast 1 - S01
		|	|- Podcast 1 - S01E01 - Title 1.mp3
		|	|- ...
		|- $CONSUMED_FOLDER_NAME ("Z Consumed")
	$DOWNLOADED_MOVIES_PATH
		|- Movie 1 (Year).mp4
		|- Movie 2 (Year)
		|	|- Movie 2 (Year)_Disc1.iso
		|	|- ...
		|- $CONSUMED_FOLDER_NAME ("Z Consumed")
		|- ISOPNG
		|- Z Converted
	$PURGE_PATH

Note also that CinAdmin supports configuring subdirectories with symbolic links, allowing you fine control in how you manage your storage (see https://blogs.windows.com/windowsdeveloper/2016/12/02/symlinks-windows-10/). For example, for very large series, you can configure them to be on dedicated volumes and to be moved to that same volume when consumed, though they appear to just be a large subdirectory of a library.


### PlayOn ###
--------------

By default, PlayOn is detected as installed and the scripts will already detect when files are being recorded by it. Note also that PlayOn's library viewing application hasn't been supported for some while, so CinAdmin no longer tries to periodically restart the service to keep its library looking up to date.


### Movies ###
--------------

In general, movies are scraped accurately by Plex and Kodi, depending on how you name your files. While CinAdmin does not rename or use databases to rename your files, it does include a routine to help you audit your movie file names, that can help nudge you in getting all of your movies to scrape correctly. Movie files tend to scrape accurately no matter what directory structure you use, so CinAdmin's use of subfolders to track consumed movies works well.

However, you should make sure that either you have a flat directory structure or utilize *.plexignore* files (see https://support.plex.tv/articles/201381883-special-keyword-file-folder-exclusion/) and then explicitly add those directories to your libraries in Plex, the reason being if you touch these directories Plex will rescan these files twice and get confused and at times reset or forget scraped information like manually set thumbnail posters. 

Generally, movie ISO disc images tend to be created with the volume name of the disc, which is frequently not descriptive enough for scraping services to figure out the title. Also, movies that have been remade or reinterpreted benefit greatly from renaming the file with the release date appended for example:

	Robin Hood (2010).iso

Finally, clients like Plex and Kodi support media stacking, where multiple media files are presented as a single title, which is useful for movie and disc sets. But it can also misfire depending on the naming of your files. For example, both Harry Potter and the Deathly Hallows and the Mockingjay Hunger Games movies are multipart, but also have separate theatrical releases.

In almost all cases you can correct mis-scraped movies by renaming your files, or re-integrating the content in your library, or both.


### Episodic Content (TV, Podcasts) ###
---------------------------------------

Things get a little more complicated when making sure your episodic media integrates well with Kodi and Plex. While both originated from the same codebase, now they are very different with noticeable differences and quirks. Episodic video generally works fine as-is for libraries exposed as generic video libraries, but explicitly having them identified as TV Show libraries causes some of the quirks to crop in.

For podcasts, generally both Kodi and Plex are more focused on music and less so with series like podcasts, so making them present well is more about integrating them with their existing artist / album framework. Plex does not appear to want to present files without track information consistently, so CinAdmin will set them in metadata for you, even for topical content that has no formal episode number, instead deriving a unique number from the aired date. To reduce the confusion of identical folder names being interpreted as the same album, seasoned folders have their series name prepended (i.e., Serial S02) and is stamped as the album name in metadata. This is also helpful when transferring media to other devices that present to you a single folder name when navigating, like a MP3 player or car.

For episodic video, or TV, both Kodi and Plex seem to have problems with CinAdmin's directory structure. Both prefer media to be in their own folders and not off the root, so the setting $EPISODES_AGGREGATE_THRESHOLD should be set as 0 or 1 in this case. Both also work best with only one directory level set per show, so CinAdmin's preference of moving files to different subfolders when consumed requires a bit of additional work to integrate well with Kodi and Plex. Kodi allows you to set subfolder-specific settings. The consumed subfolder should have the setting "Selected folder contains a single TV show" disabled.

For Plex, you can get around this by setting up Plex Ignore files (.plexIgnore) that can specify subfolders to ignore (see: https://support.plex.tv/articles/201381883-special-keyword-file-folder-exclusion/). CinAdmin will automatically configure one for you if you configured $PLEX_INTEGRATION to $true. You can then add both the root and the consumed folder subfolder explicitly either as separate libraries in Plex or the same library with multiple folders.


### Kodi ###
------------

For Kodi, to enable integration ($KODI_INTEGRATION), you first need to enable JSON-RPC control as outlined here: https://kodi.wiki/view/JSON-RPC_API (under "Enabling_JSON-RPC"). Once you do so, you can configure CinAdmin to connect to one or more Kodi server endpoints via $KODI_SERVERS. This is a dictionary with each RPC endpoint configured as the key, and the name of the credential (stored via *Store-MediaCredential.ps1*) as the value.

	$KODI_SERVERS = @{
						'http://kodiserver1:8080/jsonrpc' = 'kodi';
						'http://kodiserver2:8080/jsonrpc' = 'kodi';
					}

It is recommended that you share out your libraries as SMB network shares, with CinAdmin accessing them locally. When you access those files, CinAdmin will be able to detect via *handle.exe* that a given title is being consumed.

You can additionally configure $KODI_FOLDER_TRANSLATE to help CinAdmin translate from your Kodi library SMB server items to local directory that CinAdmin monitors. This isn't needed to tell if a local file has been opened, because CinAdmin sees files being accessed via SMB shares by the process "System" on Windows, however it is needed to map files that are watched to be marked as watched.

To help configure your Kodi libraries to work well with CinAdmin, consider using the Kodi audit routine in *Audit-PlexLibaries.ps1*.


### Plex ###
------------

Once you configure CinAdmin to better integrate with Plex, it will do its best to synchronize watched and unwatched state, and it should also detect if the main administrator of the server watches new content to track consumption. If you share your library to other users, CinAdmin should notify you of what they are watching but not track that media as being watched. There is a caveat: to signal to CinAdmin that a given library should be scanned to mark media as watched, you need to explicitly add the consumed folder of your libraries in Plex, even if that is redundant.

To better integrate with Plex ($PLEX_INTEGRATION), CinAdmin leverages PlexAPI, a Python-based way of controlling Plex. Once you configure that, you can configure the following options:

	$PLEX_SERVERNAME = "SERVERNAME"
	$PLEX_CREDENTIALS = "plex"

To correctly log into your plex server to read and set configuration, you have the option of explicitly using a username and password in your credentials (which is slower and requires contacting Plex's service every instance), or use a server token instead. To use a token, explicitly set your username as "localhost" when adding your Plex credentials via *Store-MediaCredential.ps1*, and then use your token as your password. To find your Plex server's token, see https://support.plex.tv/articles/204059436-finding-an-authentication-token-x-plex-token/.

Note also that the server name configured in CinAdmin is optional --- if it's not configured, "LOCALHOST" is assumed.

Getting Plex to work well with episodic content has a few additional considerations as detailed above and may require you to create *.plexIgnore* files and explicitly add what would appear to be redundant subfolders to your libraries.

While Plex is very handy, it does not support streaming from every format that is also supported by Kodi. For example, if you use disc images like ISO files, and do not wish to convert them, Plex will not stream them like other programs (see https://support.plex.tv/articles/201426506-why-are-iso-video-ts-and-other-disk-image-formats-not-supported/). To work past this, CinAdmin has a way to do this, albeit in a bit of a clunky fashion. Here's how it works.

Periodically a new script named *Convert-Media.ps1* will scan for ISOs in your library and will create an image file for each one under $ISO_PNG_PATH with the same name. In Plex, you can share out that folder as a photo library. To stream a ISO, you need to first open one of those images in Plex (note that you need to select just one of those files and hit play. If you double click or directly click on the play button for one of them, it will iterate through all the photographs in the library, which you do not want to do).

The script *Convert-Media.ps1* should detect that you are looking at one of those pictures and will automatically convert the title chapters within as a set of MKVs, using MakeMKV (set via $MAKEMKV_BINARY). When it does so, it drops them off into the "Converted" Plex library (a local folder configured as $ISO_CONVERT_PATH) for you to stream. Everything should be in there --- subtitles, extras, additional audio --- everything except for the DVD navigation menus.

CinAdmin should also attempt to mark the original ISO as watched if you stream one of the MKVs.

Note that Plex does not keep the pictures as "opened" in the dashboard perpetually even if open in the app, which is different compared to video and audio --- my tests keep it open for about 3 minutes. So, if you are trying to use this feature and are not seeing the converted streams, keep opening that file and make sure that it is listed as opened in the dashboard. You could hit a timing window where the polling interval of the script missed seeing the opened photo it before it is dropped from the dashboard.

For more specific steps on how to use this feature, consult the PNG file called #_Instructions that should be viewable to you and your Plex users.

Finally, to help get your libraries look better in Plex, there is also a separate Plex audit routine in *Audit-MediaLibraries.ps1* to assist you.


### Exceptions ###
------------------

No matter what you do, some of the content in your libraries, episodic or movies will likely not be matchable and look correctly in Plex and Kodi.

Some of the content may not really correspond to a formalized released content, especially if you periodically grab content through YouTube. For these cases, you move that content into subdirectories and stamp directories as exclusions ($DOWNLOADED_PATH_EXCLUSIONS), which will largely cause CinAdmin to leave them alone.

These subfolders can be explicitly set by Kodi as content type "none" for it not to be scraped, and you can also set the "Exclude Path from library updates" as well. In Plex, you can exclude folders from being scraped through using *.plexIgnore* files and share out your exceptions via a separate "Other Videos" library, which is not scraped as aggressively. When you do this, you'll be able to access this content through a regular video interface, but this content won't be presented as a part of your movie and TV libraries.

Finally, it is recommended that you do not configure $PURGE_PATH in other Kodi and Plex libraries and their paths, as you'll see partially processed temporary content in there that will confuse and create duplicate library entries with your other content. If you really need to, it should be off its own unique root path. 


## Handles ##
---------------

As mentioned before, CinAdmin uses handles and SMB access information to determine if media is currently being recorded or watched/consumed. It also moves those files around, converts them by embedding subtitles and metadata, and writes to log and cache files to keep track of state. 

If you run any of these scripts with *-WhatIf*, generally anything that requires file manipulation is noted and skipped.

Generally, if a file is open or held up in any way, CinAdmin will note that in the logs and move on. In specific circumstances they may wait and try again. 

If you notice that something is holding files and you want to see what, you can configure $EXTRACT_HANDLE_DIAGNOSTICS to $true in the configuration file. This outputs collision information into HandleDiagnostics.log. I find that it is very useful to add exclusions to Windows' Virus and threat protection settings to skip scanning files under CinAdmin's $DATA_PATH and its base install path.


## Regarding YouTube-DL / YT-DLP ##
-------------------------------------

YouTube-DL, while a great open-source project, sometimes lags behind as websites' content shifts and struggles to access the underlying content when it changes. At times, I've been forced to patch the code myself to get specific websites or scenarios to work with specific subscriptions for this project.

Starting late in 2020, the maintainers of YouTube-DL effectively stepped away from the project. I recommend moving to a fork of the project, YT-DLP instead. Not only does it proactively fix issues, it incorporates fixes from other forks and have shown initiative to build new features to evolve the project.


## Licensing ##
---------------

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.


## Known issues and considerations ##
-------------------------------------

A few things to keep in mind when using these scripts:

1. These scripts tend to standardize around, and is best tested with, MP4 videos and MP3 audio files. Files like MKV and WMA and AVI will work for basic features but may experience problems when removing ads or replacing metadata like title and series names and embedding subtitles.

2. In the configuration file, you can configure process names that, if detected as having a media file open, are treated as "watching" versus "recording". One of these can be "System" which indicates that the file is open via sharing or SMB to another program that is reading the file remotely. This is a way of using these scripts for those who have configurations that directly access file shares, be it home theater PCs, different Linux set top boxes, or other devices. However, the scripts are not smart enough to be able to figure out what remote device has the file open, so just be aware that you might get false positives (media incorrectly tracked) if you are accessing these files in different ways with different programs.

3. While running these scripts, particularly *Organize-Media.ps1*, *Remove-MediaAds.ps1* and *Get-Media.ps1*, you may notice that running a script again may not "see" configuration changes like you expect it might. That could be due to caching where the scripts will remember which media it has either renamed, subtitled, or removed ads. Renaming takes a bit of time per file due to the number of patterns it attempts to match as well as retrieval of information from TheTVDB.com, so if you have a large library this can slow things down substantially. After a file is renamed, it is added to $RENAMED_CSV. In the case of COMSKIP, analyzing a file can take up to twice as long as recording it and can become confused if you run it multiple times against the same file, so it is also cached in $AD_REMOVE_CSV. In both cases, you can remove or delete these files, and optionally re-create these files via their special parameters (*-BuildCache*).

4. You might experience temporary hangs when executing these scripts manually while the scheduled task is polling. This is because to reduce concurrency problems, semaphore files are used to coordinate when multiple scripts are being attempted to run at once, and this will serialize them to try to reduce timing problems. Currently CinAdmin does not detect PC shutdowns at all, so it's possible you might need to clear this file from time to time to continue polling (or execute scripts manually that offer the *-ClearSemaphore* parameter).


### Who do I talk to if I have problems with CinAdmin? ###

Please message me via comments or the BitBucket.org inbox.


## History and Versioning ##

**2.4.0 - 03.05.2024** - cURL integration and command execution rewrite. Many fixes.

**2.3.0 - 03.05.2024** - Revamp subtitles, playlists, ad removal, transferring, and many fixes.

**2.2.6 - 08.28.2023** - Handle detection rewrite and consolidation.

**2.2.0 - 08.04.2023** - Performance fixes, many fixes.

**2.1.0 - 12.22.2022** - Customizable acquisitions, many fixes.

**2.0.0 - 09.28.2022** - Better exception handling, performance audit.

**1.9.2 - 03.31.2022** - Better extended character support, auditing, a few fixes

**1.9.0 - 03.18.2022** - WT Preview support, extended character support, guest watching, library sync robustness, polite poll termination, many fixes

**1.8.0 - 11.03.2021** - Plex and Kodi sync improvements, transfer logic improvements, SMB targeting, many fixes.

**1.7.0 - 03.29.2021** - Happy 5th Anniversary, CinAdmin! File semaphore, ISO Conversion for Plex, symlink support, fix setup, deeper Kodi and Plex integration and auditing, many fixes.

**1.2.0 - 09.03.2020** - Bidirectional subscriptions, NBC subscriptions, consolidate cache cleaning, logging, output, many fixes.

**1.0.0 - 08.07.2020** - MEDIAPOLLING is now the CINEMATIC ADMINISTRATOR (CinAdmin). Audit / consolidate scripts, add icons, Remote / PS Core / Python 3 / Windows Terminal support.

**5.3.0 - 05.13.2020** - Robustness, aliases, progress status, continuity auditing, subtitle fit and finish

**5.2.0 - 04.24.2020** - ComSkip tuning, Plex guest auditing, Extensions upgrade, OpenSubtitles.org integration

**5.0.0 - 03.30.2020** - Concurrency management, FFMPEG rewrite, thumbnail / subtitle support, Transfer-Media profiles, Kodi / Plex library integration

**4.4.0 - 11.05.2019** - Subscription and acquisition overrides, Transfer-Media v2, Daily designations, many fixes

**4.0.0 - 03.04.2019** - Audio fit and finish, Cloud synchronization, many, many fixes

**3.8.0 - 11.13.2018** - Subscriptions v2, Audio file support, many fixes

**3.2.1 - 06.22.2018** - Misc. fixes

**3.2.0 - 05.19.2018** - YouTube-DL integration, subscriptions, many fixes

**2.7.0 - 02.12.2018** - ComSkip integration, logfile script, many misc. fixes and refinements

**2.2.0 - 09.11.2017** - PlayOn failed subscription notifications, published extensions, misc. fixes

**2.0.2 - 03.29.2017** - TV vs. Movie configuration naming, better handle.exe file normalization, version tracking

**2.0.1 - 03.28.2017** - Miscellaneous fixes

**2.0.0 - 03.24.2017** - Script extensions, flexible execution model, TV and movie separate handling

**1.7.0 - 01.09.2017** - Implemented renamed cache, performance improvements

**1.6.0 - 05.23.2016** - New season checking, via TVDB, remove PlayPass

**1.5.0 - 05.05.2016** - TVDB and intelligent rename support

**1.0.0 - 03.29.2016** - Initial public release
