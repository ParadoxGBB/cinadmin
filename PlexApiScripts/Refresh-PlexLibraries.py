#!/usr/bin/env python
# coding: utf-8

import sys, os, getopt, datetime

#DEFAULTS
verbose = False
plexUserName = None
plexPassword = None

scriptDescription = 'Refresh-PlexLibraries.py: Rescan specified Plex library.'
scriptSyntax = 'Refresh-PlexLibraries.py -w <PlexApiDir> -s <serverName> -u <username> -p <password> [-v] [-h]'

try:
	opts, args = getopt.getopt(sys.argv[1:],"vhw:u:p:s:",["wdir=","username=","password=","server="])
except getopt.GetoptError:
	print('INVALID ARGUMENTS: ' + scriptSyntax)
	sys.exit(2)

for opt, arg in opts:
	if opt == '-h':
		print(scriptDescription)
		print(scriptSyntax)
		sys.exit()
	elif opt in ("-p", "--password"):
		plexPassword = arg
	elif opt in ("-s", "--server"):
		plexServerName = arg
	elif opt in ("-u", "--username"):
		plexUserName = arg
	elif opt in ("-w", "--wdir"):
		plexApiWd = arg
	elif opt in ("-v", "--verbose"):
		verbose = True

if verbose:
	def verboseprint(*args):
		output = ""
		for arg in args:
			output = output + str(arg)
		print(output)
else:
	# do-nothing function
	verboseprint = lambda *a: None

verboseprint('ARGUMENT:plexApiWd = ', plexApiWd)
verboseprint('ARGUMENT:Verbose = ', verbose)
verboseprint('ARGUMENT:plexServerName = ', plexServerName)

if plexApiWd != None:
	sys.path.append(plexApiWd)

from plexapi.myplex import MyPlexAccount
from plexapi.library import LibrarySection
from plexapi.server import PlexServer

if plexServerName == None or plexServerName.lower() == 'localhost':
	verboseprint("Using token")
	try:
		plexServer:PlexServer = PlexServer()
	except:
		raise Exception('Could not connect to Plex via token!')
else:
	verboseprint("Using UN PW")
	try:
		if plexUserName == None and plexPassword == None:
			account:MyPlexAccount = PlexServer.myPlexAccount()
		else:
			account:MyPlexAccount = MyPlexAccount(plexUserName, plexPassword)
		plexServer:PlexServer = account.resource(plexServerName).connect()
	except:
		raise Exception('Could not connect to Plex via credentials!')

currentSection: LibrarySection
for currentSection in plexServer.library.sections():
	if currentSection.refreshing == True:
		continue

	verboseprint('UPDATING LIBRARY SECTION: [', currentSection.title,'] [', currentSection.updatedAt,'] [', currentSection.refreshing,']')
	currentSection.update()