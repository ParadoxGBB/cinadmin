#!/usr/bin/env python
# coding: utf-8

import sys, os, getopt
import sys, os, getopt, datetime
from datetime import timedelta

#DEFAULTS
verbose = False
plexUserName = None
plexPassword = None
override = False

scriptDescription = 'Mark-PlexMediaWatched.py: Mark a Plex Library as Watched.'
scriptSyntax = 'Mark-PlexMediaWatched.py -w <PlexApiDir> -s <serverName> -u <username> -p <password> -t <targetFolder> [-o] [-v] [-h]'

try:
	opts, args = getopt.getopt(sys.argv[1:],"vhow:u:p:s:t:",["wdir=","username=","password=","server=","target="])
except getopt.GetoptError:
	print('INVALID ARGUMENTS: ' + scriptSyntax)
	sys.exit(2)

for opt, arg in opts:
	if opt == '-h':
		print(scriptDescription)
		print(scriptSyntax)
		sys.exit()
	elif opt in ("-t", "--target"):
		plexTargetFolder = arg
	elif opt in ("-p", "--password"):
		plexPassword = arg
	elif opt in ("-s", "--server"):
		plexServerName = arg
	elif opt in ("-u", "--username"):
		plexUserName = arg
	elif opt in ("-w", "--wdir"):
		plexApiWd = arg
	elif opt in ("-v", "--verbose"):
		verbose = True
	elif opt in ("-o", "--override"):
		override = True

if verbose:
	def verboseprint(*args):
		output = ""
		for arg in args:
			output = output + str(arg)
		print(output)
else:
	# do-nothing function
	verboseprint = lambda *a: None

verboseprint('ARGUMENT:plexApiWd = ', plexApiWd)
verboseprint('ARGUMENT:Verbose = ', verbose)
verboseprint('ARGUMENT:plexServerName = ', plexServerName)
verboseprint('ARGUMENT:plexTargetFolder = ', plexTargetFolder)
verboseprint('ARGUMENT:override = ', override)

if plexApiWd != None:
	sys.path.append(plexApiWd)

from plexapi.myplex import MyPlexAccount
from plexapi.library import LibrarySection, Location
from plexapi.server import PlexServer

if plexServerName == None or plexServerName.lower() == 'localhost':
	verboseprint("Using token")
	try:
		plexServer:PlexServer = PlexServer()
	except:
		raise Exception('Could not connect to Plex via token!')
else:
	verboseprint("Using UN PW")
	try:
		if plexUserName == None and plexPassword == None:
			account:MyPlexAccount = PlexServer.myPlexAccount()
		else:
			account:MyPlexAccount = MyPlexAccount(plexUserName, plexPassword)
		plexServer:PlexServer = account.resource(plexServerName).connect()
	except:
		raise Exception('Could not connect to Plex via credentials!')

cMatchedSections = 0
currentSection: LibrarySection
for currentSection in plexServer.library.sections():
	if override == True:
		useSection = True
	else:
		useSection = False
		currentPath:Location
		for currentPath in currentSection.locations:
			if plexTargetFolder.lower() in currentPath.lower():
				verboseprint('CONSIDERING PLEX LIBRARY SECTION: [',currentSection.title,'] [',currentPath,']')
				useSection = True
				break

	if useSection == False:
		continue
	else:
		cMatchedSections = cMatchedSections + 1

	if override == True:
		unwatchedLibraryItems = currentSection.all()
		verboseprint('CONSIDERING PLEX LIBRARY SECTION: [',currentSection.title,']')
	else:
		if currentSection.type == 'movie':
			unwatchedLibraryItems = currentSection.search(unwatched=True)
		elif currentSection.type == 'video':
			unwatchedLibraryItems = currentSection.search(unwatched=True)
		elif currentSection.type == 'show':
			unwatchedLibraryItems = currentSection.searchEpisodes(**{'episode.unwatched':True})
		else:
			verboseprint('UNSUPPORTED LIBRARY TYPE: [',currentSection.title,'] [',currentSection.type,']')
			continue

	for unwatchedLibraryItemStack in unwatchedLibraryItems:
		if unwatchedLibraryItemStack.type == 'artist' or unwatchedLibraryItemStack.type == 'photo':
			verboseprint('UNSUPPORTED ITEM TYPE: [',unwatchedLibraryItemStack.title,'] [',unwatchedLibraryItemStack.type,']')
			continue

		if unwatchedLibraryItemStack.type == 'show':
			unwatchedLibraryItemCollection = unwatchedLibraryItemStack.episodes()
		else:
			unwatchedLibraryItemCollection = unwatchedLibraryItemStack

		for unwatchedLibraryItem in unwatchedLibraryItemCollection:
			unwatchedItemFullPath = str(unwatchedLibraryItem.media[0].parts[0].file)
			if plexTargetFolder.lower() not in unwatchedItemFullPath.lower():
				verboseprint('PLEX UNWATCHED SKIPPED: [',currentSection.title,'] [',unwatchedItemFullPath,']')
				continue
			if override == True:
				if unwatchedLibraryItem.isPlayed == True:
					if plexServerName == 'localhost':
						print('SET PLEX UNWATCHED: [',currentSection.title,'] [',unwatchedItemFullPath,']')
					else:
						print('SET PLEX UNWATCHED: [',plexServerName,'] [',currentSection.title,'] [',unwatchedItemFullPath,']')

					unwatchedLibraryItem.markUnplayed()
			else:
				if plexServerName == 'localhost':
					print('SET PLEX WATCHED: [',currentSection.title,'] [',unwatchedItemFullPath,']')
				else:
					print('SET PLEX WATCHED: [',plexServerName,'] [',currentSection.title,'] [',unwatchedItemFullPath,']')

				unwatchedLibraryItem.markPlayed()

if cMatchedSections == 0:
	print('NO PLEX LIBRARIES MATCHED FOLDER: [',plexTargetFolder,']')