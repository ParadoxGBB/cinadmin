#!/usr/bin/env python
# coding: utf-8

import sys, os, getopt, io
import sys, os, getopt, datetime
from datetime import timedelta

#DEFAULTS
verbose = False
plexUserName = None
plexPassword = None

scriptDescription = 'Audit-PlexLibraries.py: Output a CSV formatted representation of all media in Plex.'
scriptSyntax = 'Audit-PlexLibraries -w <PlexApiDir> -s <serverName> -u <username> -p <password> -o <outputCsv> [-v] [-h]'

try:
	opts, args = getopt.getopt(sys.argv[1:],"vhw:u:p:s:o:",["wdir=","username=","password=","server=","output="])
except getopt.GetoptError:
	print('INVALID ARGUMENTS: ' + scriptSyntax)
	sys.exit(2)

for opt, arg in opts:
	if opt == '-h':
		print(scriptDescription)
		print(scriptSyntax)
		sys.exit()
	elif opt in ("-o", "--output"):
		outputFileName = arg
	elif opt in ("-p", "--password"):
		plexPassword = arg
	elif opt in ("-s", "--server"):
		plexServerName = arg
	elif opt in ("-u", "--username"):
		plexUserName = arg
	elif opt in ("-w", "--wdir"):
		plexApiWd = arg
	elif opt in ("-v", "--verbose"):
		verbose = True

if verbose:
	def verboseprint(*args):
		output = ""
		for arg in args:
			output = output + str(arg)
		print(output)
else:
	# do-nothing function
	verboseprint = lambda *a: None

verboseprint('ARGUMENT:plexApiWd = ', plexApiWd)
verboseprint('ARGUMENT:Verbose = ', verbose)
verboseprint('ARGUMENT:plexServerName = ', plexServerName)
verboseprint('ARGUMENT:outputFileName = ', outputFileName)

if plexApiWd != None:
	sys.path.append(plexApiWd)

from plexapi.myplex import MyPlexAccount
from plexapi.server import PlexServer
from plexapi.library import LibrarySection

if plexServerName == None or plexServerName.lower() == 'localhost':
	verboseprint("Using token")
	try:
		plexServer:PlexServer = PlexServer()
	except:
		raise Exception('Could not connect to Plex via token!')
else:
	verboseprint("Using UN PW")
	try:
		if plexUserName == None and plexPassword == None:
			account:MyPlexAccount = PlexServer.myPlexAccount()
		else:
			account:MyPlexAccount = MyPlexAccount(plexUserName, plexPassword)
		plexServer:PlexServer = account.resource(plexServerName).connect()
	except:
		raise Exception('Could not connect to Plex via credentials!')

writeHeader = True
if os.path.isfile(outputFileName):
	print("Refreshing: " + str(outputFileName))
	os.remove(outputFileName)

f= io.open(outputFileName,"a+", encoding="utf8")

if writeHeader:
	outputLine = '"DateTime",' + '"LibraryTitle",' + '"Type",' + '"MediaIndex",' + '"PartIndex",' + '"Watched",' + '"FullPath"'
	outputLine = str(outputLine)
	f.write(outputLine + "\n")

currentSection: LibrarySection
for currentSection in plexServer.library.sections():
	verboseprint('CONSIDERING PLEX LIBRARY SECTION: [',currentSection.title,']')

	for currentLibraryItem in currentSection.all():
		if currentLibraryItem.type == 'movie':
			currentLibraryItemCollection = currentLibraryItem
		elif currentLibraryItem.type == 'video':
			currentLibraryItemCollection = currentLibraryItem
		elif currentLibraryItem.type == 'show':
			currentLibraryItemCollection = currentLibraryItem.episodes()
		elif currentLibraryItem.type == 'artist':
			currentLibraryItemCollection = currentLibraryItem.tracks()
		else:
			verboseprint('UNSUPPORTED LIBRARY ITEM: [',currentLibraryItem.title,'] [',currentLibraryItem.type,']')
			continue

		for currentLibraryItemCollectionItem in currentLibraryItemCollection:
			for mediaIndex in range(len(currentLibraryItemCollectionItem.media)):
				for partIndex in range(len(currentLibraryItemCollectionItem.media[mediaIndex].parts)):
					libraryItemFullPath = currentLibraryItemCollectionItem.media[mediaIndex].parts[partIndex].file
					if currentLibraryItemCollectionItem.type == 'track':
						watchedState = ""
					else:
						watchedState = str(currentLibraryItemCollectionItem.isPlayed)
					outputLine = '"' + str(datetime.datetime.now()) + '","' + currentSection.title + '","' + currentLibraryItemCollectionItem.type + '","' + str(mediaIndex) + '","' + str(partIndex) + '","' + watchedState + '","' + libraryItemFullPath + '"'
					#print(outputLine)
					outputLine = str(outputLine)
					f.write(outputLine + "\n")

f.close()
