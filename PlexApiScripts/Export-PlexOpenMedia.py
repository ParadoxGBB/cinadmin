#!/usr/bin/env python
# coding: utf-8

import sys, os, getopt, io
import sys, os, getopt, datetime
from datetime import timedelta

#DEFAULTS
verbose = False
plexUserName = None
plexPassword = None

scriptDescription = 'Export-PlexOpenMedia.py: Output a CSV formatted representation of opened media in Plex.'
scriptSyntax = 'Export-PlexOpenMedia.py -w <PlexApiDir> -s <serverName> -u <username> -p <password> -o <outputCsv> [-v] [-h]'

try:
	opts, args = getopt.getopt(sys.argv[1:],"vhw:u:p:s:o:",["wdir=","username=","password=","server=","output="])
except getopt.GetoptError:
	print('INVALID ARGUMENTS: ' + scriptSyntax)
	sys.exit(2)

for opt, arg in opts:
	if opt == '-h':
		print(scriptDescription)
		print(scriptSyntax)
		sys.exit()
	elif opt in ("-o", "--output"):
		outputFileName = arg
	elif opt in ("-p", "--password"):
		plexPassword = arg
	elif opt in ("-s", "--server"):
		plexServerName = arg
	elif opt in ("-u", "--username"):
		plexUserName = arg
	elif opt in ("-w", "--wdir"):
		plexApiWd = arg
	elif opt in ("-v", "--verbose"):
		verbose = True

if verbose:
	def verboseprint(*args):
		output = ""
		for arg in args:
			output = output + str(arg)
		print(output)
else:
	# do-nothing function
	verboseprint = lambda *a: None

verboseprint('ARGUMENT:plexApiWd = ', plexApiWd)
verboseprint('ARGUMENT:Verbose = ', verbose)
verboseprint('ARGUMENT:plexServerName = ', plexServerName)
verboseprint('ARGUMENT:outputFileName = ', outputFileName)

if plexApiWd != None:
	sys.path.append(plexApiWd)

from plexapi.myplex import MyPlexAccount
from plexapi.server import PlexServer
from plexapi.media import Media,MediaPart
from plexapi.client import PlexClient
from plexapi.base import Playable

if plexServerName == None or plexServerName.lower() == 'localhost':
	verboseprint("Using token")
	try:
		plexServer:PlexServer = PlexServer()
		account:MyPlexAccount = plexServer.myPlexAccount()
	except:
		raise Exception('Could not connect to Plex via token!')
else:
	verboseprint("Using UN PW")
	try:
		if plexUserName == None and plexPassword == None:
			account:MyPlexAccount = PlexServer.myPlexAccount()
		else:
			account:MyPlexAccount = MyPlexAccount(plexUserName, plexPassword)
		plexServer:PlexServer = account.resource(plexServerName).connect()
	except:
		raise Exception('Could not connect to Plex via credentials!')

dateTime = str(datetime.datetime.now())
writeHeader = True
if os.path.isfile(outputFileName):
	lastRefreshed = datetime.datetime.fromtimestamp(os.path.getmtime(outputFileName))
	verboseprint("Last Refreshed: " + str(lastRefreshed))
	writeHeader = False

f= io.open(outputFileName,"a+", encoding="utf8")

if writeHeader:
	outputLine = '"DateTime",' + '"SessionName",' + '"SessionUserName",' + '"UserNameIsPlexAdmin",' + '"SessionOpenMediaFile",' + '"SessionClientName",' + '"SessionClientIdentifier",' + '"SessionClientState"'
	outputLine = str(outputLine)
	f.write(outputLine + "\n")

currentSession:Playable
for currentSession in plexServer.sessions():
	if currentSession == None:
		continue
	elif currentSession.listType != 'photo':
		verboseprint("Found session: " + str(currentSession) + " " + currentSession.listType + ", isFullObject=" + str(currentSession.isFullObject()))
		sessionName = str(currentSession.session).strip("[]\'")
		sessionUserName = str(currentSession.usernames).strip("[]\'")
		currentClient:PlexClient
		for currentClient in currentSession.players:
			sessionClientName = str(currentClient.title)
			sessionClientIdentifier = str(currentClient.machineIdentifier)
			sessionClientState = str(currentClient.state)
	else:
		sessionName = str(currentSession.title)
		sessionUserName = 'None'
		sessionClientName = 'None'
		sessionClientIdentifier = 'None'
		sessionClientState = 'None'
	# For some reason, remote video sessions have their media part filenames stripped unless you refetch the session.
	currentSession = currentSession.source()
	currentMedia:Media
	for currentMedia in currentSession.media:
		sessionOpenMediaFile = None
		verboseprint("Found media: " + str(currentMedia))
		currentMediaPart:MediaPart
		for currentMediaPart in currentMedia.parts:
			sessionOpenMediaFile = str(currentMediaPart.file)
			verboseprint("Found media part: " + str(currentMediaPart) + " " + sessionOpenMediaFile)
			if sessionOpenMediaFile != None and sessionOpenMediaFile != 'None':
				break
		if sessionOpenMediaFile != None and sessionOpenMediaFile != 'None':
			break

	if sessionOpenMediaFile == None or sessionOpenMediaFile == 'None':
		raise Exception('Could not match session media to file name!')

	userNameIsPlexAdmin = str( (str(account.username) == sessionUserName) or (str(account.email) == sessionUserName) )
	verboseprint("userNameIsPlexAdmin: " + userNameIsPlexAdmin)

	outputLine = '"' + dateTime + '","' + sessionName + '","' + sessionUserName + '","' + userNameIsPlexAdmin + '","' + sessionOpenMediaFile + '","' + sessionClientName + '","' + sessionClientIdentifier + '","' + sessionClientState + '"'
	verboseprint(outputLine)
	outputLine = str(outputLine)
	f.write(outputLine + "\n")

f.close()
