[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [switch] $BuildCache,
    [switch] $CleanCache,
    [switch] $Force = $false,
    [Parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true, Position=0)]
    [Alias('FullName')]
    [String[]] $Path
)

#########
#
# MAIN
#
#########

Begin
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1
	trap { ReleaseScriptConcurrency; break }

	LockScriptConcurrency

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

	CleanAdRemoveCache

	#Prepare work
	if($Force)
	{
		$currentAdRemovedMedia = @{}
	}
	else
	{
		$currentAdRemovedMedia = LoadAdRemovedMedia
	}

	if($Path.Count -eq 0)
	{
		$Path = $DOWNLOADED_TV_PATH
	}

	#Cull old logs
	if((IsValidPath $AD_REMOVE_LOGS) -and ($AD_REMOVE_MAX_LOG_AGE -gt 0))
	{
		$rightNow = Get-Date

		foreach($currentLogFile in (Get-ChildItem -LiteralPath $AD_REMOVE_LOGS -File))
		{
			$difference = ($rightNow - ($currentLogFile.CreationTime.Date))
			if($difference.TotalDays -ge $AD_REMOVE_MAX_LOG_AGE)
			{
				DeleteItem ($currentLogFile.FullName)
			}
		}
	}

}
Process
{
	try
	{
		if($Help)
		{
			return
		}

		if(IsInvalidPath $COMSKIP_BINARY)
		{
			LogError "ComSkip is either not installed or not properly configured. Please fix."
			return
		}

		if(IsInvalidPath $FFMPEG_BINARY)
		{
			LogError "FFMpeg is either not installed or not properly configured. Please fix."
			return
		}

		CleanAdRemoveCache $CleanCache
		if($CleanCache)
		{
			return
		}
		else
		{
			foreach($Target in $Path)
			{
				$Target = LoadPath $Target
				if(IsInvalidPath $Target) { continue }

				$files =  Get-ChildItem -LiteralPath $Target -Recurse -FollowSymlink -File
				$cCurrentProgress = 0
				foreach($currentOpenedFile in $files)
				{
					$currentOpenedFile = Get-Item -LiteralPath (TranslatePathToSymLink ($currentOpenedFile.FullName))
					$cCurrentProgress++

					if(-not $Force)
					{
						if($currentOpenedFile.Extension -NotIn $MEDIA_AD_REMOVE_EXTENSIONS)
						{
							VerboseLogInfo EXCLUDED FILE EXTENSION: [($currentOpenedFile.FullName)]
							continue
						}

						if(IsExcludedPath $currentOpenedFile.FullName)
						{
							VerboseLogInfo EXCLUDED DIR: [($currentOpenedFile.FullName)]
							continue
						}

						if(IsWatched $currentOpenedFile.FullName)
						{
							VerboseLogInfo EXCLUDED CONSUMED: [($currentOpenedFile.FullName)]
							continue
						}

						if(IsMovie $currentOpenedFile.FullName)
						{
							VerboseLogInfo EXCLUDED MOVIE: [($currentOpenedFile.FullName)]
							continue
						}
					}

					if($BuildCache)
					{
						OutputProgressBar ("Building Cache [$Target]...") ($cCurrentProgress/$files.Count)
						TrackAdRemovedMedia ($currentOpenedFile.FullName)
					}
					else
					{
						OutputProgressBar ("Removing Ads [$Target]...") ($cCurrentProgress/$files.Count)

						if(-not $Force)
						{
							if($currentAdRemovedMedia.ContainsKey([System.IO.Path]::GetFileNameWithoutExtension($currentOpenedFile.FullName)))
							{
								VerboseLogInfo FILE ALREADY PROCESSED: [($currentOpenedFile.FullName)]
								continue
							}

							$subscriptionOverride = LoadSubscriptionOverrideVariable $currentOpenedFile "ACQUISITION_REMOVE_ADS"
							if(($subscriptionOverride -ne $null) -and (-not $subscriptionOverride))
							{
								VerboseLogInfo EXCLUDED VIA SUBSCRIPTION: [($currentOpenedFile.FullName)]
								continue
							}

							if(-not (CanMediaHaveAdsRemoved ($currentOpenedFile.FullName)))
							{
								VerboseLogInfo FILE CANNOT HAVE ADS REMOVED: [($currentOpenedFile.FullName)]
								continue
							}
						}

						$segments = Get-ChildItem -LiteralPath $CONVERSION_TMP -Filter ($currentOpenedFile.Name + ".S*" + $currentOpenedFile.Extension) -Recurse -File -ErrorAction SilentlyContinue
						if($segments.Count -gt 0)
						{
							LogWarning Partial Segments detected for file. Skipping... [($currentOpenedFile.FullName)]
							continue
						}

						TrackAdRemovedMedia ($currentOpenedFile.FullName)
						RemoveMediaAds $currentOpenedFile
					}
				}
			}
		}
	}
	catch
	{
		ReleaseScriptConcurrency
		throw
	}
}
End
{
	ReleaseScriptConcurrency
	PurgeEmptyDirectories $CONVERSION_TMP $true
	PauseIfClicked
}