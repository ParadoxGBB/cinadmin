[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [switch] $AlbumDirectory,
    [switch] $BuildCache,
    [switch] $CleanCache,
    [switch] $ClearSemaphore,
    [switch] $Force = $false,
    [Parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true, Position=0)]
    [Alias('FullName')]
    [String[]] $Path
)

#########
#
# MAIN
#
#########

Begin
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1
	trap { ReleaseScriptConcurrency; break }

	if($ClearSemaphore)
	{
		RemoveSemaphore
		exit
	}
	LockScriptConcurrency

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

	if($Force)
	{
		#Handled Inside Functions
	}

	CleanRenamedCache $CleanCache
	if($CleanCache)
	{
		return
	}

	$directoriesToOrganize = New-Object -TypeName System.Collections.ArrayList

	$PassedInPath = $false
	if($Path.Count -eq 0)
	{
		if(IsValidPath $DOWNLOADED_TV_PATH)
		{
			$directoriesToOrganize.Add($DOWNLOADED_TV_PATH) > $null
		}

		if(IsValidPath $DOWNLOADED_PODCASTS_PATH)
		{
			$directoriesToOrganize.Add($DOWNLOADED_PODCASTS_PATH) > $null
		}

		if($directoriesToOrganize.Count -eq 0)
		{
			LogError No valid paths to organize.
			return
		}
		else
		{
			$Path = $directoriesToOrganize.ToArray()
		}
	}
	else
	{
		$directoriesToOrganize = $Path
		$PassedInPath = $true
	}
}
Process
{
	try
	{
		if($Help)
		{
			return
		}

		if($CleanCache)
		{
			return
		}

		if($BuildCache -and (-not (UseTvDB)))
		{
			LogError Scripts are not configured correctly for TVDB lookup.
			return
		}

		$seenSeries = @{}
		foreach($Target in $Path)
		{
			$Target = LoadPath $Target
			if(IsInvalidPath $Target)
			{
				if($Force)
				{
					LogWarning File or path not found: [$Target]
				}
				continue
			}

			if($BuildCache)
			{
				$files =  Get-ChildItem -LiteralPath $Target -Recurse -FollowSymlink -File
				$cCurrentProgress = 0
				foreach($currentOpenedFile in $files)
				{
					$currentOpenedFile = Get-Item -LiteralPath (TranslatePathToSymLink ($currentOpenedFile.FullName))
					$cCurrentProgress++

					if($currentOpenedFile.Extension -NotIn ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
					{
						LogWarning EXCLUDED FILE EXTENSION: [($currentOpenedFile.FullName)]
						continue
					}

					if(IsExcludedPath $currentOpenedFile.FullName)
					{
						VerboseLogInfo EXCLUDED DIR: [($currentOpenedFile.FullName)]
						continue
					}

					if(IsUnderPath $currentOpenedFile.FullName $PURGE_PATH)
					{
						VerboseLogInfo EXCLUDED PURGE: [($currentOpenedFile.FullName)]
						continue
					}

					OutputProgressBar ("Building Cache...") ($cCurrentProgress/$files.Count)

					$tvDbId = SearchTvDbForSeries $currentOpenedFile $true
					if($tvDbId -eq 0)
					{
						VerboseLogInfo TVDB SERIES MARKED SKIPPED: [$currentOpenedFile] [$tvDbId]
					}
					else
					{
						VerboseLogInfo FOUND TVDB SERIES: [$currentOpenedFile] [$tvDbId]

						$tvDbId = $tvDbId.ToString().Trim()
						if(-not $seenSeries.ContainsKey($tvDbId))
						{
							$seenSeries.Add($tvDbId, $currentOpenedFile)
						}
					}
				}
			}
			elseif($AlbumDirectory)
			{
				foreach($Target in $Path)
				{
					OrganizeMediaAlbumDirectory $Target
				}
			}
			else
			{
				$canClean = $false
				if((Get-Item -LiteralPath $Target).Attributes -Match "Directory")
				{
					$canClean = $true
				}

				# PROCESS NEW OR DOWNLOADED MEDIA
				VerboseLogInfo Organizing downloaded media: [$Target]...
				OrganizeDownloadedMedia $Target
			}
		}
	}
	catch
	{
		ReleaseScriptConcurrency
		throw
	}
}
End
{
	try
	{
		if($Help)
		{
			return
		}

		if($BuildCache)
		{
			LogInfo Found [($seenSeries.Count)] series.

			$tvDbCache = LoadTvDBDictionary
			foreach($tvDbCacheEntry in $tvDbCache.Keys)
			{
				if($seenSeries.ContainsKey(($tvDbCache[$tvDbCacheEntry]).ToString()))
				{
					continue
				}

				if(($tvDbCache[$tvDbCacheEntry]) -eq 0)
				{
					LogInfo Media that matches [($tvDbCacheEntry)] is ignored and bypasses TVDB lookup.
					continue
				}

				LogWarning There was no media found that matched the following TVDB series cache: [$tvDbCacheEntry] [($tvDbCache[$tvDbCacheEntry])]
			}
		}
		else
		{
			# PROCESS DETECTED WATCHED MEDIA
			VerboseLogInfo Organizing consumed media...
			OrganizeWatchedEpisodicMedia

			if(-not $PassedInPath)
			{
				VerboseLogInfo Culling media...

				if((-not $Force) -and (IsValidPath $LAST_CULLED))
				{
					$lastCulled = (Get-Item -LiteralPath $LAST_CULLED).LastWriteTime
					if( ((Get-Date) - $lastCulled).TotalHours -le 12 )
					{
						VerboseLogInfo Skipping culling expired media. Last detected: [$lastCulled]
						return
					}
				}

				#SOFT DELETE / TRIM PURGED DIRECTORY
				CullPurgeDirectory

				#PURGE OLD DAILY / Topical / Archived MEDIA
				CullExpiredMedia

				if(IsValidPath $LAST_CULLED)
				{
					DeleteItem $LAST_CULLED
				}
				"Culled" | SafeOutFile -LiteralPath $LAST_CULLED
			}
		}
	}
	finally
	{
		# REMOVE EMPTY FOLDERS
		if($canClean -or $PassedInPath)
		{
			foreach($currentDirectoryToOrganize in $directoriesToOrganize)
			{
				PurgeEmptyDirectories $currentDirectoryToOrganize $false
			}
		}
		PurgeEmptyDirectories $CONVERSION_TMP $true

		ReleaseScriptConcurrency
		PauseIfClicked
	}
}