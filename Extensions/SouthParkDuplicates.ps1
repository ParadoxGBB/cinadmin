[CmdletBinding(SupportsShouldProcess=$true)]
param()

try
{
	$SCRIPT_PATH = Split-Path -parent $PSScriptRoot
	. $SCRIPT_PATH\CinAdmin.Functions.ps1

	LockScriptConcurrency

	#########
	#
	# MAIN
	#
	#########

	$spPath = $DOWNLOADED_TV_PATH + "\South Park"

	$episodes = Get-ChildItem -Recurse -LiteralPath $spPath -File
	foreach($currentEpisode in $episodes)
	{
		if($currentEpisode.Extension -NotIn $MEDIA_EXTENSIONS)
		{
			continue
		}

		if($currentEpisode.Name.Contains(".0."))
		{
			LogInfo Removing SP Dupe: [($currentEpisode.FullName)]
			PurgeItem $currentEpisode.FullName
			continue
		}

		$spWatchedPath = $spPath.Replace($DOWNLOADED_TV_PATH, (DeriveWatchedPath $DOWNLOADED_TV_PATH))
		if(IsValidPath $spWatchedPath)
		{
			$watchedEpisodes = Get-ChildItem -Recurse -LiteralPath $spWatchedPath -Filter ($currentEpisode.BaseName + "*")
			foreach($currentWatchedEpisode in $watchedEpisodes)
			{
				if($currentWatchedEpisode.Extension -In $MEDIA_EXTENSIONS)
				{
					LogInfo PURGING REDUNDANT SP EPISODE: [($currentWatchedEpisode.FullName)]
					PurgeItem ($currentWatchedEpisode.FullName)
				}
			}
		}
	}
}
finally
{
	ReleaseScriptConcurrency
}