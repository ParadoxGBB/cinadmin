[CmdletBinding(SupportsShouldProcess=$true)]
param()

#####################################
#
# GeneratePlayPassFile
#
#####################################
function GeneratePlayPassFile($browsePath, $EpisodeName)
{
	$playPassFile = $DATA_PATH +"\RecordEpisode.cfg"

	if(IsValidPath $playPassFile)
	{
		PurgeItem $playPassFile
	}

	$browseTree = $browsePath.Split("|")

	'<playpass>' | Out-File -LiteralPath $playPassFile -Append
	'	<settings>' | Out-File -LiteralPath $playPassFile -Append
	'		<queuelist settings="Provider=FileQueueList;Data Source=' + $EXT_PLAYPASS_SKIPS + '" />' | Out-File -LiteralPath $playPassFile -Append
	'	</settings>' | Out-File -LiteralPath $playPassFile -Append
	'	<passes>' | Out-File -LiteralPath $playPassFile -Append
	'		<pass enabled="1" description="ReRecord">' | Out-File -LiteralPath $playPassFile -Append

	foreach($currentBrowsePath in $browseTree)
	{
		if($currentBrowsePath.Contains("Search results for"))
		{
			LogWarning "Can't re-record: [" ($browseTree) "]"
			exit
		}

		$currentBrowsePath = $currentBrowsePath.Trim()
		$currentBrowsePath = [System.Security.SecurityElement]::Escape($currentBrowsePath)
		'			<scan name="' + $currentBrowsePath + '">' | Out-File -LiteralPath $playPassFile -Append
	}

	$EpisodeName = [System.Security.SecurityElement]::Escape($EpisodeName)
	'				<queue name="' + $EpisodeName + '" />' | Out-File -LiteralPath $playPassFile -Append

	for($i=0; $i -lt $browseTree.Count; $i++)
	{
		'			</scan>' | Out-File -LiteralPath $playPassFile -Append
	}

	'		</pass>' | Out-File -LiteralPath $playPassFile -Append
	'	</passes>' | Out-File -LiteralPath $playPassFile -Append
	'</playpass>' | Out-File -LiteralPath $playPassFile -Append

	return $playPassFile
}

try
{
	$SCRIPT_PATH = Split-Path -parent $PSScriptRoot
	. $SCRIPT_PATH\CinAdmin.Functions.ps1

	LockScriptConcurrency

	#########
	#
	# MAIN
	#
	#########

	if(-not (test-path $EXT_PLAYPASS_EXE))
	{
		LogError '$EXT_PLAYPASS_SKIPS' needs to be configured.
		return
	}

	if(-not (test-path $EXT_PLAYPASS_EXE))
	{
		LogError '$EXT_PLAYPASS_SKIPS' needs to be configured.
		return
	}

	DisplayPlayOnSubscriptionProblems
	if(-not (test-path $RECORDING_PROBLEMS_CSV))
	{
		return
	}

	$problemCsv = Import-Csv -LiteralPath $RECORDING_PROBLEMS_CSV
	foreach($csvEntry in $problemCsv)
	{
		$series = $csvEntry.Series
		$browsePath = $csvEntry.BrowsePath
		$EpisodeName = $csvEntry.EpisodeName
		$status = $csvEntry.Status

		if($status -eq 3)
		{
			LogInfo RECORDING: [SHOW: $series] [EPISODE: $EpisodeName]

			$recordingConfig = GeneratePlayPassFile $browsePath $EpisodeName
			LogInfo CREATED: [$recordingConfig]
			ExecuteCommand $EXT_PLAYPASS_EXE "-queue" $recordingConfig
		}
	}
}
finally
{
	ReleaseScriptConcurrency
}