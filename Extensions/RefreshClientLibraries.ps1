[CmdletBinding(SupportsShouldProcess=$true)]
param(
     [switch] $Force
)

try
{
	$SCRIPT_PATH = Split-Path -parent $PSScriptRoot
	. $SCRIPT_PATH\CinAdmin.Functions.ps1

	LockScriptConcurrency

	#########
	#
	# MAIN
	#
	#########

	if($PLEX_INTEGRATION)
	{
		Write-Progress -Activity "Updating Plex Watched Media..." -PercentComplete 20
		MarkPlexMediaWatched

		#Detecting how often a library in Plex has last been refreshed is unreliable via PlexAPI.
		# For now, fine tune the refresh intervals in the library settings itself instead.
		#Write-Progress -Activity "Refreshing Plex..." -PercentComplete 50
		#RefreshPlex

		#Make sure consumed folder doesn't mess up parent directory's TV library
		$tvConsumedFolder = DeriveWatchedPath $DOWNLOADED_TV_PATH
		if(IsValidPath $tvConsumedFolder)
		{
			$ignoreFile = (Get-Item -LiteralPath $tvConsumedFolder).Parent.FullName + "\.plexIgnore"
			$targetPattern = "*" +(Get-Item -LiteralPath $tvConsumedFolder).Name + "/*"
			$patternFound = $false
			if(IsValidPath $ignoreFile)
			{
				foreach($currentLine in (Get-Content $ignoreFile))
				{
					if($currentLine -eq $targetPattern)
					{
						$patternFound = $true
						break
					}
				}
			}

			if(-not $patternFound)
			{
				LogInfo Adding Plex Ignore Entry: [$ignoreFile] [$targetPattern]
				"`n" | SafeOutFile -LiteralPath $ignoreFile
				$targetPattern | SafeOutFile -LiteralPath $ignoreFile
			}
		}
	}

	if($KODI_INTEGRATION)
	{
		Write-Progress -Activity "Refreshing Kodi Library..." -PercentComplete 40
		#Note that cleaning removes database items but it's very expensive / prompts in the client. Here a simple scan will add new items, but not remove stale ones.
		RefreshKodi

		Write-Progress -Activity "Removing Missing Kodi Media..." -PercentComplete 60
		RemoveMissingKodiItems

		Write-Progress -Activity "Updating Kodi Consumed Media..." -PercentComplete 80
		MarkKodiMediaWatched

		Write-Progress -Activity "Updating Kodi Unconsumed Media..." -PercentComplete 100
		MarkKodiMediaUnwatched
	}
}
finally
{
	ReleaseScriptConcurrency
}