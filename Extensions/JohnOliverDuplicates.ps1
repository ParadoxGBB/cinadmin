[CmdletBinding(SupportsShouldProcess=$true)]
param()

try
{
	$SCRIPT_PATH = Split-Path -parent $PSScriptRoot
	. $SCRIPT_PATH\CinAdmin.Functions.ps1

	LockScriptConcurrency

	#########
	#
	# MAIN
	#
	#########

	$joPath = $DOWNLOADED_TV_PATH + "\Last Week Tonight with John Oliver"

	if(IsInvalidPath $joPath)
	{
		return
	}

	$seenDates = New-Object -TypeName System.Collections.ArrayList
	$episodes = Get-ChildItem -Recurse -LiteralPath $joPath -File
	foreach($currentEpisode in $episodes)
	{
		if($currentEpisode.Extension -NotIn $MEDIA_EXTENSIONS)
		{
			continue
		}

		if($currentEpisode.Name.Contains(".0."))
		{
			LogInfo Removing JO Dupe: [($currentEpisode.FullName)]
			PurgeItem $currentEpisode.FullName
			continue
		}

		$results = ExtractShowAirDate ($currentEpisode.FullName)
		$airedDate = $results[0]
		$airedDateIsParsed = $results[1]

		if(($airedDate -eq $null) -and (-not $airedDateIsParsed))
		{
			continue
		}
		$airedDate = FormatAirDate $airedDate

		#LogInfo Found [($currentEpisode.FullName)] [($airedDate)]
		if(-not ($seenDates.Contains($airedDate)))
		{
			$seenDates.Add($airedDate) > $null
		}
		else
		{
			LogInfo Removing JO Date Dupe: [($currentEpisode.FullName)] [($airedDate)]
			PurgeItem $currentEpisode.FullName
			continue
		}
	}
}
finally
{
	ReleaseScriptConcurrency
}