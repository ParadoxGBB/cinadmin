[CmdletBinding(SupportsShouldProcess=$true)]
param()

try
{
	$SCRIPT_PATH = Split-Path -parent $PSScriptRoot
	. $SCRIPT_PATH\CinAdmin.Functions.ps1

	LockScriptConcurrency

	if(-not (IsPlayOnInstalled))
	{
		return
	}

	$currentVersion = (Get-Item -LiteralPath "${env:ProgramFiles(x86)}\MediaMall\PlayOn.exe").VersionInfo.ProductVersion

	if(($Global:PLAYON_VERSION -ne $null) -and ($currentVersion -ne $Global:PLAYON_VERSION))
	{
		$notificationTitle = "New PlayOn Version Detected"
		$notificationText = "A new version of PlayOn is now installed: `n`n" + ($Global:PLAYON_VERSION) + " --> " + $currentVersion
		DisplayNotification $notificationTitle $notificationText
		FlashMessage 'Green' $notificationText
	}
	$Global:PLAYON_VERSION = $currentVersion
}
finally
{
	ReleaseScriptConcurrency
}