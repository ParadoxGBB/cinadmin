[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [switch] $RebuildCsv
)

try
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1

	LockScriptConcurrency

	#########
	#
	# MAIN
	#
	#########

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

	if(IsInvalidPath $TVDB_SERIES_CSV)
	{
		LogError "Could not find TVDB series file [$TVDB_SERIES_CSV]. Please make sure you've executed Organize-Media.ps1 with the -BuildCache parameter."
		PauseIfClicked
		return
	}

	if($RebuildCsv)
	{
		DeleteItem $CURRENT_SEASONS_CSV
	}

	CheckForNewSeasons
}
finally
{
	ReleaseScriptConcurrency
	PauseIfClicked
}