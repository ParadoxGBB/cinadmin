[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Clicked,
    [switch] $Help,
    [switch] $Visible,
    [switch] $SkipConfiguration,
    [switch] $ConfigureSslRemoting,
    [switch] $SkipTask,
    [switch] $Remote,
    [Parameter(Position=0)]
    [string] $Target
)

$SCRIPT_PATH = Split-Path -parent $PSCommandPath
if(-not (Test-Path -LiteralPath $SCRIPT_PATH\CinAdmin.Functions.ps1))
{
	Write-Error ("Library not found: [" + $SCRIPT_PATH + "\CinAdmin.Functions.ps1]")
	return
}
$initalVariableState = Get-Variable -Scope 0
. $SCRIPT_PATH\CinAdmin.Functions.ps1

#########
#
# MAIN
#
#########

if($Help)
{
	ShowSyntax
	PauseIfClicked
	return
}
ShowConfiguration

if($Clicked)
{
	#Handled Inside Functions
}

#TARGET IS REQUIRED FOR LOCAL INSTALLS
if((-not $ConfigureSslRemoting) -and (-not $Remote) -and (($Target -eq $null) -or ($Target.Length -eq 0)))
{
	$providedTarget = PromptGetUserString "Please specify the install target folder location" ($SCRIPT_PATH + "\" + $PROJECT_NAME)
	$providedTarget = $providedTarget.Trim()
	$Target = $providedTarget
}

if((-not $Remote) -and (-not (HasAdministratorRights)))
{
	LogError This script needs to be executed with administrator privileges.
	PauseIfClicked
	return
}

if(IsRemoteSession)
{
	LogError This script needs to be executed in a local, not remote, session.
	PauseIfClicked
	return
}

if($ConfigureSslRemoting)
{
	ConfigureRemoteSSL
	return
}

if($Target.EndsWith("\"))
{
	$Target = $Target.TrimEnd("\");
}

if($Remote)
{
	#//START REMOTE INSTALL
	if(-not (Test-Path -LiteralPath $LOG_OUTPUT))
	{
		$logOutputPath = [System.IO.Path]::GetDirectoryName($LOG_OUTPUT)
		if(-not (Test-Path -LiteralPath $logOutputPath))
		{
			mkdir -Path $logOutputPath -Force -ErrorAction 'SilentlyContinue'
			if(-not (Test-Path -LiteralPath $logOutputPath))
			{
				$LOG_OUTPUT = ".\data\" + $PROJECT_NAME + ".Install.log"
				Write-Warning "Log location not accessible. Setting to: [$LOG_OUTPUT]"
			}
		}
	}

	if(IsInvalidPath ($env:ProgramFiles + "\PowerShell\7\pwsh.exe"))
	{
		LogError This script needs PowerShell Core installed to install.
		PauseIfClicked
		return
	}

	if(-not (GetUserResponse "Would you like to configure this device to allow Remote PowerShell access to your previous CinAdmin install?" $true))
	{
		LogWarning Aborting...
		PauseIfClicked
		return
	}

	$defaultRemoteUserName = GetCurrentUserName
	$defaultRemoteHost = $env:USERDOMAIN
	$defaultRemoteInstallPath = $SCRIPT_PATH

	if(IsInvalidPath $INSTALL_INFORMATION)
	{
		if(IsValidPath $Target)
		{
			if((Get-ChildItem "F:\GREG\OneDrive\tools\CinAdmin\installinfo.txt" -File).Count -eq 1)
			{
				LogInfo Loaded
				$INSTALL_INFORMATION = $Target
			}
			elseif((Get-ChildItem "F:\GREG\OneDrive\tools\CinAdmin\installinfo.txt" -File).Count -gt 1)
			{
				$possibleInstallInfo = $Target + "\installinfo.txt"
				if(IsValidPath $possibleInstallInfo)
				{
					$INSTALL_INFORMATION = $possibleInstallInfo
				}
			}
		}
	}

	if(IsValidPath $INSTALL_INFORMATION)
	{
		foreach($currentLine in (Get-Content -LiteralPath $INSTALL_INFORMATION))
		{
			$splitString = $currentLine.Split("=")
			if(($splitString[0]).Trim() -eq "UserName")
			{
				$defaultRemoteUserName = ($splitString[1]).Trim()
			}
			if(($splitString[0]).Trim() -eq "Server")
			{
				$defaultRemoteHost = ($splitString[1]).Trim()
			}
			if(($splitString[0]).Trim() -eq "InstallPath")
			{
				$defaultRemoteInstallPath = ($splitString[1]).Trim()
			}
		}
	}

	$remoteUserName = PromptGetUserString "Please specify the remote username" ($defaultRemoteUserName)
	$remoteUserName = $remoteUserName.Trim()

	$remoteHost = PromptGetUserString "Please specify the remote host target" ($defaultRemoteHost)
	$remoteHost = $remoteHost.Trim()

	$remoteInstallPath = PromptGetUserString ("Please specify the full path that " + $PROJECT_NAME + " is installed on the remote host") ($defaultRemoteInstallPath)
	$remoteInstallPath = $remoteInstallPath.Trim()

	if(-not (IsStringDefined $Target))
	{
		$Target = $env:USERPROFILE + "\" + $PROJECT_NAME
	}
	$targetFullPath = "%USERPROFILE%\" + $PROJECT_NAME

	foreach($currentFile in @($PSC_ICON, $STARTUP_SCRIPT))
	{
		$targetFile = [System.IO.Path]::Combine($Target, [System.IO.Path]::GetFileName($currentFile))
		LogInfo INSTALL: [$currentFile] --> [$targetFile]
		CreateDirInPath $targetFile
		Copy-Item -LiteralPath $currentFile -Destination $targetFile -Force
	}

	$splat = @(	$REMOTE_PS_SHORTCUT.Replace($SHORTCUT_PATH, $Target),
				($env:ProgramFiles + "\PowerShell\7\pwsh.exe"),
				$targetFullPath,
				([String]::Format('-NoExit -Command "{0}\{1} -RemoteUserName {2} -RemoteHost {3} -RemoteInstallPath {4}"', $targetFullPath, [System.IO.Path]::GetFileName($STARTUP_SCRIPT), $remoteUserName, $remoteHost, $remoteInstallPath)),
				$PSC_ICON.Replace($SHORTCUT_PATH, $targetFullPath),
				$false )
	CreateShortcut @splat

	if(IsWTInstalled)
	{
		$splat = @(	('Remote ' + $PROJECT_NAME + ' Session'),
					"{4fa41658-d23b-4d9a-af4b-645a21706774}",
					([String]::Format('{0} -NoExit -NoLogo -Command {1}\{2} -RemoteUserName {3} -RemoteHost {4} -RemoteInstallPath {5}', ($env:ProgramFiles + "\PowerShell\7\pwsh.exe"), $targetFullPath, [System.IO.Path]::GetFileName($STARTUP_SCRIPT), $remoteUserName, $remoteHost, $remoteInstallPath)),
					$targetFullPath,
					$PSC_ICON.Replace($SHORTCUT_PATH, $targetFullPath) )
		CreateWTProfile @splat
	}

	#DONE!
	LogInfo "Finished."
	PauseIfClicked
	return

} #//END REMOTE INSTALL

#
# START LOCAL INSTALL
#

$Error.Clear()

if(IsInvalidPath ($env:ProgramFiles + "\PowerShell\7\pwsh.exe"))
{
	if(-not (GetUserResponse "It appears that you don't have PowerShell Core installed. Are you sure you want to use CinAdmin under Windows PowerShell instead?" $true))
	{
		LogWarning Aborting...
		PauseIfClicked
		return
	}
}

#CREATE DIRECTORY
if(IsInvalidPath $Target)
{
	if(GetUserResponse ("Create target directory [" + $Target + "]?") $true)
	{
		LogInfo "Creating target directory: [$Target]..."
		mkdir -Path $Target > $null
		if(IsInvalidPath $Target)
		{
			LogError Error Creating Install Target Location. Aborted install.
			PauseIfClicked
			return
		}
	}
	else
	{
		LogError Aborted install.
		PauseIfClicked
		return
	}
}

QuitIfError

$targetFullPath = (Get-Item -LiteralPath $Target).FullName

$copyScripts = @(	$ACQUIRE_SCRIPT,
			$AUDIT_SCRIPT,
			$AUDIO_EXTRACT_SCRIPT,
			$CHECK_SUBSCRIPTION_SCRIPT,
			$CONVERT_ISO_SCRIPT,
			$CREDENTIAL_SCRIPT,
			$FUNCTION_SCRIPT,
			$GENERIC_FUNCTION_SCRIPT,
			$INSTALL_LAUNCHER,
			$INSTALL_SCRIPT,
			$LICENSE_FILE,
			$NOTIFY_SCRIPT,
			$ORGANIZE_SCRIPT,
			$REMOVE_AD_SCRIPT,
			$SHOW_LOGS_SCRIPT,
			$STARTUP_SCRIPT,
			$SUBTITLES_SCRIPT,
			$TRACK_SCRIPT,
			$TRANSFER_SCRIPT
		)

$pollScripts = @(	$AUDIO_EXTRACT_SCRIPT,
			$CHECK_SUBSCRIPTION_SCRIPT,
			$CONVERT_ISO_SCRIPT,
			$NOTIFY_SCRIPT,
			$ORGANIZE_SCRIPT,
			$REMOVE_AD_SCRIPT,
			$SUBTITLES_SCRIPT,
			$TRACK_SCRIPT
		)

$extensionScripts = @(
			"JohnOliverDuplicates.ps1",
			"PlayOnVersion.ps1",
			"RefreshClientLibraries.ps1",
			"ReRecordPartialRecordings.ps1",
			"SouthParkDuplicates.ps1"
			)

$subscriptionScripts = @(
				"544Days.ps1",
				"AbbottElementary.ps1",
				"ABC.ps1",
				"AETv.ps1",
				"AgainstTheRules.ps1",
				"AllThereIs.ps1",
				"AppleTV.ps1",
				"AmazonPrime.ps1",
				"AskMeAnother.ps1",
				"BBCNews.ps1",
				"Blockbuster.ps1",
				"BoomBustHqTrivia.ps1",
				"BusinessWars.ps1",
				"CatchAndKill.ps1",
				"CBS.ps1",
				"ChernobylPodcast.ps1",
				"ComedyCentral.ps1",
				"Crimetown.ps1",
				"CurseOfOakIsland.ps1",
				"defaults.ps1",
				"Disney.ps1",
				"Edith.ps1",
				"FallOfCivilizations.ps1",
				"FirstRingDaily.ps1",
				"FreakonomicsRadio.ps1",
				"FreshAir.ps1",
				"Frontline.ps1",
				"FullFrontalWithSamanthaBee.ps1",
				"Function.ps1",
				"HallOfShame.ps1",
				"HBO.ps1",
				"HotZone.ps1",
				"HouseMD.ps1",
				"Hulu.ps1",
				"IWasNeverThere.ps1",
				"Invisibilia.ps1",
				"KidsOfRutherfordCounty.ps1",
				"LandOfTheGiants.ps1",
				"LastWeekTonight.ps1",
				"LateNightSethMeyers.ps1",
				"LeahReminiScientologyAndTheAftermath.ps1",
				"LifeinPieces.ps1",
				"MLBRecaps.ps1",
				"ModernFamily.ps1",
				"MotherCountryRadicals.ps1",
				"MurphyBrown.ps1",
				"NatGeo.ps1",
				"NBC.ps1",
				"Netflix.ps1",
				"Offline.ps1",
				"Olympics.ps1",
				"OnceUponATimeInTheValley.ps1",
				"PennAndTellerFoolUs.ps1",
				"PodSaveAmerica.ps1",
				"RealTimeOvertime.ps1",
				"RecodeDecode.ps1",
				"RevisionistHistory.ps1",
				"RoughTranslation.ps1",
				"RSS.ps1",
				"Rubicon.ps1",
				"S-Town.ps1",
				"Sample.ps1.info",
				"SeattleNow.ps1",
				"Serial.ps1",
				"SethMeyersCloserLook.ps1",
				"SNL.ps1",
				"SouthPark.ps1",
				"TBS.ps1",
				"TheAssemblyLine.ps1",
				"TheBigBangTheory.ps1",
				"TheCW.ps1",
				"TheDailyShow.ps1",
				"TheDropout.ps1",
				"TheImpact.ps1",
				"TheImprovementAssociation.ps1",
				"TheLateShowWithStephenColbert.ps1",
				"TheLateShowPodShow.ps1",
				"TheProblemWithJonStewartPodcast.ps1",
				"TheWilderness.ps1",
				"Throughline.ps1",
				"Ultra.ps1",
				"Uncover.ps1",
				"UpFirst.ps1",
				"ValleyOfTheBoom.ps1",
				"VirtualRuns.ps1",
				"WaitWaitDontTellMe.ps1",
				"WeCrashed.ps1",
				"WeLiveHereNow.ps1",
				"WeekInReview.ps1",
				"WhatADay.ps1",
				"WildThings.ps1",
				"WindOfChange.ps1",
				"YoungSheldon.ps1",
				"YouTube.ps1"
			)

$plexApiScripts = @(
				$PLEX_AUDIT_SCRIPT,
				$PLEX_OPENED_SCRIPT,
				$PLEX_REFRESH_SCRIPT,
				$PLEX_WATCHED_SCRIPT
			)

$icons = @(
			$PSC_ICON,
			$WT_ICON,
			$WPS_ICON,
			$POLL_ICON,
			$ISO_PNG,
			$ISO_INSTRUCTIONS_PNG
			)

#COPY FILES
CopyFileBatch $copyScripts $targetFullPath $null
CopyFileBatch $extensionScripts $targetFullPath $EXTENSION_FOLDER
CopyFileBatch $subscriptionScripts $targetFullPath $SUBSCRIPTION_FOLDER
CopyFileBatch $plexApiScripts $targetFullPath $PLEX_API_FOLDER
CopyFileBatch $icons $targetFullPath $SHORTCUT_FOLDER

$readmeDestination = $README_FILE
$readmeDestination = $readmeDestination.Replace($SCRIPT_PATH, $targetFullPath)
LogInfo INSTALL: "$README_FILE -->  $readmeDestination"
Copy-Item -Path $README_FILE -Destination $readmeDestination -Force

QuitIfError

$psCoreInstalled = $false
if(IsValidPath ($env:ProgramFiles + "\PowerShell\7\pwsh.exe"))
{
	$psCoreInstalled = $true
}

$psWindowsInstalled = $false
if(IsValidPath ($env:SystemRoot + "\System32\WindowsPowerShell\v1.0\powershell.exe"))
{
	$psWindowsInstalled = $true
	if(-not $psCoreInstalled)
	{
		if(-not (GetUserResponse "It appears that PowerShell Core is not installed, which is needed for remoting over SSL. Do you wish to configure PowerShell Rempoting over SSL?" $true))
		{
			LogWarning Aborting...
			PauseIfClicked
			return
		}
	}
}

#CREATE SHORTCUTS

if($psCoreInstalled)
{
	$splat = @(	$PSC_SHORTCUT.Replace($SCRIPT_PATH, $targetFullPath),
				($env:ProgramFiles + "\PowerShell\7\pwsh.exe"),
				$targetFullPath,
				([String]::Format("-NoExit -NoLogo -File {0}",
				$STARTUP_SCRIPT.Replace($SCRIPT_PATH, $targetFullPath))),
				$PSC_ICON.Replace($SCRIPT_PATH, $targetFullPath),
				$true )
	CreateShortcut @splat

	if($configureRemotePs)
	{
		$splat = @(	$REMOTE_PS_SHORTCUT.Replace($SCRIPT_PATH, $targetFullPath),
					($env:ProgramFiles + "\PowerShell\7\pwsh.exe"),
					$targetFullPath,
					([String]::Format('-NoExit -NoLogo -Command "{0} -Remote"', $STARTUP_SCRIPT.Replace($SCRIPT_PATH, $targetFullPath))),
					$PSC_ICON.Replace($SCRIPT_PATH, $targetFullPath),
					$false )
		CreateShortcut @splat
	}
}

if($psWindowsInstalled)
{
	$splat = @(	$WPS_SHORTCUT.Replace($SCRIPT_PATH, $targetFullPath),
				($env:SystemRoot + "\system32\WindowsPowerShell\v1.0\powershell.exe"),
				$targetFullPath,
				([String]::Format("-NoExit -NoLogo -File {0}", $STARTUP_SCRIPT.Replace($SCRIPT_PATH, $targetFullPath))),
				$WPS_ICON.Replace($SCRIPT_PATH, $targetFullPath),
				$true )
	CreateShortcut @splat
}

QuitIfError

$windowsTerminalInstalled = IsWTInstalled
if($windowsTerminalInstalled)
{
	$windowsTerminalIntegrate = $false
	if(GetUserResponse ("It appears that you have Windows Terminal installed. Would you like " + $PROJECT_NAME + " to integrate and add new profiles?") $true)
	{
		$windowsTerminalIntegrate = $true
	}

	if($windowsTerminalIntegrate -and $psWindowsInstalled)
	{

		$splat = @(	($PROJECT_NAME + ' (Windows PowerShell)'),
					"{444601b2-486e-4f16-9f71-5cec9e4bd180}",
					([String]::Format('{0} -NoExit -NoLogo -File {1}', ($env:SystemRoot + "\System32\WindowsPowerShell\v1.0\powershell.exe"), $STARTUP_SCRIPT.Replace($SCRIPT_PATH, $targetFullPath))),
					$targetFullPath,
					$WPS_ICON.Replace($SCRIPT_PATH, $targetFullPath) )
		CreateWTProfile @splat
	}

	if($windowsTerminalIntegrate -and $psCoreInstalled)
	{
		$splat = @(	($PROJECT_NAME + ' (PowerShell Core)'),
					"{39f76c07-af3c-40b3-9337-aadd637df4ce}",
					([String]::Format('{0} -NoExit -NoLogo -File {1}', ($env:ProgramFiles + "\PowerShell\7\pwsh.exe"), $STARTUP_SCRIPT.Replace($SCRIPT_PATH, $targetFullPath))),
					$targetFullPath,
					$PSC_ICON.Replace($SCRIPT_PATH, $targetFullPath) )
		CreateWTProfile @splat
	}

	if($windowsTerminalIntegrate)
	{
		if($psCoreInstalled)
		{
			$wtShortcutProfile = $PROJECT_NAME + ' (PowerShell Core)'
		}
		else
		{
			$wtShortcutProfile = $PROJECT_NAME + ' (Windows PowerShell)'
		}

		$requireAdmin = -not $WINDOWS_TERMINAL_BINARY.Contains("Preview")
		$splat = @(	$WT_SHORTCUT.Replace($SCRIPT_PATH, $targetFullPath),
					$WINDOWS_TERMINAL_BINARY,
					$targetFullPath,
					([String]::Format('-p "{0}"', $wtShortcutProfile)),
					$WT_ICON.Replace($SCRIPT_PATH, $targetFullPath),
					$requireAdmin )
		CreateShortcut @splat
	}
}

QuitIfError

$configureRemotePs = $false
if($psCoreInstalled)
{
	if(GetUserResponse "Do you wish to configure PowerShell Remoting over SSL?" $true)
	{
		$configureRemotePs = $true
	}
}

if($configureRemotePs)
{
	ConfigureRemoteSSL
}

QuitIfError

if(-not $SkipConfiguration)
{
	#COPY CONFIGURATION
	$copyConfig = $true
	$scriptPath = $CONFIG_FILE.Replace($SCRIPT_PATH, $targetFullPath)
	if(IsValidPath $scriptPath)
	{
		if(-not (GetUserResponse "Overwrite Configuration file [$scriptPath]?" $false))
		{
			$copyConfig = $false
		}
	}

	if($copyConfig)
	{
		Copy-Item -LiteralPath $CONFIG_FILE -Destination $targetFullPath -Force
		LogInfo INSTALL: "$CONFIG_FILE -->  $scriptPath"
	}

	QuitIfError

	#CONFIGURE
	$canContinue = $false
	$popUpConfig = $copyConfig
	while(-not $canContinue)
	{
		LogInfo "Configuring..."
		ConfigureCinAdmin $targetFullPath $popUpConfig
		$SCRIPT_PATH = $targetFullPath
		. $FUNCTION_SCRIPT
		ShowConfiguration $true
		ValidateConfiguration
		if(($Error -ne $null) -and ($Error.Count -gt 0))
		{
			$canContinue = -not (GetUserResponse "Do you wish to try to configure the script again?" $true)
			$Error.Clear()
			$popUpConfig = $false
		}
		else
		{
			$canContinue = $true
		}
	}
}

# AUDIT COPIED FILES
AuditInstall $SCRIPT_PATH  $targetFullPath

QuitIfError

# RESET VARIABLE STATE
$newFunctionScript = $FUNCTION_SCRIPT.Replace($SCRIPT_PATH, $targetFullPath)
foreach($currentVariable in (Get-Variable -Scope 0))
{
	if($currentVariable.Name -eq "LOG_OUTPUT") { continue }
	if(-not (IsVariableDefinedInScope $currentVariable.Name $initalVariableState))
	{
		if($currentVariable.Name.ToUpper() -ceq $currentVariable.Name)
		{
			DebugLogInfo Variable Has Been REMOVED: [($currentVariable.Name)]
			Remove-Variable -Name ($currentVariable.Name) -Scope 0
		}
	}
}

$SCRIPT_PATH = $targetFullPath
. $newFunctionScript

if(IsValidPath $INSTALL_INFORMATION)
{
	PermanentlyDeleteItem $INSTALL_INFORMATION
}

("InstallPath=" + $SCRIPT_PATH) | SafeOutFile $INSTALL_INFORMATION
("UserName=") + (GetCurrentUserName) | SafeOutFile $INSTALL_INFORMATION
("Server=") + ($ENV:USERDOMAIN.Trim('\')) | SafeOutFile $INSTALL_INFORMATION
LogInfo INSTALL INFORMATION SAVED: [$INSTALL_INFORMATION]

QuitIfError

$pollScripts = @(	$AUDIO_EXTRACT_SCRIPT,
			$CHECK_SUBSCRIPTION_SCRIPT,
			$NOTIFY_SCRIPT,
			$ORGANIZE_SCRIPT,
			$REMOVE_AD_SCRIPT,
			$SUBTITLES_SCRIPT,
			$TRACK_SCRIPT
		)

VerboseLogInfo Path [$SCRIPT_PATH]
VerboseLogInfo Library [$FUNCTION_SCRIPT]
VerboseLogInfo Config [$CONFIG_FILE]

QuitIfError

#TEST
if($WHATIF_MODE)
{
	$testScripts = $true
}
elseif(GetUserResponse "Do you wish to test each script with the -WhatIf parameter?" $true)
{
	$testScripts = $true
}
else
{
	$testScripts = $false
}

if($testScripts)
{
	foreach($currentScript in $pollScripts)
	{
		$canContinue = $false
		while(-not $canContinue)
		{
			$scriptPath = $currentScript
			LogWarning "Testing [($scriptPath) -WhatIf -Verbose:($VERBOSE_MODE) -Debug:($DEBUG_MODE)]..."

			if($currentScript -match [RegEx]::Escape($ORGANIZE_SCRIPT))
			{
				LogWarning "(Please be patient, this one can take a while.)"
				LogWarning "(For best results, first run this script with the -BuildCache parameter before polling.)"
			}

			if($currentScript -match [RegEx]::Escape($REMOVE_AD_SCRIPT))
			{
				LogWarning "(Please be patient, this one can take a while.)"
				LogWarning "(For best results, first run this script with the -BuildCache parameter before polling.)"
			}

			&($scriptPath) -WhatIf -Verbose:($VERBOSE_MODE) -Debug:($DEBUG_MODE)

			if(($Error -ne $null) -and ($Error.Count -gt 0))
			{
				LogWarning Error Detected: [($Error[0])]
				$canContinue = -not (GetUserResponse "Do you wish to try to test this script again?" $true)
				$Error.Clear()
			}
			else
			{
				$canContinue = $true
			}
		}
	}
}

QuitIfError

if(-not $SkipTask)
{
	$taskInstalled = $false
	$task = Get-ScheduledTask -TaskName $SCHEDULED_TASK_NAME -ErrorAction Ignore
	if($task -ne $null)
	{
		LogInfo Task already present.
		$taskInstalled = $true
	}

	if(IsInvalidPath $POLL_SHORTCUT)
	{
		#CREATE POLLING SHORTCUT
		if($Visible)
		{
			$taskVisible = $true
		}
		elseif(GetUserResponse "Do you wish the scheduled task to be visible when executing?" $true)
		{
			$taskVisible = $true
		}
		else
		{
			$taskVisible = $false
		}

		if($taskVisible)
		{
			$windowStyle = "Minimized"
		}
		else
		{
			$windowStyle = "Hidden"
		}

		if($psCoreInstalled)
		{
			$pollExecutable = $env:ProgramFiles + "\PowerShell\7\pwsh.exe"
		}
		else
		{
			$pollExecutable = $env:SystemRoot + "\System32\WindowsPowerShell\v1.0\powershell.exe"
		}

		LogInfo SHORTCUT [$POLL_SHORTCUT]

		$splat = @(	$POLL_SHORTCUT,
					$pollExecutable,
					$targetFullPath,
					([String]::Format("-WindowStyle {0} -ExecutionPolicy Bypass -NoProfile {1}", $windowStyle, $POLL_COMMAND)),
					$POLL_ICON,
					$true )
		CreateShortcut @splat

		if($taskInstalled)
		{
			LogInfo "Enabling Scheduled Task..."
			StartScheduledTask
		}
		else
		{
			LogInfo "Enabling Scheduled Task..."
			InstallScheduledTask $windowStyle
		}
	}
}

QuitIfError

#DONE!
LogInfo "Finished."

PauseIfClicked