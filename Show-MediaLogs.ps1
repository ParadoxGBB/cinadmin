param(
    [switch] $Help,
    [switch] $ConvertToCsv,
    [switch] $All,
    [switch] $Log,
    [switch] $CommandExecution,
    [switch] $Sessions,
    [switch] $Popup,
    [int] $TailLines = -1,
    [Parameter(Position=0)]
    [string] $Search
)

$SCRIPT_PATH = Split-Path -parent $PSCommandPath
. $SCRIPT_PATH\CinAdmin.Functions.ps1

#########
#
# MAIN
#
#########

if($Help)
{
	ShowSyntax
	PauseIfClicked
	return
}
ShowConfiguration
TestConfiguration

$logFileTargets = New-Object -TypeName System.Collections.ArrayList

if($DEBUG_MODE -or $VERBOSE_MODE)
{
	LogWarning Cannot execute this script in debug or verbose mode.
	return
}

if(-not ($Log -or $CommandExecution -or $Sessions))
{
	#Default
	$Log = $true
}

if($TailLines -eq -1)
{
	#Default
	$TailLines = 50
}

if((IsRemoteSession) -and $Popup)
{
	LogError Cannot execute notepad in a remote session.
	return
}

if((-not $Popup) -and (-not $ConvertToCsv))
{
	if($TailLines -gt 0)
	{
		OutputColoredText 'Yellow' ([String]::Format("[{0}] Showing last [{1}] lines of files.", (Get-Date), $TailLines))
	}
	else
	{
		OutputColoredText 'Yellow' ([String]::Format("[{0}] Showing ALL lines of files.", (Get-Date)))
	}
}
elseif($ConvertToCsv)
{
	if($TailLines -gt 0)
	{
		OutputColoredText 'Yellow' ([String]::Format("[{0}] Converting last [{1}] days from input files.", (Get-Date), $TailLines))
	}
	else
	{
		OutputColoredText 'Yellow' ([String]::Format("[{0}] Converting ALL days from input files.", (Get-Date)))
	}
}

if($Log -and (IsValidPath $LOG_OUTPUT))
{
	$logFileTargets.Add($LOG_OUTPUT) > $null
}

if($CommandExecution -and (IsValidPath $COMMAND_LOG_OUTPUT))
{
	$logFileTargets.Add($COMMAND_LOG_OUTPUT) > $null
}

if($Sessions -and (IsValidPath $SESSION_LOG_OUTPUT))
{
	$logFileTargets.Add($SESSION_LOG_OUTPUT) > $null
}

if($All -and (IsValidPath $LOG_ARCHIVE))
{
	foreach($currentLogFile in (Get-ChildItem -LiteralPath $LOG_ARCHIVE -Filter *.log | Sort-Object LastWriteTime))
	{
		if($currentLogFile.Name.StartsWith("CommandExecution"))
		{
			if($CommandExecution)
			{
				$logFileTargets.Add($currentLogFile.FullName) > $null
			}
		}
		elseif($currentLogFile.Name.StartsWith("Media"))
		{
			if($Log)
			{
				$logFileTargets.Add($currentLogFile.FullName) > $null
			}
		}
		elseif($currentLogFile.Name.StartsWith("Sessions"))
		{
			if($Sessions)
			{
				$logFileTargets.Add($currentLogFile.FullName) > $null
			}
		}
		else
		{
			LogWarning Current log file is not recognized: [($currentLogFile.FullName)]
		}
	}
}

if($ConvertToCsv)
{
	foreach($inputItem in $logFileTargets)
	{
		$inputItemExtension = [System.IO.Path]::GetExtension($inputItem)
		$outputCsv = $inputItem
		if(IsStringDefined $Search)
		{
			$outputCsv = $outputCsv.Replace($inputItemExtension, ("." + $Search + $inputItemExtension))
		}
		$outputCsv = $outputCsv.Replace($inputItemExtension, ".csv")
		if(IsValidPath $outputCsv)
		{
			LogInfo Clearing CSV: [$outputCsv]
			DeleteItem $outputCsv
		}

		$currentLogFile = Get-Item -LiteralPath $inputItem
		if($currentLogFile.Name.StartsWith("CommandExecution"))
		{
			$pattern = "\[\s*(?<timestamp>[^\]]+)\s*\]\s*COMMAND\:\s*\[\s*(?<text>.+)\s*\]\s*\[\s*(?<duration>\d+\:\d+\:\d+(\.\d+)*)\s*\]"
		}
		else
		{
			$pattern = "\[\s*(?<timestamp>[^\]]+)\s*\]\s*(?<text>.*)"
		}

		LogInfo CONVERTING: [$inputItem]
		foreach($currentLine in (Get-Content -LiteralPath $inputItem))
		{
			$currentLine = $currentLine.Trim()
			if($currentLine -NotMatch $pattern)
			{
				DebugLogInfo SKIPPING: [$currentLine]
				continue
			}
			else
			{
				DebugLogInfo CONSIDERING: [$currentLine]
			}

			$timestamp = ($matches["timestamp"]).Trim()
			$text = ($matches["text"]).Trim()
			if($currentLogFile.Name.StartsWith("CommandExecution"))
			{
				$duration = ($matches["duration"]).Trim()
			}

			if($TailLines -gt 0)
			{
				$parsedTimeStamp = [DateTime]::MinValue
				if(-not ([DateTime]::TryParse($timestamp, [ref] $parsedTimeStamp)))
				{
					continue
				}

				if(((Get-Date) - $parsedTimeStamp).TotalDays -gt $TailLines)
				{
					continue
				}
			}

			if(IsStringDefined $Search)
			{
				if($text -NotMatch $Search)
				{
					continue
				}
			}

			if($currentLogFile.Name.StartsWith("CommandExecution"))
			{
				$csvEntry = New-Object -TypeName PSObject -Property @{ "Timestamp" = $timestamp; "Text" = $text; "Duration" = $duration; }
			}
			else
			{
				$csvEntry = New-Object -TypeName PSObject -Property @{ "Timestamp" = $timestamp; "Text" = $text; }
			}
			AddCsvEntry $csvEntry $outputCsv
		}

		if(IsInvalidPath $outputCsv)
		{
			LogWarning NOT FOUND: [$outputCsv]
		}
		else
		{
			LogInfo CONVERTED: [$outputCsv]
		}
	}
}
else
{
	foreach($inputItem in $logFileTargets)
	{
		if(IsStringDefined $Search)
		{
			OutputColoredText 'Blue' [$inputItem]
			if($TailLines -gt 0)
			{
				OutputLinesInColor (Get-Content -LiteralPath $inputItem | Select-String $Search | Select-Object -Last $TailLines)
			}
			else
			{
				OutputLinesInColor (Get-Content -LiteralPath $inputItem | Select-String $Search)
			}
		}
		else
		{
			if($Popup)
			{
				ExecuteCommand notepad $inputItem > $null
			}
			else
			{
				OutputColoredText 'Blue' [$inputItem]
				if($TailLines -gt 0)
				{
					OutputLinesInColor (Get-Content $inputItem -Tail $TailLines)
				}
				else
				{
					OutputLinesInColor (Get-Content $inputItem)
				}
			}
		}
	}
}

PauseIfClicked