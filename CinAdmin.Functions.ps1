##################
#
# Globals
#
##################

#Generic
$PROJECT_NAME = "CinAdmin"
$PROJECT_VERSION = "2.4.0"
$POLLING_START = "Started " + $PROJECT_NAME + " polling."
$POLLING_WIN_TITLE = $PROJECT_NAME + " Poller: "
$SCHEDULED_TASK_NAME = "Cinematic Administrator (" + $PROJECT_NAME + ") Poller"

$EXTENSION_FOLDER = "Extensions"
$EXTENSION_INSTALL_PATH = $SCRIPT_PATH + "\" + $EXTENSION_FOLDER
$SUBSCRIPTION_FOLDER = "Subscriptions"
$SUBSCRIPTION_INSTALL_PATH = $SCRIPT_PATH + "\" + $SUBSCRIPTION_FOLDER
$PLEX_API_FOLDER = "PlexApiScripts"
$PLEX_API_INSTALL_PATH = $SCRIPT_PATH + "\" + $PLEX_API_FOLDER
$SHORTCUT_FOLDER = "Shortcuts"
$SHORTCUT_PATH = $SCRIPT_PATH + "\" + $SHORTCUT_FOLDER

$TVDB_API_KEY = "2b7c6e9d-a6a0-4119-9c31-f54bd6975113" #Version4
$TVDB_SERVER = "https://api4.thetvdb.com/v4"
$TVDB_API_TOKEN = $null
$ACKNOWLEDGED_TVDB = $false
$TVDB_SERIES_CACHE = $null
$TVDB_CACHE_AGE_HOURS = 6

$OPENSUBTITLES_API_KEY = "8Hq3gcCy1lHCTOI6BjLaUlBZzUVvXD5K"
$OPENSUBTITLES_BASE = "https://api.opensubtitles.com"
$OPENSUBITLES_USER_AGENT = "CinAdmin"
$OPENSUBTITLES_TOKEN = $null
$ACKNOWLEDGED_OPENSUBTITLES = $false

$EMBED_THUMBNAIL = $true

$LOG_FILES_SIZE_CHECKED = $false
$SERIES_SUBSCRIPTION_MAPPING = $null
$MTP_DEVICE_PATH = $null
$MTP_DEVICE_NAME = $null

$DUPLICATE_PATTERN = "[^\d]\.\d$"

#Scripts
if($SCRIPT_PATH -eq $null)
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
}
$ACQUIRE_SCRIPT = $SCRIPT_PATH + "\Get-Media.ps1"
$AUDIT_SCRIPT = $SCRIPT_PATH + "\Audit-MediaLibraries.ps1"
$AUDIO_EXTRACT_SCRIPT = $SCRIPT_PATH + "\Copy-MediaAudio.ps1"
$CHECK_SUBSCRIPTION_SCRIPT = $SCRIPT_PATH + "\Check-MediaSubscription.ps1"
$CONVERT_ISO_SCRIPT = $SCRIPT_PATH + "\Convert-Media.ps1"
$CONFIG_FILE = $SCRIPT_PATH + "\" + $PROJECT_NAME + ".Configuration.ps1"
$CREDENTIAL_SCRIPT = $SCRIPT_PATH + "\Store-MediaCredential.ps1"
$FUNCTION_SCRIPT = $SCRIPT_PATH + "\" + $PROJECT_NAME + ".Functions.ps1"
$GENERIC_FUNCTION_SCRIPT = $SCRIPT_PATH + "\Functions.ps1"
$INSTALL_SCRIPT = $SCRIPT_PATH + "\Install-" + $PROJECT_NAME + ".ps1"
$NOTIFY_SCRIPT = $SCRIPT_PATH + "\Notify-MediaSeason.ps1"
$ORGANIZE_SCRIPT = $SCRIPT_PATH + "\Organize-Media.ps1"
$PLEX_AUDIT_SCRIPT = $PLEX_API_INSTALL_PATH + "\Audit-PlexLibraries.py"
$PLEX_OPENED_SCRIPT = $PLEX_API_INSTALL_PATH + "\Export-PlexOpenMedia.py"
$PLEX_REFRESH_SCRIPT = $PLEX_API_INSTALL_PATH + "\Refresh-PlexLibraries.py"
$PLEX_WATCHED_SCRIPT = $PLEX_API_INSTALL_PATH + "\Mark-PlexMediaWatched.py"
$REMOVE_AD_SCRIPT = $SCRIPT_PATH + "\Remove-MediaAds.ps1"
$SHOW_LOGS_SCRIPT = $SCRIPT_PATH + "\Show-MediaLogs.ps1"
$STARTUP_SCRIPT = $SCRIPT_PATH + "\Start-MediaSession.ps1"
$SUBTITLES_SCRIPT = $SCRIPT_PATH + "\Get-MediaSubtitles.ps1"
$TRACK_SCRIPT = $SCRIPT_PATH + "\Track-Media.ps1"
$TRANSFER_SCRIPT = $SCRIPT_PATH + "\Transfer-Media.ps1"

$POLL_COMMAND = $STARTUP_SCRIPT + " -Poll"

#Aliases
$ALIASES = @{
	$ACQUIRE_SCRIPT            = @(
		"getmed",
		"Acquire-Audio",
		"Acquire-Video",
		"Get-Video"
	)
	$AUDIT_SCRIPT              = @(
		"audmed",
		"Audit-Audio",
		"Audit-Video"
	)
	$AUDIO_EXTRACT_SCRIPT      = @()
	$CHECK_SUBSCRIPTION_SCRIPT = @(
		"chksub",
		"Check-Subscription"
	)
	$CONVERT_ISO_SCRIPT        = @(
		"convdisc",
		"Convert-Disc",
		"Convert-Image",
		"Convert-ISO",
		"convmed",
		"Convert-MediaDisc",
		"Convert-MP4"
	)
	$CREDENTIAL_SCRIPT         = @()
	$INSTALL_SCRIPT            = @()
	$NOTIFY_SCRIPT             = @(
		"notesea"
	)
	$ORGANIZE_SCRIPT           = @(
		"orgmed",
		"Organize-MediaLibrary",
		"Organize-Video",
		"Organize-Audio"
	)
	$REMOVE_AD_SCRIPT          = @(
		"delad",
		"Remove-VideoAd",
		"Remove-AudioAd"
	)
	$SHOW_LOGS_SCRIPT          = @(
		"medlog",
		"Display-MediaLogs",
		"Search-MediaLogs"
	)
	$STARTUP_SCRIPT            = @(
		"startms",
		"Enter-MediaSession",
		"New-MediaSession",
		"Start-RemoteMediaSession",
		"startrms",
		"Enter-RemoteMediaSession",
		"New-RemoteMediaSession",
		"Stop-MediaSessionPolling",
		"Stop-MediaPolling",
		"Terminate-MediaSessionPolling",
		"Terminate-MediaPolling",
		"termmp",
		"stopmp",
		"Start-MediaPollingTask",
		"Start-MediaSessionTask",
		"Start-MediaTask",
		"startmt"
	)
	$SUBTITLES_SCRIPT          = @(
		"impsub",
		"Import-MediaSubtitles",
		"Import-VideoSubtitles",
		"Embed-MediaSubtitles",
		"Embed-VideoSubtitles",
		"Acquire-MediaSubtitles",
		"Acquire-VideoSubtitles",
		"Get-VideoSubtitles"
	)
	$TRACK_SCRIPT              = @(
		"trackmed",
		"Track-ViewedMedia",
		"Track-ConsumedMedia",
		"Track-ListenedMedia",
		"resetmed",
		"Reset-MediaState",
		"Reset-ConsumedMediaState",
		"Reset-WatchedMedia",
		"Reset-WatchedMediaState",
		"Reset-ListenedMediaState"
	)
	$TRANSFER_SCRIPT           = @(
		"transmed",
		"Transfer-Video",
		"Transfer-Audio"
	)
}

#Files
$INSTALL_LAUNCHER = $SCRIPT_PATH + "\Install" + $PROJECT_NAME + ".bat"
$WINDOWS_TERMINAL_SETTINGS_JSON = $null
$WINDOWS_TERMINAL_BINARY = $null
$LICENSE_FILE = $SCRIPT_PATH + "\license.txt"
$INSTALL_INFORMATION = $SCRIPT_PATH + "\installinfo.txt"
$README_FILE = $SCRIPT_PATH + "\readme.md"
$ISO_PNG = $SHORTCUT_PATH + "\dvd.png"
$ISO_INSTRUCTIONS_PNG = $SHORTCUT_PATH + "\#_Instructions.png"

$PSC_SHORTCUT = $SHORTCUT_PATH + "\" + $PROJECT_NAME + " Session (Core).lnk"
$PSC_ICON = $SHORTCUT_PATH + "\pcore.ico"
$WT_SHORTCUT = $SHORTCUT_PATH + "\" + $PROJECT_NAME + " Session (Terminal).lnk"
$WT_ICON = $SHORTCUT_PATH + "\winterm.ico"
$WPS_SHORTCUT = $SHORTCUT_PATH + "\" + $PROJECT_NAME + " Session (Win).lnk"
$WPS_ICON = $SHORTCUT_PATH + "\winps.ico"
$POLL_SHORTCUT = $SHORTCUT_PATH + "\Polling " + $PROJECT_NAME + " Session.lnk"
$POLL_ICON = $SHORTCUT_PATH + "\schd.ico"
$REMOTE_PS_SHORTCUT = $SHORTCUT_PATH + "\Remote " + $PROJECT_NAME + " Session.lnk"

$METADATA_EXTENSION = ".metadata"
$SEGMENTS_EXTENSION = ".segments"
$SUBTITLE_EXTENSIONS = @(".vtt", ".srt", ".scc")
$SUBTITLE_EXTENSION_EXCLUSIONS = @(".mkv", ".avi", ".iso", ".mpg")
$THUMBNAIL_EXTENSIONS = @(".jpg", ".png", ".webp")
$AUDIO_EXTENSIONS = @(".m4a", ".mp3", ".ogg", ".wav", ".wma")
$MEDIA_AD_REMOVE_EXTENSIONS = $MEDIA_EXTENSIONS
$SPLIT_LINE_THRESHOLD = 5000

#Load Libraries and Configuration
##GLOBAL##
. $CONFIG_FILE
. $GENERIC_FUNCTION_SCRIPT

#Data
$LOG_OUTPUT = $DATA_PATH + "\Media.log"
$LOG_ARCHIVE = $DATA_PATH + "\LogArchive"
$COMMAND_LOG_OUTPUT = $DATA_PATH + "\CommandExecution.log"
$SESSION_LOG_OUTPUT = $DATA_PATH + "\Sessions.log"
$TRACKING_CSV = $DATA_PATH + "\TrackedMedia.csv"
$PLEX_AUDIT_CSV = $DATA_PATH + "\PlexLibraryMedia.csv"
$PLEX_OPENED_CSV = $DATA_PATH + "\PlexOpenedMedia.csv"
$RECORDING_CSV = $DATA_PATH + "\RecordingMedia.csv"
$RECORDING_PROBLEMS_CSV = $DATA_PATH + "\RecordingProblems.csv"
$TVDB_SERIES_CSV = $DATA_PATH + "\tvdb.csv"
$CURRENT_SEASONS_CSV = $DATA_PATH + "\CurrentSeasons.csv"
$TVDB_CACHE_PATH = $DATA_PATH + "\tvdb"
$RENAMED_CSV = $DATA_PATH + "\RenamedMedia.csv"
$AD_REMOVE_LOGS = $DATA_PATH + "\AdRemove"
$AD_REMOVE_MAX_LOG_AGE = 14
$CONVERSION_FOLDER = "Conversion"
$CONVERSION_TMP = $DATA_PATH + "\" + $CONVERSION_FOLDER
$AD_REMOVE_CSV = $DATA_PATH + "\AdRemovedMedia.csv"
$SUBTITLE_CSV = $DATA_PATH + "\SubtitledMedia.csv"
$SUBSCRIPTION_TMP = $DATA_PATH + "\Subscriptions"
$CREDENTIAL_FILE = $DATA_PATH + "\MediaCredentials.txt"
$ISO_PNG_PATH = $DOWNLOADED_MOVIES_PATH + "\ISOPNG"
$ISO_PNG_REFRESH_HOURS = 24

$CONCURRENCY_MUTEX = $null
$CONCURRENCY_FILE = $DATA_PATH + "\SEMAPHORE"
$LAST_EXECUTION = Get-Date
$MAX_CONCURRENCY_WAIT_MINUTES = 10
$LAST_TRACKED = $DATA_PATH + "\LASTTRACKED"
$LAST_CULLED = $DATA_PATH + "\LASTCULLED"
$SYMLINK_CSV = $DATA_PATH + "\SYMLINKS.csv"
$SUBTITLE_QUOTA = $DATA_PATH + "\SUBTITLEQUOTA"
$TERMINATION_REQUEST = $DATA_PATH + "\TERMREQUEST"
$CURRENT_SCRIPT_CLEANUP_NEEDED = $false

#Dependencies
$RECORDING_DATABASE = "%PROGRAMDATA%\MediaMall\Recording\recording.db"
$RECORDING_DATABASE = [System.Environment]::ExpandEnvironmentVariables($RECORDING_DATABASE)
$SQL_LITE = "%PROGRAMFILES(X86)%\MediaMall\System.Data.SQLite.dll"
$SQL_LITE = [System.Environment]::ExpandEnvironmentVariables($SQL_LITE)

$TIMEZONES =	@{
	"ACDT" =	"+1030";
	"ACST" =	"+0930";
	"ADT"  =	"-0300";
	"AEDT" =	"+1100";
	"AEST" =	"+1000";
	"AHDT" =	"-0900";
	"AHST" =	"-1000";
	"AST"  =	"-0400";
	"AT"   =	"-0200";
	"AWDT" =	"+0900";
	"AWST" =	"+0800";
	"BAT"  =	"+0300";
	"BDST" =	"+0200";
	"BET"  =	"-1100";
	"BST"  =	"-0300";
	"BT"   =	"+0300";
	"BZT2" =	"-0300";
	"CADT" =	"+1030";
	"CAST" =	"+0930";
	"CAT"  =	"-1000";
	"CCT"  =	"+0800";
	"CDT"  =	"-0500";
	"CED"  =	"+0200";
	"CET"  =	"+0100";
	"CST"  =	"-0600";
	"EAST" =	"+1000";
	"EDT"  =	"-0400";
	"EED"  =	"+0300";
	"EET"  =	"+0200";
	"EEST" =	"+0300";
	"EST"  =	"-0500";
	"FST"  =	"+0200";
	"FWT"  =	"+0100";
	"GMT"  =	"-0000";
	"GST"  =	"+1000";
	"HDT"  =	"-0900";
	"HST"  =	"-1000";
	"IDLE" =	"+1200";
	"IDLW" =	"-1200";
	"IST"  =	"+0530";
	"IT"   =	"+0330";
	"JST"  =	"+0900";
	"JT"   =	"+0700";
	"MDT"  =	"-0600";
	"MED"  =	"+0200";
	"MET"  =	"+0100";
	"MEST" =	"+0200";
	"MEWT" =	"+0100";
	"MST"  =	"-0700";
	"MT"   =	"+0800";
	"NDT"  =	"-0230";
	"NFT"  =	"-0330";
	"NT"   =	"-1100";
	"NST"  =	"+0630";
	"NZ"   =	"+1100";
	"NZST" =	"+1200";
	"NZDT" =	"+1300";
	"NZT"  =	"+1200";
	"PDT"  =	"-0700";
	"PST"  =	"-0800";
	"ROK"  =	"+0900";
	"SAD"  =	"+1000";
	"SAST" =	"+0900";
	"SAT"  =	"+0900";
	"SDT"  =	"+1000";
	"SST"  =	"+0200";
	"SWT"  =	"+0100";
	"USZ3" =	"+0400";
	"USZ4" =	"+0500";
	"USZ5" =	"+0600";
	"USZ6" =	"+0700";
	"UT"   =	"-0000";
	"UTC"  =	"-0000";
	"UZ10" =	"+1100";
	"WAT"  =	"-0100";
	"WET"  =	"-0000";
	"WST"  =	"+0800";
	"YDT"  =	"-0800";
	"YST"  =	"-0900";
	"ZP4"  =	"+0400";
	"ZP5"  =	"+0500";
	"ZP6"  =	"+0600";
}

$TVDB_NETWORK_SUBSCRIPTION_MAPPING = @{
	"ABC (US)"      = "$SUBSCRIPTION_INSTALL_PATH\ABC.ps1";
	"AeTV"          = "$SUBSCRIPTION_INSTALL_PATH\AeTV.ps1";
	"Amazon Freevee"= "$SUBSCRIPTION_INSTALL_PATH\AmazonPrime.ps1";
	"Amazon Prime Video"  = "$SUBSCRIPTION_INSTALL_PATH\AmazonPrime.ps1";
	"Amazon Studios"= "$SUBSCRIPTION_INSTALL_PATH\AmazonPrime.ps1";
	"Apple TV+"     = "$SUBSCRIPTION_INSTALL_PATH\AppleTV.ps1";
	"CBS"           = "$SUBSCRIPTION_INSTALL_PATH\CBS.ps1";
	"CBS All Access"= "$SUBSCRIPTION_INSTALL_PATH\CBS.ps1";
	"Comedy Central"= "$SUBSCRIPTION_INSTALL_PATH\ComedyCentral.ps1";
	"Disney+"       = "$SUBSCRIPTION_INSTALL_PATH\Disney.ps1";
	"HBO"           = "$SUBSCRIPTION_INSTALL_PATH\HBO.ps1";
	"Hulu"          = "$SUBSCRIPTION_INSTALL_PATH\Hulu.ps1";
	"National Geographic" = "$SUBSCRIPTION_INSTALL_PATH\NatGeo.ps1";
	"NBC"           = "$SUBSCRIPTION_INSTALL_PATH\NBC.ps1";
	"Netflix"       = "$SUBSCRIPTION_INSTALL_PATH\Netflix.ps1";
	"TBS"           = "$SUBSCRIPTION_INSTALL_PATH\TBS.ps1";
	"The CW"        = "$SUBSCRIPTION_INSTALL_PATH\TheCW.ps1";
}

#######################################
#
# FUNCTION DisplayNewSeasonNotification
#
#######################################
function DisplayNewSeasonNotification($newSeason, $currentLatestSeriesAiredDate)
{
	if(FailedStringValidation $newSeason) { return }

	$notificationTitle = "A New Season Was Found"
	$notificationText = "A potential new season was found: `n`n" + $newSeason + " ( " + $currentLatestSeriesAiredDate + " )"

	DisplayNotification $notificationTitle $notificationText
	DisplayKodiNotification $notificationTitle $notificationText $null
}

##################################
#
# FUNCTION DisplayKodiNotification
#
##################################
function DisplayKodiNotification($title, $message, $displayTimeSeconds)
{
	if(FailedStringValidation $title) { return }
	if(FailedStringValidation $message) { return }

	if(-not (IsKodiConfigured))
	{
		return
	}

	if($displayTimeSeconds -eq $null)
	{
		$displayTimeSeconds = 30
	}

	$message = ($message | ConvertTo-Json).Trim('"')

	foreach ($currentKodiUri in $KODI_SERVERS.Keys)
	{
		VerboseLogInfo Sending notification to Kodi [$currentKodiUri]...

		$webclient = New-Object System.Net.WebClient
		$webclient.Encoding = [System.Text.Encoding]::UTF8
		$extractedCred = GetCredential ($KODI_SERVERS[$currentKodiUri])
		if($extractedCred -eq $null)
		{
			continue
		}
		$webclient.Credentials = New-Object System.Net.NetworkCredential(($extractedCred[0]), ($extractedCred[1]))

		$jsonRequest = '{"jsonrpc": "2.0", "method": "GUI.ShowNotification", "params": { "title": "' + $title + '", "message": "' + $message + '", "displaytime": ' + ($displayTimeSeconds * 1000) + ' }, "id": "1"}'
		$postData = [System.Text.Encoding]::UTF8.GetBytes($jsonRequest)

		DebugLogInfo POSTING: [$jsonRequest]

		try
		{
			$response = $webclient.UploadData($currentKodiUri, "POST", $postData)
		}
		catch
		{
			if($_.ToString().Contains("Unable to connect to the remote server") -or $_.ToString().Contains("connected host has failed to respond") -or $_.ToString().Contains("actively refused it"))
			{
				VerboseLogInfo Server appears unavailable or Kodi not accessible: [$currentKodiUri]
				$Error.Clear()
			}
			else
			{
				LogWarning Problem setting Kodi state: [$currentKodiUri] [($_.ToString())]
			}
			continue
		}

		$response = [System.Text.Encoding]::UTF8.GetString($response)
		DebugLogInfo RESPONSE [$response]

		$resultsJson = $response | ConvertFrom-Json

		if($resultsJson.error -ne $null)
		{
			LogError JSON ERROR RESPONSE [($response)] [Request: ($jsonRequest)]
		}
	}
}

#################################################
#
# FUNCTION DisplaySubscriptionProblemNotification
#
#################################################
function DisplaySubscriptionProblemNotification($subscription)
{
	if(FailedStringValidation $subscription) { return }

	$tipText = "There is a problem checking this PlayOn subscription: `n`n" + $subscription

	Remove-Event BalloonClicked_event -ErrorAction Ignore
	Unregister-Event -SourceIdentifier BalloonClicked_event -ErrorAction Ignore
	Remove-Event BalloonClosed_event -ErrorAction Ignore
	Unregister-Event -SourceIdentifier BalloonClosed_event -ErrorAction Ignore

	$notification = New-Object System.Windows.Forms.NotifyIcon

	$notification.Icon = [System.Drawing.SystemIcons]::Information
	$notification.BalloonTipTitle = "Subscription Problem Detected"
	$notification.BalloonTipIcon = "Info"
	$notification.BalloonTipText = $tipText
	$notification.Visible = $True

	$notification.ShowBalloonTip(60 * 1000)
	$block = { param($tipText) Add-Type -AssemblyName System.Windows.Forms; [System.Windows.Forms.MessageBox]::Show($tipText, "Subscription Problem Detected") }
	(Start-Job -ScriptBlock $block -ArgumentList $tipText) > $null
}

######################
#
# FUNCTION ShowSyntax
#
######################
function ShowSyntax()
{
	if(-not $Help)
	{
		return
	}

	$scriptFile = GetExecutingScriptName ($MyInvocation.ScriptName)
	$scriptFile = [System.IO.Path]::GetFileName($scriptFile)

	switch ($scriptFile)
	{
		"Audit-MediaLibraries.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Audit-MediaLibraries.ps1:'
			OutputColoredText $null '   Performs extensive checks on your media libraries and reports back the results.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Audit-MediaLibraries.ps1 [-Duplicates] [-EpisodeContinuity] [-EpisodeNames] [-Kodi] [-Movies] [-Plex] [-Sizes] [-SeasonDirectories] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'

			OutputColoredText $null '   -Duplicates:        Audit library for duplicates. Overrides $AUDIT_DUPLICATES.'
			OutputColoredText $null '   -EpisodeContinuity: Audit library for missing episodes. Overrides $AUDIT_EPISODE_CONTINUITY.'
			OutputColoredText $null '   -EpisodeNames:      Audit episodes for correct naming. Overrides $AUDIT_EPISODE_NAMES.'
			OutputColoredText $null '   -Kodi:              Audit Kodi libraries. Overrides $AUDIT_PLEX.'
			OutputColoredText $null '   -Movies:            Audit movie library. Overrides $AUDIT_MOVIES.'
			OutputColoredText $null '   -Plex:              Audit Plex libraries. Overrides $AUDIT_PLEX.'
			OutputColoredText $null '   -Sizes:             Audit library file sizes. Overrides $AUDIT_SIZES.'
			OutputColoredText $null '   -SeasonDirectories: Audit episode directories. Overrides $AUDIT_SEASON_DIRECTORIES.'
			OutputColoredText $null '   -Help:              This message. Optional.'
			OutputColoredText $null '   -WhatIf:            Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose:           Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Audit-MediaLibraries.ps1 -Plex'
			OutputColoredText $null ""
		}
		"Check-MediaSubscription.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Check-MediaSubscription.ps1:'
			OutputColoredText $null '   Checks a subscription whether there is any new media content to acquire. If no subscription is specified, all pollable subscriptions under $SUBSCRIPTION_INSTALL_PATH are checked. If previously unacquired, this script will use Get-Media.ps1 to grab it. Subscription files provide instructions on how to extract content from RSS and web pages.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Check-MediaSubscription.ps1 [-Subscription <Subscription File>] [-BuildCache] [-ShowSource] [-Force] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Force:        Even if the subscription is disabled, force script to check the subscription. Optional.'
			OutputColoredText $null '   -Subscription: Path to subscription file in which to extract settings from. If none is specified, all the subscriptions in $SUBSCRIPTION_INSTALL_PATH are checked.'
			OutputColoredText $null '   -BuildCache:   Rather than acquire new media, cache all the content found as already acquired. Optional.'
			OutputColoredText $null '   -ClearSemaphore: Clears the SEMAPHORE file so other scripts can run, useful when concurrency is blocked. Optional.'
			OutputColoredText $null '   -ShowSource:   Open the source Uri of the subscription in your browser.'
			OutputColoredText $null '   -Help:         This message. Optional.'
			OutputColoredText $null '   -WhatIf:       Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose:      Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Check-MediaSubscription.ps1 -Subscription .\Subscriptions\TheDailyShow.ps1'
			OutputColoredText $null ""
		}
		"Copy-MediaAudio.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Copy-MediaAudio.ps1:'
			OutputColoredText $null '   Extracts the audio from video media to the path configured by $DOWNLOADED_PODCASTS_PATH. Subscriptions can indicate that audio should be cloned by the $ACQUISITION_EXTRACT_AUDIO setting. Note that this script supports pipelining.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Copy-MediaAudio.ps1 [-Path <source>] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Path:    Source path to consider scanning for media to extract audio. If left unspecified, media under the configured downloaded path $DOWNLOADED_PODCASTS_PATH is considered.'
			OutputColoredText $null '   -Help:    This message. Optional.'
			OutputColoredText $null '   -WhatIf:  Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose: Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Copy-MediaAudio.ps1'
			OutputColoredText $null ""
		}
		"Convert-Media.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Convert-Media.ps1:'
			OutputColoredText $null '   Detects and converts ISO files into streamable formats via MakeMKV. This script is designed to assist Plex clients access disc images from your library.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Convert-Media.ps1 [-Path <source>] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Path:    Source path to consider scanning for media to convert. If left unspecified, media under the configured downloaded path $DOWNLOADED_MOVIES_PATH is considered.'
			OutputColoredText $null '   -Help:    This message. Optional.'
			OutputColoredText $null '   -WhatIf:  Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose: Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Convert-Media.ps1'
			OutputColoredText $null ""
		}
		"Get-Media.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Get-Media.ps1:'
			OutputColoredText $null '   Given a URI, acquires media content from that location while optionally using configuration from a subscription file to process it.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Get-Media.ps1 -Uri <Acquisition Uri> [-Subscription <Subscription File>] [-RenameTo <Name>] [-Force] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Force:                         Even if cached, force script to re-acquire the content specified in the URI. Optional.'
			OutputColoredText $null '   -AdRemoveSubscriptionOverride:  Overrides subscription setting to remove ads after acquisition. Optional.'
			OutputColoredText $null '   -RenameSubscriptionOverride:    Overrides subscription setting to rename media after acquisition. Optional.'
			OutputColoredText $null '   -ExtractAudioSubscriptionOverride: Overrides subscription setting to extract audio after acquisition. Optional.'
			OutputColoredText $null '   -Subscription:                  Path to subscription file in which to extract settings from. If none is specified, defaults are used instead.'
			OutputColoredText $null '   -RenameTo:                      Rename media to this name after acquiring it. Optional.'
			OutputColoredText $null '   -Uri:                           The URI in which to extract the media. Mandatory.'
			OutputColoredText $null '   -Help:                          This message. Optional.'
			OutputColoredText $null '   -WhatIf:                        Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose:                       Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Get-Media.ps1 -Subscription .\Subscriptions\TheDailyShow.ps1 -Uri "http://www.cc.com/episodes/969osg/the-daily-show-with-trevor-noah-your-moment-of-them--the-best-of-roy-wood-jr--vol--2-season-23-ep-9973"'
			OutputColoredText $null ""
		}
		"Get-MediaSubtitles.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Get-MediaSubtitles.ps1'
			OutputColoredText $null '   If supporting configuration exists, search, match, download, and/or embed subtitles from OpenSubtitles.org into media files.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Get-MediaSubtitles.ps1: [-BuildCache] [-CleanCache] [-Path <search pattern>] [-Force] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Path:        Path to process media. Optional. If not specified, the paths specified in the configuration are used.'
			OutputColoredText $null '   -BuildCache:  Scan and recreate the already processed cache ($SUBTITLE_CSV).'
			OutputColoredText $null '   -CleanCache:  Rather than import subtitles into media, audit and remove obsolete entries from cache ($SUBTITLE_CSV).'
			OutputColoredText $null '   -Force:       Ignore cache when considering media to import subtitles.'
			OutputColoredText $null '   -Help:        This message. Optional.'
			OutputColoredText $null '   -WhatIf:      Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose:     Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLES:'
			OutputColoredText $null '   Get-MediaSubtitles.ps1 -Verbose'
			OutputColoredText $null '   dir ..\MyVideos | Get-MediaSubtitles.ps1'
			OutputColoredText $null ""
		}
		"Install-CinAdmin.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Install-CinAdmin.ps1'
			OutputColoredText $null '   Copies, configures, tests, and enables the'$PROJECT_NAME' scripts. To install CinAdmin, it is recommended that you launch the wrapping script InstallCinAdmin.bat instead.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Install-CinAdmin.ps1 -Target <install directory> [-Clicked] [-Remote] [-Visible] [-SkipConfiguration] [-SkipTask] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Target:        Target directory to copy the needed scripts. Mandatory.'
			OutputColoredText $null '   -Clicked:       Internal hint to indicate that install was launched via a click or pop-up. Should not be needed to be explicitly specified.'
			OutputColoredText $null '   -Remote:        If specified, will install a minimal version of CineAdmin that allows remote PowerShell access to a full install on the same network.'
			OutputColoredText $null '   -Visible:       If specified, creates the task such that user sees the execution window when executing. Otherwise, user is prompted.'
			OutputColoredText $null '   -SkipConfiguration: If specified, do not copy, ask user to edit, or test MediaConfiguration.ps1 on install.'
			OutputColoredText $null '   -SkipTask:      If specified, do not create or enable the media polling scheduled task on install.'
			OutputColoredText $null '   -Help:          This message. Optional.'
			OutputColoredText $null '   -WhatIf:        During the testing phase, execute each with the -WhatIf parameter without prompting. Optional.'
			OutputColoredText $null '   -Verbose:       During the testing phase, execute each with the -Verbose parameter without prompting. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Install-CinAdmin.ps1 -Target C:\tools\CinAdmin -WhatIf'
			OutputColoredText $null ""
		}
		"Notify-MediaSeason.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Notify-MediaSeason.ps1'
			OutputColoredText $null '   This script queries TVDB.com to look for new seasons for the shows defined in $TVDB_SERIES_CSV and stores the results in $CURRENT_SEASONS_CSV and will notify you via a popup window and notification if any are found that will air $NEW_SEASON_LEAD_NOTICE_DAYS in the future. It is recommended you run this script every week or so. If you run this via Start-MediaSession.ps1 -Poll, it runs every Sunday at 3AM.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Notify-MediaSeason.ps1: [-RebuildCsv] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -RebuildCsv:  Purge the current season tally ($CURRENT_SEASONS_CSV) and rebuilds it from $TVDB_SERIES_CSV. Optional.'
			OutputColoredText $null '   -Help:        This message. Optional.'
			OutputColoredText $null '   -WhatIf:      Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose:     Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Notify-MediaSeason.ps1 -Verbose'
			OutputColoredText $null ""
		}
		"Organize-Media.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Organize-Media.ps1'
			OutputColoredText $null '   This script reads the directory mappings you specify via settings like $DOWNLOADED_TV_PATH and moves them according to their consumed status when the tracking threshold is up. The script can be smart about how it clusters content into subfolders minimize the amount of surfing you need to do to get to your content (see the $EPISODES_AGGREGATE_THRESHOLD configuration setting). If you have $USE_TVDB configured, TVDB information is also gathered to attempt to rename each of the files (but deferred if multiple/no results are found --- to configure those, run this script with the -BuildCache parameter). Shows that are matched as a "Topical Show" (set via $TOPICAL_SHOWS) have their older shows moved to an archive location if they are not watched, and those files are eventually purged. Consumed topical shows are moved directly to the purge folder and are not mapped to a watched folder. It is recommended that you can run this script every hour or so. Note that now renaming is supported with TVDB information, this can take a bit of time to complete depending on the size of your library, so please be patient if you see delays when executing this script. Note that this script supports pipelining.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Organize-Media.ps1: [-Path <search pattern>] [-BuildCache] [-CleanCache] [-Force] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Path:        Path to process media for organization. Optional. If not specified, the paths specified in the configuration are used ($DOWNLOADED_TV_PATH and $DOWNLOADED_MOVIES_PATH).'
			OutputColoredText $null '   -BuildCache:  Just looks up each file in TVDB and allow you to configure the series mapping. Optional.'
			OutputColoredText $null '   -CleanCache:  Instead of organizing, audit and clean out obsolete entries in the rename cache.'
			OutputColoredText $null '   -ClearSemaphore: Clears the SEMAPHORE file so other scripts can run, useful when concurrency is blocked. Optional.'
			OutputColoredText $null '   -Force:       Ignore cache when considering organization and renaming and metadata regeneration.'
			OutputColoredText $null '   -Help:        This message. Optional.'
			OutputColoredText $null '   -WhatIf:      Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose:     Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Organize-Media.ps1 -Verbose'
			OutputColoredText $null '   dir ..\MyVideos | Organize-Media.ps1'
			OutputColoredText $null ""
		}
		"Remove-MediaAds.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Remove-MediaAds.ps1'
			OutputColoredText $null '   Utilizes tools like COMSKIP and FFMPEG to detect and strip advertising from media. Currently, only matched extensions that are downloaded TV in non-excluded locations are considered for ad removal. Note that this script supports pipelining. It is recommended that you can run this script every hour or so.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Remove-MediaAds.ps1: [-Path <search pattern>] [-BuildCache] [-CleanCache] [-Force] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Path:        Path to process media. Optional. If not specified, the paths specified in the configuration are used.'
			OutputColoredText $null '   -BuildCache:  Scan and recreate the already processed cache ($AD_REMOVE_CSV).'
			OutputColoredText $null '   -CleanCache:  Rather than remove ads from media, audit and remove obsolete entries from cache ($AD_REMOVE_CSV).'
			OutputColoredText $null '   -Force:       Ignore cache when considering media to remove ads.'
			OutputColoredText $null '   -Help:        This message. Optional.'
			OutputColoredText $null '   -WhatIf:      Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose:     Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLES:'
			OutputColoredText $null '   Remove-MediaAds.ps1 -Verbose'
			OutputColoredText $null '   dir ..\MyVideos | Remove-MediaAds.ps1'
			OutputColoredText $null ""
		}
		"Show-MediaLogs.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Show-MediaLogs.ps1:'
			OutputColoredText $null '   Displays the script and execution logs, or optionally search for a pattern within them.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Show-MediaLogs.ps1 [-Search <search pattern>] [-TailLines <number of lines>] [-Popup] [-All] [-Log] [-CommandExecution] [-ConvertToCsv] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -All:               If specified, show or search all archived log files as well as the current log file. Optional.'
			OutputColoredText $null '   -CommandExecution:  If specified, show or search the command execution log files (CommandExecution.log). Optional.'
			OutputColoredText $null '   -ConvertToCsv:      If specified, rather than display, convert the log file. into a CSV. This is useful to do analysis on performance trends.'
			OutputColoredText $null '   -Log:               If specified, show or search the general log files (Media.log). Optional. If this is not specified, it is the default.'
			OutputColoredText $null '   -Popup:             If specified, opens notepad with the targeted logfile. Requires a local (non-Remote) PowerShell session. Optional.'
			OutputColoredText $null '   -TailLines:         Specifies the number of tailing lines to show in the logs. Defaults to 50. Optional.'
			OutputColoredText $null '   -Search:            If specified, search the files for the specified pattern and display it in the console instead. Case insensitive. Optional.'
			OutputColoredText $null '   -Help:              This message. Optional.'
			OutputColoredText $null '   -WhatIf:            Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose:           Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Show-MediaLogs.ps1 -Search "*.iso"'
			OutputColoredText $null ""
		}
		"Store-MediaCredential.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Store-MediaCredential.ps1:'
			OutputColoredText $null '   Stores credentials in an encrypted form to be used for acquisition of content in secure form. After executing, you will be prompted for a username and password. Stored credentials are referenced and retrieved by a unique identifier.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Store-MediaCredential.ps1 [-Id <identifier>] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Id:      Identifier used to describe these credentials. This term can be specified in a subscription file so it can be found by scripts on acquisition. If this identifier is not specified, you will be prompted to supply one.'
			OutputColoredText $null '   -Help:    This message. Optional.'
			OutputColoredText $null '   -WhatIf:  Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose: Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Store-MediaCredential.ps1 -Id Verizon'
			OutputColoredText $null ""
		}
		"Track-Media.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Track-Media.ps1:'
			OutputColoredText $null '   Scan via handle information to determine whether new media should be tracked as consumed. If a path is specified, manually add or remove media to be tracked as consumed after $TRACK_AGE_HOURS has passed. Media marked as watched is shifted to their mapped consumed directory. Track-Media.ps1 -Remove also either forgets media that is currently being tracked, or resets watched media by moving them back into the to watch downloaded paths. Note that this script supports pipelining and requires administrator rights.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Track-Media.ps1 [-Path <path>] [-Remove] [-Purge] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -ClearSemaphore: Clears the SEMAPHORE file so other scripts can run, useful when concurrency is blocked. Optional.'
			OutputColoredText $null '   -Path:    Path and search pattern to search recursively for media. Optional.'
			OutputColoredText $null '   -Purge:   Instead of tracking, directly move media to $PURGE_PATH. Optional.'
			OutputColoredText $null '   -Remove:  Signals that the files indicated in Path are to be removed from being actively tracked. If you specify media that has already been tracked as watched, it is instead copied back into $DOWNLOADED_TV_PATH, $DOWNLOADED_MOVIES_PATH, or $DOWNLOADED_PODCASTS_PATH.'
			OutputColoredText $null '   -Force:   Force Plex data refresh if needed to when detecting watched media.'
			OutputColoredText $null '   -Help:    This message. Optional.'
			OutputColoredText $null '   -WhatIf:  Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose: Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Track-Media.ps1'
			OutputColoredText $null '   Track-Media.ps1 -Path c:\recordedmedia\BigBang*mp4 -WhatIf'
			OutputColoredText $null '   dir ..\The* | Track-Media.ps1'
			OutputColoredText $null ""
		}
		"Transfer-Media.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Transfer-Media.ps1:'
			OutputColoredText $null '   Recursively searches, sorts, and copies media to a new location. Note that this script supports pipelining.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Transfer-Media.ps1 [-Path <path>] [-Destination <target>] [-Profile <proileName>] [-Clobber] [-CopyPlaylists] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Path:              Source path to transfer media files from. Mandatory.'
			OutputColoredText $null '   -Destination:       Target path to transfer media files to. Mandatory.'
			OutputColoredText $null '   -Clobber:           Before copying, remove media from destination first. Optional.'
			OutputColoredText $null '   -Profile:           Specify the IDs of profiles you wish to automatically expand parameters. See the help file for more details how to configure these.'
			OutputColoredText $null '   -CopyPlaylists:     Refresh and copy M3U playlists on a per-folder basis. This is useful when transferring music. Optional.'
			OutputColoredText $null '   -StripExtendedChars: Before transferring, check if filename or metadata title contains extended characters and downgrade to ASCII. Good for older devices. Optional.'
			OutputColoredText $null '   -Help:              This message. Optional.'
			OutputColoredText $null '   -WhatIf:            Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose:           Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Transfer-Media.ps1 -Path "C:\Media\Podcasts\" -Target "G:\"'
			OutputColoredText $null ""
		}
		"Start-MediaSession.ps1"
		{
			OutputColoredText $null ""
			OutputColoredText "Green" ' Start-MediaSession.ps1:'
			OutputColoredText $null '   Starts a PowerShell session to start using'$PROJECT_NAME', either interactively or an automated polling session via a scheduled task. Interactive sessions can be either started locally or through remote PowerShell (via the -Remote parameter).'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' SYNTAX:'
			OutputColoredText $null '   Start-MediaSession.ps1 [-Remote] [-RemoteUserName <username>] [-RemoteHostName <hostname>] [-RemoteInstallPath <installpath>] [-Poll] [-Task] [-WhatIf] [-Verbose] [-Help]'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' PARAMETERS:'
			OutputColoredText $null '   -Remote:            Start a remote PowerShell session.'
			OutputColoredText $null '   -RemoteUserName:    Specifies what user name will be used to start a remote PowerShell session. If not specified and is needed, you will be prompted for one.'
			OutputColoredText $null '   -RemoteHostName:    Specifies what remote host name will be used to start a remote PowerShell session. If not specified and is needed, you will be prompted for one.'
			OutputColoredText $null '   -RemoteInstallPath: Specifies what directory the remote install of'$PROJECT_NAME' is in that is to be used to start a remote PowerShell session. If not specified and is needed, you will be prompted for one.'
			OutputColoredText $null '   -Poll:              Indicates that the session should poll in the current interactive PowerShell session, utilizing the scripts specified in $POLLING_SCRIPTS.'
			OutputColoredText $null '   -Task:              Indicates that the session should poll by starting the installed Windows Scheduled Task, utilizing the scripts specified in $POLLING_SCRIPTS.'
			OutputColoredText $null '   -TerminatePoll:     If CinAdmin is currently polling, send a graceful termination request that the script will try to honor as soon as it can.'
			OutputColoredText $null '   -Help:              This message. Optional.'
			OutputColoredText $null '   -WhatIf:            Execute commandlet without actually committing result to any file. Optional.'
			OutputColoredText $null '   -Verbose:           Additional output to console and log. Optional.'
			OutputColoredText $null ""
			OutputColoredText "Yellow" ' EXAMPLE:'
			OutputColoredText $null '   Start-MediaSession.ps1'
			OutputColoredText $null ""
		}
		default
		{
			OutputColoredText $null ""
			LogError "File '$scriptFile' does not have any help syntax defined."
			OutputColoredText $null ""
		}
	}

	OutputColoredText "Yellow" ' RELATED CONFIGURATION PARAMETERS:'
	if($scriptFile -in @("Organize-Media.ps1", "Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $DOWNLOADED_TV_PATH ['$DOWNLOADED_TV_PATH' ]:'
		OutputColoredText $null '       Root folder location for newly downloaded media to manage. Shows that cannot be mapped to a folder default to being copied in the root, to help minimize folder traversal.'
		OutputColoredText $null ""

		OutputColoredText $null '   $DOWNLOADED_PODCASTS_PATH ['$DOWNLOADED_PODCASTS_PATH' ]:'
		OutputColoredText $null '       Root path where audio media is copied or extracted. Note that this is intended to contain episodic content, not music.'
		OutputColoredText $null ""

		OutputColoredText $null '   $DOWNLOADED_MOVIES_PATH ['$DOWNLOADED_MOVIES_PATH' ]:'
		OutputColoredText $null '       Root folder location for longer media like movies to manage. Media managed here do not get organized or aggregated or mapped in folders per show, undergo renaming with TV database information, or attempt to parse season, air, and episode information from the name.'
		OutputColoredText $null ""

		OutputColoredText $null '   $ORGANIZE_PATH_OVERRIDE:'
		OutputColoredText $null '       Directory Mapping to apply to newly downloaded files. Note you may include folder variables like $DOWNLOADED_TV_PATH and they will be expanded.'
		OutputColoredText $null ""

		OutputColoredText $null '   $CONSUMED_PATH_OVERRIDE:'
		OutputColoredText $null '       Directory Mapping to apply to watched files. Note you may include folder variables like $DOWNLOADED_TV_PATH and they will be expanded.'
		OutputColoredText $null ""

		OutputColoredText $null '   $DOWNLOADED_PATH_EXCLUSIONS:'
		OutputColoredText $null '       List of paths that will not have their children media organized or marked as consumed. Note you may include folder variables like $DOWNLOADED_TV_PATH and they will be expanded.'
		OutputColoredText $null ""

		OutputColoredText $null '   $EPISODES_AGGREGATE_THRESHOLD ['$EPISODES_AGGREGATE_THRESHOLD' ]:'
		OutputColoredText $null '       If new media is not mapped, the show name is extracted. If it can, if there is more than this number of shows matching that name, those shows will be moved into its own dedicated folder. This is convenient for minimizing navigation to new media however may confuse clients that require media to be in its own folder.'
		OutputColoredText $null ""

		OutputColoredText $null '   $DATA_PATH ['$DATA_PATH' ]:'
		OutputColoredText $null '       Root Path where logs, cache/state CSV files, and temporary media files are stored.'
		OutputColoredText $null ""

		OutputColoredText $null '   $MEDIA_EXTENSIONS ['$MEDIA_EXTENSIONS' ]:'
		OutputColoredText $null '       File extensions that are considered when organizing media.'
		OutputColoredText $null ""

		OutputColoredText $null '   $USE_TVDB ['$USE_TVDB' ]:'
		OutputColoredText $null '       When renaming media, use information from TheTVDB.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PURGE_PATH ['$PURGE_PATH' ]:'
		OutputColoredText $null '       Root folder location to transfer consumed or discarded media that has expired. This folder is regularly culled with the oldest files being transferred to the recycle bin.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PURGE_PATTERNS ['$PURGE_PATTERNS' ]:'
		OutputColoredText $null '       As media is organized and empty folders are purged, this setting keeps track of files that are deleted when doing so. This is useful for getting rid of files such as TXT and NFO that come with much downloaded media.'
		OutputColoredText $null ""

		OutputColoredText $null '   $ARCHIVE_AGE_DAYS ['$ARCHIVE_AGE_DAYS' ]:'
		OutputColoredText $null '       Age threshold of media in $TOPICAL_SHOWS that are transferred to a dedicated subfolder specified in $ARCHIVE_FOLDER_NAME, if defined.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PURGE_TOPICAL_AGE_DAYS ['$PURGE_TOPICAL_AGE_DAYS' ]:'
		OutputColoredText $null '       Age threshold of media in archive folders that is culled to the folder specified in $PURGE_PATH, and threshold that media in $PURGE_PATH is culled into the recycle bin.'
		OutputColoredText $null ""

		OutputColoredText $null '   $ARCHIVE_FOLDER_NAME ['$ARCHIVE_FOLDER_NAME' ]:'
		OutputColoredText $null '       Name of the folder that older, unconsumed topical shows are moved within before they are purged. If is not set, archiving is effectively turned off.'
		OutputColoredText $null ""

		OutputColoredText $null '   $DAILY_FOLDER_NAME ['$DAILY_FOLDER_NAME' ]:'
		OutputColoredText $null '       Name of the folder that daily shows are moved to. Daily shows are clustered together in this folder to provide a briefing location and are purged after a day if unwatched. If is not set, archiving is effectively turned off.'
		OutputColoredText $null ""

		OutputColoredText $null '   $TOPICAL_SHOWS:'
		OutputColoredText $null '       List of topical show names. Media that matches these names have their older shows archived and eventually purged and are directly moved to $PURGE_PATH when watched.'
		OutputColoredText $null ""

		OutputColoredText $null '   $DAILY_SHOWS:'
		OutputColoredText $null '       List of daily show names. Media that matches these names are moved to $DAILY_FOLDER_NAME and are directly moved to $PURGE_PATH when watched or within 24 hours. This is ideal for daily news media.'
		OutputColoredText $null ""

		OutputColoredText $null '   $RENAME_TEMPLATE ['$RENAME_TEMPLATE' ]:'
		OutputColoredText $null '       The template that is expanded to derive a new name from the data within if the file is not mapped as a topical show. Valid expansions are {SeriesName}, {AiredDate}, {SeasonAndEpisode}, {EpisodeTitle}.'
		OutputColoredText $null ""

		OutputColoredText $null '   $TOPICAL_RENAME_TEMPLATE ['$TOPICAL_RENAME_TEMPLATE' ]:'
		OutputColoredText $null '       The template that is expanded to derive a new name from the data within if the file is mapped as a topical show. Valid expansions are {SeriesName}, {AiredDate}, {SeasonAndEpisode}, {EpisodeTitle}.'
		OutputColoredText $null ""
	}

	if($scriptFile -in @("Organize-Media.ps1", "Track-Media.ps1", "Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $CONSUMED_FOLDER_NAME ['$CONSUMED_FOLDER_NAME' ]:'
		OutputColoredText $null '       Folder name under downloaded paths to move consumed media that is not purged.'
		OutputColoredText $null ""

		OutputColoredText $null '   $TRACK_AGE_HOURS ['$TRACK_AGE_HOURS' ]:'
		OutputColoredText $null '       Time after media flagged as watched has their watched mappings applied.'
		OutputColoredText $null ""

		OutputColoredText $null '   $KODI_INTEGRATION ['$KODI_INTEGRATION' ]:'
		OutputColoredText $null '       When set to $true, toggles library integration with Kodi if configured. When configured, consumed state is synchronized and its library is periodically refreshed.'
		OutputColoredText $null ""

		OutputColoredText $null '   $KODI_FOLDER_TRANSLATE:'
		OutputColoredText $null '       Specifies one or more folders configured in Kodi libraries (such as SMB shares) that need to be translated to local library paths.'
		OutputColoredText $null ""

		OutputColoredText $null '   $KODI_SERVERS:'
		OutputColoredText $null '       A dictionary containing the JSON-RPC endpoints and credential IDs for configured Kodi servers.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PLAYON_WARN_SUBSCRIPTION_PROBLEMS ['$PLAYON_WARN_SUBSCRIPTION_PROBLEMS' ]:'
		OutputColoredText $null '       If PlayOn server is installed, periodically display errors to the console if a subscription is having problems.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PLEX_INTEGRATION ['$PLEX_INTEGRATION' ]:'
		OutputColoredText $null '       When set to $true, toggles library integration with Plex if configured. When configured, consumed state is synchronized.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PLEX_SERVERNAME ['$PLEX_SERVERNAME' ]:'
		OutputColoredText $null '       Name of a configured Plex server.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PLEX_CREDENTIALS ['$PLEX_CREDENTIALS' ]:'
		OutputColoredText $null '       Credential ID for a configured Plex server. Note that this can be a username and password, or a direct server access token. See the comments in the configuration file.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PLEX_API ['$PLEX_API' ]:'
		OutputColoredText $null '       Location of Python PlexAPI. Needed for Plex library integration.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PLEX_GUESTS ['$PLEX_GUESTS' ]:'
		OutputColoredText $null '       Plex library guest users that will trigger a watch and purge on media consumption. Can be fine tuned in the PlexGuests.ps1 subscription file.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PLEX_GUEST_SERIES ['$PLEX_GUEST_SERIES' ]:'
		OutputColoredText $null '       Media titles that will trigger a watch and purge on media consumption. Can be fine tuned in the PlexGuests.ps1 subscription file.'
		OutputColoredText $null ""
	}

	if($scriptFile -in @("Track-Media.ps1", "Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $HANDLE_BINARY ['$HANDLE_BINARY' ]:'
		OutputColoredText $null '       Location of handle.exe.'
		OutputColoredText $null ""

		OutputColoredText $null '   $WATCHING_HOSTS ['$WATCHING_HOSTS' ]:'
		OutputColoredText $null '       These are executables that trigger that a given media is being watched by handle.exe. Note that "System" can specify any file that is accessed via a remote computer via a shared network folder. You can also specify SMB access information in the format "CLIENTMACHINE - SERVER\UserName".'
		OutputColoredText $null ""

		OutputColoredText $null '   $RECORDING_HOSTS ['$RECORDING_HOSTS' ]:'
		OutputColoredText $null '       These are executables that trigger that a given media is being recorded, utilized by handle.exe.  You can also specify SMB access information in the format "CLIENTMACHINE - SERVER\UserName".'
		OutputColoredText $null ""
	}

	if($scriptFile -in @("Get-Media.ps1", "Check-MediaSubscription.ps1", "Store-MediaCredential.ps1", "Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $PYTHON_BINARY ['$PYTHON_BINARY' ]:'
		OutputColoredText $null '       Location of Python. Needed if YouTube-DL is not a standalone executable and for Plex integration.'
		OutputColoredText $null ""

		OutputColoredText $null '   $YOUTUBE_DL_BINARY ['$YOUTUBE_DL_BINARY' ]:'
		OutputColoredText $null '       Location of the YouTube-DL tool. Used to acquire media content from the sources specified in the subscription files.'
		OutputColoredText $null ""
	}

	if($scriptFile -in @("Get-MediaSubtitles.ps1", "Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $EMBED_SUBTITLES ['$EMBED_SUBTITLES' ]:'
		OutputColoredText $null '       Embed acquired subtitle files into the media.'
		OutputColoredText $null ""

		OutputColoredText $null '   $OPENSUBTITLES_CREDENTIAL ['$OPENSUBTITLES_CREDENTIAL' ]:'
		OutputColoredText $null '       Credential ID for a configured account to OpenSubtitles.org. This is needed to download and embed subtitles.'
		OutputColoredText $null ""

		OutputColoredText $null '   $OPENSUBTITLES_LANGUAGE_CODE ['$OPENSUBTITLES_LANGUAGE_CODE' ]:'
		OutputColoredText $null '       Default ISO 639 code to search for subtitles. This is needed to download and embed subtitles.'
		OutputColoredText $null ""
	}

	if($scriptFile -in @("Notify-NewSeason.ps1", "Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $NEW_SEASON_LEAD_NOTICE_DAYS ['$NEW_SEASON_LEAD_NOTICE_DAYS' ]:'
		OutputColoredText $null '       If a new season is found with an episode starting this many days in the future, the Notify-NewSeason.ps1 script will notify you.'
		OutputColoredText $null ""
	}

	if($scriptFile -in @("Audit-MediaLibraries.ps1", "Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $AUDIT_EXCLUSIONS:'
		OutputColoredText $null '       Ignore items in these paths when auditing libraries.'
		OutputColoredText $null ""

		OutputColoredText $null '   $AUDIT_DUPLICATES ['$AUDIT_DUPLICATES' ]:'
		OutputColoredText $null '       Check all library items for duplicates.'
		OutputColoredText $null ""

		OutputColoredText $null '   $AUDIT_EPISODE_NAMES ['$AUDIT_EPISODE_NAMES' ]:'
		OutputColoredText $null '       Check all episodic library items for incorrect naming.'
		OutputColoredText $null ""

		OutputColoredText $null '   $AUDIT_SEASON_DIRECTORIES ['$AUDIT_SEASON_DIRECTORIES' ]:'
		OutputColoredText $null '       Check directory names for incorrect season format.'
		OutputColoredText $null ""

		OutputColoredText $null '   $AUDIT_SIZES ['$AUDIT_SIZES' ]:'
		OutputColoredText $null '       Check all library items a very small size, 5KB. This is useful for flagging corrupted or incorrectly converted media.'
		OutputColoredText $null ""

		OutputColoredText $null '   $AUDIT_EPISODE_CONTINUITY ['$AUDIT_EPISODE_CONTINUITY' ]:'
		OutputColoredText $null '       Analyze episodic library items for possible missing episodes.'
		OutputColoredText $null ""

		OutputColoredText $null '   $AUDIT_MOVIES ['$AUDIT_MOVIES' ]:'
		OutputColoredText $null '       Check all library movies for possible metadata and title mismatches.'
		OutputColoredText $null ""

		OutputColoredText $null '   $AUDIT_PLEX ['$AUDIT_PLEX' ]:'
		OutputColoredText $null '       Scan Plex library items for missing content and incorrect watch state.'
		OutputColoredText $null ""

		OutputColoredText $null '   $AUDIT_KODI ['$AUDIT_KODI' ]:'
		OutputColoredText $null '       Scan Kodi library items for missing content and incorrect watch state.'
		OutputColoredText $null ""
	}

	if($scriptFile -in @("Convert-Media.ps1", "Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $MAKEMKV_BINARY ['$MAKEMKV_BINARY' ]:'
		OutputColoredText $null '       Location of MakeMKV, which is used to convert ISO files.'
		OutputColoredText $null ""

		OutputColoredText $null '   $ISO_CONVERT_PATH ['$ISO_CONVERT_PATH' ]:'
		OutputColoredText $null '       When an ISO file is converted, its resulting files are copied to a folder in this path. The intention here is that this is shared via Plex for streaming to clients.'
		OutputColoredText $null ""
	}

	if($scriptFile -in @("Copy-MediaAudio.ps1", "Transfer-Media.ps1", "Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $MEDIA_TRANSFER_PROFILES:'
		OutputColoredText $null '       Configures profiles that Transfer-Media.ps1 can use to conveniently expand parameters. See the readme file for more details.'
		OutputColoredText $null ""
	}

	if($scriptFile -in @("Remove-MediaAds.ps1", "Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $COMSKIP_BINARY ['$COMSKIP_BINARY' ]:'
		OutputColoredText $null '       Location where COMSKIP tool is located. This is used to transfer metadata for renamed media and to stitch together media after ads have been identified to remove them.'
		OutputColoredText $null ""

		OutputColoredText $null '   $COMSKIP_PROFILE_PATH ['$COMSKIP_PROFILE_PATH' ]:'
		OutputColoredText $null '       Location where individual ComSkip profiles are located. You can configure individual profile files via the $COMSKIP_PROFILE setting on a per-subscription basis.'
		OutputColoredText $null ""

		OutputColoredText $null '   $FFMPEG_BINARY ['$FFMPEG_BINARY' ]:'
		OutputColoredText $null '       Location where FFMPEG tool is located. This is used to transfer metadata for renamed media and to stitch together media after ads have been identified to remove them.'
		OutputColoredText $null ""

		OutputColoredText $null '   $FFPROBE_BINARY ['$FFPROBE_BINARY' ]:'
		OutputColoredText $null '       Location where $FFPROBE tool is located.'
		OutputColoredText $null ""

		OutputColoredText $null '   $AD_REMOVE_DIAGNOSTICS ['$AD_REMOVE_DIAGNOSTICS' ]:'
		OutputColoredText $null '       If toggled, exports videoclips marked as ads into the purge folder for debugging purposes.'
		OutputColoredText $null ""
	}

	if($scriptFile -in @("Start-MediaSession.ps1"))
	{
		OutputColoredText $null '   $STARTUP_VERBOSE_ALIASES ['$STARTUP_VERBOSE_ALIASES' ]:'
		OutputColoredText $null '       When starting a new session, show additional alias options available to execute.'
		OutputColoredText $null ""

		OutputColoredText $null '   $POLLING_SCRIPTS:'
		OutputColoredText $null '       Registry of scripts and what regularity they should be executed. Note that this can be edited to remove scripts you do not want to execute, as well as add scripts to extend functionality.'
		OutputColoredText $null ""

		OutputColoredText $null '   $MAX_CACHE_AGE_DAYS ['$MAX_CACHE_AGE_DAYS' ]:'
		OutputColoredText $null '       After this time, the caches will be cleaned and pruned by the script that uses them.'
		OutputColoredText $null ""

		OutputColoredText $null '   $TASK_SCHEDULING ['$TASK_SCHEDULING' ]:'
		OutputColoredText $null '       Advanced diagnostic. Causes polling script to output more information about which script will run next at what time.'
		OutputColoredText $null ""

		OutputColoredText $null '   $SHOW_ERROR_EXCEPTIONS ['$SHOW_ERROR_EXCEPTIONS' ]:'
		OutputColoredText $null '       Advanced diagnostic. Displays full error exceptions to console while executing.'
		OutputColoredText $null ""

		OutputColoredText $null '   $ERROR_EXCLUSION_BINARY ['$ERROR_EXCLUSION_BINARY' ]:'
		OutputColoredText $null '       These binaries are excluded from checks for standard error output when executing them.'
		OutputColoredText $null ""

		OutputColoredText $null '   $EXTRACT_HANDLE_DIAGNOSTICS ['$EXTRACT_HANDLE_DIAGNOSTICS' ]:'
		OutputColoredText $null '       Advanced diagnostic. If a file conflict is detected, handle.exe outputs process information in a separate file.'
		OutputColoredText $null ""

		OutputColoredText $null '   $EXTENDED_CHAR_DIAGNOSTICS ['$EXTENDED_CHAR_DIAGNOSTICS' ]:'
		OutputColoredText $null '       Advanced diagnostic. Displays considerations around extended characters when processing media.'
		OutputColoredText $null ""

		OutputColoredText $null '   $PERFORMANCE_DIAGNOSTICS ['$PERFORMANCE_DIAGNOSTICS' ]:'
		OutputColoredText $null '       Advanced diagnostic. Exports last media used cache usage file to diagnose unused cache entries and throws when misused.'
		OutputColoredText $null ""
	}

	OutputColoredText $null ""
}

#################################
#
# FUNCTION IsProbablyPattern
#
#################################
function IsProbablyPattern([string] $inputString)
{
	if(-not (IsStringDefined $inputString))
	{
		return $false
	}

	#This is called before IsValidPath is defined
	if(Test-Path -LiteralPath $inputString)
	{
		return $false
	}

	if($inputString.Contains('%'))
	{
		return $false
	}

	if($inputString.Contains('$') -and -not ($inputString.EndsWith('$')))
	{
		return $false
	}

	if($inputString -match '^(?:[A-Z]|[a-z]):\\')
	{
		return $false
	}

	return $true
}

#################################
#
# FUNCTION ExpandAndFixPath
#
#################################
function ExpandAndFixPath($path)
{
	return (ExpandAndFixPath $path, $true)
}

#################################
#
# FUNCTION ExpandAndFixPath
#
#################################
function ExpandAndFixPath($path, $verbose)
{
	if((-not $verbose) -and $VERBOSE_MODE)
	{
		$verbose = $true
	}

	if($path -eq $null)
	{
		return $null
	}

	if($path.GetType().ToString() -ne "System.String")
	{
		LogWarning Path is of an invalid type: [$path] [($path.GetType())]
		return $null
	}

	if($path.Length -eq 0)
	{
		return $null
	}

	if(IsProbablyPattern $path)
	{
		if($verbose)
		{
			LogWarning Path might be a pattern: [$path]
		}
	}
	$oldPath = $path
	$path = $path.Replace('$SCRIPT_PATH', $SCRIPT_PATH)
	$path = $path.Replace('$DOWNLOADED_TV_PATH', $DOWNLOADED_TV_PATH)
	$path = $path.Replace('$DOWNLOADED_MOVIES_PATH', $DOWNLOADED_MOVIES_PATH)
	$path = $path.Replace('$DOWNLOADED_PODCASTS_PATH', $DOWNLOADED_PODCASTS_PATH)
	$path = $path.Replace('$PURGE_PATH', $PURGE_PATH)
	$path = $path.Replace('$CONSUMED_FOLDER_NAME', $CONSUMED_FOLDER_NAME)
	$path = $path.Replace('$ARCHIVE_FOLDER_NAME', $ARCHIVE_FOLDER_NAME)
	$path = $path.Replace('$DAILY_FOLDER_NAME', $DAILY_FOLDER_NAME)
	$path = [System.Environment]::ExpandEnvironmentVariables($path)
	$path = $path.TrimEnd('\', '/')
	if((-not $path.StartsWith("\\")) -and (-not $path.ToLower().StartsWith("smb:")))
	{
		$path = $path.Replace("\\", "\")
	}
	$path = $path.Replace("//", "/")

	foreach ($currentInvalidChar in [System.IO.Path]::GetInvalidFileNameChars())
	{
		if(($currentInvalidChar -eq '\') -or ($currentInvalidChar -eq ':'))
		{
			continue
		}

		if($path.Contains($currentInvalidChar))
		{
			if($verbose)
			{
				LogWarning Path contains invalid characters: [$path] [$currentInvalidChar]
			}
		}
	}

	$directoryFullName = $path
	if(-not ($path.EndsWith([System.IO.Path]::GetFileNameWithoutExtension($path))))
	{
		$directoryFullName = [System.IO.Path]::GetDirectoryName($path)
	}
	if($directoryFullName.Contains('$') -or $directoryFullName.Contains('%'))
	{
		if($verbose)
		{
			LogWarning Path contains invalid characters: [$directoryFullName] ['$' '%']
		}
	}

	if($path -ne $oldPath)
	{
		DebugLogInfo LOADED PATH: [$path]
	}
	return $path
}

#################################
#
# FUNCTION ExpandLibraryPathList
#
#################################
function ExpandLibraryPathList($pathCollection)
{
	$convertedExclusionList = New-Object -TypeName System.Collections.ArrayList
	foreach($currentExclusion in $pathCollection)
	{
		if($currentExclusion.Count -gt 1)
		{
			foreach($currentSubExclusion in (ExpandLibraryPathList $currentExclusion))
			{
				$convertedExclusionList.Add($currentSubExclusion) > $null
			}
		}
		elseif(IsProbablyPattern $currentExclusion)
		{
			$convertedExclusionList.Add($currentExclusion) > $null
		}
		else
		{
			$convertedExclusionList.Add((ExpandAndFixPath $currentExclusion)) > $null
		}
	}
	return $convertedExclusionList
}

#################################
#
# FUNCTION ExpandLibraryPaths
#
#################################
function ExpandLibraryPaths()
{
	if($Remote)
	{
		return
	}

	if($CONFIG_FILE -eq $null)
	{
		$SCRIPT_PATH = Split-Path -parent $PSCommandPath
		if(-not (Test-Path -LiteralPath $SCRIPT_PATH\CinAdmin.Configuration.ps1))
		{
			LogError Could not load Configuration file. Exiting...
			exit
		}
		. $CONFIG_FILE
	}

	$Script:DOWNLOADED_TV_PATH = ExpandAndFixPath $DOWNLOADED_TV_PATH
	$Script:DOWNLOADED_PODCASTS_PATH = ExpandAndFixPath $DOWNLOADED_PODCASTS_PATH
	$Script:DOWNLOADED_MOVIES_PATH = ExpandAndFixPath $DOWNLOADED_MOVIES_PATH
	$Script:ISO_CONVERT_PATH = ExpandAndFixPath $ISO_CONVERT_PATH
	$Script:PURGE_PATH = ExpandAndFixPath $PURGE_PATH
	$Script:DATA_PATH   = ExpandAndFixPath $DATA_PATH

	$Script:DOWNLOADED_PATH_EXCLUSIONS = (ExpandLibraryPathList $DOWNLOADED_PATH_EXCLUSIONS)
	$Script:AUDIT_EXCLUSIONS = (ExpandLibraryPathList $AUDIT_EXCLUSIONS)

	$convertedOverrideList = @{}
	foreach($currentPathOverride in $ORGANIZE_PATH_OVERRIDE.Keys)
	{
		$convertedOverrideList.Add($currentPathOverride, (ExpandAndFixPath ($ORGANIZE_PATH_OVERRIDE[$currentPathOverride])))
	}
	$Script:ORGANIZE_PATH_OVERRIDE = $convertedOverrideList

	$convertedOverrideList = @{}
	foreach($currentPathOverride in $CONSUMED_PATH_OVERRIDE.Keys)
	{
		$convertedOverrideList.Add($currentPathOverride, (ExpandAndFixPath ($CONSUMED_PATH_OVERRIDE[$currentPathOverride])))
	}
	$Script:CONSUMED_PATH_OVERRIDE = $convertedOverrideList
}

##GLOBAL##
ExpandLibraryPaths

#################################
#
# FUNCTION IsInvalidPathLenient
#
#################################
function IsInvalidPathLenient($path)
{
	return (IsInvalidPath $path)
}

#################################
#
# FUNCTION IsValidPath
#
#################################
function IsValidPath($path)
{
	if(-not (IsStringDefined $path))
	{
		return $false
	}

	return (test-path -LiteralPath $path)
}

#################################
#
# FUNCTION IsInvalidPath
#
#################################
function IsInvalidPath($path)
{
	return (-not (IsValidPath $path))
}

#########################
#
# FUNCTION LoadPath
#
#########################
function LoadPath($path)
{
	return (LoadPath $path, $true)
}

#########################
#
# FUNCTION LoadPath
#
#########################
function LoadPath($path, $verbose)
{
	if((-not $verbose) -and $VERBOSE_MODE)
	{
		$verbose = $true
	}

	if(-not (IsStringDefined $path))
	{
		if($verbose)
		{
			LogWarning String not defined.
		}
		return $null
	}

	$path = ExpandAndFixPath $path $verbose

	if(IsInvalidPath $path)
	{
		if($verbose)
		{
			LogWarning Invalid Path Specified: [$path]
		}
	}
	return $path
}

############################
#
# FUNCTION BuildSymlinkTable
#
############################
function BuildSymlinkTable()
{
	if($Script:SYMLINK_TABLE -eq $null)
	{
		$newSymTable = @{}
		foreach($currentPath in @($DOWNLOADED_TV_PATH, (DeriveWatchedPath $DOWNLOADED_TV_PATH), $DOWNLOADED_PODCASTS_PATH, (DeriveWatchedPath $DOWNLOADED_PODCASTS_PATH), $DOWNLOADED_MOVIES_PATH, (DeriveWatchedPath $DOWNLOADED_MOVIES_PATH)))
		{
			if(IsInvalidPath $currentPath) { continue }

			$symLinks = Get-ChildItem -LiteralPath $currentPath  -FollowSymlink -Recurse | Where-Object { $_.LinkType -ne $null}
			foreach($currentLink in $symLinks)
			{
				#NOTE: Windows PowerShell wraps Targets in an array
				$currentLinkTarget = FlattenArguments ($currentLink.Target)
				if(-not $newSymTable.ContainsKey($currentLinkTarget))
				{
					DebugLogInfo SYMLINK [($currentLinkTarget)] --> [($currentLink.FullName)]
					$newSymTable.Add($currentLinkTarget, $currentLink.FullName)
				}
			}
		}
		$Script:SYMLINK_TABLE = $newSymTable

		$replaceCsv = $false
		if((IsValidPath $SYMLINK_CSV) -and ($Script:SYMLINK_TABLE.Count -gt 0))
		{
			if($Script:SYMLINK_TABLE.Count -ne (Import-Csv -LiteralPath $SYMLINK_CSV).Count)
			{
				$replaceCsv = $true
			}

			foreach($currentCsvEntry in (Import-Csv -LiteralPath $SYMLINK_CSV))
			{
				if($Script:SYMLINK_TABLE.ContainsKey($currentCsvEntry.Target) -and $Script:SYMLINK_TABLE[$currentCsvEntry.Target] -eq $currentCsvEntry.FullName)
				{
					if((IsValidPath $currentCsvEntry.Target) -and (IsValidPath $currentCsvEntry.FullName))
					{
						DebugLogInfo Verified Symlink: [($currentCsvEntry.Target)] [($currentCsvEntry.FullName)]
					}
					else
					{
						$Script:SYMLINK_TABLE = $null
						LogError SYMLINK MISSING. Please delete the archive file ($SYMLINK_CSV) or rerun your linking commands, like mklink. [($currentCsvEntry.Target)] [($currentCsvEntry.FullName)]
						QuitIfError
					}
				}
				else
				{
					$Script:SYMLINK_TABLE = $null
					LogError SYMLINK MISSING. Please delete the archive file ($SYMLINK_CSV) or rerun your linking commands, like mklink. [($currentCsvEntry.Target)] [($currentCsvEntry.FullName)]
					QuitIfError
				}
			}
		}
		else
		{
			$replaceCsv = $true
		}

		if($replaceCsv)
		{
			DeleteItem $SYMLINK_CSV

			if($Script:SYMLINK_TABLE.Count -gt 0)
			{
				foreach($currentEntry in $Script:SYMLINK_TABLE.Keys)
				{
					DebugLogInfo Remembering Symlink: [($currentEntry)] [($Script:SYMLINK_TABLE[$currentEntry])]
					$csvEntry = New-Object -TypeName PSObject -Property @{ "Target" = $currentEntry; "FullName" = ($Script:SYMLINK_TABLE[$currentEntry]); }
					AddCsvEntry $csvEntry $SYMLINK_CSV
				}
			}
		}
	}

	return $Script:SYMLINK_TABLE
}

#########################
#
# FUNCTION CopyFileBatch
#
#########################
function CopyFileBatch($files, $targetPath, $folderName)
{
	$targetPath = [System.IO.Path]::Combine($targetPath, $folderName)
	$sourcePath = [System.IO.Path]::Combine($SCRIPT_PATH, $folderName)
	foreach ($currentScript in $files)
	{
		$currentFileName = [System.IO.Path]::GetFileName($currentScript)
		$sourceScript = [System.IO.Path]::Combine($sourcePath, $currentFileName)
		if(-not (Test-Path -LiteralPath $sourceScript))
		{
			$possible = Get-ChildItem -Recurse -Literal $sourcePath -Filter $currentScript
			if($possible -is "System.IO.FileSystemInfo")
			{
				$sourceScript = $possible.FullName
			}
		}

		$targetScript = [System.IO.Path]::Combine($targetPath, $currentFileName)
		CreateDirInPath $targetScript
		Copy-Item -LiteralPath $sourceScript -Destination $targetScript -Force
		LogInfo INSTALL: "$sourceScript -->  $targetScript"
		QuitIfError
	}
}

################
#
# FUNCTION AuditInstall
#
################
function AuditInstall($sourcePath, $targetPath)
{
	$sourceFiles = Get-ChildItem -Recurse -Literal $sourcePath -Filter *.ps1 -File
	$targetFiles = Get-ChildItem -Recurse -Literal $targetPath -Filter *.ps1 -File

	$sourcePath = $sourcePath.Trim('\')
	$targetPath = $targetPath.Trim('\')

	if($sourceFiles.Length -ne $targetFiles.Length)
	{
		LogWarning Installation has different number of script files: [Source: ($sourceFiles.Length)] [Target: ($targetFiles.Length)]
	}

	foreach ($currentSourceFile in $sourceFiles)
	{
		$currentTargetFileName = $currentSourceFile.FullName.Replace($sourcePath, $targetPath)
		#LogInfo Compare [($currentSourceFile.FullName)] [$currentTargetFileName]
		if(-not (Test-Path -LiteralPath $currentTargetFileName))
		{
			LogWarning Target not found: [($currentSourceFile.Name)]
		}
	}

	foreach ($currentTargetFile in $targetFiles)
	{
		$currentSourceFileName = $currentTargetFile.FullName.Replace($targetPath, $sourcePath)
		#LogInfo Compare [($currentTargetFile.FullName)] [$currentSourceFileName]
		if((-not (Test-Path -LiteralPath $currentSourceFileName)) -and (-not $currentSourceFileName.Contains(".dev.")))
		{
			LogWarning Source not found: [($currentTargetFile.Name)]
		}
	}
}

#################################
#
# FUNCTION ConfigureRemoteSSL
#
#################################
function ConfigureRemoteSSL()
{
	if($PSEdition -ne 'Core')
	{
		LogError "Installing remote Powershell over SSL requires running under PowerShell Core."
		QuitIfError
	}

	LogInfo Configuring PowerShell remoting over SSL...

	Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0
	Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0

	Set-Service -Name sshd -StartupType 'Automatic'

	Start-Service sshd
	LogInfo Started SSHD service.

	Install-Module -Name Microsoft.PowerShell.RemotingTools
	Enable-SSHRemoting -Force

	Restart-Service sshd
	LogInfo Restarted SSHD service.
}

#################################
#
# FUNCTION InstallScheduledTask
#
#################################
function InstallScheduledTask([string] $windowStyle)
{
	if(IsInvalidPath $POWERSHELL_FILE)
	{
		LogError "PowerShell executable not detected. No new task was created."
		PauseIfClicked
		return
	}

	$executingUser = [Security.Principal.WindowsIdentity]::GetCurrent().Name

	$powershellFileInTask = $POWERSHELL_FILE
	$powershellFileInTask = $powershellFileInTask.Replace($env:ProgramFiles, "%ProgramFiles%")
	$powershellFileInTask = $powershellFileInTask.Replace($env:SystemRoot, "%SystemRoot%")

	if(IsValidPath $POLL_SHORTCUT)
	{
		$taskArguments = '-WindowStyle Minimized -ExecutionPolicy Bypass -NoProfile -Command "&amp;(' + "'" + $POLL_SHORTCUT + "'" + ')"'
	}
	else
	{
		$taskArguments = '-WindowStyle ' + $windowStyle + ' -ExecutionPolicy Bypass -NoProfile "' + $POLL_COMMAND + '"'
	}

	$scheduledTaskDefinition =
	'<?xml version="1.0" encoding="UTF-16"?>
	<Task version="1.4" xmlns="http://schemas.microsoft.com/windows/2004/02/mit/task">
	  <RegistrationInfo>
		<Date>2016-02-08T14:57:34.8095236</Date>
		<Author>ParadoxGBB</Author>
		<URI>\' + $SCHEDULED_TASK_NAME + '</URI>
		<SecurityDescriptor></SecurityDescriptor>
	  </RegistrationInfo>
	  <Triggers>
		<LogonTrigger>
		  <Enabled>true</Enabled>
		  <UserId>' + $executingUser + '</UserId>
		</LogonTrigger>
	  </Triggers>
	  <Principals>
		<Principal id="Author">
		  <UserId>' + $executingUser + '</UserId>
		  <LogonType>InteractiveToken</LogonType>
		  <RunLevel>HighestAvailable</RunLevel>
		</Principal>
	  </Principals>
	  <Settings>
		<MultipleInstancesPolicy>IgnoreNew</MultipleInstancesPolicy>
		<DisallowStartIfOnBatteries>false</DisallowStartIfOnBatteries>
		<StopIfGoingOnBatteries>true</StopIfGoingOnBatteries>
		<AllowHardTerminate>false</AllowHardTerminate>
		<StartWhenAvailable>false</StartWhenAvailable>
		<RunOnlyIfNetworkAvailable>false</RunOnlyIfNetworkAvailable>
		<IdleSettings>
		  <Duration>PT10M</Duration>
		  <WaitTimeout>PT1H</WaitTimeout>
		  <StopOnIdleEnd>true</StopOnIdleEnd>
		  <RestartOnIdle>false</RestartOnIdle>
		</IdleSettings>
		<AllowStartOnDemand>true</AllowStartOnDemand>
		<Enabled>true</Enabled>
		<Hidden>false</Hidden>
		<RunOnlyIfIdle>false</RunOnlyIfIdle>
		<DisallowStartOnRemoteAppSession>false</DisallowStartOnRemoteAppSession>
		<UseUnifiedSchedulingEngine>true</UseUnifiedSchedulingEngine>
		<WakeToRun>false</WakeToRun>
		<ExecutionTimeLimit>PT0S</ExecutionTimeLimit>
		<Priority>7</Priority>
	  </Settings>
	  <Actions>
		<Exec>
		  <Command>' + $powershellFileInTask + '</Command>
		  <Arguments>' + $taskArguments + '</Arguments>
		  <WorkingDirectory>' + $SCRIPT_PATH + '</WorkingDirectory>
		</Exec>
	  </Actions>
	</Task>'

	$task = Get-ScheduledTask -TaskName $SCHEDULED_TASK_NAME -ErrorAction Ignore
	if($task -ne $null)
	{
		LogWarning "Scheduled Task was already found. No new task was created."
	}
	else
	{
		VerboseLogInfo Task XML Defintion:
		VerboseLogInfo $scheduledTaskDefinition
		if($WHATIF_MODE)
		{
			LogInfo WHATIF: [Register-ScheduledTask -Xml $scheduledTaskDefinition -TaskName $SCHEDULED_TASK_NAME]
		}
		else
		{
			(Register-ScheduledTask -Xml $scheduledTaskDefinition -TaskName $SCHEDULED_TASK_NAME) > $null
			LogInfo "Task '$SCHEDULED_TASK_NAME' created."
		}
	}

	if(IsCurrentlyPolling)
	{
		LogWarning "Detected that the polling command [$POLL_COMMAND] is currently already executing (or an executing PowerShell window title contains '$POLLING_WIN_TITLE'). Please close it."
		return
	}
	StartScheduledTask
}

#################################
#
# FUNCTION StartScheduledTask
#
#################################
function StartScheduledTask()
{
	$task = Get-ScheduledTask -TaskName $SCHEDULED_TASK_NAME -ErrorAction Ignore
	if($task -eq $null)
	{
		LogError "Cannot start scheduled task: task $SCHEDULED_TASK_NAME' not found."
		return
	}

	if(-not $task.Settings.Enabled)
	{
		if($WHATIF_MODE)
		{
			LogInfo WHATIF: [$task | Enable-ScheduledTask > $null]
		}
		else
		{
			$task | Enable-ScheduledTask > $null
			LogInfo Task enabled.
		}
	}

	if($task.State -NotMatch 'Running')
	{
		if($WHATIF_MODE)
		{
			LogInfo WHATIF: [$task | Start-ScheduledTask > $null]
		}
		else
		{
			$task | Start-ScheduledTask > $null
			LogInfo "Task '$SCHEDULED_TASK_NAME' is now running."
		}

		for($cTries=1; $cTries -le 3; $cTries++)
		{
			if(IsCurrentlyPolling)
			{
				break
			}
			SleepFor 5
		}

		if(-not (IsCurrentlyPolling))
		{
			LogError "Task is not correctly running. Please manually test execution of '$POLL_COMMAND'."
		}
	}
}

#################################
#
# FUNCTION CreateShortcut
#
#################################
function CreateShortcut($targetShortCutFile, $executable, $workingDir, $arguments, $icon, $requireAdmin)
{
	LogInfo Creating: [$targetShortCutFile]
	CreateDirInPath $targetShortCutFile

	$WshShell = New-Object -ComObject WScript.Shell
	$Shortcut = $WshShell.CreateShortcut($targetShortCutFile)
	$Shortcut.TargetPath = $executable
	$Shortcut.WorkingDirectory = $workingDir
	$Shortcut.Arguments = $arguments
	$Shortcut.IconLocation = $icon
	$Shortcut.Save()

	#REQUIRE ADMIN ACCESS
	if($requireAdmin)
	{
		$bytes = [System.IO.File]::ReadAllBytes($targetShortCutFile)
		$bytes[0x15] = $bytes[0x15] -bor 0x20
		[System.IO.File]::WriteAllBytes($targetShortCutFile, $bytes)
	}
}

#################################
#
# FUNCTION IsWTInstalled
#
#################################
function IsWTInstalled()
{
	if(($WINDOWS_TERMINAL_SETTINGS_JSON -ne $null) -and ($WINDOWS_TERMINAL_BINARY -ne $null))
	{
		return $true
	}

	if(IsValidPath ($env:LOCALAPPDATA + "\Packages\Microsoft.WindowsTerminalPreview_8wekyb3d8bbwe"))
	{
		$Script:WINDOWS_TERMINAL_SETTINGS_JSON = ($env:LOCALAPPDATA + "\Packages\Microsoft.WindowsTerminalPreview_8wekyb3d8bbwe\LocalState\settings.json")
		$Script:WINDOWS_TERMINAL_BINARY = ($env:USERPROFILE + "\AppData\Local\Microsoft\WindowsApps\Microsoft.WindowsTerminalPreview_8wekyb3d8bbwe\wt.exe")
		return $true
	}

	if(IsValidPath ($env:LOCALAPPDATA + "\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe"))
	{
		$Script:WINDOWS_TERMINAL_SETTINGS_JSON = ($env:LOCALAPPDATA + "\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json")
		$Script:WINDOWS_TERMINAL_BINARY = ($env:USERPROFILE + "\AppData\Local\Microsoft\WindowsApps\Microsoft.WindowsTerminal_8wekyb3d8bbwe\wt.exe")
		return $true
	}

	return $false
}

#################################
#
# FUNCTION CreateWTProfile
#
#################################
function CreateWTProfile($name, $guid, $executable, $workingDir, $icon)
{
	if(IsInvalidPath $WINDOWS_TERMINAL_SETTINGS_JSON)
	{
		LogError Windows Terminal Settings file not found: [$WINDOWS_TERMINAL_SETTINGS_JSON]
		return
	}

	$wtSettings = Get-Content -Raw -LiteralPath ($WINDOWS_TERMINAL_SETTINGS_JSON) | ConvertFrom-Json
	foreach ($currentWtProfile in $wtSettings.profiles.list)
	{
		if($currentWtProfile.guid -eq $guid)
		{
			LogWarning Windows Terminal Profile Already Exists: [$name] [$guid]
			return
		}
	}

	LogInfo Adding Windows Terminal Profile: [$name]

	$profileToAdd = [PSCustomObject]@{
		'guid'              = $guid
		'hidden'            = $false
		'elevate'           = $true #Currently only in preview
		'startingDirectory' = $workingDir
		'commandline'       = $executable
		'name'              = $name
		'icon'              = $icon
	}
	$wtSettings.profiles.list += $profileToAdd

	$backupIdentifier = [System.IO.Path]::GetFileNameWithoutExtension($icon)

	$cNumber = -1
	$backupSettings = $WINDOWS_TERMINAL_SETTINGS_JSON.Replace(".json", ".json." + $backupIdentifier)
	while (IsValidPath $backupSettings)
	{
		$cNumber++
		$backupSettings = $WINDOWS_TERMINAL_SETTINGS_JSON.Replace(".json", ".json." + $backupIdentifier + "." + $cNumber)
	}
	Rename-Item -LiteralPath $WINDOWS_TERMINAL_SETTINGS_JSON -NewName $backupSettings -Force

	$wtSettings | ConvertTo-Json -Depth 3 | SafeOutFile -LiteralPath $WINDOWS_TERMINAL_SETTINGS_JSON
}

#################################
#
# FUNCTION ConfigureCinAdmin
#
#################################
function ConfigureCinAdmin([string] $refreshPath, [bool] $popupConfig)
{
	$originalPath = $SCRIPT_PATH
	if($refreshPath -eq $null)
	{
		$refreshPath = $SCRIPT_PATH
	}

	$SCRIPT_PATH = $refreshPath
	$CONFIG_FILE = $CONFIG_FILE.Replace($originalPath, $refreshPath)
	$GENERIC_FUNCTION_SCRIPT = $GENERIC_FUNCTION_SCRIPT.Replace($originalPath, $refreshPath)
	$FUNCTION_SCRIPT = $FUNCTION_SCRIPT.Replace($originalPath, $refreshPath)

	$configfileName = (Get-Item -LiteralPath $CONFIG_FILE).Name

	if($popupConfig)
	{
		LogInfo Edit the following file and save / close all instances when finished.
		notepad $CONFIG_FILE

		$canContinue = $false
		while (-not $canContinue)
		{
			Start-Sleep -Seconds 5

			$proc = Get-Process notepad -ErrorAction Ignore | Where-Object -FilterScript { $_.MainWindowTitle -match $configfileName }
			if($proc -eq $null)
			{
				$canContinue = $true
			}
		}
	}

	foreach ($currentScript in @($CONFIG_FILE, $GENERIC_FUNCTION_SCRIPT, $FUNCTION_SCRIPT))
	{
		try
		{
			$Error.Clear()
			LogInfo CHECKING and REFRESHING: [$currentScript]
			. $currentScript
		}
		catch
		{
			LogError DETECTED ERROR IN: [$currentScript]
			$Error
		}
	}

	LogInfo Finished.
}

#################################
#
# FUNCTION IsHandleInstalled
#
#################################
function IsHandleInstalled()
{
	$binaryExists = -not (IsInvalidPath $HANDLE_BINARY)

	if($binaryExists)
	{
		if((Get-ItemProperty -Path "HKCU:\SOFTWARE\Sysinternals\Handle" -Ea SilentlyContinue).EulaAccepted -ne 1)
		{
			LogWarning Handle.exe does not appear to have its EULA accepted. Please run this tool manually to accept it.
			return $false
		}
	}
	else
	{
		LogWarning "Could not find Handle executable [$HANDLE_BINARY]. Please download it at 'https://docs.microsoft.com/en-us/sysinternals/downloads/handle'."
	}

	return ($binaryExists)
}

#################################
#
# FUNCTION IsPlayOnInstalled
#
#################################
function IsPlayOnInstalled()
{
	$isDisabled = (Get-Service "MediaMall Server").StartType -eq "Disabled"
	return (-not ((IsInvalidPath $RECORDING_DATABASE) -or (IsInvalidPath $SQL_LITE) -or ($isDisabled)))
}

#################################
#
# FUNCTION UseTvDB
#
#################################
function UseTvDB()
{
	return ($USE_TVDB)
}

#################################
#
# FUNCTION TestConfiguration
#
#################################
function TestConfiguration()
{
	#BLOCK ISE EXECUTION
	if(IsISE)
	{
		Write-Error "These scripts cannot execute in the PowerShell ISE."
		exit
	}

	#ENFORCE PS Version
	if($PSVersionTable.PSVersion.Major -lt 4)
	{
		Write-Error "These scripts require PowerShell 4.0 or higher to execute."
		exit
	}

	#ENFORCE WINDOWS (for now)
	if(-not (IsRunningOnWindows))
	{
		Write-Error "These scripts require Microsoft Windows to execute."
		exit
	}

	#Need relaxed Execution Policy
	if((Get-ExecutionPolicy) -eq 'Restricted')
	{
		Write-Error "These scripts require a more relaxed execution policy, like RemoteSigned. Please execute 'Set-ExecutionPolicy RemoteSigned'."
		exit
	}

	if($WHATIF_MODE)
	{
		WarnIfUnusedExtensions
	}

	ValidateConfiguration
}

#################################
#
# FUNCTION WarnIfUnusedExtensions
#
#################################
function WarnIfUnusedExtensions()
{
	LogInfo Validating Extension Validity...
	$extensionFileCounts = @{}

	foreach($currentExtension in $MEDIA_EXTENSIONS)
	{
		$extensionFileCounts.Add($currentExtension, 0)
	}

	foreach($currentPossiblePath in @($DOWNLOADED_TV_PATH, $DOWNLOADED_PODCASTS_PATH, $DOWNLOADED_MOVIES_PATH))
	{
		if(IsInvalidPath $currentPossiblePath) { continue }

		foreach($currentExtension in $MEDIA_EXTENSIONS)
		{
			$currentExtensionCount = $extensionFileCounts[$currentExtension]
			$currentExtensionCount = $currentExtensionCount + (Get-ChildItem -Recurse -File -LiteralPath $currentPossiblePath | Where-Object { $_.Extension -eq $currentExtension }).Count
			$extensionFileCounts[$currentExtension] = $currentExtensionCount
		}
	}
}

#################################
#
# FUNCTION ValidateConfiguration
#
#################################
function ValidateConfiguration()
{
	if($WHATIF_MODE)
	{
		LogInfo "Validating paths..."
	}

	if(IsInvalidPath $DOWNLOADED_TV_PATH)
	{
		LogWarning Path is not set or invalid: ['$DOWNLOADED_TV_PATH']:[$DOWNLOADED_TV_PATH]
	}

	if(IsInvalidPath $DOWNLOADED_MOVIES_PATH)
	{
		LogWarning Path is not set or invalid: ['$DOWNLOADED_MOVIES_PATH']:[$DOWNLOADED_MOVIES_PATH]
	}

	if(IsInvalidPath $PURGE_PATH)
	{
		LogWarning Path is not set or invalid: ['$PURGE_PATH']:[$PURGE_PATH]
	}

	if(IsInvalidPath $DOWNLOADED_PODCASTS_PATH)
	{
		LogWarning Path is not set or invalid: ['$DOWNLOADED_PODCASTS_PATH']:[$DOWNLOADED_PODCASTS_PATH]
	}

	#OPTIONAL

	if(IsInvalidPath $EXTENSION_INSTALL_PATH)
	{
		LogWarning Path is not set or invalid: ['$EXTENSION_INSTALL_PATH']:[$EXTENSION_INSTALL_PATH]
	}

	if(IsInvalidPath $SUBSCRIPTION_INSTALL_PATH)
	{
		LogWarning Path is not set or invalid: ['$SUBSCRIPTION_INSTALL_PATH']:[$SUBSCRIPTION_INSTALL_PATH]
	}

	if(IsInvalidPath $PLEX_API_INSTALL_PATH)
	{
		LogWarning Path is not set or invalid: ['$PLEX_API_INSTALL_PATH']:[$PLEX_API_INSTALL_PATH]
	}

	if(IsInvalidPath $DATA_PATH)
	{
		LogWarning Path is not set or invalid: ['$DATA_PATH']:[$DATA_PATH]
	}

	if(IsInvalidPath $TVDB_CACHE_PATH)
	{
		LogWarning Path is not set or invalid: ['$TVDB_CACHE_PATH']:[$TVDB_CACHE_PATH]
	}

	foreach ($currentPath in $DOWNLOADED_PATH_EXCLUSIONS)
	{
		if(IsInvalidPath $currentPath)
		{
			if($DEBUG_MODE -or $VERBOSE_MODE)
			{
				LogWarning "Path is not set or invalid, assuming pattern: " ['$DOWNLOADED_PATH_EXCLUSIONS']:[$currentPath]
			}
		}
	}

	foreach ($currentPath in $ORGANIZE_PATH_OVERRIDE.Values)
	{
		if(IsInvalidPath $currentPath)
		{
			LogWarning Path is not set or invalid: ['$ORGANIZE_PATH_OVERRIDE']:[$currentPath]
		}
	}

	foreach ($currentPath in $CONSUMED_PATH_OVERRIDE.Values)
	{
		if(IsInvalidPath $currentPath)
		{
			LogWarning Path is not set or invalid: ['$CONSUMED_PATH_OVERRIDE']:[$currentPath]
		}
	}

	if(($CONSUMED_FOLDER_NAME -eq $null) -or ($CONSUMED_FOLDER_NAME.Length -eq 0))
	{
		LogError Invalid '$CONSUMED_FOLDER_NAME': [$CONSUMED_FOLDER_NAME]
	}

	if(($ARCHIVE_FOLDER_NAME -eq $null) -or ($ARCHIVE_FOLDER_NAME.Length -eq 0))
	{
		if($DEBUG_MODE -or $VERBOSE_MODE)
		{
			LogWarning Invalid '$ARCHIVE_FOLDER_NAME'- assuming disabled: [$ARCHIVE_FOLDER_NAME]
		}
	}

	if(($DAILY_FOLDER_NAME -eq $null) -or ($DAILY_FOLDER_NAME.Length -eq 0))
	{
		if($DEBUG_MODE -or $VERBOSE_MODE)
		{
			LogWarning Invalid '$DAILY_FOLDER_NAME'- assuming disabled: [$DAILY_FOLDER_NAME]
		}
	}

	if(($EXTENSION_FOLDER -eq $null) -or ($EXTENSION_FOLDER.Length -eq 0))
	{
		LogError Invalid '$EXTENSION_FOLDER': [$EXTENSION_FOLDER]
	}

	if(($CONVERSION_FOLDER -eq $null) -or ($CONVERSION_FOLDER.Length -eq 0))
	{
		LogError Invalid '$CONVERSION_FOLDER': [$CONVERSION_FOLDER]
	}

	if(($PLEX_API_FOLDER -eq $null) -or ($PLEX_API_FOLDER.Length -eq 0))
	{
		LogError Invalid '$PLEX_API_FOLDER': [$PLEX_API_FOLDER]
	}

	if($WHATIF_MODE)
	{
		LogInfo "Validating extensions..."
	}

	if($MEDIA_EXTENSIONS.Count -eq 0)
	{
		LogError No '$MEDIA_EXTENSIONS' specified.
	}

	foreach ($currentExtension in $MEDIA_EXTENSIONS)
	{
		if(-not $currentExtension.StartsWith('.'))
		{
			LogError "Invalid Extension '$currentExtension', needs to start with a period."
		}
	}

	if($SUBTITLE_EXTENSIONS.Count -eq 0)
	{
		LogError No '$SUBTITLE_EXTENSIONS' specified.
	}

	foreach ($currentExtension in $SUBTITLE_EXTENSIONS)
	{
		if(-not $currentExtension.StartsWith('.'))
		{
			LogError "Invalid Extension '$currentExtension', needs to start with a period."
		}
	}

	if($THUMBNAIL_EXTENSIONS.Count -eq 0)
	{
		LogError No '$THUMBNAIL_EXTENSIONS' specified.
	}

	foreach ($currentExtension in $THUMBNAIL_EXTENSIONS)
	{
		if(-not $currentExtension.StartsWith('.'))
		{
			LogError "Invalid Extension '$currentExtension', needs to start with a period."
		}
	}

	if($METADATA_EXTENSION.Count -eq 0)
	{
		LogError No '$METADATA_EXTENSION' specified.
	}

	foreach ($currentExtension in $METADATA_EXTENSION)
	{
		if(-not $currentExtension.StartsWith('.'))
		{
			LogError "Invalid Extension '$currentExtension', needs to start with a period."
		}
	}

	if($PURGE_PATTERNS.Count -eq 0)
	{
		LogWarning No '$PURGE_PATTERNS' specified.
	}

	if($WHATIF_MODE)
	{
		LogInfo "Validating lists..."
	}

	if($TOPICAL_SHOWS.Count -eq 0)
	{
		LogWarning No '$TOPICAL_SHOWS' specified.
	}

	if($DAILY_SHOWS.Count -eq 0)
	{
		LogWarning No '$DAILY_SHOWS' specified.
	}

	if($KODI_INTEGRATION)
	{
		if($WHATIF_MODE)
		{
			LogInfo "Validating Kodi Configuration..."
		}

		if($KODI_FOLDER_TRANSLATE.Count -eq 0)
		{
			LogWarning No '$KODI_FOLDER_TRANSLATE' specified.
		}

		if($KODI_SERVERS.Count -eq 0)
		{
			LogError No '$KODI_SERVERS' specified.
		}

		foreach ($currentKodiServer in $KODI_SERVERS.Keys)
		{
			if(-not (IsStringDefined $currentKodiServer) -or (-not (IsStringDefined ($KODI_SERVERS[$currentKodiServer]))))
			{
				LogError Invalid Kodi Server Entry: [$currentKodiServer]
				continue
			}
		}
	}

	if($PLEX_INTEGRATION)
	{
		if($WHATIF_MODE)
		{
			LogInfo "Validating Plex Configuration..."
		}

		if(($PLEX_CREDENTIALS -eq $null) -or ($PLEX_CREDENTIALS.Length -eq 0) )
		{
			LogError Invalid Plex credentials: [$PLEX_CREDENTIALS]
		}

		if(($PLEX_SERVERNAME -eq $null) -or ($PLEX_SERVERNAME.Length -eq 0) )
		{
			if($DEBUG_MODE -or $VERBOSE_MODE)
			{
				LogWarning Invalid Plex ServerName- assuming localhost: [$currentValue]
			}
		}
	}

	if($WHATIF_MODE)
	{
		LogInfo "Validating polling scripts..."
	}

	if($POLLING_SCRIPTS.Count -eq 0)
	{
		LogError No '$POLLING_SCRIPTS' specified.
	}

	foreach ($currentScript in $POLLING_SCRIPTS.Keys)
	{
		$tmpParsedTimeSpan = $null
		$tmpTimeSpan = $POLLING_SCRIPTS[$currentScript]
		$ErrorActionPreference = 'SilentlyContinue'
		$tmpParsedTimeSpan = ([TimeSpan] $tmpTimeSpan)
		$ErrorActionPreference = 'Continue'

		if($tmpParsedTimeSpan -eq $null)
		{
			LogError Invalid polling interval configured for script: [$currentScript] Interval: [$tmpTimeSpan]
		}

		if(IsInvalidPath $currentScript)
		{
			LogError Invalid '$POLLING_SCRIPTS' configured: [$currentScript]
		}
	}

	if($WHATIF_MODE)
	{
		LogInfo "Validating executables..."
	}

	if($WATCHING_HOSTS.Count -eq 0)
	{
		LogError No '$WATCHING_HOSTS' specified.
	}

	if($RECORDING_HOSTS.Count -eq 0)
	{
		LogError No '$RECORDING_HOSTS' specified.
	}

	if($WHATIF_MODE)
	{
		LogInfo "Validating dependent files..."
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($LOG_OUTPUT)))
	{
		LogWarning Path is not set or invalid: ['$LOG_OUTPUT']:[([System.IO.Path]::GetDirectoryName($LOG_OUTPUT))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($COMMAND_LOG_OUTPUT)))
	{
		LogWarning Path is not set or invalid: ['$COMMAND_LOG_OUTPUT']:[([System.IO.Path]::GetDirectoryName($COMMAND_LOG_OUTPUT))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($LOG_ARCHIVE)))
	{
		LogWarning Path is not set or invalid: ['$LOG_ARCHIVE']:[([System.IO.Path]::GetDirectoryName($LOG_ARCHIVE))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($SESSION_LOG_OUTPUT)))
	{
		LogWarning Path is not set or invalid: ['$SESSION_LOG_OUTPUT']:[([System.IO.Path]::GetDirectoryName($SESSION_LOG_OUTPUT))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($AD_REMOVE_LOGS)))
	{
		LogWarning Path is not set or invalid: ['$AD_REMOVE_LOGS']:[([System.IO.Path]::GetDirectoryName($AD_REMOVE_LOGS))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($CONVERSION_TMP)))
	{
		LogWarning Path is not set or invalid: ['$CONVERSION_TMP']:[([System.IO.Path]::GetDirectoryName($CONVERSION_TMP))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($SUBSCRIPTION_TMP)))
	{
		LogWarning Path is not set or invalid: ['$SUBSCRIPTION_TMP']:[([System.IO.Path]::GetDirectoryName($SUBSCRIPTION_TMP))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($TRACKING_CSV)))
	{
		LogWarning Path is not set or invalid: ['$TRACKING_CSV']:[([System.IO.Path]::GetDirectoryName($TRACKING_CSV))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($RECORDING_CSV)))
	{
		LogWarning Path is not set or invalid: ['$RECORDING_CSV']:[([System.IO.Path]::GetDirectoryName($RECORDING_CSV))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($RECORDING_PROBLEMS_CSV)))
	{
		LogWarning Path is not set or invalid: ['$RECORDING_PROBLEMS_CSV']:[([System.IO.Path]::GetDirectoryName($RECORDING_PROBLEMS_CSV))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($TVDB_SERIES_CSV)))
	{
		LogWarning Path is not set or invalid: ['$TVDB_SERIES_CSV']:[([System.IO.Path]::GetDirectoryName($TVDB_SERIES_CSV))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($CURRENT_SEASONS_CSV)))
	{
		LogWarning Path is not set or invalid: ['$CURRENT_SEASONS_CSV']:[([System.IO.Path]::GetDirectoryName($CURRENT_SEASONS_CSV))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($RENAMED_CSV)))
	{
		LogWarning Path is not set or invalid: ['$RENAMED_CSV']:[([System.IO.Path]::GetDirectoryName($RENAMED_CSV))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($AD_REMOVE_CSV)))
	{
		LogWarning Path is not set or invalid: ['$AD_REMOVE_CSV']:[([System.IO.Path]::GetDirectoryName($AD_REMOVE_CSV))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($SUBTITLE_CSV)))
	{
		LogWarning Path is not set or invalid: ['$SUBTITLE_CSV']:[([System.IO.Path]::GetDirectoryName($SUBTITLE_CSV))]
	}

	if(IsInvalidPath ([System.IO.Path]::GetDirectoryName($CREDENTIAL_FILE)))
	{
		LogWarning Path is not set or invalid: ['$CREDENTIAL_FILE']:[([System.IO.Path]::GetDirectoryName($CREDENTIAL_FILE))]
	}

	if($WHATIF_MODE)
	{
		LogInfo "Validating dependent programs..."
	}

	if(IsInvalidPath $HANDLE_BINARY)
	{
		LogWarning Path is not set or invalid: ['$HANDLE_BINARY']:[$HANDLE_BINARY]
	}

	if(IsInvalidPath $POWERSHELL_FILE)
	{
		LogWarning Path is not set or invalid: ['$POWERSHELL_FILE']:[$POWERSHELL_FILE]
	}

	if(IsInvalidPath $POWERSHELL_32_FILE)
	{
		LogWarning Path is not set or invalid: ['$POWERSHELL_32_FILE']:[$POWERSHELL_32_FILE]
	}

	if(IsInvalidPath $SQL_LITE)
	{
		LogWarning Path is not set or invalid: ['$SQL_LITE']:[$SQL_LITE]
	}

	if(IsInvalidPath $RECORDING_DATABASE)
	{
		LogWarning Path is not set or invalid: ['$RECORDING_DATABASE']:[$RECORDING_DATABASE]
	}

	if(IsInvalidPath $COMSKIP_BINARY)
	{
		LogWarning Path is not set or invalid: ['$COMSKIP_BINARY']:[$COMSKIP_BINARY]
	}

	if(IsInvalidPath $COMSKIP_PROFILE_PATH)
	{
		LogWarning Path is not set or invalid: ['$COMSKIP_PROFILE_PATH']:[$COMSKIP_PROFILE_PATH]
	}

	if(IsInvalidPath $FFMPEG_BINARY)
	{
		LogWarning Path is not set or invalid: ['$FFMPEG_BINARY']:[$FFMPEG_BINARY]
	}

	if(IsInvalidPath $FFPROBE_BINARY)
	{
		LogWarning Path is not set or invalid: ['$FFPROBE_BINARY']:[$FFPROBE_BINARY]
	}

	if(IsInvalidPath $YOUTUBE_DL_BINARY)
	{
		LogWarning Path is not set or invalid: ['$YOUTUBE_DL_BINARY']:[$YOUTUBE_DL_BINARY]
	}

	if(IsInvalidPath $PYTHON_BINARY)
	{
		LogWarning Path is not set or invalid: ['$PYTHON_BINARY']:[$PYTHON_BINARY]
	}

	if(IsInvalidPath $PLEX_API)
	{
		LogWarning Path is not set or invalid: ['$PLEX_API']:[$PLEX_API]
	}

	if($WHATIF_MODE)
	{
		LogInfo "Validating templates..."
	}

	if(($TOPICAL_RENAME_TEMPLATE -eq $null) -or ($TOPICAL_RENAME_TEMPLATE.Length -eq 0))
	{
		LogWarning Invalid '$TOPICAL_RENAME_TEMPLATE': [$TOPICAL_RENAME_TEMPLATE]
	}

	if(($RENAME_TEMPLATE -eq $null) -or ($RENAME_TEMPLATE.Length -eq 0))
	{
		LogWarning Invalid '$RENAME_TEMPLATE': [$RENAME_TEMPLATE]
	}

	$validExpansions = @("{SeriesName}", "{AiredDate}", "{SeasonAndEpisode}", "{EpisodeTitle}")

	$tmpTemplate = $TOPICAL_RENAME_TEMPLATE
	foreach ($currentExpansion in $validExpansions)
	{
		if($tmpTemplate -match $currentExpansion)
		{
			$tmpTemplate = $tmpTemplate.Replace($matches[0], "")
		}
	}

	if($tmpTemplate.Contains('}') -or $tmpTemplate.Contains('{'))
	{
		LogError Invalid variable in '$TOPICAL_RENAME_TEMPLATE': [$tmpTemplate]
	}

	$tmpTemplate = $RENAME_TEMPLATE
	foreach ($currentExpansion in $validExpansions)
	{
		if($tmpTemplate -match $currentExpansion)
		{
			$tmpTemplate = $tmpTemplate.Replace($matches[0], "")
		}
	}

	if($tmpTemplate.Contains('}') -or $tmpTemplate.Contains('{'))
	{
		LogError Invalid variable in '$RENAME_TEMPLATE': [$tmpTemplate]
	}

	if($WHATIF_MODE)
	{
		LogInfo "Validating profiles..."
	}

	foreach ($currentTransferProfile in $MEDIA_TRANSFER_PROFILES.Keys)
	{
		if(-not (IsStringDefined $currentTransferProfile))
		{
			LogError Invalid Profile Key: [$currentTransferProfile]
			continue
		}

		foreach ($currentTransferProfileEntry in $currentTransferProfile[$currentTransferProfile].Keys)
		{
			if(-not (IsStringDefined $currentTransferProfileEntry) -or (-not (IsStringDefined ($currentTransferProfile[$currentTransferProfile][$currentTransferProfileEntry]))))
			{
				LogError Invalid Profile Entry: [$currentTransferProfile]
				continue
			}
		}
	}

	if($WHATIF_MODE)
	{
		LogInfo "Validating values..."
	}

	#NUMBERS
	foreach ($currentValue in @($ARCHIVE_AGE_DAYS, $PURGE_TOPICAL_AGE_DAYS, $TRACK_AGE_HOURS, $EPISODES_AGGREGATE_THRESHOLD, $NEW_SEASON_LEAD_NOTICE_DAYS, $LOG_ROLLOVER_MB, $MAX_CACHE_AGE_DAYS))
	{
		#NOTE: if null, this casts to 0.
		[int] $currentValue > $null
		if($currentValue -le 0)
		{
			LogError Invalid value: [$currentValue]
		}
	}

	#BOOLEANS
	foreach ($currentValue in @($KODI_INTEGRATION, $PLEX_INTEGRATION, $EMBED_SUBTITLES, $EMBED_THUMBNAIL, $TASK_SCHEDULING, $SHOW_ERROR_EXCEPTIONS, $EXTRACT_HANDLE_DIAGNOSTICS, $AD_REMOVE_DIAGNOSTICS, $STARTUP_VERBOSE_ALIASES, $USE_TVDB))
	{
		if(($currentValue -ne $true) -and ($currentValue -ne $false) )
		{
			LogError Invalid value: [$currentValue]
		}
	}

	#STRINGS
	foreach ($currentValue in @($OPENSUBTITLES_CREDENTIAL))
	{
		if(($currentValue -eq $null) -or ($currentValue.Length -eq 0) )
		{
			LogError Invalid value: [$currentValue]
		}
	}
}

################################
#
# FUNCTION PurgeEmptyFolder
#
################################
function PurgeEmptyFolder($Path)
{
	if(IsInvalidPath $Path)
	{
		return
	}

	if((Get-Item -LiteralPath $Path).Attributes -notmatch 'Directory')
	{
		return
	}

	if((Get-ChildItem -LiteralPath $Path).Count -gt 0)
	{
		return
	}

	DeleteItem $Path
}

################################
#
# FUNCTION PurgeEmptyDirectories
#
################################
function PurgeEmptyDirectories($RootPath, $verboseComplain)
{
	if(-not (IsStringDefined $RootPath))
	{
		return
	}

	if(IsInvalidPath $RootPath)
	{
		LogWarning No valid path is defined [$RootPath].
		return
	}

	if((-not $Force) -and (-not $CURRENT_SCRIPT_CLEANUP_NEEDED))
	{
		if(($RootPath -eq $DOWNLOADED_TV_PATH) -or ($RootPath -eq $DOWNLOADED_PODCASTS_PATH))
		{
			VerboseLogInfo Directory cleanup of [$RootPath] skipped.
			return
		}
	}

	#PURGE EMPTY FOLDERS
	$touchedDir = $true
	while ($touchedDir)
	{
		$touchedDir = $false
		$targetDir = $RootPath
		if(IsInvalidPath $targetDir)
		{
			continue
		}
		$cCurrentProgress = 0
		$dirs = Get-ChildItem -LiteralPath $targetDir -Attributes Directory -Recurse -FollowSymlink
		foreach ($currentDir in $dirs)
		{
			$cCurrentProgress++
			OutputProgressBar ("Cleaning Empties [$currentDir]...") ($cCurrentProgress/($dirs.Count+1))

			if(IsExcludedPath $currentDir.FullName)
			{
				VerboseLogInfo EXCLUDED DIR: [($currentDir.FullName)]
				continue
			}

			$isSymLinked = $false
			foreach($currentSymLink in $SYMLINK_TABLE.Values)
			{
				if(IsUnderPath $currentDir.FullName $currentSymLink)
				{
					$isSymLinked = $true
				}
			}

			if($isSymLinked)
			{
				VerboseLogInfo EXCLUDED SYMLINK: [($currentDir.FullName)]
				continue
			}

			if(IsUnderPath $currentDir.FullName $PURGE_PATH)
			{
				VerboseLogInfo EXCLUDED PURGED: [($currentDir.FullName)]
				continue
			}

			DebugLogInfo CONSIDERING EMPTY: [($currentDir.FullName)]

			#FORCE IS REQUIRED TO SEE HIDDEN FILES IN A FOLDER
			$children = Get-ChildItem -LiteralPath $currentDir.FullName -Force
			if($children -eq $null)
			{
				if($WHATIF_MODE)
				{
					LogInfo WHATIF: [DeleteItem ($currentDir.FullName)]
					$touchedDir = $false
				}
				else
				{
					DeleteItem $currentDir.FullName
					if(IsInvalidPath $currentDir.FullName)
					{
						$touchedDir = $true
					}
				}
			}
			else
			{
				$subDirs = $children | Where-Object { ($_.PSIsContainer -eq $true) }
				$children = $children | Where-Object { ($_.PSIsContainer -eq $false) }

				if($subDirs.Count -ne 0)
				{
					continue
				}

				foreach ($currentPurgePattern in $PURGE_PATTERNS)
				{
					$escapedPath = $currentDir.FullName
					$escapedPath = $escapedPath.Replace('[', '`[')
					$escapedPath = $escapedPath.Replace(']', '`]')

					if(IsInvalidPath $escapedPath)
					{
						LogWarning Issue with path: [$escapedPath]
						continue
					}

					$possibleDeletingChildren = [System.IO.Path]::Combine($escapedPath, $currentPurgePattern)

					foreach ($possibleDeletingChild in (Get-Item -Path $possibleDeletingChildren -ErrorAction Ignore -Force))
					{
						$touchedDir = $true
						if($WHATIF_MODE)
						{
							LogInfo WHATIF DELETE: [($possibleDeletingChild.FullName)]
						}
						else
						{
							VerboseLogInfo DELETING: [($possibleDeletingChild.FullName)]
							DeleteItem $possibleDeletingChild.FullName
						}
					}
				}

				if($touchedDir)
				{
					continue
				}

				$mediaFound = $false
				foreach ($currentChild in $children)
				{
					if($currentChild.Extension -In ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
					{
						$mediaFound = $true
						break
					}
				}

				if($children.Count -gt 0)
				{
					if(-not $mediaFound)
					{
						LogWarning Directory not removed due to leftover non-media files: [($currentDir.FullName)]
					}
					elseif($verboseComplain)
					{
						LogWarning Directory not removed due to leftover media files: [($currentDir.FullName)]
					}
				}
			}
		}
	}
}

################################
#
# FUNCTION CullPurgeDirectory
#
################################
function CullPurgeDirectory()
{
	if((-not $Force) -and (IsValidPath $LAST_CULLED))
	{
		$lastCulled = (Get-Item -LiteralPath $LAST_CULLED).LastWriteTime
		if( ((Get-Date) - $lastCulled).TotalHours -le 12 )
		{
			LogInfo Skipping culling purged, last detected: [$lastCulled]
			return
		}
	}

	$shows = Get-ChildItem -LiteralPath $PURGE_PATH -Recurse -FollowSymlink -File
	foreach ($currentShow in $shows)
	{
		$airDate = ExtractShowAirDate $currentShow
		if($airDate[0] -eq $null)
		{
			LogError COULD NOT EXTRACT AIR DATE: [($currentShow.FullName)]
			continue
		}

		if(((get-date).Date - $airDate[0]).TotalDays -ge $PURGE_TOPICAL_AGE_DAYS)
		{
			if(IsExcludedPath $currentShow.FullName)
			{
				VerboseLogInfo EXCLUDED DIR: [($currentShow.FullName)]
				continue
			}

			DeleteItem $currentShow.FullName
		}
	}
}

#################################
#
# FUNCTION CullExpiredMedia
#
#################################
function CullExpiredMedia()
{
	$isArchiving = IsStringDefined $ARCHIVE_FOLDER_NAME
	$isDailyShowing = (IsStringDefined $DAILY_FOLDER_NAME) -and ($DAILY_SHOWS.Count -gt 0)

	foreach ($currentDir in @($DOWNLOADED_TV_PATH, $DOWNLOADED_PODCASTS_PATH))
	{
		$cCurrentProgress = 0
		if(IsInvalidPath $currentDir) { continue }

		$files = Get-ChildItem -Recurse -LiteralPath $currentDir -File -FollowSymlink | Where-Object { IsNotUnderPath $_.Directory.FullName (DeriveWatchedPath $currentDir) }
		foreach ($currentOpenedFile in $files)
		{
			$cCurrentProgress++
			OutputProgressBar ("Culling Old Media [$currentDir]...") ($cCurrentProgress/$files.Count)

			if($currentOpenedFile.Extension -NotIn ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
			{
				VerboseLogInfo EXCLUDED FILE EXTENSION: [($currentOpenedFile.FullName)]
				continue
			}

			if(IsExcludedPath $currentOpenedFile.FullName)
			{
				VerboseLogInfo EXCLUDED DIR: [($currentOpenedFile.FullName)]
				continue
			}

			$purgeAgeToUse = $PURGE_TOPICAL_AGE_DAYS
			$archiveAgeOverride = LoadSubscriptionOverrideVariable $currentOpenedFile "PURGE_TOPICAL_AGE_DAYS_OVERRIDE"
			if($archiveAgeOverride -ne $null)
			{
				$purgeAgeToUse = $archiveAgeOverride
			}
			elseif($isDailyShowing -and $currentOpenedFile.Directory.FullName.EndsWith($DAILY_FOLDER_NAME))
			{
				$purgeAgeToUse = 1
			}
			elseif($isArchiving -and -not $currentOpenedFile.Directory.FullName.EndsWith($ARCHIVE_FOLDER_NAME))
			{
				#Not overridden and not archiving, skip
				continue
			}

			if(($archiveAgeOverride -eq $null) -and -not ((IsTopicalShow $currentOpenedFile) -or (IsDailyShow $currentOpenedFile)))
			{
				VerboseLogInfo EXCLUDED NON-DAILY/TOPICAL: [($currentOpenedFile.FullName)]
				continue
			}

			$airDate = ExtractShowAirDate $currentOpenedFile
			$archiveShowAge = ((get-date).Date - ($airDate[0])).TotalDays
			if($archiveShowAge -ge $purgeAgeToUse)
			{
				VerboseLogInfo CULLING: $currentOpenedFile.FullName [Age: $archiveShowAge] [DaysUntilPurge: ($purgeAgeToUse - $archiveShowAge)]
				PurgeItem $currentOpenedFile.FullName
				$Script:CURRENT_SCRIPT_CLEANUP_NEEDED = $true
			}
			else
			{
				VerboseLogInfo WAITING TO CULL: $currentOpenedFile.FullName [Age: $archiveShowAge] [DaysUntilPurge: ($purgeAgeToUse - $archiveShowAge)]
			}
		}
	}
}

##############################
#
# FUNCTION MapHandleFileOutput
#
##############################
function MapHandleFileOutput($fileName)
{
	if(IsValidPath $fileName)
	{
		return $fileName
	}

	$parent = [System.IO.Directory]::GetParent($fileName).FullName
	if(IsInvalidPath $parent)
	{
		return $null
	}

	# Handle.exe can return ? for extended characters.
	# Handle EXE can mistranslate accented letters as Greek symbols

	$instancesToReplace = ($fileName.Split('?').Length - 1)
	foreach ($currentChar in $fileName.ToCharArray())
	{
		if((([int] $currentChar) -gt 127) -or ($currentChar -In @('"',"'",'_')))
		{
			$instancesToReplace++
		}
	}

	$condenders = Get-ChildItem -LiteralPath $parent -File

	if($instancesToReplace -gt 0)
	{
		foreach ($currentContender in $condenders)
		{
			$lv = LevenshteinDistance $fileName ($currentContender.FullName)
			if($lv -le $instancesToReplace)
			{
				$newFileName = $currentContender.FullName
				if(IsValidPath $newFileName)
				{
					VerboseLogInfo NORMALIZED: [$newFileName]
					return $newFileName
				}
			}
		}
	}

	VerboseLogInfo Could not normalize name: $fileName
	return $null
}

###########################
#
# FUNCTION IsTopicalShow
#
###########################
function IsTopicalShow([System.IO.FileSystemInfo] $show)
{
	if(IsMediaInformationCached $show.FullName "IsTopicalShow")
	{
		return $CURRENT_MEDIA_EXTRACTED_INFO.IsTopicalShow
	}

	$showName = ExtractShowName $show

	if(($showName -eq $null) -or ($TOPICAL_SHOWS -eq $null))
	{
		UpdateMediaCacheInformation $show.FullName "IsTopicalShow" $false
		return $false
	}

	foreach ($topicalShow in $TOPICAL_SHOWS)
	{
		if($showName -match $topicalShow)
		{
			DebugLogInfo TOPICAL: [$showName]
			UpdateMediaCacheInformation $show.FullName "IsTopicalShow" $true
			return $true
		}
	}

	if($DEBUG_MODE)
	{
		if(UseTvDB)
		{
			$seriesId = 0

			$tvdbSeries = LoadTvDBDictionary
			foreach ($currentSeries in $tvdbSeries.Keys)
			{
				if($currentSeries -match [RegEx]::Escape($showName))
				{
					$seriesId = $tvdbSeries[$currentSeries]
					break
				}
			}

			if($seriesId -ne 0)
			{
				foreach ($currentSeries in $tvdbSeries.Keys)
				{
					if(($tvdbSeries[$currentSeries] -eq $seriesId) -and ($currentSeries -in $TOPICAL_SHOWS))
					{
						LogWarning Show [$showName] may map to TVDB Series [$currentSeries]. Please update the Topical shows list to reflect this.
						break
					}
				}
			}
		}
	}

	UpdateMediaCacheInformation $show.FullName "IsTopicalShow" $false
	return $false
}

###########################
#
# FUNCTION IsDailyShow
#
###########################
function IsDailyShow([System.IO.FileSystemInfo] $show)
{
	if(IsMediaInformationCached $show.FullName "IsDailyShow")
	{
		return $CURRENT_MEDIA_EXTRACTED_INFO.IsDailyShow
	}

	$showName = ExtractShowName $show

	if(($showName -eq $null) -or ($DAILY_SHOWS.Count -eq 0))
	{
		UpdateMediaCacheInformation $show.FullName "IsDailyShow" $false
		return $false
	}

	foreach ($dailyShow in $DAILY_SHOWS)
	{
		if($showName -match $dailyShow)
		{
			DebugLogInfo DAILY: [$showName]
			UpdateMediaCacheInformation $show.FullName "IsDailyShow" $true
			return $true
		}
	}

	if($DEBUG_MODE)
	{
		if(UseTvDB)
		{
			$seriesId = 0

			$tvdbSeries = LoadTvDBDictionary
			foreach ($currentSeries in $tvdbSeries.Keys)
			{
				if($currentSeries -match [RegEx]::Escape($showName))
				{
					$seriesId = $tvdbSeries[$currentSeries]
					break
				}
			}

			if($seriesId -ne 0)
			{
				foreach ($currentSeries in $tvdbSeries.Keys)
				{
					if(($tvdbSeries[$currentSeries] -eq $seriesId) -and ($currentSeries -in $DAILY_SHOWS))
					{
						LogWarning Show [$showName] may map to TVDB Series [$currentSeries]. Please update the Daily shows list to reflect this.
						break
					}
				}
			}
		}
	}

	UpdateMediaCacheInformation $show.FullName "IsDailyShow" $false
	return $false
}

###########################
#
# FUNCTION IsExcludedPath
#
###########################
function IsExcludedPath($path)
{
	return (IsPathInExclusionList $path $DOWNLOADED_PATH_EXCLUSIONS)
}

###########################
#
# FUNCTION IsPathInExclusionList
#
###########################
function IsPathInExclusionList($path, $exclusionCollection)
{
	if($exclusionCollection -eq $null)
	{
		return $false
	}

	if($Force)
	{
		return $false
	}

	foreach ($excludedPath in $exclusionCollection)
	{
		if($excludedPath -ne $null -and $excludedPath.Count -gt 1)
		{
			if(IsPathInExclusionList $path $excludedPath)
			{
				return $true
			}
			continue
		}

		if(-not (IsStringDefined $excludedPath))
		{
			continue
		}

		if(-not (IsProbablyPattern $excludedPath))
		{
			if(IsValidPath $excludedPath)
			{
				if(IsUnderPath $path $excludedPath)
				{
					return $true
				}
			}

			# NOTE: Exclusion Lists should have been expanded already.
			#$excludedPath = ExpandAndFixPath $excludedPath $false
			if($excludedPath.Contains('$'))
			{
				LogWarning Path not Expanded: [$excludedPath]
				return
			}

			if(IsValidPath $excludedPath)
			{
				if(IsUnderPath $path $excludedPath)
				{
					return $true
				}
			}

			if([System.IO.Path]::GetFileName($path) -Match [RegEx]::Escape($excludedPath))
			{
				return $true
			}

			if($path -Match [RegEx]::Escape($excludedPath))
			{
				return $true
			}
		}
		else
		{
			if([System.IO.Path]::GetFileName($path) -Match $excludedPath)
			{
				return $true
			}
			if($path -Match $excludedPath)
			{
				return $true
			}
		}
	}

	return $false
}

##########################
#
# FUNCTION IsUnderPath
#
##########################
function IsUnderPath($file, $path)
{
	return ($file -match [RegEx]::Escape($path))
}

#############################
#
# FUNCTION IsNotUnderPath
#
#############################
function IsNotUnderPath($file, $path)
{
	return (-not (IsUnderPath $file $path))
}

###################################
#
# FUNCTION IsAudio
#
###################################
function IsAudio($file)
{
	if(IsMediaInformationCached $file "IsAudio")
	{
		return $CURRENT_MEDIA_EXTRACTED_INFO.IsAudio
	}

	$tmpValue = $false

	if((IsStringDefined $DOWNLOADED_PODCASTS_PATH) -and (IsUnderPath $file $DOWNLOADED_PODCASTS_PATH))
	{
		$tmpValue = $true
	}
	else
	{
		$currentOpenedFileExtension = [System.IO.Path]::GetExtension($file)
		if($currentOpenedFileExtension -In $AUDIO_EXTENSIONS)
		{
			$tmpValue = $true
		}
	}

	UpdateMediaCacheInformation $file "IsAudio" $tmpValue

	return $tmpValue
}

#############################
#
# FUNCTION IsMovie
#
#############################
function IsMovie($file)
{
	$tmpValue = $false

	if(IsAudio $file)
	{
		$tmpValue = $false
	}
	elseif((IsStringDefined $DOWNLOADED_MOVIES_PATH) -and (IsUnderPath $file $DOWNLOADED_MOVIES_PATH))
	{
		$tmpValue = $true
	}

	return $tmpValue
}

function MoveIfProbablyMovie($file)
{
	if(IsInvalidPath $DOWNLOADED_MOVIES_PATH)
	{
		return
	}

	$subscriptionOverride = LoadSubscriptionOverrideVariable $file "ACQUISITION_ALLOW_RENAME"
	if(($subscriptionOverride -ne $null) -and (-not $subscriptionOverride))
	{
		VerboseLogInfo EXCLUDED VIA SUBSCRIPTION: [($file.FullName)]
		return
	}

	#Is it possibily a movie instead?
	#Anything over 1 hour 15 minutes is a good guess for a movie.
	$duration = ReturnDuration $file.FullName
	if($duration.TotalMinutes -ge 75)
	{
		LogInfo Assuming possibly a movie.
		MoveFile $file.FullName $DOWNLOADED_MOVIES_PATH
		$Script:CURRENT_SCRIPT_CLEANUP_NEEDED = $true
	}
}

#############################
#
# FUNCTION IsUnwatchedEpisodic
#
#############################
function IsUnwatchedEpisodic($file)
{
	if(IsInvalidPath $file)
	{
		return $false
	}
	elseif(IsMovie $file)
	{
		return $false
	}
	elseif(IsLibraryUnwatched $file)
	{
		return $true
	}

	return $false
}

#############################
#
# FUNCTION IsWatched
#
#############################
function IsWatched($file)
{
	if(IsInvalidPath $file)
	{
		$tmpValue = $false
	}
	elseif(IsUnderPath $file $PURGE_PATH)
	{
		$tmpValue = $true
	}
	else
	{
		$tmpValue = IsUnderPath $file (DeriveWatchedPath $file)
	}

	return ($tmpValue)
}

#############################
#
# FUNCTION IsLibraryUnwatched
#
#############################
function IsLibraryUnwatched($file)
{
	if(IsInvalidPath $file)
	{
		return $false
	}

	if(IsWatched $file)
	{
		return $false
	}

	if(IsUnderPath $file $DOWNLOADED_TV_PATH)
	{
		return $true
	}
	elseif(IsUnderPath $file $DOWNLOADED_MOVIES_PATH)
	{
		return $true
	}
	elseif(IsUnderPath $file $DOWNLOADED_PODCASTS_PATH)
	{
		return $true
	}

	return $false
}

#############################
#
# FUNCTION DeriveWatchedPath
#
#############################
function DeriveWatchedPath($file)
{
	if(-not (IsStringDefined $CONSUMED_FOLDER_NAME))
	{
		$CONSUMED_FOLDER_NAME = "Watched"
	}

	$basePath = "."
	if(IsUnderPath $file $DOWNLOADED_TV_PATH)
	{
		$basePath = $DOWNLOADED_TV_PATH
	}
	elseif(IsUnderPath $file $DOWNLOADED_MOVIES_PATH)
	{
		$basePath = $DOWNLOADED_MOVIES_PATH
	}
	elseif(IsUnderPath $file $DOWNLOADED_PODCASTS_PATH)
	{
		$basePath = $DOWNLOADED_PODCASTS_PATH
	}

	$tmpValue = [System.IO.Path]::Combine($basePath, $CONSUMED_FOLDER_NAME)
	return $tmpValue
}

###########################################
#
# FUNCTION IsMediaCacheInformationCurrent
#
###########################################
function IsMediaCacheInformationCurrent($fullName)
{
	if($CURRENT_MEDIA_EXTRACTED_INFO.FullName -ne $fullName)
	{
		$Script:CURRENT_MEDIA_EXTRACTED_INFO = InitializeMediaCacheInformation $fullName
		return $false
	}
	return $true
}

##########################################
#
# FUNCTION InitializeMediaCacheInformation
#
##########################################
function InitializeMediaCacheInformation($fullName)
{
	if($PERFORMANCE_DIAGNOSTICS)
	{
		if(($fullName -ne $null) -and ((Get-Item -LiteralPath $fullName).Attributes -eq "Directory"))
		{
			if($DEBUG_MODE -or $VERBOSE_MODE)
			{
				try
				{
					throw ("Cache used for non-file [ " + $fullName + " ]")
				}
				catch
				{
					HandleError $_
				}
			}
			else
			{
				LogWarning "Cache used for non-file" [$fullName]
			}
		}
	}

	$cachedProperties = @{	"ExtractShowName" = $null;
							"ExtractShowSeason" = $null;
							"IsAudio" = $null;
							"IsDailyShow" = $null;
							"IsTopicalShow" = $null;
							"IsSeasonedFolder" = $null }

	if($PERFORMANCE_DIAGNOSTICS)
	{
		if($CURRENT_MEDIA_EXTRACTED_INFO.FullName -ne $null)
		{
			$tmpCsvProps = $CURRENT_MEDIA_EXTRACTED_INFO.CacheHits
			$tmpCsvProps.Add("AFullName", $CURRENT_MEDIA_EXTRACTED_INFO.FullName) > $null
			$csvEntry = New-Object -TypeName PSObject -Property ($tmpCsvProps)
			$now = [String]::Format("{0:D4}-{1:D2}-{2:D2}-{3:D2}-{4:D2}", $LAST_EXECUTION.Year, $LAST_EXECUTION.Month, $LAST_EXECUTION.Day, $LAST_EXECUTION.Hour, $LAST_EXECUTION.Minute)
			$csvName = [System.IO.Path]::GetFileNameWithoutExtension($PSCmdlet.CommandRuntime.ToString()) + ".CacheStats." + $now + ".csv"
			AddCsvEntry $csvEntry $csvName

			if($DEBUG_MODE)
			{
				foreach($currentCacheHit in $CURRENT_MEDIA_EXTRACTED_INFO.CacheHits.Keys)
				{
					if($CURRENT_MEDIA_EXTRACTED_INFO.CacheHits[$currentCacheHit] -eq 0)
					{
						LogWarning Cache Entry not used [($CURRENT_MEDIA_EXTRACTED_INFO.FullName)] [$currentCacheHit]
					}
				}
			}
		}

		$cacheHits = @{}
		foreach($currentProperty in $cachedProperties.Keys)
		{
			$cacheHits.Add($currentProperty, 0) > $null
		}
		$cachedProperties.Add("CacheHits", $cacheHits) > $null
	}

	$cachedProperties.Add("FullName", $fullName) > $null
	return ( New-Object -TypeName PSObject -Property $cachedProperties )
}
$CURRENT_MEDIA_EXTRACTED_INFO = InitializeMediaCacheInformation $null

###################################
#
# FUNCTION IsMediaInformationCached
#
###################################
function IsMediaInformationCached($fullName, $propertyName)
{
	if((IsMediaCacheInformationCurrent $fullName) -and ($CURRENT_MEDIA_EXTRACTED_INFO.$propertyName -ne $null))
	{
		if($PERFORMANCE_DIAGNOSTICS)
		{
			DebugLogInfo CacheHit [$fullName] [$propertyName]
			$Script:CURRENT_MEDIA_EXTRACTED_INFO.CacheHits[$propertyName]++
		}
		return $true
	}
	else
	{
		return $false
	}
}

######################################
#
# FUNCTION UpdateMediaCacheInformation
#
######################################
function UpdateMediaCacheInformation($fullName, $propertyName, $newValue)
{
	if(FailedStringValidation $fullName) { return $false }
	if(FailedStringValidation $propertyName) { return $false }

	if(-not (IsMediaCacheInformationCurrent $fullName))
	{
		#Cache should now be reinitialized.
		if($PERFORMANCE_DIAGNOSTICS)
		{
			if($Script:CURRENT_MEDIA_EXTRACTED_INFO.CacheHits[$propertyName] -eq 0)
			{
				LogWarning Cache clobbered and underused: [$fullName] [$propertyName]
			}
		}
	}

	if($CURRENT_MEDIA_EXTRACTED_INFO.$propertyName -ne $newValue)
	{
		$Script:CURRENT_MEDIA_EXTRACTED_INFO.$propertyName = $newValue
		if($PERFORMANCE_DIAGNOSTICS)
		{
			DebugLogInfo Cache Set [$fullName] [$propertyName] [($CURRENT_MEDIA_EXTRACTED_INFO.$propertyName)]
		}
	}
}

###########################
#
# FUNCTION IsSeasonedFolder
#
###########################
function IsSeasonedFolder([System.IO.FileSystemInfo] $show, $targetDirectory)
{
	if(FailedStringValidation $targetDirectory) { return $false }

	if(IsMediaInformationCached $show.FullName "IsSeasonedFolder")
	{
		return $CURRENT_MEDIA_EXTRACTED_INFO.IsSeasonedFolder
	}

	if(IsInvalidPath $targetDirectory)
	{
		UpdateMediaCacheInformation $show.FullName "IsSeasonedFolder" $false
		return $false
	}

	$season = ExtractShowSeason $show
	if($season -eq $null)
	{
		UpdateMediaCacheInformation $show.FullName "IsSeasonedFolder" $false
		return $false
	}

	#WHEN TRANSFERRING A MOVIE, SEASONS DO NOT APPLY
	if(IsMovie $show.FullName)
	{
		UpdateMediaCacheInformation $show.FullName "IsSeasonedFolder" $false
		return $false
	}

	$watchedPath = DeriveWatchedPath $show.FullName

	if(($targetDirectory -eq $watchedPath) -or ($targetDirectory -eq $PURGE_PATH))
	{
		UpdateMediaCacheInformation $show.FullName "IsSeasonedFolder" $false
		return $false
	}

	#WHEN TRANSFERRING TO WATCHED, ALWAYS SORT TV INTO SEASONS
	if(IsUnderPath $targetDirectory $watchedPath)
	{
		UpdateMediaCacheInformation $show.FullName "IsSeasonedFolder" $true
		return $true
	}

	#WHEN TRANSFERRING TOPICAL/DAILY, SEASONS DO NOT APPLY
	if((IsTopicalShow $show) -or (IsDailyShow $show))
	{
		UpdateMediaCacheInformation $show.FullName "IsSeasonedFolder" $false
		return $false
	}

	if(IsValidPath $targetDirectory)
	{
		$dirs = Get-ChildItem -LiteralPath $targetDirectory -Directory
		foreach ($currentDir in $dirs)
		{
			if($currentDir.Name -Match '[Ss][0]?\d+$')
			{
				UpdateMediaCacheInformation $show.FullName "IsSeasonedFolder" $true
				return $true
			}
		}

		if((IsStringDefined $season) -and ((Get-ChildItem -LiteralPath $targetDirectory -File).Count -ge 5))
		{
			UpdateMediaCacheInformation $show.FullName "IsSeasonedFolder" $true
			return $true
		}

		$parentDir = (Get-Item -LiteralPath $targetDirectory).Parent.FullName
		$dirs = Get-ChildItem -LiteralPath $parentDir -Directory
		foreach ($currentDir in $dirs)
		{
			if($currentDir.Name -Match '[Ss][0]?\d+$')
			{
				UpdateMediaCacheInformation $show.FullName "IsSeasonedFolder" $true
				return $true
			}
		}
	}

	UpdateMediaCacheInformation $show.FullName "IsSeasonedFolder" $false
	return $false
}

#############################
#
# FUNCTION ExtractShowAirDate
#
#############################
function ExtractShowAirDate([System.IO.FileSystemInfo] $show)
{
	#DETERMINE SHOW AIR DATE
	$airDate = $null

	$showName = [System.IO.Path]::GetFileNameWithoutExtension($show.Name)

	$results = ExtractShowAirDate $showName
	$airDate = $results[0]
	$airDateParsedFromName = $results[1]
	$marker = $results[2]

	if(-not $airDateParsedFromName)
	{
		$airDate = $show.CreationTime.Date
	}

	return @($airDate, $airDateParsedFromName, $marker)
}

#############################
#
# FUNCTION ExtractShowAirDate
#
#############################
function ExtractShowAirDate([string] $showName)
{
	$originalFullName = $showName
	$showName = [System.IO.Path]::GetFileName($showName)

	#DETERMINE SHOW AIR DATE
	$airDate = [DateTime]::MinValue
	$showParts = $showName.Split('-')

	$airDateParsedFromName = $true

	if($showName.Contains('_') -and ($airDate -eq [DateTime]::MinValue))
	{
		#EXAMPLES
		#The Late Show with Stephen Colbert - s01e85 - 2_4_2016 (Michael Strahan, Samantha Bee, Wilco).mp4
		#The Late Show with Stephen Colbert - 3_12_20 (Dr. Sanjay Gupta).mp4

		foreach ($showPart in $showParts)
		{
			if($showPart.Contains('_'))
			{
				if($showPart -match "(?<date>\d+_\d+_\d+).*")
				{
					$marker = $matches["date"]
					$tmp = $matches["date"].Replace('_', "/")
					if([DateTime]::TryParse($tmp, [ref] $airDate))
					{
						break
					}
				}
			}
		}
	}

	if($showName.Contains(',') -and ($airDate -eq [DateTime]::MinValue))
	{
		foreach ($showPart in $showParts)
		{
			if($showPart.Contains(','))
			{
				#EXAMPLE
				#The Daily Show With Trevor Noah - February 2, 2016 - Peter Bergen.mp4
				#The Nightly Show with Larry Wilmore - s02e56 - Thu, Jan 28, 2016.mp4
				if($showPart -match "(?<date>\w+\s+\d+,\s+\d{4})")
				{
					$marker = $matches["date"]
					if([DateTime]::TryParse($matches["date"], [ref] $airDate))
					{
						break
					}
				}
			}
		}
	}

	$delimiter = "(\s+|-|\.)"

	if($showName.Contains('.') -and ($airDate -eq [DateTime]::MinValue))
	{
		$showPart = $showName

		#EXAMPLE
		#David.Letterman.2015.05.01.Steve.Martin.HDTV.AC3.XviD.avi
		if(	($showPart -match $delimiter + "(?<date>\d{4}" + $delimiter + "\d{1,2}" + $delimiter + "\d{1,2})" + $delimiter) -or
			($showPart -match "^(?<date>\d{4}" + $delimiter + "\d{1,2}" + $delimiter + "\d{1,2})" + $delimiter) -or
			($showPart -match $delimiter + "(?<date>\d{4}" + $delimiter + "\d{1,2}" + $delimiter + "\d{1,2})$")
		)
		{
			$marker = $matches["date"]
			if([DateTime]::TryParse(($matches["date"]), [ref] $airDate))
			{
				#Got it
			}
		}
	}

	if($showName.Contains('-') -and ($airDate -eq [DateTime]::MinValue))
	{
		#EXAMPLE
		#ABC News - 9-12-2019 - Third Democratic Debate.mp4
		if($showName -match "(?<date>\d+-\d+-\d{4}).*")
		{
			$marker = $matches["date"]
			$tmp = $matches["date"].Replace('-', "/")
			if([DateTime]::TryParse(($tmp), [ref] $airDate))
			{
				#Got it
			}
		}
	}

	if($airDate -eq [DateTime]::MinValue)
	{
		if(IsInvalidPath $originalFullName)
		{
			$airDate = Get-Date
		}
		else
		{
			$airDate = (Get-Item -LiteralPath $originalFullName).CreationTime
			$airDate = $airDate.AddDays(-1) #Most online publishing date is about 24 hours after airwaves aired date.
		}

		$airDateParsedFromName = $false
	}

	DebugLogInfo AIR DATE: [Source: $showName] [($airDate.ToShortDateString())] [Parsed: $airDateParsedFromName] [Marker: $marker]
	$tmpValue = @($airDate, $airDateParsedFromName, $marker)
	return $tmpValue
}

########################
#
# FUNCTION ParseAirDate
#
########################
function ParseAirDate($airDate)
{
	if(FailedStringValidation ($airDate.ToString())) { return }

	$possibleZone = $airDate.SubString($airDate.Length - 4).ToUpper()
	if($TIMEZONES.ContainsKey($possibleZone))
	{
		$airDate = $airDate.Replace($possibleZone, $TIMEZONES[$possibleZone])
	}

	$possibleZone = $airDate.SubString($airDate.Length - 3).ToUpper()
	if($TIMEZONES.ContainsKey($possibleZone))
	{
		$airDate = $airDate.Replace($possibleZone, $TIMEZONES[$possibleZone])
	}

	$possibleZone = $airDate.SubString($airDate.Length - 2).ToUpper()
	if($TIMEZONES.ContainsKey($possibleZone))
	{
		$airDate = $airDate.Replace($possibleZone, $TIMEZONES[$possibleZone])
	}

	return ([System.DateTime]::Parse($airDate))
}

########################
#
# FUNCTION FormatAirDate
#
########################
function FormatAirDate($airDate)
{
	if(FailedStringValidation ($airDate.ToString())) { return }

	$pattern = '(?<timeNumber>\d+)\s+(?<timeUnit>year|month|week|day|hour|minute|second)s*\s+ago'
	$parsedDate = [DateTime]::MinValue
	if($airDate -match $pattern)
	{
		switch ($matches["timeUnit"].ToLower())
		{
			"year"
			{
				$adjust = [TimeSpan]::FromYears($matches["timeNumber"])
			}
			"month"
			{
				$adjust = [TimeSpan]::FromDays(([int]$matches["timeNumber"]) * 30)
			}
			"week"
			{
				$adjust = [TimeSpan]::FromDays(([int]$matches["timeNumber"]) * 7)
			}
			"day"
			{
				$adjust = [TimeSpan]::FromDays($matches["timeNumber"])
			}
			"hour"
			{
				$adjust = [TimeSpan]::FromHours($matches["timeNumber"])
			}
			"minute"
			{
				$adjust = [TimeSpan]::FromMinutes($matches["timeNumber"])
			}
			"second"
			{
				$adjust = [TimeSpan]::FromSeconds($matches["timeNumber"])
			}
			default
			{
				LogError Unknown time unit: [($matches["timeUnit"])]
				return
			}
		}
		$parsedDate = (Get-Date).Subtract($adjust)
	}
	elseif($airDate -match "just now")
	{
		$parsedDate = Get-Date
	}
	else
	{
		$airDate = $airDate.ToString()

		#Timezones can shift date when coverted to local time
		# this can cause TVDB lookups to fail

		$possibleZone = $airDate.SubString($airDate.Length - 4).ToUpper()
		if($TIMEZONES.ContainsKey($possibleZone))
		{
			$airDate = $airDate.Replace($possibleZone, "")
		}

		$possibleZone = $airDate.SubString($airDate.Length - 3).ToUpper()
		if($TIMEZONES.ContainsKey($possibleZone))
		{
			$airDate = $airDate.Replace($possibleZone, "")
		}

		$possibleZone = $airDate.SubString($airDate.Length - 2).ToUpper()
		if($TIMEZONES.ContainsKey($possibleZone))
		{
			$airDate = $airDate.Replace($possibleZone, "")
		}

		$airDate = $airDate -replace "\s+[+-]\d+$"

		if(-not ([DateTime]::TryParse($airDate, [ref] $parsedDate)))
		{
			$results = ExtractShowAirDate $airDate
			$parsedDate = $results[0]
			$marker = $results[2]

			if(IsStringDefined $marker)
			{
				$formattedDate = $parsedDate.ToLongDateString().Substring($parsedDate.ToLongDateString().IndexOf(',') + 2)
				return $airDate.Replace($marker, $formattedDate).Trim()
			}
		}
	}

	# Wednesday, April 6, 2016 --> April 6, 2016
	return ($parsedDate.ToLongDateString().Substring($parsedDate.ToLongDateString().IndexOf(',') + 2))
}

###################################
#
# FUNCTION ExtractNamePriorToMarker
#
###################################
function ExtractNamePriorToMarker([System.IO.FileSystemInfo] $show)
{
	$showName = $show.BaseName

	$marker = (ExtractShowAirDate $show.FullName)[2]
	if($marker -eq $null)
	{
		$marker = (ParseSeasonViaString $showName)[1]
	}

	if($marker -ne $null)
	{
		$showName = $showName.SubString(0, $showName.IndexOf($marker))
		$showName = $showName.Trim()
	}

	return $showName
}

############################
#
# FUNCTION ExtractShowSeason
#
############################
function ExtractShowSeason([System.IO.FileSystemInfo] $show)
{
	if(IsMediaInformationCached $show.FullName "ExtractShowSeason")
	{
		return $CURRENT_MEDIA_EXTRACTED_INFO.ExtractShowSeason
	}

	$parseResult = ParseSeason $show
	$season = $parseResult[0]
	$seasonMarker = $parseResult[1]

	$tmpInt = 0
	if($seasonMarker -eq $null -or ([int]::TryParse($seasonMarker, [ref] $tmpInt)))
	{
		$tmpSeason = (ParseSeason $show)[0]
		if($tmpSeason -ne $null)
		{
			if([int]::TryParse($tmpSeason, [ref] $tmpInt))
			{
				$season = ([String]::Format("{0:D2}", $tmpInt))
			}
			else
			{
				$season = $tmpSeason
			}
		}
	}

	if($season -ne $null)
	{
		if($season.Length -eq 0)
		{
			$season = "00"
		}
	}

	if($season -eq $null)
	{
		DebugLogInfo SEASON NOT DETECTED: [($show.Name)]
		return $null
	}

	$tmpValue = ("S" + $season)
	UpdateMediaCacheInformation $show.FullName "ExtractShowSeason" $tmpValue
	return $tmpValue
}

############################
#
# FUNCTION ParseSeason
#
############################
function ParseSeason([System.IO.FileSystemInfo] $show, [bool] $airDateParsed)
{
	$showName = [System.IO.Path]::GetFileNameWithoutExtension($show.Name)
	$parseSeasonResults = ParseSeasonViaString $showName $airDateParsed

	return $parseSeasonResults
}

##############################
#
# FUNCTION FormatSeasonEpisode
#
##############################
function FormatSeasonEpisode($season, $episode)
{
	if(($season -ne $null) -and ($season.GetType().Name -eq "Int32"))
	{
		$season = $season.ToString()
	}

	if(($episode -ne $null) -and ($episode.GetType().Name -eq "Int32"))
	{
		$episode = $episode.ToString()
	}

	if((-not (IsStringDefined $season)) -and (-not (IsStringDefined $episode)))
	{
		LogError Invalid Season and Episode. [$season] [$episode]
		return
	}

	if((-not (IsStringDefined $season)) -and ($episode -eq '0'))
	{
		LogError Invalid Episode. [$episode]
		return
	}

	$stringToMatch = $null
	$stripSeason = ($season -eq $null)

	if(IsStringDefined $season)
	{
		$pattern = '[Ss]\s*(?<season>\d+)'
		if($season -match $pattern)
		{
			$stringToMatch = $stringToMatch + "S" + $matches["season"]
		}
		else
		{
			$stringToMatch = $stringToMatch + "S" + $season
		}
		$pattern = '[Ee]\s*(?<episode>\d+)'
		if($season -match $pattern)
		{
			$stringToMatch = $stringToMatch + "E" + $matches["episode"]
		}
	}

	if(IsStringDefined $episode)
	{
		$pattern = '[Ee]\s*(?<episode>\d+)'
		if($episode -match $pattern)
		{
			$stringToMatch = $stringToMatch + "E" + $matches["episode"]
		}
		else
		{
			$stringToMatch = $stringToMatch + "E" + $episode
		}
	}

	if($stringToMatch -notmatch '[Ss]\s*(?<season>\d+)')
	{
		$stringToMatch =  "S0" + $stringToMatch
	}

	$results = ParseSeasonViaString (" " + $stringToMatch)
	if(IsStringDefined $results[0])
	{
		$season = $results[0]
	}
	if(IsStringDefined $results[2])
	{
		$episode = $results[2]
	}
	if(IsStringDefined $results[1])
	{
		$marker = $results[1]
	}

	if(-not (IsStringDefined $season))
	{
		$seasonEpisodeString = SeasonEpisodeString "0" $episode
	}
	else
	{
		$seasonEpisodeString = SeasonEpisodeString $season $episode
	}
	$seasonEpisodeString = $stringToMatch.Replace($marker, $seasonEpisodeString).Trim()

	if($stripSeason)
	{
		$seasonEpisodeString =  $seasonEpisodeString -replace "[Ss]\d+";
	}

	return $seasonEpisodeString
}

##############################
#
# FUNCTION SeasonEpisodeString
#
##############################
function SeasonEpisodeString($season, $episode)
{
	if(FailedStringValidation $episode) { return }

	$seasonNumber = 0
	$episodeNumber = 0

	if(($season -ne $null) -and -not ([int]::TryParse($season, [ref] $seasonNumber)))
	{
		LogError Invalid Season: [$season]
		return $null
	}

	if(-not ([int]::TryParse($episode, [ref] $episodeNumber)))
	{
		LogError Invalid Episode: [$episode]
		return $null
	}

	$result = ""
	if($season -ne $null)
	{
		$result = $result + [String]::Format("S{0:D2}", $seasonNumber)
	}

	if($episodeNumber -gt 99)
	{
		$result = $result + [String]::Format("E{0:D3}", $episodeNumber)
	}
	else
	{
		$result = $result + [String]::Format("E{0:D2}", $episodeNumber)
	}
	return $result
}

###############################
#
# FUNCTION ParseSeasonViaString
#
###############################
function ParseSeasonViaString($showName, [bool] $dateParsed)
{
	if(FailedStringValidation $showName) { return }

	$season = $null
	$episode = $null
	$marker = $null

	$delimiter = "(\s+|a|b|,|\]|\[|-|\.)"

	if(	($showName -match ($delimiter + "(?<marker>(S|s)(?<season>\d+)((E|e)(?<episode>\d+))+)" + $delimiter)) -or
		($showName -match ("^(?<marker>(S|s)(?<season>\d+)((E|e)(?<episode>\d+))+)" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>(S|s)(?<season>\d+)((E|e)(?<episode>\d+))+)$"))
	)
	{

		#EXAMPLE: Community - S01E08 - Home Economics.avi
		$season = $matches["season"]
		$marker = $matches["marker"]
		$episode = $matches["episode"]
	}
	elseif(	($showName -match ($delimiter + "(?<marker>(?<season>\d+)((X|x)(?<episode>\d+))+)" + $delimiter)) -or
		($showName -match ("^(?<marker>(?<season>\d+)((X|x)(?<episode>\d+))+)" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>(?<season>\d+)((X|x)(?<episode>\d+))+)$"))
	)
	{
		#EXAMPLE: Girl Meets World 1x03 Girl Meets Sneak Attack.mkv
		$season = $matches["season"]
		$marker = $matches["marker"]
		$episode = $matches["episode"]
	}
	elseif(	($showName -match ($delimiter + "(?<marker>Season\s*(?<season>\d+))" + $delimiter)) -or
		($showName -match ("^(?<marker>Season\s*(?<season>\d+))" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>Season\s*(?<season>\d+))$"))
	)
	{
		#EXAMPLE: Firefly Season 1 - s01e03 - Bushwhacked.mp4
		#EXAMPLE: The X-Files Season 09 Episode 12 - Underneath.avi
		$season = $matches["season"]
		$marker = $matches["marker"]

		if(	($showName -match ($delimiter + "(?<marker>Episode\s*(?<episode>\d+))" + $delimiter)) -or
			($showName -match ("^(?<marker>Episode\s*(?<episode>\d+))" + $delimiter)) -or
			($showName -match ($delimiter + "(?<marker>Episode\s*(?<episode>\d+))$"))
		)
		{
			$episode = $matches["episode"]
		}

	}
	elseif(	($showName -match ($delimiter + "(?<marker>(S|s)\s*(?<season>\d+)\s*((E|e)\s*(?<episode>\d+))+)" + $delimiter)) -or
		($showName -match ("^(?<marker>(S|s)\s*(?<season>\d+)\s*((E|e)\s*(?<episode>\d+))+)" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>(S|s)\s*(?<season>\d+)\s*((E|e)\s*(?<episode>\d+))+)$"))
	)
	{
		#EXAMPLE: Arrested Development - S3 E 12 - Exit Strategy.avi
		$season = $matches["season"]
		$marker = $matches["marker"]
		$episode = $matches["episode"]
	}
	elseif(	($showName -match ($delimiter + "(?<marker>(S|s)(?<season>\d+)" + $delimiter + "((E|e)(?<episode>\d+))+)" + $delimiter)) -or
		($showName -match ("^(?<marker>(S|s)(?<season>\d+)" + $delimiter + "((E|e)(?<episode>\d+))+)" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>(S|s)(?<season>\d+)" + $delimiter + "((E|e)(?<episode>\d+))+)$"))
	)
	{
		#EXAMPLE: serial - s03e01.mp3
		$season = $matches["season"]
		$marker = $matches["marker"]
		$episode = $matches["episode"]
	}
	elseif(	($showName -match ($delimiter + "(?<marker>Episode\s*(?<episode>\d+))" + $delimiter)) -or
		($showName -match ("^(?<marker>Episode\s*(?<episode>\d+))" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>Episode\s*(?<episode>\d+))$"))
	)
	{
		$season = "1"
		$marker = $matches["marker"]
		$episode = $matches["episode"]
	}
	elseif(	($showName -match ($delimiter + "(?<marker>(E|e)(?<episode>\d+))" + $delimiter)) -or
		($showName -match ("^(?<marker>(E|e)(?<episode>\d+))" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>(E|e)(?<episode>\d+))$"))
	)
	{
		#EXAMPLE: Up First - E1829 - Wednesday, November 23rd, 2022.mp3
		$season = $null
		$marker = $matches["marker"]
		$episode = $matches["episode"]
	}
	elseif(	(-not $dateParsed) -and
	(($showName -match ("^(?<marker>(?<season>\d{3}))" + $delimiter)) -or
		($showName -match ("^(?<marker>(?<season>\d{4}))" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>(?<season>\d{3}))" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>(?<season>\d{4}))" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>(?<season>\d{3}))$")) -or
		($showName -match ($delimiter + "(?<marker>(?<season>\d{4}))$"))
	))
	{
		#EXAMPLE: 315 - Half-Wit.avi
		#C:\South Park\Season 12\1207 - Super Fun Time.avi

		if($matches["season"].Length -eq 3)
		{
			$season = $matches["season"].Substring(0, 1)
			$marker = $matches["marker"]
			$episode = $matches["season"].Substring(1)
		}
		elseif(($matches["season"].Length -eq 4) -and -not ($matches["season"].StartsWith("20")))
		{
			$season = $matches["season"].Substring(0, 2)
			$marker = $matches["marker"]
			$episode = $matches["season"].Substring(2)
		}
		else
		{
			$season = $null
			$marker = $null
			$episode = $null
		}
	}
	elseif(	(-not $dateParsed) -and
		(($showName -match ($delimiter + "(?<marker>Part\s*(?<episode>(I|V|X)+))" + $delimiter)) -or
		($showName -match ("^(?<marker>Part\s*(?<episode>(I|V|X)+))" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>Part\s*(?<episode>(I|V|X)+))$"))
	))
	{
		#EXAMPLE: House of Saddam - Part I.mp4
		$season = "1"
		$marker = $matches["marker"]
		$romanNumber = $matches["episode"]
		$episode = (ParseRomanNumeral $romanNumber).ToString()
	}
	elseif(	(-not $dateParsed) -and
		(($showName -match ($delimiter + "(?<marker>Part\s*(?<episode>\d+))" + $delimiter)) -or
		($showName -match ("^(?<marker>Part\s*(?<episode>\d+))" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>Part\s*(?<episode>\d+))$"))
	))
	{
		#EXAMPLE: The Jinx - The Life and Deaths of Robert Durst Part 1.mkv
		$season = "1"
		$marker = $matches["marker"]
		$episode = $matches["episode"]
	}
	elseif( (-not $dateParsed) -and
		(($showName -match ($delimiter + "(?<marker>Chapter\s*(?<episode>\d+))" + $delimiter)) -or
		($showName -match ("^(?<marker>Chapter\s*(?<episode>\d+))" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>Chapter\s*(?<episode>\d+))$"))
	))
	{
		#EXAMPLE: The Jinx - The Life and Deaths of Robert Durst Chapter 1.mkv
		$season = "1"
		$marker = $matches["marker"]
		$episode = $matches["episode"]
	}
	elseif( (-not $dateParsed) -and
		(($showName -match ($delimiter + "(?<marker>(S|s)(?<season>\d+))" + $delimiter)) -or
		($showName -match ("^(?<marker>(S|s)(?<season>\d+))" + $delimiter)) -or
		($showName -match ($delimiter + "(?<marker>(S|s)(?<season>\d+))$"))
))
	{
		#EXAMPLE: C:\South Park\S1\101 - Cartman Gets an Anal Probe.avi
		$season = $matches["season"]
		$marker = $matches["marker"]
		$episode = $null
	}

	DebugLogInfo SEASON NUMBER: $showName [$season]
	DebugLogInfo EPISODE NUMBER: $showName [$episode]
	DebugLogInfo SEASON MARKER: $showName [$marker]

	return @($season, $marker, $episode)
}

############################
#
# FUNCTION ExtractShowName
#
############################
function ExtractShowName([System.IO.FileSystemInfo] $show)
{
	if(FailedStringValidation $show.FullName) { return }

	if(IsMediaInformationCached $show.FullName "ExtractShowName")
	{
		return $CURRENT_MEDIA_EXTRACTED_INFO.ExtractShowName
	}

	$showName = $null;

	if(($show -ne $null) -and ($show.Name -ne $null))
	{
		# Streamline assumptions if we think this was already renamed by us
		if($show.BaseName.Contains(" - "))
		{
			$tmpShowName = $show.BaseName
			$tmpShowName = $tmpShowName.Substring(0, $tmpShowName.IndexOf(" -")).Trim()
			return $tmpShowName
		}

		$showName = ExtractNamePriorToMarker $show

		#EXAMPLES
		#The Daily Show With Trevor Noah - February 2, 2016 - Peter Bergen.mp4
		#The Nightly Show with Larry Wilmore - s02e56 - Thu, Jan 28, 2016.mp4

		if($showName.Contains(" -"))
		{
			$showName = $showName.Substring(0, $showName.IndexOf(" -")).Trim()
		}

		if($showName.Contains(" _"))
		{
			$showName = $showName.Substring(0, $showName.IndexOf(" _")).Trim()
		}

		#CAPITOLIZE
		if(IsStringDefined $showName)
		{
			$showName = $showName.Chars(0).ToString().ToUpper() + $showName.SubString(1)
		}

		#SOME NAMES USE '.' RATHER THAN SPACES
		if($showName.EndsWith('.'))
		{
			$showName = $showName.Replace('.', ' ')
			$showName = $showName.Trim()
		}

		if($showName.Contains('.') -and -not($showName.Contains(' ')))
		{
			$showName = $showName.Replace('.', ' ')
			$showName = $showName.Trim()
		}

		#REMOVE 'SEASON X' if in title
		if($showName -match "\s+Season \d+")
		{
			$showName = $showName.SubString(0, $showName.IndexOf($matches[0]))
		}

		#REPLACE UNDERSCORES, BRACKETS
		$showName = $showName.Trim(']', '[', ' ', '-', '_')
	}

	if(($showName -ne $null) -and ($showName.Length -eq [System.IO.Path]::GetFileNameWithoutExtension($show.Name).Length))
	{
		$showName = $null
	}

	if($showName -eq $null)
	{
		#TRY TO EXTRACT FROM PATH
		#EXAMPLE C:\CinAdmin\DATA\Media\TV\Watched\House MD\Season 2\201 - Acceptance.avi
		$tmpInt = 0
		$isInteger = ([int]::TryParse($showName, [ref] $tmpInt))
		if($isInteger -or ($showName -eq $null) -or ($showName.Length -eq 0))
		{
			$showName = ExtractShowNameFromPath $show
		}

		if($showName -ne $null)
		{
			$showName = $showName.Trim()
			if($showName.Length -eq 0)
			{
				$showName = $null
			}
		}
	}

	$originalShowName = $showName
	if($showName -ne $null)
	{
		if($showName.EndsWith(("(Podcast)")))
		{
			$showName = $showName.Substring(0, ($showName.Length - "(Podcast)".Length)).Trim()
		}

		if($showName.Contains(":"))
		{
			$showName = $showName.Replace(":", "_")
		}
	}

	if($showName -eq $null)
	{
		DebugLogInfo NAME NOT DETECTED: [($show.Name)]
	}
	else
	{
		DebugLogInfo NAME: $show.Name [$showName]
		if($originalShowName -ne $showName)
		{
			LogWarning Incompatible Show Name extracted: [($show.Name)] [$showName]
		}
	}

	UpdateMediaCacheInformation $show.FullName "ExtractShowName" $showName

	return $showName
}

##################################
#
# FUNCTION ExtractShowNameFromPath
#
##################################
function ExtractShowNameFromPath([System.IO.FileSystemInfo] $show)
{
	$showName = $null

	$toParse = $show.Directory.FullName

	if($toParse -eq $null)
	{
		LogWarning Cannot extract show name from path: [($show.FullName)]
		return $null
	}

	# IMPORTANT: ORDER DIRECTORY LIST FROM SPECIFIC TO MORE GENERAL PATHS
	foreach ($currentDirectory in @($PURGE_PATH, (DeriveWatchedPath $show.FullName), $DOWNLOADED_PODCASTS_PATH, $DOWNLOADED_TV_PATH, $DOWNLOADED_MOVIES_PATH))
	{
		if($toParse.Contains($currentDirectory))
		{
			$toParse = $toParse.Substring(($toParse.IndexOf($currentDirectory) + $currentDirectory.Length))
			$toParse = $toParse.Trim('\')
		}
	}

	$showParts = $toParse.Split('\')
	for ($i = ($showParts.Length - 1); $i -ge 0; $i--)
	{
		$toParse = $showParts[$i].Trim()
		$toParse = " " + $toParse + " "
		$season = (ParseSeasonViaString $toParse)[0]
		if($season -eq $null)
		{
			$showName = $toParse.Trim()
			break
		}
	}

	return $showName
}

##########################
#
# FUNCTION RefreshMetadata
#
##########################
function RefreshMetadata([System.IO.FileSystemInfo] $file, $seriesName, $title)
{
	if(-not (CanEmbedMetadata $file.FullName))
	{
		return
	}

	if($seriesName -eq $null)
	{
		$seriesName = ""
	}
	elseif($EXTENDED_CHAR_DIAGNOSTICS -and ($seriesName.Contains("_")))
	{
		LogWarning Series name stripped for metadata [$seriesName]
	}

	if($title -eq $null)
	{
		$title = ""
	}

	$metaData = ExtractMetadataTags $file.FullName
	$originalMetaData = $metaData.Clone()

	$seriesName = $seriesName.Trim(' ', '-')
	$title = $title.Trim(' ', '-')
	$airedDateResults = ExtractShowAirDate $file
	$airedDate = $airedDateResults[0]

	if(IsStringDefined $airedDateResults[2])
	{
		$title = SanitizeTitleFromRedundantParts $title ($airedDateResults[2]) $true
	}

	if(-not $metaData.ContainsKey("title"))
	{
		$metaData.Add("title", $title)
	}
	elseif($metaData["title"] -ne $title)
	{
		if(($metaData["title"] | BeautifyAndDemoteString) -eq ($title | BeautifyAndDemoteString))
		{
			if($EXTENDED_CHAR_DIAGNOSTICS -and ($metaData["title"] -ne $title))
			{
				LogWarning Keeping more fidelity in metadata title: [($metaData["title"])]
			}
		}
		else
		{
			$metaData["title"] = $title | BeautifyAndDemoteString
		}
	}

	if($EXTENDED_CHAR_DIAGNOSTICS -and $title.Contains("_"))
	{
		LogWarning Metadata might have stripped extended characters: [($file.FullName)] [$title]
	}

	if(IsAudio $file.FullName)
	{
		$seriesMetaDataProperty = "artist"
	}
	else
	{
		$seriesMetaDataProperty = "show"
	}

	$targetAlbumName = $null
	$targetTrackNumber = $null
	if(IsAudio $file.FullName)
	{
		$results = ParseSeason $file
		$seasonString = $results[0]
		$episodeString = $results[2]

		if((IsDailyShow $file) -and (IsStringDefined $DAILY_FOLDER_NAME))
		{
			$targetAlbumName = $DAILY_FOLDER_NAME
		}
		elseif((IsStringDefined $ARCHIVE_FOLDER_NAME) -and ($file.Directory.Name -eq $ARCHIVE_FOLDER_NAME))
		{
			$targetAlbumName = $seriesName + " - " + $ARCHIVE_FOLDER_NAME
		}
		elseif($seasonString -ne $null)
		{
			$targetAlbumName = $seriesName + " - " + $seasonString
		}
		else
		{
			$targetAlbumName = $seriesName
		}

		if(-not $metaData.ContainsKey("album"))
		{
			$metaData.Add("album", $targetAlbumName)
		}
		else
		{
			$metaData["album"] = $targetAlbumName
		}

		if(-not $metaData.ContainsKey("album_artist"))
		{
			$metaData.Add("album_artist", $seriesName)
		}
		else
		{
			$metaData["album_artist"] = $seriesName
		}

		#
		# To be grouped as an album in Plex, each file in a folder needs unique track number.
		#	(https://forums.plex.tv/t/music-one-new-album-per-song-after-scan-library-files-when-tracks-have-no-title-number/482616/2)
		#
		if($episodeString -ne $null)
		{
			$targetTrackNumber = [String]::Format("{0:N2}", $episodeString)
		}
		else
		{
			$targetTrackNumber = ([String]::Format("{0:D4}{1:D2}{2:D2}", $airedDate.Year, $airedDate.Month, $airedDate.Day))
		}

		if(-not $metaData.ContainsKey("track"))
		{
			$metaData.Add("track", $targetTrackNumber)
		}
		else
		{
			$metaData["track"] = $targetTrackNumber
		}
	}

	if(-not $metaData.ContainsKey($seriesMetaDataProperty))
	{
		$metaData.Add($seriesMetaDataProperty, $seriesName)
	}
	else
	{
		if(($metaData[$seriesMetaDataProperty] | BeautifyAndDemoteString) -eq ($seriesName | BeautifyAndDemoteString))
		{
			if($EXTENDED_CHAR_DIAGNOSTICS -and ($metaData[$seriesMetaDataProperty] -ne $seriesName))
			{
				LogWarning Keeping more fidelity in metadata series: [($metaData[$seriesMetaDataProperty])]
			}
		}
		else
		{
			$metaData[$seriesMetaDataProperty] = $seriesName | BeautifyAndDemoteString
		}
	}

	$metadataTargetPath = $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
	$metadataTarget = $metadataTargetPath + "\" + ($file.Name.Replace($file.Extension, ($file.Extension + $METADATA_EXTENSION)))
	$targetMedia = $metadataTargetPath + "\" + $file.Name
	CreateDirInPath $metadataTarget

	if(IsValidPath $metadataTarget)
	{
		LogWarning Overwriting: [$metadataTarget]
		DeleteItem $metadataTarget
	}

	$sourceMetadata = $metadata
	";FFMETADATA1" | SafeOutFile -LiteralPath $metadataTarget

	$cChanges = 0
	foreach ($currentSourceTag in $sourceMetadata.Keys)
	{
		$metaString = $sourceMetadata[$currentSourceTag]
		#[RegEx]::Escape() is overzealous in this case
		$metaString = $metaString.Replace('#', '\#');
		$metaString = $metaString.Replace(';', '\;');
		$metaString = $metaString.Replace('=', '\=');
		$metaString = $currentSourceTag + "=" + $metaString
		if(-not $originalMetaData.ContainsKey($currentSourceTag))
		{
			VerboseLogInfo ADD METADATA TAG: [$metadataTarget] [$metaString]
			$cChanges++
		}
		elseif($originalMetaData[$currentSourceTag] -cne $sourceMetadata[$currentSourceTag])
		{
			VerboseLogInfo MODIFY METADATA TAG: [$metadataTarget] [$currentSourceTag] [($originalMetaData[$currentSourceTag])] --> [$metaString]
			$cChanges++
		}
		else
		{
			VerboseLogInfo COPY METADATA TAG: [$metadataTarget] [$metaString]
		}

		$metaString | SafeOutFile -LiteralPath $metadataTarget
	}

	if($WHATIF_MODE)
	{
		return
	}

	if($cChanges -eq 0)
	{
		VerboseLogInfo No Metadata to refresh.

		if(IsValidPath $metadataTarget)
		{
			DeleteItem $metadataTarget
		}
		return
	}

	if(IsInvalidPath $metadataTarget)
	{
		LogError Metadata file not persisted: [$metadataTarget]
		return
	}

	ConvertMedia ($file.FullName) $targetMedia

	if(IsInvalidPath $targetMedia)
	{
		LogError Metadata not copied to: [$targetMedia]
		return
	}

	$metadataProblem = $false
	$newMetaData = ExtractMetadataTags $targetMedia
	foreach ($currentSourceTag in $sourceMetadata.Keys)
	{
		if($currentSourceTag -NotIn @("title", "artist", "show", "album", "track", "album_artist"))
		{
			continue
		}

		if(-not $newMetaData.ContainsKey($currentSourceTag))
		{
			LogWarning Metadata was not added. Need to convert? [$targetMedia] [$currentSourceTag]
			$metadataProblem = $true
			continue
		}
		if($newMetaData[$currentSourceTag] -cne $sourceMetadata[$currentSourceTag])
		{
			LogWarning Metadata was not copied. Need to convert? [$targetMedia] [$currentSourceTag]
			$metadataProblem = $true
			continue
		}
	}

	if($metadataProblem)
	{
		DeleteItem $targetMedia
		return
	}

	DeleteItem $file.FullName

	MoveFile $targetMedia ($file.Directory.FullName)
	if(IsValidPath $targetMedia)
	{
		LogWarning Original Conversion Media still exists: [$targetMedia]
	}

	#WAIT UNTIL HANDLES ARE RELEASED
	if(IsValidPath $file.FullName)
	{
		if(IsFileCurrentlyOpen $file.FullName)
		{
			SleepFor 5
		}
	}

	if(IsValidPath $targetMedia)
	{
		if(IsFileCurrentlyOpen $targetMedia)
		{
			SleepFor 5
		}
	}

	PurgeEmptyFolder $metadataTargetPath
}

#####################################
#
# FindReferenceSegmentFile
#
#####################################
function FindReferenceSegmentFile($rootFolder, [String[]] $extensions)
{
	$relatedFiles = FindRelatedFiles @($rootFolder + "\tmp.txt") $extensions
	$reference = $null
	foreach ($currentRelatedFile in $relatedFiles)
	{
		$currentRelatedFile = Get-Item -LiteralPath $currentRelatedFile
		if($currentRelatedFile.Name.Contains(".S1."))
		{
			$reference = $currentRelatedFile
			break
		}
	}

	if($reference -eq $null)
	{
		if($relatedFiles.Count -eq 0)
		{
			DebugLogInfo Returning reference: null
			return $null
		}
		elseif($relatedFiles.Count -eq 1)
		{
			$reference = Get-Item -LiteralPath $relatedFiles
		}
		else
		{
			$reference = Get-Item -LiteralPath ($relatedFiles[0])
		}
	}

	DebugLogInfo Returning reference: [($reference.FullName)]
	return $reference
}

#####################################
#
# FindRelatedFiles
#
#####################################
function FindRelatedFiles([String[]] $references, [String[]] $extensions)
{
	$relatedMatches = New-Object -TypeName System.Collections.ArrayList
	foreach ($currentReference in $references)
	{
		$currentReferenceLocation = [System.IO.Path]::GetDirectoryName($currentReference)
		if(IsInvalidPath $currentReferenceLocation)
		{
			continue
		}

		foreach ($currentExtension in $extensions)
		{
			if(-not $currentExtension.StartsWith("*"))
			{
				$currentExtension = "*" + $currentExtension
			}
			foreach ($currentMatch in (Get-ChildItem -LiteralPath $currentReferenceLocation -Filter $currentExtension | Sort-Object -Property Name))
			{
				if((IsUnderPath $currentMatch.Directory.FullName $DATA_PATH) -and ($relatedMatches.Count -eq 0))
				{
					if((-not $relatedMatches.Contains($currentMatch.FullName)))
					{
						$relatedMatches.Add($currentMatch.FullName) > $null
					}
				}
				else
				{
					if((-not $relatedMatches.Contains($currentMatch.FullName)))
					{
						$relatedMatches.Add($currentMatch.FullName) > $null
					}
				}
			}
		}
	}
	return $relatedMatches
}

#####################################
#
# CanEmbedMetadata
#
#####################################
function CanEmbedMetadata($inputMedia)
{
	if([System.IO.Path]::GetExtension($inputMedia) -NotIn $MEDIA_EXTENSIONS)
	{
		return $false
	}

	if($WHATIF_MODE)
	{
		return $false
	}

	return $true
}

#####################################
#
# MediaHasSubtitles
#
#####################################
function MediaHasSubtitles($inputMedia)
{
	if([System.IO.Path]::GetExtension($inputMedia) -NotIn $MEDIA_EXTENSIONS)
	{
		return $false
	}

	if([System.IO.Path]::GetExtension($inputMedia) -In ".iso")
	{
		return $true
	}

	# EXAMPLES
	#index=0|codec_type=video
	#index=1|codec_type=subtitle
	#index=2|codec_type=subtitle
	#index=3|codec_type=audio
	#index=4|codec_type=video

	$output = ExecuteCommand $FFPROBE_BINARY "-i" $inputMedia "-print_format" "compact=print_section=0" "-show_entries" "stream=index,codec_type" "-v" "quiet"
	foreach ($currentOutputLine in $output)
	{
		if($currentOutputLine.Contains("codec_type=subtitle"))
		{
			return $true
			break
		}
	}

	return $false
}

#####################################
#
# CanEmbedSubtitles
#
#####################################
function CanEmbedSubtitles($inputMedia)
{
	if([System.IO.Path]::GetExtension($inputMedia) -NotIn $MEDIA_EXTENSIONS)
	{
		return $false
	}

	if(IsAudio $inputMedia)
	{
		return $false
	}

	if([System.IO.Path]::GetExtension($inputMedia) -In $SUBTITLE_EXTENSION_EXCLUSIONS)
	{
		return $false
	}

	if(-not $EMBED_SUBTITLES)
	{
		return $false
	}

	return $true
}

#####################################
#
# LongSum
#
#####################################
function LongSum([UInt64]$a, [UInt64]$b)
{
	return [UInt64](([Decimal]$a + $b) % ([Decimal]([UInt64]::MaxValue) + 1))
}

#####################################
#
# StreamHash
#
#####################################
function StreamHash([System.IO.Stream]$stream)
{
	$hashLength = 8
	[UInt64]$lhash = 0
	[byte[]]$buffer = New-Object byte[] $hashLength
	$i = 0
	while ( ($i -lt (65536 / $hashLength)) -and ($stream.Read($buffer, 0, $hashLength) -gt 0) )
	{
		$i++
		$lhash = LongSum $lhash ([BitConverter]::ToUInt64($buffer, 0))
	}
	return $lhash
}

#####################################
#
# MovieHash
#
#####################################
function MovieHash([string]$path)
{
	# REFERENCE:
	#	https://trac.opensubtitles.org/projects/opensubtitles/wiki/HashSourceCodes

	if(IsFileCurrentlyOpen $path)
	{
		return 0
	}

	$stream = $null
	try
	{
		$stream = [System.IO.File]::OpenRead($path)
		[UInt64]$lhash = $stream.Length
		$lhash = LongSum $lhash (StreamHash $stream)
		$stream.Position = [Math]::Max(0L, $stream.Length - 65536)
		$lhash = LongSum $lhash (StreamHash $stream)
		return ("{0:X}" -f $lhash)
	}
	finally
	{
		if($stream -ne $null)
		{
			$stream.Close()
		}
	}
}

#####################################
#
# SearchForSubtitles
#
#####################################
function SearchForSubtitles($query, $parameters)
{
	# OpenSubtitles allows anonymous searches
	#$token = GetOpenSubtitlesToken
	#if(-not (IsStringDefined($token)))
	#{
	#	return
	#}

	$versionedUserAgent = $OPENSUBTITLES_USER_AGENT + " " + $PROJECT_VERSION

	$headers = @{}
	$headers.Add("User-Agent", $versionedUserAgent)
	$headers.Add("Api-Key", $OPENSUBTITLES_API_KEY)

	$searchUri = ($OPENSUBTITLES_BASE + "/api/v1" + "/subtitles?query=" + $query)
	foreach($currentParameter in $parameters.Keys)
	{
		$searchUri = $searchUri + "&" + $currentParameter + "=" + $parameters[$currentParameter]
	}
	DebugLogInfo Querying: [$searchUri]

	try
	{
		$response = Invoke-WebRequest -Uri $searchUri -Method GET -Headers $headers
	}
	catch
	{
		LogWarning Problem Initiating Subtitle search.
		LogError $_
		return $null
	}

	DebugLogInfo RESPONSE [$response]
	return ($response | ConvertFrom-Json)
}

#####################################
#
# DownloadSubtitleResult
#
#####################################
function DownloadSubtitleResult($targetMedia, $result)
{
	$token = GetOpenSubtitlesToken
	if(-not (IsStringDefined($token)))
	{
		return
	}

	$targetSrt = $targetMedia.Replace([System.IO.Path]::GetExtension($targetMedia), ".srt")

	if(IsValidPath $targetSrt)
	{
		LogWarning Subtitle alrady exists: [$targetSrt]
		return
	}

	$output = ExecuteCommand $FFPROBE_BINARY "-i" $targetMedia "-print_format" "compact=print_section=0" "-show_entries" "stream=r_frame_rate" "-v" "quiet"
	$sourceMediaFps = [String]::Format("{0:N2}", 0)
	foreach($currentOutputLine in $output)
	{
		if($currentOutputLine -match "r_frame_rate=(?<num>\d+)/(?<dom>\d+)$")
		{
			$numerator = [Int]::Parse($matches["num"])
			$denominator = [Int]::Parse($matches["dom"])

			if(($numerator -eq 0) -or ($denominator -eq 0))
			{
				continue
			}
			else
			{
				$sourceMediaFps = [String]::Format("{0:N3}", (($numerator/$denominator)))
				$subtitleFps = [String]::Format("{0:N3}", ($currentResult.attributes.fps))
				break
			}
		}
	}

	$versionedUserAgent = $OPENSUBTITLES_USER_AGENT + " " + $PROJECT_VERSION

	$headers = @{}
	$headers.Add("User-Agent", $versionedUserAgent)
	$headers.Add("Content-Type", "application/json")
	$headers.Add("Accept", "application/json")
	$headers.Add("Api-Key", $OPENSUBTITLES_API_KEY)
	$headers.Add("Authorization", "Bearer " + $token)

	if(($sourceMediaFps -ne "0.00") -and ($subtitleFps -ne $null) -and ($sourceMediaFps -ne $subtitleFps))
	{
		DebugLogInfo Convert FPS [$destinationFps] --> [$sourceMediaFps]
		$postData =   '{ "file_id": "' + ($currentResult.attributes.files[0].file_id) + '", ' +
						'"in_fps": '   + $subtitleFps + ', ' +
						'"out_fps": '  + $sourceMediaFps + ', ' +
						'"sub_format": "srt"' + ' }'
	}
	else
	{
		$postData =   '{ "file_id": "' + ($currentResult.attributes.files[0].file_id) + '", ' +
						'"sub_format": "srt"' + ' }'
	}

	DebugLogInfo OPENSUBITLE POST: [$postData]
	$searchUri = ($OPENSUBTITLES_BASE + "/api/v1" + "/download")
	try
	{
		$response = Invoke-WebRequest -Uri $searchUri -Method POST -Headers $headers -Body $postData -SkipHttpErrorCheck
	}
	catch
	{
		LogWarning Problem Initiating Subtitle search.
		LogError $_
		return
	}

	DebugLogInfo OPENSUBITLE RESPONSE: [$response]
	$jsonResult = ($response | ConvertFrom-Json)

	VerboseLogInfo OpenSubtitles Quota: [Remaining: ($jsonResult.remaining)] [Reset: ($jsonResult.reset_time)]
	if($jsonResult.remaining -eq "0")
	{
		$timeUntilQuotaReset = ([DateTime]::Parse($jsonResult.reset_time_utc)) - ([DateTime]::UtcNow)
		$timeUntilQuotaReset = (Get-Date).Add($timeUntilQuotaReset)

		LogWarning "OpenSubtitles now over quota until:" [($timeUntilQuotaReset.ToString())]
		if(IsValidPath $SUBTITLE_QUOTA)
		{
			DeleteItem $SUBTITLE_QUOTA
		}
		$timeUntilQuotaReset.ToString() | SafeOutFile -LiteralPath $SUBTITLE_QUOTA
		return
	}

	$source = $jsonResult.link
	if(-not (IsStringDefined $source))
	{
		LogError Problem downloading subtitle: [$response]
		return
	}
	DebugLogInfo DOWNLOADING SUBTITLE: [$source] --> [$targetSrt]
	CreateDirInPath $targetSrt

	if($WHATIF_MODE)
	{
		LogInfo WHATIF: DOWNLOADING SUBTITLE: [$source] --> [$targetSrt]
		continue
	}
	else
	{
		Invoke-WebRequest $source -OutFile $targetSrt
	}
}

#####################################
#
# OffsetMediaSubtitles
#
#####################################
function OffsetMediaSubtitles($inputMedia, $tsOffset)
{
	if(FailedStringValidation $inputMedia) { return }
	if(FailedStringValidation $tsOffset) { return }

	if(IsInvalidPath $SUBTITLE_EDIT_BINARY)
	{
		LogWarning Subtitle Edit is incorrectly configured.
		return
	}

	$Target = $inputMedia

	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

    $tmpLocation = $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
	$sourceItem = (Get-Item -LiteralPath $Target)
    $sourceSrt = $sourceItem.FullName.Replace($sourceItem.Directory.FullName, $tmpLocation)
    $sourceSrt = $sourceSrt.Replace($sourceItem.Extension, ".srt")

    LogInfo Exporting [$sourceSrt]

    #Export Subtitle
    CreateDirInPath $sourceSrt
    $dynamicParameters.Add("-y") > $null
    $dynamicParameters.Add("-i") > $null
    $dynamicParameters.Add($Target) > $null
    $dynamicParameters.Add($sourceSrt) > $null
    ExecuteFFmpegCommand $sourceSrt $dynamicParameters $null

    #Offset SRT
    LogInfo Ofsetting by [$tsOffset]...
    $dynamicParameters.Clear()
    $dynamicParameters.Add("/convert") > $null
    $dynamicParameters.Add($sourceSrt) > $null
    $dynamicParameters.Add("SubRip") > $null
    $dynamicParameters.Add("/offset:" + $tsOffset) > $null
    $dynamicParameters.Add("/overwrite") > $null
    ExecuteCommand $SUBTITLE_EDIT_BINARY $dynamicParameters > $null

    Copy-Item -LiteralPath $Target -Destination $tmpLocation
    LogInfo [$Target] --> [$tmpLocation]

    #Strip and incorporate new Subtitles
	$sourceMedia = [System.IO.Path]::Combine($tmpLocation, $sourceItem.Name)
	ConvertMedia $sourceMedia $Target $true

    $canContinue = $false
    $start = Get-Date
    while((-not $canContinue) -and (((Get-Date) - $start)).TotalSeconds -le 30)
    {
        if(IsFileCurrentlyOpen $Target)
        {
            SleepFor 5
        }
        else
        {
            $canContinue = $true
        }
    }

    if(-not $canContinue)
    {
        LogWarning Source media still open. Attempting to replace anyway... [$Target]
    }

    if(IsValidPath $sourceMedia)
    {
        $targetMediaInputRenamedExtension = [System.IO.Path]::GetExtension($Target)
        $targetMediaInputRenamed = $sourceMedia.Replace($targetMediaInputRenamedExtension, ".OldSubs" + $targetMediaInputRenamedExtension)
        RenameFile $sourceMedia $targetMediaInputRenamed
        PurgeItem $targetMediaInputRenamed
    }
    TrackSubtitledMedia $Target (Get-Date)
    PurgeEmptyDirectories $CONVERSION_TMP $true
}

#####################################
#
# ConvertMedia
#
#####################################
function ConvertMedia($inputMedia, $outputMedia)
{
	ConvertMedia $inputMedia $outputMedia $Force
}

#####################################
#
# ConvertMedia
#
#####################################
function ConvertMedia($inputMedia, $outputMedia, $forceConversion)
{
	if(FailedStringValidation $inputMedia) { return }
	if(FailedStringValidation $outputMedia) { return }

	if(IsInvalidPath $FFMPEG_BINARY)
	{
		LogWarning FFMPEG_BINARY is not configured correctly. Metadata cannot be refreshed.
		return;
	}

	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList
	$filesToDelete = New-Object -TypeName System.Collections.ArrayList

	if(IsValidPath $outputMedia)
	{
		LogWarning Overwriting media: [$outputMedia]...
	}
	$dynamicParameters.Add("-y") > $null

	if(IsInvalidPath $inputMedia)
	{
		LogWarning Source media not found: [$inputMedia]
		return
	}

	$sourceMediaDirectory = (Get-Item -LiteralPath $inputMedia).Directory.FullName

	if($DEBUG_MODE)
	{
		LogInfo Folder Contents: [$sourceMediaDirectory]
		$cFile = 0
		foreach ($currentSourceDirectoryFile in (Get-ChildItem -LiteralPath $sourceMediaDirectory -File))
		{
			$cFile++
			LogInfo [$cFile]: [($currentSourceDirectoryFile.FullName)]
		}

		$destinationMediaDirectory = [System.IO.Path]::GetDirectoryName($outputMedia)
		LogInfo Folder Contents: [$destinationMediaDirectory]
		$cFile = 0
		foreach ($currentDestinationDirectoryFile in (Get-ChildItem -LiteralPath $destinationMediaDirectory -File))
		{
			$cFile++
			LogInfo [$cFile]: [($currentDestinationDirectoryFile.FullName)]
		}
	}

	$cMapIndex = 0

	$concatMedia = $false
	if($inputMedia.EndsWith($SEGMENTS_EXTENSION))
	{
		$dynamicParameters.Add("-f") > $null
		$dynamicParameters.Add("concat") > $null
		$dynamicParameters.Add("-safe") > $null
		$dynamicParameters.Add("0") > $null

		$filesToDelete.Add($inputMedia) > $null
		$concatMedia = $true
	}

	$convertFormat = $false
	$inputExtension = [System.IO.Path]::GetExtension($inputMedia)
	$outputExtension = [System.IO.Path]::GetExtension($outputMedia)
	if(($inputExtension -In $MEDIA_EXTENSIONS) -and ($outputExtension -In $MEDIA_EXTENSIONS) -and ($inputExtension -ne $outputExtension))
	{
		$convertFormat = $true
	}

	#Detect Subtitles
	$useSubtitles = $false
	if((CanEmbedSubtitles $inputMedia) -and (-not $concatMedia))
	{
		$sourceSubtitleFiles = FindRelatedFiles @($inputMedia, $outputMedia) $SUBTITLE_EXTENSIONS
		$selectedSubtitleFile = $null
		foreach ($sourceSubtitleFile in $sourceSubtitleFiles)
		{
			$selectedSubtitleFile = $sourceSubtitleFile
			break
		}

		if((-not (IsStringDefined $selectedSubtitleFile)) -and (IsUnderPath $selectedSubtitleFile $CONVERSION_TMP))
		{
			if($sourceSubtitleFiles.Count -eq 1)
			{
				$selectedSubtitleFile = $sourceSubtitleFiles
			}
			else
			{
				LogError Too many subtitle files found: [($sourceSubtitleFiles.Count)]
				return
			}
		}

		if($selectedSubtitleFile -ne $null)
		{
			$dynamicParameters.Add("-i") > $null
			$dynamicParameters.Add($selectedSubtitleFile) > $null
			$filesToDelete.Add($selectedSubtitleFile) > $null

			$subtitleIndex = $cMapIndex
			$cMapIndex++

			$useSubtitles = $true
		}
	}

	$stripSubtitles = $false
	if($useSubtitles -and $forceConversion)
	{
		$stripSubtitles = $true
	}

	#Detect Thumbnails
	if($EMBED_THUMBNAIL -and (-not $concatMedia))
	{
		$sourceThumbnailFiles = FindRelatedFiles @($inputMedia, $outputMedia) $THUMBNAIL_EXTENSIONS
		if($sourceThumbnailFiles.Count -gt 1)
		{
			LogError Too many thumbnail files found: [($sourceThumbnailFiles.Count)]
			return
		}

		$useThumbnails = $false
		foreach ($sourceThumbnailFile in $sourceThumbnailFiles)
		{
			$dynamicParameters.Add("-i") > $null
			$dynamicParameters.Add($sourceThumbnailFile) > $null
			$filesToDelete.Add($sourceThumbnailFile) > $null

			$thumbnailIndex = $cMapIndex
			$cMapIndex++

			$useThumbnails = $true
		}
	}

	$dynamicParameters.Add("-i") > $null
	$dynamicParameters.Add($inputMedia) > $null
	$sourceMediaIndex = $cMapIndex
	$cMapIndex++

	#Detect Metadata
	$useMetadata = $false
	if((CanEmbedMetadata $inputMedia) -or $concatMedia)
	{
		$sourceMetadataFiles = FindRelatedFiles @($inputMedia, $outputMedia) @($METADATA_EXTENSION)
		if($sourceMetadataFiles.Count -gt 1)
		{
			LogError Too many metadata files found: [($sourceMetadataFiles.Count)]
			return
		}

		foreach ($sourceMetadataFile in $sourceMetadataFiles)
		{
			$dynamicParameters.Add("-i") > $null
			$dynamicParameters.Add($sourceMetadataFile) > $null
			$filesToDelete.Add($sourceMetadataFile) > $null

			$metadataIndex = $cMapIndex
			$cMapIndex++

			$useMetadata = $true
		}
	}

	if($useMetadata)
	{
		$dynamicParameters.Add("-map_metadata") > $null
		$dynamicParameters.Add($metadataIndex) > $null
	}
	else
	{
		$dynamicParameters.Add("-map_metadata") > $null
		$dynamicParameters.Add($sourceMediaIndex) > $null
	}

	if($useSubtitles)
	{
		$dynamicParameters.Add("-map") > $null
		$dynamicParameters.Add($subtitleIndex) > $null

	}

	if($useThumbnails)
	{
		$dynamicParameters.Add("-map") > $null
		$dynamicParameters.Add($thumbnailIndex) > $null
	}

	if((-not (IsAudio $outputMedia)) -and (-not $convertFormat))
	{
		if($stripSubtitles)
		{
			#EXAMPLES
			# Stream #0:3(eng): Video: h264 (High) (avc1 / 0x31637661), yuv420p(tv, bt709), 848x480, 379 kb/s, SAR 1:1 DAR 53:30, 23.98 fps, 23.98 tbr, 16k tbn, 180k tbc (default)
			# Stream #0:4(eng): Audio: aac (LC) (mp4a / 0x6134706D), 48000 Hz, stereo, fltp, 79 kb/s (default)
			$output = ExecuteCommand $FFPROBE_BINARY $inputMedia

			$videoStreamNo = $null
			$audioStreamNo = $null
			$thumbStreamNo = $null
			foreach ($currentOutputLine in $output)
			{
				$currentOutputLine = $currentOutputLine.ToString()
				if(($currentOutputLine -match 'Stream\s+\#\d:(?<streamNo>\d)([^\:]+)?:\s+(Video:)') -and (-not $currentOutputLine.Contains("Video: png")) -and (-not $currentOutputLine.Contains("(attached pic)")))
				{
					$videoStreamNo = $matches["streamNo"].Trim()
				}

				if($currentOutputLine -match 'Stream\s+\#\d:(?<streamNo>\d)([^\:]+)?:\s+(Audio:)')
				{
					$audioStreamNo = $matches["streamNo"].Trim()
				}

				if(-not $useThumbnails)
				{
					if(($currentOutputLine -match 'Stream\s+\#\d:(?<streamNo>\d)([^\:]+)?:\s+(Video:)') -and (($currentOutputLine.Contains("Video: png")) -or ($currentOutputLine.Contains("(attached pic)"))))
					{
						$thumbStreamNo = $matches["streamNo"].Trim()
					}
				}
			}

			DebugLogInfo Detected Streams: Audio [$audioStreamNo] Video [$videoStreamNo]

			if(($videoStreamNo -eq $null) -or ($audioStreamNo -eq $null))
			{
				LogError Video and/or Audio streams were not properly detected for stripping Subtitles.
				return
			}

			$dynamicParameters.Add("-map") > $null
			$dynamicParameters.Add([String]::Format("{0}:{1}", $sourceMediaIndex, $audioStreamNo)) > $null

			$dynamicParameters.Add("-map") > $null
			$dynamicParameters.Add([String]::Format("{0}:{1}", $sourceMediaIndex, $videoStreamNo)) > $null

			if(-not $useThumbnails)
			{
				$dynamicParameters.Add("-map") > $null
				$dynamicParameters.Add([String]::Format("{0}:{1}", $sourceMediaIndex, $thumbStreamNo)) > $null
			}
		}
		else
		{
			#Audio explicit mapping causes major slowdown in conversion
			$dynamicParameters.Add("-map") > $null
			$dynamicParameters.Add($sourceMediaIndex) > $null
		}

		if($concatMedia)
		{
			$dynamicParameters.Add("-codec") > $null
			$dynamicParameters.Add("copy") > $null
		}
		else
		{
			if($useThumbnails)
			{
				$dynamicParameters.Add("-c:v:1") > $null
				$dynamicParameters.Add("copy") > $null

				$dynamicParameters.Add("-c:v:0") > $null
				$dynamicParameters.Add("png") > $null
			}
			else
			{
				$dynamicParameters.Add("-c:v") > $null
				$dynamicParameters.Add("copy") > $null
			}

			$dynamicParameters.Add("-c:a") > $null
			$dynamicParameters.Add("copy") > $null

			if($useSubtitles)
			{
				$dynamicParameters.Add("-c:s") > $null
				$dynamicParameters.Add("mov_text") > $null
			}
			else
			{
				$dynamicParameters.Add("-c:s") > $null
				$dynamicParameters.Add("copy") > $null
			}
		}
	}

	if($useThumbnails)
	{
		$dynamicParameters.Add("-disposition:v:0") > $null
		$dynamicParameters.Add("attached_pic") > $null
	}

	$dynamicParameters.Add("-ignore_chapters") > $null
	$dynamicParameters.Add("1") > $null

	$dynamicParameters.Add($outputMedia) > $null

	if((-not (IsAudio $outputMedia)) -and (-not $concatMedia) -and (-not $useMetadata) -and (-not $useSubtitles) -and (-not $useThumbnails))
	{
		if((-not $stripSubtitles) -and ([System.IO.Path]::GetExtension($inputMedia) -eq [System.IO.Path]::GetExtension($outputMedia)))
		{
			LogWarning No conversion work detected. Executing FFMPEG anyway...
		}
	}
	ExecuteFFmpegCommand $outputMedia $dynamicParameters $filesToDelete
}

#####################################
#
# ConvertMediaToMp4
#
#####################################
function ConvertMediaToMpX($source)
{
	if(FailedStringValidation $source) { return }

	if(IsInvalidPath $source)
	{
		LogError Source media not valid: [$source]
		return
	}

	$targetExtension = ".mp4"
	if(IsAudio $source)
	{
		$targetExtension = ".mp3"
	}

	if([System.IO.Path]::GetExtension($source) -eq $targetExtension)
	{
		LogWarning Source media is already MP4 or MP3: [$source]
		return
	}

	$tmpLocation = $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
	$sourceDirectory = (Get-Item -LiteralPath $source).Directory.FullName

	$target = $source
	$target = $target.Replace($sourceDirectory, $tmpLocation)
	$target = $target.Replace((Get-Item -LiteralPath $source).Extension, $targetExtension)

	if(IsValidPath $target)
	{
		LogWarning Media is already converted to MP4 or MP3: [$target]
		return
	}

	CreateDirInPath $target
	ConvertMedia $source $target

	if(IsValidPath $target)
	{
		#IF SHOW WAS RENAMED, REMEMBER
		if(HasFileBeenRenamed $source)
		{
			# Non-Mp4 media might have incorrect metadata, force recheck
			UnTrackRenamedMedia $target
		}

		#IF SHOW WAS ALREADY TRACKED BEFORE, CONTINUE TO TRACK
		$currentlyTrackedMedia = LoadTrackedMedia
		if($currentlyTrackedMedia.ContainsKey($source))
		{
			$timeStamp = [System.DateTime]::Parse($currentlyTrackedMedia[$source])
			TrackWatchedMedia $target $timeStamp
		}

		#IF SHOW ALREADY HAD ADS REMOVED, REMEMBER
		$currentAdRemovedMedia = LoadAdRemovedMedia
		if($currentAdRemovedMedia.ContainsKey([System.IO.Path]::GetFileNameWithoutExtension($source)))
		{
			TrackAdRemovedMedia $target
		}
		if((-not (CanMediaHaveAdsRemoved $source)) -and (CanMediaHaveAdsRemoved $target))
		{
			TrackAdRemovedMedia $target
		}

		#SWAP CONVERTED FILE
		PurgeItem $source
		MoveFile $target $sourceDirectory
	}

	PurgeEmptyFolder $tmpLocation
}

#####################################
#
# RefreshISOPNGs
#
#####################################
function RefreshISOPNGs()
{
	VerboseLogInfo Refreshing PNG
	$seenIsos = New-Object -TypeName System.Collections.ArrayList
	foreach($currentPathToCheck in @($DOWNLOADED_MOVIES_PATH, (DeriveWatchedPath $DOWNLOADED_MOVIES_PATH)))
	{
		if(IsInvalidPath $currentPathToCheck) { continue }

		$media = Get-ChildItem -Recurse -LiteralPath $currentPathToCheck -File | Where-Object {$_.Extension.ToLower() -eq ".iso"}
		foreach($currentMedia in $media)
		{
			if($seenIsos.Contains($currentMedia.Name.ToLower()))
			{
				continue
			}
			$seenIsos.Add($currentMedia.Name.ToLower()) > $null
			$targetPng = [System.IO.Path]::Combine($ISO_PNG_PATH, ($currentMedia.BaseName + ".png"))
			if(IsInvalidPath $targetPng)
			{
				LogInfo CREATING: [$targetPng]
				CreateDirInPath $targetPng
				Copy-Item -LiteralPath $ISO_PNG -Destination $targetPng -Force
			}
		}
	}

	$targetInstructions = $ISO_INSTRUCTIONS_PNG.Replace($SHORTCUT_PATH, $ISO_PNG_PATH)
	if(IsInvalidPath $targetInstructions)
	{
		Copy-Item -LiteralPath $ISO_INSTRUCTIONS_PNG -Destination $targetInstructions -Force
	}
	elseif((Get-Item -LiteralPath $ISO_INSTRUCTIONS_PNG).Length -ne (Get-Item -LiteralPath $targetInstructions).Length)
	{
		Copy-Item -LiteralPath $ISO_INSTRUCTIONS_PNG -Destination $targetInstructions -Force
	}

	$isoPngs = Get-ChildItem -Recurse -LiteralPath $ISO_PNG_PATH -File | Where-Object {$_.Extension.ToLower() -eq ".png"}
	foreach($currentPng in $isoPngs)
	{
		if($currentPng.BaseName.StartsWith("#_"))
		{
			continue
		}
		if(-not ($seenIsos.Contains($currentPng.BaseName.ToLower() + ".iso")))
		{
			DeleteItem $currentPng.FullName
		}
	}
}

#####################################
#
# ScanForViewedISOPNG
#
#####################################
function ScanForViewedISOPNG()
{
	$isosToConvert = New-Object -TypeName System.Collections.ArrayList
	$start = Get-Date
	ExportPlexCurrentlyWatchingMedia

	if((IsValidPath $PLEX_OPENED_CSV) -and ((Get-Item -LiteralPath $PLEX_OPENED_CSV).LastWriteTime -lt $start))
	{
		return
	}

	$inputCSV = Import-Csv $PLEX_OPENED_CSV

	#Make sure still open and not randomly scanned by a client's app - measure twice
	$newerStart = Get-Date
	SleepFor 5
	ExportPlexCurrentlyWatchingMedia

	if((Get-Item -LiteralPath $PLEX_OPENED_CSV).LastWriteTime -lt $newerStart)
	{
		return
	}

	$newerInputCSV = Import-Csv $PLEX_OPENED_CSV

	#First measure
	foreach($currentCsvEntry in $inputCSV)
	{
		$openedMedia = $currentCsvEntry.SessionOpenMediaFile
		$plexTimestamp = [System.DateTime]::Parse($currentCsvEntry.DateTime)

		if($plexTimestamp -lt $start)
		{
			continue
		}

		if([System.IO.Path]::GetExtension($openedMedia) -In $MEDIA_EXTENSIONS)
		{
			#Not png file, something else
			continue
		}

		$openedMediaBaseName = [System.IO.Path]::GetFileNameWithoutExtension($openedMedia)
		$targetIsoFileName = $openedMediaBaseName + ".iso"

		if($openedMediaBaseName.StartsWith("#_"))
		{
			continue
		}

		#Second measure
		$stillOpen = $false
		foreach($newerCurrentCsvEntry in $newerInputCSV)
		{
			if(($newerCurrentCsvEntry.SessionOpenMediaFile -eq $openedMedia) -and ([System.DateTime]::Parse($newerCurrentCsvEntry.DateTime) -gt $newerStart))
			{
				$stillOpen = $true
			}
		}

		if(-not $stillOpen)
		{
			DebugLogInfo Item no longer open: [$openedMedia] [$plexTimestamp]
			continue
		}

		DebugLogInfo Searching for ISO: [$targetIsoFileName] [$plexTimestamp]
		foreach($currentPathToCheck in @($DOWNLOADED_MOVIES_PATH, (DeriveWatchedPath $DOWNLOADED_MOVIES_PATH)))
		{
			if(IsInvalidPath $currentPathToCheck) { continue }

			$files = Get-ChildItem -Recurse -LiteralPath $currentPathToCheck -File -Filter $targetIsoFileName
			foreach($currentOpenedFile in $files)
			{
				if($isosToConvert.Contains($currentOpenedFile.FullName))
				{
					continue
				}
				VerboseLogInfo FOUND ISO [($currentOpenedFile.FullName)]
				$isosToConvert.Add($currentOpenedFile.FullName) > $null
				$oldMediaName = $openedMedia
				$openedMedia = $openedMedia.Replace($openedMediaBaseName, "!" + $openedMediaBaseName + ".DETECTED")
				RenameFile $oldMediaName $openedMedia
			}
		}
	}
	return $isosToConvert
}

#####################################
#
# ConvertIsoToMkv
#
#####################################
function ConvertIsoToMkv($targetIsoFileName)
{
	if(FailedStringValidation $targetIsoFileName) { return }

	if(IsInvalidPath $targetIsoFileName)
	{
		LogWarning Target not found: [$targetIsoFileName]
		return
	}

	$openedMediaBaseName = [System.IO.Path]::GetFileNameWithoutExtension($targetIsoFileName)
	if(-not (IsStringDefined $ISO_CONVERT_PATH))
	{
		$targetIsoConversionPath = [System.IO.Path]::GetDirectoryName($targetIsoFileName)
		$targetIsoConversionPath = [System.IO.Path]::Combine($targetIsoConversionPath, $openedMediaBaseName)
	}
	else
	{
		$targetIsoConversionPath = [System.IO.Path]::Combine($ISO_CONVERT_PATH, $openedMediaBaseName)
	}

	if((IsValidPath $targetIsoConversionPath) -and ((Get-ChildItem -LiteralPath $targetIsoConversionPath).Count -gt 0))
	{
		VerboseLogInfo Already converted ISO: [$targetIsoConversionPath]
		continue
	}

	if(IsInvalidPath $targetIsoConversionPath)
	{
		CreateDirInPath ($targetIsoConversionPath + "\converted.mp4")
	}

	if($WHATIF_MODE)
	{
		LogInfo WHATIF: $MAKEMKV_BINARY "mkv" ([String]::Format("iso:{0}",$targetIsoFileName)) "all" $targetIsoConversionPath
		continue
	}
	else
	{
		ExecuteCommand $MAKEMKV_BINARY "mkv" ([String]::Format("iso:{0}",$targetIsoFileName)) "all" $targetIsoConversionPath > $null
	}

	foreach($convertedTitle in (Get-ChildItem -LiteralPath $targetIsoConversionPath -Recurse -FollowSymlink -File -Filter *.mkv))
	{
		if($convertedTitle.BaseName.Contains("_t00"))
		{
			$oldName = $convertedTitle.FullName
			$newName = $oldName.Replace($convertedTitle.BaseName, $openedMediaBaseName)

			$canContinue = $false
			while(-not $canContinue)
			{
				if(IsFileCurrentlyOpen $oldName)
				{
					SleepFor 5
				}
				else
				{
					$canContinue = $true
				}
			}
			RenameFile $oldName $newName
		}
	}

	$convertedMedia = Get-ChildItem -LiteralPath $targetIsoConversionPath -Recurse -FollowSymlink -File | Where-Object { ($_.Extension -In $MEDIA_EXTENSIONS) }
	if($convertedMedia.Count -le 0)
	{
		LogError Converted Media Missing: [$targetIsoConversionPath]
	}
}

#####################################
#
# ExecuteFFmpegCommand
#
#####################################
function ExecuteFFmpegCommand($outputMedia, [System.Collections.ArrayList] $dynamicParameters, [System.Collections.ArrayList] $filesToDelete)
{
	if(FailedStringValidation $outputMedia) { return }

	$logArgs = New-Object -TypeName System.Collections.ArrayList
	if($VERBOSE_MODE)
	{
		$logArgs.Add("-loglevel") > $null
		$logArgs.Add("verbose") > $null
	}
	else
	{
		$logArgs.Add("-loglevel") > $null
		$logArgs.Add("quiet") > $null
	}

	$output = $null
	if($WHATIF_MODE)
	{
		LogInfo WHATIF: $FFMPEG_BINARY $logArgs $dynamicParameters
	}
	else
	{
		$output = ExecuteCommand $FFMPEG_BINARY $logArgs $dynamicParameters
	}

	if($WHATIF_MODE)
	{
		return
	}
	elseif(IsInvalidPath $outputMedia)
	{
		LogWarning Converted Media does not exist: [$outputMedia]
		foreach ($currentOutputLine in $output)
		{
			LogError [$currentOutputLine]
		}
	}
	elseif((Get-Item -LiteralPath $outputMedia).Length -le (10 * 1024))
	{
		if([System.IO.Path]::GetExtension($outputMedia) -In $MEDIA_EXTENSIONS)
		{
			LogWarning Converted Media is less than 10 kilobytes! [$outputMedia]
			foreach ($currentOutputLine in $output)
			{
				LogError [$currentOutputLine]
			}
			DeleteItem $outputMedia
		}
	}
	else
	{
		foreach ($currentFileToDelete in $filesToDelete)
		{
			while(IsFileCurrentlyOpen $currentFileToDelete)
			{
				SleepFor 5
			}
			DeleteItem $currentFileToDelete
		}
	}
}

#####################################
#
# FUNCTION RefreshPlex
#
#####################################
function RefreshPlex()
{
	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

	ExecutePlexApiCommand $PLEX_REFRESH_SCRIPT $dynamicParameters
}

##########################
#
# FUNCTION RefreshKodi
#
##########################
function RefreshKodi()
{
	if(-not (IsKodiConfigured))
	{
		return
	}

	foreach ($currentKodiUri in $KODI_SERVERS.Keys)
	{
		VerboseLogInfo Refreshing Kodi Library [$currentKodiUri]...

		$jsonRequest = '{"jsonrpc": "2.0", "method": "VideoLibrary.Scan", "showdialogs": "false", "id": "1"}'
		$resultsJson = ExecuteKodiApi $currentKodiUri $jsonRequest
		if($resultsJson -eq $null)
		{
			continue
		}
	}
}

######################################
#
# FUNCTION GetToWatchShowCount
#
######################################
function GetToWatchShowCount($showName, [System.Collections.Hashtable] $showNameDictionary, $rootPath)
{
	if(FailedStringValidation $rootPath) { return 0 }
	if(FailedStringValidation $showName) { return 0 }

	if(IsUnderPath $rootPath (DeriveWatchedPath $DOWNLOADED_MOVIES_PATH))
	{
		LogError "Don't calculated to watch count on movies!"
		return
	}

	if($showNameDictionary -eq $null)
	{
		$showNameDictionary = @{}
	}

	foreach ($currentEntry in $showNameDictionary.Keys)
	{
		if($currentEntry -eq $showName)
		{
			return [int] $showNameDictionary[$showName]
		}
	}

	$numberOfShows = 0
	$matchedShows = Get-ChildItem -LiteralPath $rootPath -Recurse -FollowSymlink -File | Where-Object { $_.Name -match [RegEx]::Escape($showName) }

	if($matchedShows.Count -eq 0)
	{
		$originalShowName = $showName
		$showName = $showName.Replace('-', '_')
		$showName = $showName.Replace(':', '_')
		$matchedShows = Get-ChildItem -LiteralPath $rootPath -Recurse -FollowSymlink -File | Where-Object { $_.Name -match [RegEx]::Escape($showName) }
	}

	if(-not (IsWatched $rootPath))
	{
		$matchedShows = $matchedShows | Where-Object { (-not (IsWatched $_.Directory.FullName)) }
	}

	if(IsNotUnderPath $rootPath $PURGE_PATH)
	{
		$matchedShows = $matchedShows | Where-Object { (IsNotUnderPath $_.Directory.FullName $PURGE_PATH) }
	}

	foreach ($excludedPath in $DOWNLOADED_PATH_EXCLUSIONS)
	{
		$matchedShows = $matchedShows | Where-Object { (IsNotUnderPath $_.Directory.FullName $excludedPath) }
	}

	if($rootPath -eq $DOWNLOADED_TV_PATH -and (IsUnderPath $DOWNLOADED_PODCASTS_PATH $DOWNLOADED_TV_PATH))
	{
		$matchedShows = $matchedShows | Where-Object { (IsNotUnderPath $_.Directory.FullName $DOWNLOADED_PODCASTS_PATH) }
	}

	if($rootPath -eq $DOWNLOADED_PODCASTS_PATH -and (IsUnderPath $DOWNLOADED_TV_PATH $DOWNLOADED_PODCASTS_PATH))
	{
		$matchedShows = $matchedShows | Where-Object { (IsNotUnderPath $_.Directory.FullName $DOWNLOADED_TV_PATH) }
	}

	if($matchedShows -ne $null)
	{
		$numberOfShows = $matchedShows.Count
	}

	if(-not $showNameDictionary.ContainsKey($showName))
	{
		$showNameDictionary.Add($showName, $numberOfShows)
	}

	if(($originalShowName -ne $null) -and (-not $showNameDictionary.ContainsKey($originalShowName)))
	{
		$showNameDictionary.Add($originalShowName, $numberOfShows)
	}

	DebugLogInfo SHOW COUNT: $showName [$numberOfShows]

	return 	$numberOfShows
}

############################
#
# FUNCTION ShowConfiguration
#
############################
function ShowConfiguration([bool] $overrideVerbose)
{
	if(-not ($VERBOSE_MODE -or $overrideVerbose))
	{
		return
	}

	foreach ($currentPathVariableName in @("DOWNLOADED_TV_PATH", "DOWNLOADED_PODCASTS_PATH", "DOWNLOADED_MOVIES_PATH"))
	{
		$currentPath = (Get-Variable -Name $currentPathVariableName).Value
		if(IsInvalidPath $currentPath) { continue }
		$currentWatchedPath = DeriveWatchedPath ($currentPath)
		LogInfo PATH [("$" + $currentPathVariableName) ]: [($currentPath) "] --> [" $currentWatchedPath ]
	}
	LogInfo PATH ['$PURGE_PATH']: $PURGE_PATH

	foreach ($currentKey in $ORGANIZE_PATH_OVERRIDE.Keys)
	{
		LogInfo PATH [DOWNLOADED MAPPING]: [$currentKey] --> [($ORGANIZE_PATH_OVERRIDE[$currentKey])]
	}

	foreach ($currentKey in $CONSUMED_PATH_OVERRIDE.Keys)
	{
		LogInfo PATH [WATCHED MAPPING]: [$currentKey] --> [($CONSUMED_PATH_OVERRIDE[$currentKey])]
	}

	foreach ($currentEntry in $DOWLOADED_EXCLUSIONS)
	{
		LogInfo PATH [EXCLUDE DIRECTORY]: $currentEntry
	}

	LogInfo PATH ['$HANDLE_BINARY']: $HANDLE_BINARY

	LogInfo PATH ['$POWERSHELL_FILE']: $POWERSHELL_FILE
	LogInfo PATH ['$POWERSHELL_32_FILE']: $POWERSHELL_32_FILE

	LogInfo PATH ['$RECORDING_DATABASE']: $RECORDING_DATABASE
	LogInfo PATH ['$SQL_LITE']: $SQL_LITE
}

################################
#
# FUNCTION GetOpenSubtitlesToken
#
################################
function GetOpenSubtitlesToken()
{
	if(IsStringDefined $OPENSUBTITLES_TOKEN)
	{
		return $OPENSUBTITLES_TOKEN
	}
	AcknowledgeOpenSubtitles

	if(-not (IsStringDefined $OPENSUBTITLES_CREDENTIAL))
	{
		LogError OpenSubtitles credential not defined.
		return
	}

	$extractedCred = GetCredential $OPENSUBTITLES_CREDENTIAL
	$versionedUserAgent = $OPENSUBTITLES_USER_AGENT + " " + $PROJECT_VERSION

	$headers = @{}
	$headers.Add("Content-Type", "application/json")
	$headers.Add("User-Agent", $versionedUserAgent)
	$headers.Add("Accept", "application/json")
	$headers.Add("Api-Key", $OPENSUBTITLES_API_KEY)
	$postData = '{   "username": "' + $extractedCred[0] + '", ' +
					'"password": "' + $extractedCred[1] + '" }'

	try
	{
		$response = Invoke-WebRequest -Uri ($OPENSUBTITLES_BASE + "/api/v1" + "/login") -Method POST -Headers $headers -Body $postData
	}
	catch
	{
		LogWarning Problem Initiating Token Retrieval.
		LogError $_
		return $null
	}

	$responsePattern = '"token"\:"(?<token>[^"]+)"'
	if($response -notmatch $responsePattern)
	{
		LogError No token extracted from OpenSubtitles.
		return $null
	}

	$token = $matches["token"].Trim()
	$Script:OPENSUBTITLES_TOKEN = $token
	DebugLogInfo Set OpenSubtitles token to [$token]

	return $token
}

###############################
#
# FUNCTION GetTvDbApiToken
#
###############################
function GetTvDbApiToken()
{
	if(IsStringDefined $TVDB_API_TOKEN)
	{
		return $TVDB_API_TOKEN
	}
	AcknowledgeTvDb

	$webclient = New-Object System.Net.WebClient
	$webclient.Headers.Add("Content-Type: application/json")
	$webclient.Encoding = [System.Text.Encoding]::UTF8
	$postData = [System.Text.Encoding]::UTF8.GetBytes('{"apikey": "' + $TVDB_API_KEY + '"}')
	try
	{
		$response = $webclient.UploadData($TVDB_SERVER + "/login", "POST", $postData)
	}
	catch
	{
		LogError Problem Initiating Token Retrieval.
		LogError $_
		return $null
	}
	$response = [System.Text.Encoding]::UTF8.GetString($response)

	if($webclient -ne $null)
	{
		$webclient.Dispose()
	}

	$responsePattern = '{"token"\:"(?<token>[^"]+)"}'
	if($response -notmatch $responsePattern)
	{
		LogError No token extracted from TVDB.
		return $null
	}

	$token = $matches["token"].Trim()
	$Script:TVDB_API_TOKEN = $token
	DebugLogInfo Set TVDB token to [$token]

	return $token
}

###############################
#
# FUNCTION GetTvDbSearchSeriesUri
#
###############################
function GetTvDbSearchSeriesUri($seriesName)
{
	if(FailedStringValidation $seriesName) { return }

	$seriesName = [System.Net.WebUtility]::UrlEncode($seriesName)
	$seriesName = $seriesName.Replace("+", "%20")

	return ([String]::Format("{0}/search?q={1}&type=series", $TVDB_SERVER, $seriesName))
}

###############################
#
# FUNCTION SearchTvDbForSeries
#
###############################
function SearchTvDbForSeries([System.IO.FileSystemInfo] $file, [bool] $interactiveMode)
{
	if(-not (UseTvDB))
	{
		return 0
	}

	$seriesName = ExtractShowName $file
	$fileName = $file.FullName

	if($seriesName -eq $null -or $seriesName.Length -lt 1)
	{
		return 0
	}

	$tvdbSeries = LoadTvDBDictionary
	if($tvdbSeries.ContainsKey($seriesName))
	{
		$tvDbId = $tvdbSeries[$seriesName]
		VerboseLogInfo Extracted TVDB ID [$seriesName] [$tvDbId]
		return ([int] $tvDbId)
	}
	elseif($tvdbSeries.ContainsKey($seriesName.Replace("_",":")))
	{
		$tvDbId = $tvdbSeries[$seriesName]
		VerboseLogInfo Extracted TVDB ID [$seriesName] [$tvDbId]
		return ([int] $tvDbId)
	}
	elseif($Force)
	{
		# Don't query under force mode as the media might be intentially left off of
		# the TVDB list implicly by long inaction
		LogWarning TVDB ID SERIES NOT CACHED: [$seriesName]
		LogWarning "(Execute with -BuildCache parameter if you want to query TVDB)"
		return 0
	}
	else
	{
		$tvDbId = QueryTvDbSeries $seriesName $fileName $interactiveMode
		$Script:TVDB_SERIES_CACHE = $null
		return ($tvDbId)
	}
}

###############################
#
# FUNCTION QueryTvDbSeries
#
###############################
function QueryTvDbSeries($seriesName, $fileName, [bool] $interactiveMode)
{
	if(FailedStringValidation $seriesName) { return }
	if(FailedStringValidation $fileName) { return }

	AcknowledgeTvDb

	$currentToken = GetTvDbApiToken
	if(-not (IsStringDefined $currentToken))
	{
		return
	}

	$searchUri = GetTvDbSearchSeriesUri $seriesName
	$webclient = New-Object System.Net.WebClient
	$webclient.Encoding = [System.Text.Encoding]::UTF8
	$webclient.Headers.Add("Accept: application/json")
	$webclient.Headers.Add("Authorization: Bearer " + $currentToken)

	VerboseLogInfo QUERYING: [$searchUri]
	try
	{
		$results = $webclient.DownloadString($searchUri)
	}
	catch [System.Net.WebException]
	{
		LogWarning Error Querying TVDB: [$searchUri] [($_.ToString())]
		$results = $null
	}

	if($webclient -ne $null)
	{
		$webclient.Dispose()
	}

	if($results -ne $null)
	{
		$results = $results | ConvertFrom-Json
	}

	if($results -eq $null -or $results.data -eq $null -or $results.data.Count -eq 0)
	{
		LogWarning "No series found for '$seriesName' when parsing [$fileName]. Please visit 'http://thetvdb.com/?tab=advancedsearch' and try to search for your show manually."
		if($interactiveMode)
		{
			$seriesId = GetSeriesIdResponse 0 $seriesName
		}
		else
		{
			LogWarning "Skipping TVDB lookup for now. Please run again with the -BuildCache parameter to update your TVDB Mapping."
			return 0
		}
	}

	if($results.data.Count -gt 1)
	{
		LogWarning "Multiple series found for '$seriesName' when parsing [$fileName]:"
		foreach ($currentSeries in $results.data)
		{
			LogInfo $currentSeries.name [($currentSeries.network)] [($currentSeries.tvdb_id)]
		}

		if($interactiveMode)
		{
			$seriesId = GetSeriesIdResponse 0 $seriesName
		}
		else
		{
			LogWarning "Skipping TVDB lookup for now. Please run again with the -BuildCache parameter to update your TVDB Mapping."
			return
		}
	}

	if($results -ne $null -and $results.data -ne $null)
	{
		if($results.data.Count -eq 1)
		{
			LogInfo Found TVDB SERIES : [($results.data.name)] [Network: ($results.data.network)] [ID: ($results.data.tvdb_id)]
			if($interactiveMode)
			{
				$seriesId = GetSeriesIdResponse $results.data.tvdb_id $seriesName
			}
			else
			{
				$seriesId = ([int] $results.data.tvdb_id)
			}
		}
	}

	if($seriesId -ne $null)
	{
		AddTvDbSeriesEntry $seriesName $seriesId
		return $seriesId
	}
	else
	{
		return 0
	}
}

###############################
#
# FUNCTION AddTvDbSeriesEntry
#
###############################
function AddTvDbSeriesEntry($currentSeries, [int] $seriesId)
{
	if($currentSeries.name -eq $null)
	{
		AddSeriesNameAndAliasesToCsv $currentSeries $seriesId
	}
	else
	{
		AddSeriesNameAndAliasesToCsv $currentSeries.name $seriesId
	}

	foreach ($currentAlias in $currentSeries.aliases)
	{
		AddSeriesNameAndAliasesToCsv $currentAlias $seriesId
	}
}

###############################
#
# FUNCTION AddCsvEntry
#
###############################
function AddCsvEntry($csvEntry, $csvFile)
{
	if($WHATIF_MODE)
	{
		LogInfo WHATIF: Export-Csv -Append -InputObject $csvEntry -LiteralPath $csvFile -NoTypeInformation -Encoding Unicode
	}
	else
	{
		Export-Csv -Append -InputObject $csvEntry -LiteralPath $csvFile -NoTypeInformation -Encoding Unicode
	}
}

###############################
#
# FUNCTION RemoveCsvOrTxtEntry
#
###############################
function RemoveCsvOrTxtEntry($entryString, $csvFile)
{
	if(FailedStringValidation $csvFile) { return }

	if(-not (IsStringDefined $entryString))
	{
		return
	}

	if(IsInvalidPath $csvFile)
	{
		return
	}

	if($WHATIF_MODE)
	{
		LogInfo WHATIF: RemoveCsvOrTxtEntry [$entryString] [$csvFile]
		return
	}

	$tmpCsvFile = GenerateTmpFileName $csvFile
	DeleteItem $tmpCsvFile

	$sourceExtension = [System.IO.Path]::GetExtension($csvFile)
	$sourceEntries = $null
	$swapCsv = $false
	if($sourceExtension -eq ".txt")
	{
		$sourceEntries = Get-Content -LiteralPath $csvFile
	}
	elseif($sourceExtension -eq ".csv")
	{
		$sourceEntries = Import-Csv $csvFile
	}
	else
	{
		LogError Invalid source: [$csvFile]
		return
	}

	foreach ($currentEntry in $sourceEntries)
	{
		$sourceComparison = $null
		if($sourceExtension -eq ".txt")
		{
			$sourceComparison = $currentEntry
		}
		elseif($sourceExtension -eq ".csv")
		{
			$sourceComparison = $currentEntry.FileName
		}

		if($sourceComparison -eq $null)
		{
			LogError Invalid input: [$csvFile]
			return
		}

		if($sourceComparison -eq $entryString)
		{
			$swapCsv = $true
		}
		else
		{
			if($sourceExtension -eq ".txt")
			{
				$currentEntry | SafeOutFile -LiteralPath $tmpCsvFile
			}
			elseif($sourceExtension -eq ".csv")
			{
				AddCsvEntry $currentEntry $tmpCsvFile
			}
		}
	}

	if($swapCsv)
	{
		SwapFileWithTmp $csvFile
	}
	else
	{
		VerboseLogInfo CSV entry not found: [$entryString] [$csvFile]
	}

	DeleteItem $tmpCsvFile
}

#######################################
#
# FUNCTION AddSeriesNameAndAliasesToCsv
#
#######################################
function AddSeriesNameAndAliasesToCsv($seriesName, [int] $seriesId)
{
	if(FailedStringValidation $seriesName) { return }

	$tvdbSeries = LoadTvDBDictionary
	if($tvdbSeries.ContainsKey($seriesName))
	{
		VerboseLogInfo Series already mapped: [$seriesName]
		return
	}

	$csvEntry = New-Object -TypeName PSObject -Property @{"Series" = $seriesName; "Id" = $seriesId; }
	LogInfo Adding TVDB CACHE ENTRY: [$seriesName] [$seriesId]
	AddCsvEntry $csvEntry $TVDB_SERIES_CSV
	$Script:TVDB_SERIES_CACHE = $null
}

###############################
#
# FUNCTION GetSeriesIdResponse
#
###############################
function GetSeriesIdResponse([int] $default, $seriesName)
{
	if(FailedStringValidation $seriesName) { return }

	$canContinue = $false
	$response = $null

	$prompt = "Which series ID matches this file (0=Skip, v=View)? [ENTER=" + $default + "]"

	$intResponse = 0
	while (-not $canContinue)
	{
		$response = Read-Host $prompt
		if($response -eq "")
		{
			$response = $default
		}

		if($response -match "v")
		{
			$uri = [String]::Format("http://www.thetvdb.com/?string={0}&searchseriesid=&tab=listseries&function=Search", $seriesName.Replace(' ', '+'))
			Start-Process -FilePath $uri
			continue
		}

		$isInteger = [int]::TryParse($response, [ref] $intResponse)
		if(-not $isInteger)
		{
			LogWarning "Invalid input, please try again."
			continue
		}
		else
		{
			$canContinue = $true
		}
	}

	return $intResponse
}

##############################
#
# FUNCTION LoadTvDBDictionary
#
##############################
function LoadTvDBDictionary()
{
	if($TVDB_SERIES_CACHE -ne $null)
	{
		return $TVDB_SERIES_CACHE
	}

	$dictionary = @{}

	if(IsValidPath $TVDB_SERIES_CSV)
	{
		$inputCSV = Import-Csv $TVDB_SERIES_CSV
		foreach ($currentEntry in $inputCSV)
		{
			if($currentEntry.Series -eq $null)
			{
				continue
			}

			if($dictionary.ContainsKey($currentEntry.Series))
			{
				continue
			}

			#DebugLogInfo CACHING: [$TVDB_SERIES_CSV] [($currentEntry.Series)] --> [($currentEntry.Id)]
			$dictionary.Add($currentEntry.Series, $currentEntry.Id)
		}
	}
	$Script:TVDB_SERIES_CACHE = $dictionary

	return $dictionary
}

########################################
#
# FUNCTION SmartRefreshMediaInfo
#
########################################
function SmartRefreshMediaInfo([System.IO.FileSystemInfo] $file, [int] $tvDbId)
{
	$renamedFileResults = GenerateSmartRenamedFileName $file $tvDbId
	if($renamedFileResults -eq $null)
	{
		if($Force)
		{
			UnTrackRenamedMedia ($file.FullName)
			if($tvDbId -eq 0)
			{
				TrackRenamedMedia ($file.FullName)
			}
		}
		return $file.FullName
	}

	$renamedFile = $renamedFileResults[0]
	$metadataSeries = $renamedFileResults[1]
	$metadataTitle = $renamedFileResults[2]

	#RENAME FILE IF NEEDED
	if($renamedFile -ne $null -and ($renamedFile -cne $file.FullName))
	{
		RenameMediaFile ($file.FullName) $renamedFile
	}

	#REFRESH METADATA IF NEEDED
	$metadataRefreshed = $true
	if($Force -or ((IsValidPath $renamedFile) -and (CanEmbedMetadata $renamedFile)))
	{
		if(-not (IsFileCurrentlyOpen($renamedFile)))
		{
			try
			{
				RefreshMetadata (Get-Item -LiteralPath $renamedFile) $metadataSeries $metadataTitle
				$metadataRefreshed = $true
			}
			catch
			{
				$metadataRefreshed = $false
			}
		}
		else
		{
			VerboseLogInfo Skipping metadata refresh, file open [$renamedFile]
			$metadataRefreshed = $false
		}
	}

	#TRACK IF BOTH METADATA AND RENAME SUCCESSFUL
	if($Force -or ($metadataRefreshed -and (IsValidPath $renamedFile)))
	{
		if(IsInvalidPath $renamedFile)
		{
			$renamedFile = $file.FullName
		}
		TrackRenamedMedia ($renamedFile)

		# If subtitle and ad entries are already there, we might have re-downloaded this
		if(-not $Force)
		{
			RemoveCsvOrTxtEntry $renamedFile $AD_REMOVE_CSV
			RemoveCsvOrTxtEntry $renamedFile $SUBTITLE_CSV
		}
	}

	return $renamedFile
}

########################################
#
# FUNCTION GenerateSmartRenamedFileName
#
########################################
function GenerateSmartRenamedFileName([System.IO.FileSystemInfo] $file, [int] $tvDbId)
{
	if($file.BaseName -match $DUPLICATE_PATTERN)
	{
		#File is likely a reaquired dupe of an existing file. Skip.
		LogWarning Possible Duplicate, Skipping Rename: [($file.FullName)]
		TrackRenamedMedia ($file.FullName)
		return $null
	}

	#PARSE
	$showName = ExtractShowName $file
	$airDateResults = ExtractShowAirDate $file
	$airDate = $airDateResults[0]
	$airDateParsed = $airDateResults[1]
	$isTopicalShow = IsTopicalShow $file

	$seasonInfo = ParseSeason $file $airDateParsed
	$season = $seasonInfo[0]
	$episode = $seasonInfo[2]
	$seasonEpisode = $null

	if($episode -ne $null)
	{
		$seasonEpisode = SeasonEpisodeString $season $episode
	}
	VerboseLogInfo PARSED: [($file.FullName)] [Series:($showName)] [Air:($airDate.ToShortDateString())] [Parsed: ($airDateParsed )] [SE $seasonEpisode] [Topical: ($isTopicalShow)]

	#POPULATE PARSED PARAMETERS

	$parsedParameters = @{}
	if($airDateParsed)
	{
		$parsedParameters.Add("{AiredDate}", (FormatAirDate $airDate))
	}

	if($seasonEpisode -ne $null)
	{
		$parsedParameters.Add("{SeasonAndEpisode}", $seasonEpisode)
	}
	if($showName -ne $null)
	{
		$parsedParameters.Add("{SeriesName}", $showName)
	}

	if((-not (UseTvDB)) -or ($tvDbId -eq 0))
	{
		$bestGuessTitle = ExtractBestGuessTitle $file
		if(($bestGuessTitle -ne $null) -and ($bestGuessTitle.Length -gt 0))
		{
			$parsedParameters.Add("{EpisodeTitle}", $bestGuessTitle)
		}
	}
	else
	{
		#EXTRACT TVDB INFO

		if($showName -ne $null)
		{
			if($airDateParsed)
			{
				$tvDBInfo = ExtractTvDbEpisodeInfo $file $tvDbId $season $episode $airDate
			}
			else
			{
				$tvDBInfo = ExtractTvDbEpisodeInfo $file $tvDbId $season $episode ([DateTime]::MinValue)
			}

			$tvDbSeriesName = $tvDBInfo[0]
			$tvDbEpisodeTitle = $tvDBInfo[1]
			$tvDbAiredDate = $tvDBInfo[2]

			#TVDB sometimes publishes AiredDate and Episode with title name
			if($tvDbEpisodeTitle -ne $null)
			{
				$scrubbedTitle = $tvDbEpisodeTitle
				if($scrubbedTitle.Contains($seasonInfo[1]))
				{
					$scrubbedTitle = $scrubbedTitle.Replace($seasonInfo[1], "")
				}
				if($scrubbedTitle.Contains($airDateResults[2]))
				{
					$scrubbedTitle = $scrubbedTitle.Replace($airDateResults[2], "")
				}
				$scrubbedTitle = $scrubbedTitle.Trim(" ", "-", ":")
				if(($scrubbedTitle.Length -gt 0) -and ($scrubbedTitle -ne $tvDbEpisodeTitle))
				{
					DebugLogInfo TVDB Title translate [$tvDbEpisodeTitle] --> [$scrubbedTitle]
					$tvDbEpisodeTitle = $scrubbedTitle
				}

				if($tvDbAiredDate -eq $null)
				{
					$tvDbAiredDateString = "null"
				}
				else
				{
					$tvDbAiredDateString = $tvDbAiredDate.ToShortDateString()
				}
				$tvDbSeasonEpisode = $tvDBInfo[3]
			}
			VerboseLogInfo EXTRACTED: [($file.FullName)] [Series:($tvDbSeriesName)] [Air:($tvDbAiredDateString)] [SE $tvDbSeasonEpisode] [Episode: ($tvDbEpisodeTitle)]
		}

		#POPULATE TVDB PARAMETERS

		$extractedParameters = @{}
		if($tvDbSeriesName -ne $null)
		{
			$extractedParameters.Add("{SeriesName}", $tvDbSeriesName)
		}

		if($tvDbAiredDate -ne $null)
		{
			$extractedParameters.Add("{AiredDate}", (FormatAirDate $tvDbAiredDate))
		}

		if($tvDbSeasonEpisode -ne $null)
		{
			$extractedParameters.Add("{SeasonAndEpisode}", $tvDbSeasonEpisode)
		}

		if($tvDbEpisodeTitle -ne $null)
		{
			$extractedParameters.Add("{EpisodeTitle}", $tvDbEpisodeTitle)
		}
	}

	#APPLY PARAMETERS TO NEW FILE NAME
	$fileNameTemplate = ReturnAppropriateFileNameTemplate $file

	#PROMOTE PARAMETERS IF NO TVDB
	if($tvDbId -eq 0)
	{
		if(($fileNameTemplate.Contains("{AiredDate}")) -and (-not ($parsedParameters.ContainsKey("{AiredDate}"))) -and ($parsedParameters.ContainsKey("{SeasonAndEpisode}")))
		{
			$parsedParameters.Add("{AiredDate}", ($parsedParameters["{SeasonAndEpisode}"]))
		}

		if(($fileNameTemplate.Contains("{SeasonAndEpisode}")) -and (-not ($parsedParameters.ContainsKey("{SeasonAndEpisode}"))) -and ($parsedParameters.ContainsKey("{AiredDate}")))
		{
			$parsedParameters.Add("{SeasonAndEpisode}", ($parsedParameters["{AiredDate}"]))
		}
	}

	$metadataTitleTemplate = $fileNameTemplate
	$metadataSeriesTemplate = "{SeriesName}"
	if($metadataTitleTemplate -match "{SeriesName}")
	{
		$metadataTitleTemplate = $metadataTitleTemplate.Replace("{SeriesName}", "")
		$metadataTitleTemplate = $metadataTitleTemplate.Trim(" - ")
		$metadataTitleTemplate = $metadataTitleTemplate.Trim(" ")
		$metadataTitleTemplate = $metadataTitleTemplate.Trim("-")
	}

	foreach ($currentParameter in $extractedParameters.Keys)
	{
		if($fileNameTemplate -match $currentParameter)
		{
			$paramToInsert = $extractedParameters[$currentParameter]
			if($currentParameter -eq "{SeriesName}")
			{
				$paramToInsert = $paramToInsert | BeautifyAndDemoteString | ConvertStringToASCII | ConvertStringToFileSystem
			}
			$fileNameTemplate = $fileNameTemplate.Replace($matches[0], $paramToInsert)
		}
		if($metadataTitleTemplate -match $currentParameter)
		{
			$metadataTitleTemplate = $metadataTitleTemplate.Replace($matches[0], $extractedParameters[$currentParameter])
		}
		if($metadataSeriesTemplate -match $currentParameter)
		{
			$metadataSeriesTemplate = $extractedParameters[$currentParameter]
		}
	}

	foreach ($currentParameter in $parsedParameters.Keys)
	{
		if($fileNameTemplate -match $currentParameter)
		{
			$paramToInsert = $parsedParameters[$currentParameter]
			if($currentParameter -eq "{SeriesName}")
			{
				$paramToInsert = $paramToInsert | BeautifyAndDemoteString | ConvertStringToASCII | ConvertStringToFileSystem
			}
			$fileNameTemplate = $fileNameTemplate.Replace($matches[0], $paramToInsert)
		}
		if($metadataTitleTemplate -match $currentParameter)
		{
			$metadataTitleTemplate = $metadataTitleTemplate.Replace($matches[0], $parsedParameters[$currentParameter])
		}
		if($metadataSeriesTemplate -match $currentParameter)
		{
			$metadataSeriesTemplate = $parsedParameters[$currentParameter]
		}
	}

	if($fileNameTemplate.Contains('{') -or $fileNameTemplate.Contains('}'))
	{
		LogWarning Not enough information to generate new file name: [($file.FullName)] --> [$fileNameTemplate]

		if($tvDbId -ne 0)
		{
			$cacheFile = GetTvDbSeriesCacheFile $tvDbId
			DeleteItem $cacheFile
			$cacheFile = GetTvDbEpisodesCacheFile $tvDbId
			DeleteItem $cacheFile
		}

		if(((-not $Force) -and -not (IsAudio $file.FullName)) -and (($fileNameTemplate.Contains('{SeasonAndEpisode}') -or $fileNameTemplate.Contains('{AiredDate}'))))
		{
			MoveIfProbablyMovie $file
		}

		return $null
	}

	#GENERATE NEW FILE NAME

	$fileDirectory = [System.IO.Path]::GetDirectoryName($file.FullName)
	$fileExtension = $file.Extension
	$renamedFile = $fileNameTemplate.Trim()
	if((($parsedParameters -ne $null) -and $parsedParameters.ContainsKey("{AiredDate}")) -or (($extractedParameters -ne $null) -and $extractedParameters.ContainsKey("{AiredDate}")))
	{
		if($parsedParameters.ContainsKey("{AiredDate}"))
		{
			$replacedDate = $parsedParameters["{AiredDate}"]
		}
		elseif($extractedParameters.ContainsKey("{AiredDate}"))
		{
			$replacedDate = $extractedParameters["{AiredDate}"]
		}

		$renamedFile = SanitizeTitleFromRedundantParts $renamedFile $replacedDate $false
		$metadataTitleTemplate = SanitizeTitleFromRedundantParts $metadataTitleTemplate $replacedDate $true
	}

	$renamedFile = $renamedFile.Replace(([Char] 8282), ':')
	$renamedFile = $renamedFile.Trim(" ", "-", ":")
	$renamedFile = $renamedFile.Replace("- :", "-")
	$renamedFile = $renamedFile + $fileExtension
	$renamedFile = $renamedFile | BeautifyAndPromoteString | ConvertStringToFileSystem
	$renamedFile = [System.IO.Path]::Combine($fileDirectory, $renamedFile)

	return @($renamedFile, $metadataSeriesTemplate, $metadataTitleTemplate)
}

##########################################
#
# FUNCTION ReturnDuration
#
##########################################
function ReturnDuration($fileName)
{
	if(IsInvalidPath $fileName)
	{
		return [TimeSpan]::Zero
	}

	$output = ExecuteCommand $FFPROBE_BINARY "-i" $fileName "-print_format" "compact=print_section=0" "-show_entries" "format=duration" "-v" "quiet"
	foreach($currentLine in $output)
	{
		if($currentLine -match "[Dd]uration=(?<timeSpan>[^\s]+)")
		{
			return ([TimeSpan]::FromSeconds($Matches["timeSpan"]))
		}
	}

	LogWarning Duration could not be extracted: [$fileName]
	return ([TimeSpan]::Zero)
}

##########################################
#
# FUNCTION ReturnAppropriateFileNameTemplate
#
##########################################
function ReturnAppropriateFileNameTemplate([System.IO.FileSystemInfo] $file)
{
	$fileNameTemplate = $null
	if(($file -eq $null) -and $SUBSCRIPTION_NAME -eq $null)
	{
		LogError Not enough context to determine template.
		return
	}

	if($RENAME_TEMPLATE -ne $null)
	{
		$fileNameTemplate = $RENAME_TEMPLATE
	}

	if($file -ne $null)
	{
		$templateOverride = LoadSubscriptionOverrideVariable $file "RENAME_TEMPLATE_OVERRIDE"
		if(IsStringDefined $templateOverride)
		{
			$fileNameTemplate = $templateOverride
		}
		elseif((IsTopicalShow $file) -and ($TOPICAL_RENAME_TEMPLATE -ne $null))
		{
			$fileNameTemplate = $TOPICAL_RENAME_TEMPLATE
		}
	}
	else
	{
		if(IsStringDefined $RENAME_TEMPLATE_OVERRIDE)
		{
			$fileNameTemplate = $RENAME_TEMPLATE_OVERRIDE
		}
		elseif(($TOPICAL_RENAME_TEMPLATE -ne $null) -and ((IsStringDefined $SUBSCRIPTION_NAME)))
		{
			if((($TOPICAL_SHOWS.Count -gt 0) -and $TOPICAL_SHOWS.Contains($SUBSCRIPTION_NAME)))
			{
				$fileNameTemplate = $TOPICAL_RENAME_TEMPLATE
			}
			elseif((($DAILY_SHOWS.Count -gt 0) -and $DAILY_SHOWS.Contains($SUBSCRIPTION_NAME)))
			{
				$fileNameTemplate = $TOPICAL_RENAME_TEMPLATE
			}
		}
	}

	if($fileNameTemplate -eq $null)
	{
		LogError Not enough information to determine template.
		return
	}

	return $fileNameTemplate
}

##########################################
#
# FUNCTION SanitizeTitleFromRedundantParts
#
##########################################
function SanitizeTitleFromRedundantParts($oldTitle, $date, $isMeta)
{
	$cSegments = $oldTitle.Split(" - ").Count
	$newTitle = $oldTitle
	$segmentThreshold = 3
	if($isMeta)
	{
		$segmentThreshold = 2
	}
	if((IsStringDefined $newTitle) -and (IsStringDefined $date) -and ($newTitle.Contains($date)))
	{
		if(($newTitle.Split($date).Count -gt 2) -and ($cSegments -gt $segmentThreshold))
		{
			$newTitle = $newTitle.Remove($newTitle.IndexOf($date), $date.Length)
			$newTitle = $newTitle.Replace("-  -", "-")
			$newTitle = $newTitle.Trim(' ', '-', ',')
			DebugLogInfo SANITIZED TITLE [$oldTitle] --> [$newTitle]
		}
	}
	return $newTitle
}

########################################
#
# FUNCTION ExtractBestGuessTitle
#
########################################
function ExtractBestGuessTitle([System.IO.FileSystemInfo] $file)
{
	#PARSE
	$showName = ExtractShowName $file

	$airDateResults = ExtractShowAirDate $file
	$airDateParsed = $airDateResults[1]
	$airedDate = $airDateResults[2]

	$seasonResults = ParseSeasonViaString $file.FullName $airDateParsed
	$seasonEposideMarker = $seasonResults[1]

	$bestGuessTitle = $file.BaseName

	#Strip out starting series name
	if($bestGuessTitle.StartsWith($showName))
	{
		$bestGuessTitle = $bestGuessTitle.Remove(0, $showName.Length)
	}

	#Some series uses the aired date as its episode title. Strip out all the others
	if($airDateParsed -and $bestGuessTitle.Contains($airedDate))
	{
		$titleSuffix = ""
		if($bestGuessTitle.EndsWith($airedDate))
		{
			$titleSuffix = $bestGuessTitle.Substring($bestGuessTitle.LastIndexOf($airedDate), $airedDate.Length)
		}
		$bestGuessTitle = $bestGuessTitle.Replace($airedDate, "") + $titleSuffix
	}

	#Strip out all instances of season episode
	if($seasonEposideMarker -ne $null)
	{
		$bestGuessTitle = $bestGuessTitle.Replace($seasonEposideMarker, "")
	}

	#Trim the residue
	$bestGuessTitle = $bestGuessTitle.Trim(' ', "-")

	if(-not (IsStringDefined $bestGuessTitle) -and $file.BaseName.Contains(" - "))
	{
		$index = $file.BaseName.LastIndexOf(" - ") + 3
		$bestGuessTitle = $file.BaseName.SubString($index, ($file.BaseName.Length-$index)).Trim(' ', "-", ":")
	}

	DebugLogInfo Title guess [($file.BaseName)] --> [$bestGuessTitle]
	return $bestGuessTitle
}

#################################
#
# FUNCTION ExtractTvDbEpisodeInfo
#
#################################
function ExtractTvDbEpisodeInfo([System.IO.FileSystemInfo] $file, [int] $id, $season, $episode, [System.DateTime] $parsedAirDate)
{
	if($season.Length -gt 1)
	{
		$season = $season.TrimStart('0')
	}

	if($episode.Length -gt 1)
	{
		$episode = $episode.TrimStart('0')
	}

	$cacheFiles = New-Object -TypeName System.Collections.ArrayList
	$cacheFiles.Add((GetTvDbSeriesCacheFile $id)) > $null
	$cacheFiles.Add((GetTvDbEpisodesCacheFile $id)) > $null

	RefreshTvDbCache $file $id

	foreach ($currentCacheFile in $cacheFiles)
	{
		if(IsInvalidPath $currentCacheFile)
		{
			if(-not $WHATIF_MODE)
			{
				LogError Cache File Not Found: [$currentCacheFile]
			}
			return @($null, $null, $null, $null)
		}
	}

	$seriesInfo = Get-Content -LiteralPath (GetTvDbSeriesCacheFile $id) -Encoding UTF8 | ConvertFrom-Json
	$episodesInfo = Get-Content -LiteralPath (GetTvDbEpisodesCacheFile $id) -Encoding UTF8 | ConvertFrom-Json

	#GET SERIES
	$seriesName = $null
	if($seriesInfo.data -ne $null)
	{
		$seriesName = $seriesInfo.data.name
	}

	if($seriesName -eq "")
	{
		$seriesName = $null
	}
	else
	{
		DebugLogInfo Extracted Series Name: [$seriesName]
	}

	if(($episodesInfo.data -eq $null) -or ($episodesInfo.data.Count -eq 0) -or $episodesInfo.data.episodes -eq $null)
	{
		LogWarning No Episodes Found: [$id]
		return @($null, $null, $null, $null)
	}

	$tvdbAllowStaleOverride = LoadSubscriptionOverrideVariable $file "ALLOW_STALE_TVDB_OVERRIDE"
	if($Force -or (($tvdbAllowStaleOverride -ne $null) -and $tvdbAllowStaleOverride))
	{
		#Skip check
	}
	elseif($parsedAirDate -ne [System.DateTime]::MinValue)
	{
		if(($seriesInfo -ne $null) -and ($seriesInfo.data.status.name -ne "Ended"))
		{
			$tempDate = [DateTime]::MinValue
			if([DateTime]::TryParse($seriesInfo.data.lastUpdated, [ref] $tempDate))
			{
				DebugLogInfo [File: ($parsedAirDate)] [Series: ($tempDate)]
				if($parsedAirDate -gt $tempDate)
				{
					LogInfo TVDB series information does not appear to be current: [($seriesInfo.data.lastUpdated)]. Skipping rename: [($file.BaseName + ": " + $parsedAirDate)] [($seriesName + ": " + $tempDate)]
					foreach($currentCacheFile in $cacheFiles)
					{
						DeleteItem $currentCacheFile
					}
					return @($null, $null, $null, $null)
				}
			}
		}
	}

	VerboseLogInfo Searching TVDB ID [$id] for: [E $episode] [S $season] [Air ($parsedAirDate.ToShortDateString())]

	$episodeCollection = $episodesInfo.data.episodes
	if($episodeCollection.GetType().ToString() -ne "System.Object[]")
	{
		$episodeCollection = @($episodeCollection)
	}

	$tvDbSeries = LoadTvDBDictionary

	$chosenEpisode = $null
	$matchedSeEpisode = MatchEpisodeBySeasonEpisodeNumber $episodeCollection $season $episode
	$matchedAiredEpisode = MatchEpisodeByAirDate $episodeCollection  $parsedAirDate $season $episode

	if(($matchedAiredEpisode -eq $null) -or ($parsedAirDate -eq [System.DateTime]::MinValue))
	{
		$matchedTitleEpisode = MatchEpisodeByTitle $episodeCollection $seriesName $file $tvDbSeries
	}

	$selectOverride = LoadSubscriptionOverrideVariable $file "TVDB_SELECT_OVERRIDE"
	if(IsStringDefined $selectOverride)
	{
		switch($selectOverride)
		{
			"AIRED"
			{
				VerboseLogInfo Choosing Aired "(Override)"
				$chosenEpisode = $matchedAiredEpisode
			}
			"SE"
			{
				VerboseLogInfo Choosing SE "(Override)"
				$chosenEpisode = $matchedSeEpisode
			}
			"TITLE"
			{
				VerboseLogInfo Choosing Title "(Override)"
				$chosenEpisode = $matchedTitleEpisode
			}
			default
			{
				LogError Invalid TVDB override: [$selectOverride] [$file]
				return
			}
		}
	}
	else
	{
		if(($matchedAiredEpisode -ne $null) -and ($parsedAirDate -ne [System.DateTime]::MinValue))
		{
			VerboseLogInfo Choosing Aired
			$chosenEpisode = $matchedAiredEpisode
		}
		elseif(($matchedSeEpisode -ne $null) -and ($matchedTitleEpisode -eq $null))
		{
			VerboseLogInfo Choosing SE
			$chosenEpisode = $matchedSeEpisode
		}
		elseif(($matchedTitleEpisode -ne $null) -and ($matchedSeEpisode -eq $null))
		{
			VerboseLogInfo Choosing Title
			$chosenEpisode = $matchedTitleEpisode
		}
		elseif(($matchedTitleEpisode -ne $null) -and ($matchedSeEpisode -ne $null))
		{
			#Need to choose between Season or Title match
			if($matchedSeEpisode.id -eq $matchedTitleEpisode.id)
			{
				VerboseLogInfo Choosing SE and Title
				$chosenEpisode = $matchedSeEpisode
			}
			else
			{
				$fileName = $file.FullName
				if(IsStringDefined $fileName.Trim())
				{
					$seLvDistance = CalculateLv $fileName $matchedSeEpisode.name $seriesName
					$titleLvDistance = CalculateLv $fileName $matchedTitleEpisode.name $seriesName
					$seWordsMatched = CalculateWordScore $fileName $matchedSeEpisode.name $seriesName $tvDbSeries
					$titleWordsMatched = CalculateWordScore $fileName $matchedTitleEpisode.name $seriesName $tvDbSeries
				}
				else
				{
					$fileName = ""
					$seLvDistance = 1
					$titleLvDistance = 10
					$seWordsMatched = 0
					$titleWordsMatched = 0
				}

				VerboseLogInfo [($file.BaseName)] [SE WS:$seWordsMatched LV:$seLvDistance] [Title WS:$titleWordsMatched LV:$titleLvDistance]
				if(-not (IsStringDefined $fileName.Trim()))
				{
					VerboseLogInfo No title so choosing SE
					$chosenEpisode = $matchedSeEpisode
				}
				elseif($titleWordsMatched -eq 0)
				{
					VerboseLogInfo Title WS 0 so choosing SE
					$chosenEpisode = $matchedSeEpisode
				}
				elseif(($seWordsMatched -eq $titleWordsMatched) -and ($seLvDistance -eq $titleLvDistance))
				{
					VerboseLogInfo SE match is just as good as Title so choosing SE
					$chosenEpisode = $matchedSeEpisode
				}
				elseif(($titleWordsMatched -ge $seWordsMatched) -and ($titleLvDistance -le $seLvDistance))
				{
					VerboseLogInfo Title LV and WS better than SE so choosing Title
					$chosenEpisode = $matchedTitleEpisode
				}
				else
				{
					$titleEpisodeWords = $matchedTitleEpisode.name.Split(' ').Count
					if((IsStringDefined ($matchedTitleEpisode.name)) -and ($titleEpisodeWords -eq 1) -and ($titleWordsMatched -gt 0))
					{
						VerboseLogInfo Title WS real close so ignoring LV so choosing Title
						$chosenEpisode = $matchedTitleEpisode
					}
					else
					{
						VerboseLogInfo Choosing SE by default
						$chosenEpisode = $matchedSeEpisode
					}
				}
			}
		}
	}

	if(($chosenEpisode -ne $null) -and (IsStringDefined ($chosenEpisode.name)))
	{
		$episodeName = $chosenEpisode.name
		if($episodeName -match "\(\d+\)$")
		{
			$episodeName = $episodeName.Substring(0, ($episodeName.IndexOf($matches[0]) - 1)).Trim()
		}

		if($episodeCollection -ne $null)
		{
			foreach ($currentEpisode in $episodeCollection)
			{
				if($currentEpisode.id -eq $chosenEpisode.id)
				{
					continue
				}

				if($currentEpisode.name -match [RegEx]::Escape($episodeName))
				{
					$episodeName = $chosenEpisode.name
					break
				}
			}
		}
	}

	if($episodeName -ne $null)
	{
		if(DirectoryContainsBetterMatch $file $episodeName $seriesName $tvDbSeries)
		{
			VerboseLogInfo A better matched file was found in this directory. Skipping...
			$chosenEpisode = $null
		}
	}

	if($chosenEpisode -ne $null)
	{
		$airedDate = [DateTime]::MinValue
		if(IsStringDefined ($chosenEpisode.aired))
		{
			$airedDate = [System.DateTime]::Parse($chosenEpisode.aired)
		}

		if(($chosenEpisode.number -ge 0) -and ($chosenEpisode.seasonNumber  -ge 0))
		{
			$seasonEpisode = SeasonEpisodeString ($chosenEpisode.seasonNumber.ToString()) ($chosenEpisode.number.ToString())
			if($seasonEpisode -eq "")
			{
				$seasonEpisode = $null
			}
		}

		$episodeName = $episodeName.Trim()

		if($seriesName.EndsWith(("(Podcast)")))
		{
			$seriesName = $seriesName.Substring(0, ($seriesName.Length - "(Podcast)".Length)).Trim()
		}

		VerboseLogInfo FOUND TVDB ENTRY: [$seriesName] [$episodeName] [($airedDate.ToShortDateString())] [$seasonEpisode]
		return @($seriesName, $episodeName, $airedDate, $seasonEpisode)
	}
	else
	{
		LogWarning EPISODE NOT FOUND: [($file.FullName)]
		return @($null, $null, $null, $null)
	}
}

#################################
#
# FUNCTION MatchEpisodeByAirDate
#
#################################
function MatchEpisodeByAirDate($episodeCollection, [System.DateTime] $parsedAirDate, $season, $episode)
{
	if($parsedAirDate -eq [System.DateTime]::MinValue)
	{
		return $null
	}

	$matchedEpisodes = 0
	$matchedEpisode = $null

	#MATCH AIR DATE
	foreach ($currentEpisode in $episodeCollection)
	{
		if(-not (IsStringDefined $currentEpisode.aired))
		{
			continue
		}

		$currentAiredDate = [System.DateTime]::Parse($currentEpisode.aired)
		if($parsedAirDate.ToShortDateString() -eq $currentAiredDate.ToShortDateString())
		{
			if(IsStringDefined $season)
			{
				if($currentEpisode.seasonNumber -ne $season)
				{
					continue
				}
			}

			$matchedEpisodes++
			$matchedEpisode = $currentEpisode
		}
	}

	if(($matchedEpisodes -eq 1) -and ($matchedEpisode -ne $null))
	{
		return $matchedEpisode
	}
	else
	{
		return $null
	}
}

############################################
#
# FUNCTION MatchEpisodeBySeasonEpisodeNumber
#
############################################
function MatchEpisodeBySeasonEpisodeNumber($episodeCollection, $season, $episode)
{
	if($season -eq $null -or $episode -eq $null)
	{
		return $null
	}

	#MATCH EPISODE AND SEASON NUMBERS
	foreach ($currentEpisode in $episodeCollection)
	{
		if(($episode -eq $null) -or ($season -eq $null))
		{
			continue
		}

		if(($currentEpisode.number -eq $episode) -and ($currentEpisode.seasonNumber -eq $season))
		{
			return $currentEpisode
		}
	}

	return $null
}

##############################
#
# FUNCTION MatchEpisodeByTitle
#
##############################
function MatchEpisodeByTitle($episodeCollection, $seriesName, [System.IO.FileSystemInfo] $file, $tvDbSeries)
{
	#MATCH EPISODE TITLE
	$BestLvDistance = 1000
	$matchedLvEpisode = $null

	$BestWordScore = -1
	$matchedWordEpisode = $null

	foreach ($currentEpisode in $episodeCollection)
	{
		if(($currentEpisode.name -eq $null) -or ($currentEpisode.name -eq ""))
		{
			continue
		}

		#How alike is the episode name to the file name?
		$lvDistance = CalculateLv $file.FullName $currentEpisode.name $seriesName
		if($lvDistance -lt $BestLvDistance)
		{
			$BestLvDistance = $lvDistance
			$matchedLvEpisode = $currentEpisode
		}

		#If no word in the title is in the filename, it's not appropriate.
		$wordsMatched = CalculateWordScore $file.FullName $currentEpisode.name $seriesName $tvDbSeries
		if($wordsMatched -gt $BestWordScore)
		{
			$BestWordScore = $wordsMatched
			$matchedWordEpisode = $currentEpisode
		}

		DebugLogInfo [($currentEpisode.name)] [LV: $lvDistance] [WS: $wordsMatched]
	}

	$wordsMatched = 0
	if($matchedLvEpisode -ne $null)
	{
		$wordsMatched = CalculateWordScore $file.FullName $matchedLvEpisode.name $seriesName $tvDbSeries
		VerboseLogInfo BEST LV: [($file.FullName)] [($matchedLvEpisode.name)] [LV: $BestLvDistance] [WS: $wordsMatched]
	}

	$lvDistance = 0
	if($matchedWordEpisode -ne $null)
	{
		$lvDistance = CalculateLv $file.FullName $matchedWordEpisode.name $seriesName
		VerboseLogInfo BEST WORD: [($file.FullName)] [($matchedWordEpisode.name)] [LV: $lvDistance] [WS: $BestWordScore]
	}

	if(($matchedLvEpisode -ne $null) -and ($matchedWordEpisode -eq $null))
	{
		VerboseLogInfo Choosing LV
		return $matchedLvEpisode
	}

	if(($matchedLvEpisode -eq $null) -and ($matchedWordEpisode -ne $null))
	{
		VerboseLogInfo Choosing WS
		return $matchedWordEpisode
	}

	if(($matchedLvEpisode -ne $null) -and ($matchedWordEpisode -ne $null))
	{
		if($BestWordScore -gt $wordsMatched)
		{
			VerboseLogInfo Choosing WS
			return $matchedWordEpisode
		}
		else
		{
			VerboseLogInfo Choosing LV
			return $matchedLvEpisode
		}
	}

	return $null
}

#######################################
#
# FUNCTION DirectoryContainsBetterMatch
#
#######################################
function DirectoryContainsBetterMatch([System.IO.FileSystemInfo] $file, $episodeName, $seriesName, $tvDbSeries)
{
	if(FailedStringValidation $episodeName) { return }

	$baseWordScore = CalculateWordScore $file.FullName $episodeName $seriesName $tvDbSeries
	$baseLvScore = CalculateLv $file.FullName $episodeName $seriesName

	DebugLogInfo BASE: [WS: $baseWordScore] [LV: $baseLvScore]

	$containingDirectory = [System.IO.Path]::GetDirectoryName($file.FullName)
	$peerFiles = Get-ChildItem -LiteralPath $containingDirectory -File | Where-Object { ($_.Extension -in ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS)) }

	$bestFile = $null

	$bestPeerWordScore = 0
	foreach ($peerFile in $peerFiles)
	{
		if($file.FullName -eq $peerFile.FullName)
		{
			continue
		}

		$currentPeerWordScore = CalculateWordScore $peerFile.FullName $episodeName $seriesName $tvDbSeries
		DebugLogInfo PEER WS: [($peerFile.FullName)] [$currentPeerWordScore]

		if($currentPeerWordScore -gt $bestPeerWordScore)
		{
			$bestPeerWordScore = $currentPeerWordScore
			$bestFile = $peerFile
		}
	}

	$bestPeerLvScore = 1000
	$bestPeerLvScoreFileName = $null
	foreach ($peerFile in $peerFiles)
	{
		if($file.FullName -eq $peerFile.FullName)
		{
			continue
		}

		$currentPeerLvScore = CalculateLv $peerFile.FullName $episodeName $seriesName

		DebugLogInfo PEER LV: [($peerFile.FullName)] [$currentPeerLvScore]

		if($currentPeerLvScore -lt $bestPeerLvScore)
		{
			$bestPeerLvScore = $currentPeerLvScore
			$bestPeerLvScoreFileName = $peerFile.FullName
		}
	}

	if(($bestPeerWordScore -gt $baseWordScore) -and ($baseWordScore -gt 0))
	{
		if(($bestFile -ne $null) -and ($seriesName -ne $null))
		{
			$bestFileShowName = ExtractShowName $bestFile
			if($bestFileShowName -eq $seriesName)
			{
				VerboseLogInfo Found Better Peer WS
				return $true
			}
		}
	}
	elseif(($bestPeerLvScore -lt $baseLvScore) -and ($baseWordScore -gt 0) -and ($bestPeerWordScore -gt 0))
	{
		$ws = CalculateWordScore $bestPeerLvScoreFileName $episodeName $seriesName $tvDbSeries
		if($ws -ge $baseWordScore)
		{
			VerboseLogInfo Found Better Peer LV but I do not care.
			return $false
		}
	}

	return $false
}

##############################
#
# FUNCTION CalculateLv
#
##############################
function CalculateLv($fileName, $episodeName, $seriesName)
{
	if(FailedStringValidation $fileName) { return }
	if(FailedStringValidation $episodeName) { return }

	$lvDistance = 0
	$stringToLv = $episodeName

	if($DEBUG_MODE)
	{
		$oldMode = $DEBUG_MODE
		$DEBUG_MODE = $false
	}

	try
	{
		if(IsValidPath $fileName)
		{
			$bestTitleGuess = ExtractBestGuessTitle (Get-Item -LiteralPath $fileName)
		}
		elseif($fileName.Contains(" - "))
		{
			$bestTitleGuess = $fileName.SubString($fileName.LastIndexOf(" - ")+3)
		}
		else
		{
			$bestTitleGuess = $fileName
		}
	}
	catch
	{
		if($oldMode)
		{
			$DEBUG_MODE = $oldMode
		}
	}

	$lvDistance = LevenshteinDistance $bestTitleGuess $stringToLv

	#LogInfo LevenshteinDistance: [$fileName] [$bestTitleGuess] [$episodeName] [$lvDistance]
	return $lvDistance
}

##############################
#
# FUNCTION CalculateWordScore
#
##############################
function CalculateWordScore($fileName, $episodeName, $seriesName, $tvSeries)
{
	if(FailedStringValidation $fileName) { return }
	if(FailedStringValidation $episodeName) { return }

	$wordsInEpisodeName = New-Object -TypeName System.Collections.ArrayList
	$wordsInFileName = New-Object -TypeName System.Collections.ArrayList

	$wordsMatched = New-Object -TypeName System.Collections.ArrayList
	$wordsNotMatched = New-Object -TypeName System.Collections.ArrayList

	if($DEBUG_MODE)
	{
		$oldMode = $DEBUG_MODE
		$DEBUG_MODE = $false
	}

	try
	{
		if(IsValidPath $fileName)
		{
			$bestTitleGuess = ExtractBestGuessTitle (Get-Item -LiteralPath $fileName)
		}
		elseif($fileName.Contains(" - "))
		{
			$bestTitleGuess = $fileName.SubString($fileName.LastIndexOf(" - ")+3)
		}
		else
		{
			$bestTitleGuess = $fileName
		}

		$excludedWords = New-Object -TypeName System.Collections.ArrayList
		$excludedWords.Add("The".ToLower()) > $null
		$excludedWords.Add("Season".ToLower()) > $null
		if(IsValidPath $fileName)
		{
			$airedResult = ExtractShowAirDate $fileName
			if(($airedResult -ne $null) -and (($airedResult[2]) -ne $null) -and (-not $episodeName.Contains($airedResult[2])))
			{
				foreach($currentDateExclusion in ($airedResult[2]).Split(' '))
				{
					$excludedWords.Add($currentDateExclusion.Trim(' ', ',')) > $null
				}
			}
		}

		$seasonResult = ParseSeasonViaString $fileName $false
		if(($seasonResult -ne $null) -and (($seasonResult[1]) -ne $null))
		{
			$excludedWords.Add($seasonResult[1].ToLower()) > $null
		}
	}
	finally
	{
		if($oldMode)
		{
			$DEBUG_MODE = $oldMode
		}
	}

	#No word in the series name should count
	foreach ($currentWord in (BreakStringIntoWords $seriesName $excludedWords))
	{
		if(-not $excludedWords.Contains($currentWord.ToLower()))
		{
			$excludedWords.Add($currentWord.ToLower()) > $null
		}
	}

	#No word for an alias of the series should count either
	if($tvSeries.ContainsKey($seriesName))
	{
		$seriesId = $tvSeries[$seriesName]
		foreach ($currentSeriesName in $tvSeries.Keys)
		{
			if($tvSeries[$currentSeriesName] -eq $seriesId)
			{
				foreach ($currentWord in (BreakStringIntoWords $currentSeriesName $excludedWords))
				{
					if(-not $excludedWords.Contains($currentWord.ToLower()))
					{
						$excludedWords.Add($currentWord.ToLower()) > $null
					}
				}
			}
		}
	}

	foreach ($startingWord in (BreakStringIntoWords $bestTitleGuess $excludedWords))
	{
		$wordsInFileName.Add($startingWord.ToLower()) > $null
	}

	foreach ($startingWord in (BreakStringIntoWords $episodeName $excludedWords))
	{
		$wordsInEpisodeName.Add($startingWord.ToLower()) > $null
	}

	if(	($bestTitleGuess -match ("\s+(?<episode>(I|V|X)+)\s+")) -or
		($bestTitleGuess -match ("\s+(?<episode>(I|V|X)+)$"))
	)
	{
		$romanNumber = $matches["episode"]
		$episode = (ParseRomanNumeral $romanNumber).ToString()

		if(-not $wordsInFileName.Contains($episode.ToLower()))
		{
			$wordsInFileName.Add($episode.ToLower()) > $null
		}

	}

	foreach ($currentWord in $wordsInEpisodeName)
	{
		if($wordsInFileName.Contains($currentWord))
		{
			if(-not $wordsMatched.Contains($currentWord.ToLower()))
			{
				#DebugLogInfo MATCHED: [$currentWord]
				$wordsMatched.Add($currentWord) > $null
			}
		}
		else
		{
			if(-not $wordsNotMatched.Contains($currentWord.ToLower()))
			{
				$wordsNotMatched.Add($currentWord.ToLower()) > $null
			}
		}
	}

	foreach ($currentWord in $wordsInFileName)
	{
		if($wordsInEpisodeName.Contains($currentWord.ToLower()))
		{
			#ALREADY COUNTED
		}
		else
		{
			if(-not $wordsNotMatched.Contains($currentWord.ToLower()))
			{
				$wordsNotMatched.Add($currentWord.ToLower()) > $null
			}
		}
	}

	$score = 0
	foreach ($currentWord in $wordsMatched)
	{
		if(-not $excludedWords.Contains($currentWord.ToLower()))
		{
			#LogInfo WS ADD: [$currentWord] [($currentWord.Length)]
			$score = $score + $currentWord.Length
		}
	}

	foreach ($currentWord in $wordsNotMatched)
	{
		if(-not $excludedWords.Contains($currentWord.ToLower()))
		{
			#LogInfo WS DEL: [$currentWord] [($currentWord.Length)]
			$score = $score - $currentWord.Length
		}
	}

	if($score -lt 0)
	{
		$score = 0
	}

	#LogInfo WORD SCORE: [$fileName] [$bestTitleGuess] [$episodeName] [$score]
	return $score
}

###################################
#
# FUNCTION GetTvDbSeriesCacheFile
#
###################################
function GetTvDbSeriesCacheFile([int] $id)
{
	return ([String]::Format("{0}\{1}\series.json", $TVDB_CACHE_PATH, $id))
}

###################################
#
# FUNCTION GetTvDbEpisodesCacheFile
#
###################################
function GetTvDbEpisodesCacheFile([int] $id)
{
	return ([String]::Format("{0}\{1}\episodes.json", $TVDB_CACHE_PATH, $id))
}

###################################
#
# FUNCTION AcknowledgeTvDb
#
###################################
function AcknowledgeTvDb()
{
	if($WHATIF_MODE)
	{
		return
	}

	if(-not $ACKNOWLEDGED_TVDB)
	{
		FlashMessage 'Cyan' "Information provided by TheTVDB.com. Please consider supporting them! (See https://www.thetvdb.com/subscribe)"
		$Script:ACKNOWLEDGED_TVDB = $true
	}
}

###################################
#
# FUNCTION AcknowledgeOpenSubtitles
#
###################################
function AcknowledgeOpenSubtitles()
{
	if($WHATIF_MODE)
	{
		return
	}

	if(-not $ACKNOWLEDGED_OPENSUBTITLES)
	{
		FlashMessage 'Blue' "Thanks to http://www.opensubtitles.com for the subtitle data. Check them out!"
		$Script:ACKNOWLEDGED_OPENSUBTITLES = $true
	}
}

###################################
#
# FUNCTION RefreshTvDbCache
#
###################################
function RefreshTvDbCache([System.IO.FileSystemInfo] $file, [int] $id)
{
	$cacheFiles = @{}
	$cacheFiles.Add((GetTvDbSeriesCacheFile($id)), [String]::Format("{0}/series/{1}/extended", $TVDB_SERVER, $id))
	$cacheFiles.Add((GetTvDbEpisodesCacheFile($id)), [String]::Format("{0}/series/{1}/episodes/default", $TVDB_SERVER, $id))

	$isSeriesEnded = $false
	if(IsValidPath ($cacheFiles.Keys[0]))
	{
		$seriesInfo = (Get-Content -LiteralPath ($cacheFiles.Keys[0]) -Encoding UTF8 | ConvertFrom-Json)
		if($seriesInfo.data.status.name -eq "Ended")
		{
			$isSeriesEnded = $true
		}
	}

	foreach ($currentCacheFile in $cacheFiles.Keys)
	{
		if(IsValidPath $currentCacheFile)
		{
			$cachedTime = (Get-Item -LiteralPath $currentCacheFile).CreationTime
			if(($file -ne $null) -and ($file.CreationTime -gt $cachedTime))
			{
				DeleteItem $currentCacheFile
			}
			elseif((-not $isSeriesEnded) -and (((Get-Date) - $cachedTime).TotalHours -ge $TVDB_CACHE_AGE_HOURS))
			{
				DeleteItem $currentCacheFile
			}
		}
		RefreshTvDbCacheFile $currentCacheFile ($cacheFiles[$currentCacheFile]) $file $id
	}
}

###################################
#
# FUNCTION RefreshTvDbCacheFile
#
###################################
function RefreshTvDbCacheFile($cacheFile, $targetUri, [System.IO.FileSystemInfo] $file, [int] $id)
{
	if(IsValidPath ($cacheFile))
	{
		return
	}
	CreateDirInPath $cacheFile

	$currentToken = GetTvDbApiToken
	if(-not (IsStringDefined $currentToken))
	{
		return
	}

	$webclient = New-Object System.Net.WebClient
	$webclient.Headers.Add("Accept: application/json")
	$webclient.Headers.Add("Authorization: Bearer " + $currentToken)

	if($WHATIF_MODE)
	{
		LogInfo WHATIF: DOWNLOADING: [$targetUri] --> [$cacheFile]
	}
	else
	{
		VerboseLogInfo DOWLOADING: [$targetUri] --> [$cacheFile]

		try
		{
			$response = $webclient.DownloadString($targetUri)
		}
		catch
		{
			LogError Problem Extracting Episode Information [$targetUri]
			HandleError $_
			return
		}
		$response = ($response | ConvertFrom-Json)

		if($webclient -ne $null)
		{
			$webclient.Dispose()
		}

		$jsonDataCollection = New-Object -TypeName System.Collections.ArrayList
		if($response.data.episodes.Count -gt 0)
		{
			VerboseLogInfo Adding [($response.data.episodes.Count)] episode entries...
			foreach ($currentEpisode in $response.data.episodes)
			{
				$jsonDataCollection.Add($currentEpisode) > $null
			}

			$nextPage = $null
			if(($response.links -ne $null) -and ($response.links.next -ne $null))
			{
				VerboseLogInfo TVDB Response has [($response.data.episodes.Count)] entries so getting next page [($response.links.next)]...
				$nextPage = $response.links.next
			}

			#ACQUIRE PAGES
			while ($nextPage -ne $null)
			{
				$pageUri = $nextPage

				$webclient = New-Object System.Net.WebClient
				$webclient.Headers.Add("Accept: application/json")
				$webclient.Headers.Add("Authorization: Bearer " + $currentToken)

				$pageResponse = $webclient.DownloadString($pageUri)
				$pageResponse = ($pageResponse | ConvertFrom-Json)

				if($webclient -ne $null)
				{
					$webclient.Dispose()
				}

				VerboseLogInfo Adding [($pageResponse.data.episodes.Count)] episode entries...
				foreach ($currentEpisode in $pageResponse.data.episodes)
				{
					$jsonDataCollection.Add($currentEpisode) > $null
				}

				$nextPage = $null
				if(($pageResponse.links -ne $null) -and ($pageResponse.links.next -ne $null))
				{
					VerboseLogInfo TVDB Response has [($pageResponse.data.episodes.Count)] entries so getting next page [($pageResponse.links.next)]...
					$nextPage = $pageResponse.links.next
				}
			}
		}

		if($jsonDataCollection.Count -gt 0)
		{
			$response.data.episodes = $jsonDataCollection
		}

		# SAVE TO DISK
		$response = ($response | ConvertTo-Json -Depth 5)
		if($VERBOSE_MODE)
		{
			$response | SafeOutFile -LiteralPath $cacheFile
		}
		else
		{
			($response | SafeOutFile -LiteralPath $cacheFile) > $null
		}

		if(IsInvalidPath $cacheFile)
		{
			LogError Cache file not persisted: [$cacheFile]
		}
	}
}

###################################
#
# FUNCTION CleanSubtitleCache
#
###################################
function CleanSubtitleCache([bool] $force)
{
	CleanCacheFile $SUBTITLE_CSV $force
}

###################################
#
# FUNCTION CleanAdRemoveCache
#
###################################
function CleanAdRemoveCache([bool] $force)
{
	CleanCacheFile $AD_REMOVE_CSV $force
}

###################################
#
# FUNCTION CleanRenamedCache
#
###################################
function CleanRenamedCache([bool] $force)
{
	CleanCacheFile $RENAMED_CSV $force
	AuditSeriesCaches
}

###################################
#
# FUNCTION CleanCacheFile
#
###################################
function CleanCacheFile($cacheFile, [bool] $force)
{
	if($MAX_CACHE_AGE_DAYS -le 0)
	{
		return
	}

	if(IsInvalidPath $cacheFile)
	{
		return
	}

	if(-not $force)
	{
		$cacheCreated = (Get-Item -LiteralPath $cacheFile).CreationTime
		if(((Get-Date) - $cacheCreated).TotalDays -le $MAX_CACHE_AGE_DAYS)
		{
			return
		}
	}

	LogInfo Cleaning cache [$cacheFile]...

	$newCache = GenerateTmpFileName $cacheFile
	if(IsValidPath $newCache)
	{
		LogWarning Cache cleaning appears to be in progress. Retry again later.
		return
	}

	$inputCSV = Import-Csv $cacheFile
	$cTotalEntries = 0
	$cRemovedEntries = 0
	foreach ($currentEntry in $inputCSV)
	{
		$cTotalEntries++

		if(-not (IsStringDefined ($currentEntry.FileName.Trim())))
		{
			LogWarning Cache item not valid: [($currentEntry.FileName)]
			continue
		}

		$hasInvalidChars = $false
		foreach ($currentChar in [System.IO.Path]::GetInvalidFileNameChars())
		{
			$currentFileName = $currentEntry.FileName
			if([System.IO.Path]::GetExtension($currentFileName) -In ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
			{
				$currentFileName = [System.IO.Path]::GetFileNameWithoutExtension($currentFileName)
			}
			if($currentFileName.Contains($currentChar))
			{
				$hasInvalidChars = $true
				break
			}
		}

		if($hasInvalidChars)
		{
			LogWarning File name has invalid filesystem characters: [($currentEntry.FileName)]
			continue
		}

		$cacheTargetFound = $false

		if(IsValidPath ($currentEntry.FileName))
		{
			VerboseLogInfo Cache Item Found: [($currentEntry.FileName)]
			$currentEntry.FileName = (Get-Item -LiteralPath $currentEntry.FileName).BaseName
			$cacheTargetFound = $true
		}
		else
		{
			$cacheSearchTerm = $currentEntry.FileName.Trim()
			if([System.IO.Path]::GetExtension($cacheSearchTerm) -In ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
			{
				$cacheSearchTerm = [System.IO.Path]::GetFileNameWithoutExtension($cacheSearchTerm)
			}
			$cacheSearchTerm = $cacheSearchTerm + "*"

			foreach ($currentPathToScan in @($DOWNLOADED_TV_PATH, (DeriveWatchedPath $DOWNLOADED_TV_PATH), $DOWNLOADED_PODCASTS_PATH, (DeriveWatchedPath $DOWNLOADED_PODCASTS_PATH)))
			{
				if (IsInvalidPath $currentPathToScan) { continue }

				if($cacheTargetFound)
				{
					break
				}

				foreach ($matchedFile in (Get-ChildItem -LiteralPath $currentPathToScan -File -Recurse -FollowSymlink -Filter $cacheSearchTerm -ErrorAction SilentlyContinue))
				{
					if(IsNotUnderPath $matchedFile.FullName $PURGE_PATH)
					{
						VerboseLogInfo Cache Item Matched: [($currentEntry.FileName)] [($matchedFile.FullName)]
						if($matchedFile.BaseName -ne $currentEntry.FileName)
						{
							$currentEntry.FileName = $matchedFile.BaseName
						}
						$cacheTargetFound = $true
						break;
					}
				}
			}
		}

		if($cacheTargetFound)
		{
			AddCsvEntry $currentEntry $newCache
		}
		else
		{
			VerboseLogInfo Cache Item Removed: [($currentEntry.FileName)]
			$cRemovedEntries++
		}
	}

	if(IsValidPath $newCache)
	{
		SwapFileWithTmp $cacheFile
	}
	LogInfo ...Finished. [TOTAL: $cTotalEntries] [REMOVED: $cRemovedEntries]
}

###################################
#
# FUNCTION AuditSeriesCaches
#
###################################
function AuditSeriesCaches()
{
	foreach ($currentCacheCsv in @($TVDB_SERIES_CSV, $CURRENT_SEASONS_CSV))
	{
		if(IsInvalidPath $currentCacheCsv)
		{
			continue
		}

		$seenSeriesIds = New-Object -TypeName System.Collections.ArrayList

		$inputCsv = Import-Csv $currentCacheCsv
		foreach ($currentCsvEntry in $inputCsv)
		{
			if(-not $seenSeriesIds.Contains($currentCsvEntry.Id))
			{
				$seenSeriesIds.Add($currentCsvEntry) > $null
			}
			else
			{
				LogWarning Duplicate TVDBID detected [($currentCacheCsv)] [($currentCsvEntry.Id)]
			}
		}
	}
}

#################################
#
# FUNCTION TranslatePathToSymLink
#
#################################
function TranslatePathToSymLink($fileName)
{
	if(FailedStringValidation $fileName) { return }

	$mappedName = MapHandleFileOutput $fileName
	if(IsStringDefined $mappedName)
	{
		$fileName = $mappedName
	}

	$symlinks = BuildSymlinkTable
	foreach ($currentSymLink in $symlinks.Keys)
	{
		if($fileName.Contains($currentSymLink))
		{
			$linkedSource = $fileName.Replace($currentSymLink, $symlinks[$currentSymLink])
			DebugLogInfo TRANSLATE SYMLINK: [$fileName] --> [$linkedSource]
			return $linkedSource
		}
	}

	return $fileName
}

####################################
#
# FUNCTION ProcessCurrentOpenedMedia
#
####################################
function ProcessCurrentOpenedMedia
{
	DeleteItem $RECORDING_CSV

	$handleSnapshotTimestamp = Get-Date
	$currentlyOpenedMedia = GetExecutingProcessesForMedia

	foreach ($currentlyOpenedMediaFile in $currentlyOpenedMedia.Keys)
	{
		DebugLogInfo CONSIDERING: [$currentlyOpenedMediaFile]

		$currentOpenedFile = $currentlyOpenedMediaFile
		$currentExe = $currentlyOpenedMedia[$currentlyOpenedMediaFile]
		$currentOpenedFile = TranslatePathToSymLink $currentOpenedFile

		$currentlyTrackedMedia = LoadTrackedMedia
		if($currentlyTrackedMedia.ContainsKey($currentOpenedFile))
		{
			VerboseLogInfo PREVIOUSLY TRACKED: [$currentOpenedFile]
			continue
		}

		switch ($currentExe)
		{
			{ $_ -in $RECORDING_HOSTS }
			{
				VerboseLogInfo MEDIA IS RECORDING: [$currentOpenedFile] [$currentExe]
				TrackRecordingMedia $currentOpenedFile $currentExe
				#RECORDING MEDIA COULD BE RE-DOWNLOADING, CLEAR WATCH STATE
				UnTrackWatchedMedia $currentOpenedFile
			}
			{ $_ -in $WATCHING_HOSTS }
			{
				if(IsFileCurrentlyRecording $currentOpenedFile $currentExe)
				{
					VerboseLogInfo MEDIA IS RECORDING: [$currentOpenedFile] [$currentExe]
					TrackRecordingMedia $currentOpenedFile $currentExe
					#RECORDING MEDIA COULD BE RE-DOWNLOADING, CLEAR WATCH STATE
					UnTrackWatchedMedia $currentOpenedFile
					continue
				}

				if($currentExe.Contains("Plex"))
				{
					#Handled elsewhere
					continue
				}
				else
				{
					if((IsLibraryUnwatched $currentOpenedFile) -and (($currentExe -eq "SYSTEM") -or ($exePatternToCheck.Count -gt 1)))
					{
						if(((Get-Date)-$handleSnapshotTimestamp).TotalSeconds -le 5)
						{
							SleepFor 5
						}
						#VERIFY STILL OPEN TO REDUCE CHANCE OF RANDOM SCANS TRIPPING TRACKING
						if((GetExecutingProcessesForFile $currentOpenedFile) -eq $null)
						{
							LogWarning "SKIPPING TRACKING (handle released): [$currentExe] [$currentOpenedFile]"
							continue
						}
					}
					VerboseLogInfo MEDIA IS BEING CONSUMED: [$currentOpenedFile]
					TrackWatchedMedia $currentOpenedFile ([System.DateTime]::MinValue) $currentExe
				}
			}
			default
			{
				VerboseLogInfo EXCLUDED EXE: [$currentExe]
			}
		}
	}
}

############################
#
# FUNCTION TrackActiveFile
#
############################
function TrackActiveFile($currentOpenedFile, $trackCsv, [System.DateTime] $timeStamp, $additionalContext)
{
	if(FailedStringValidation $currentOpenedFile) { return }

	if(IsInvalidPath $currentOpenedFile)
	{
		$currentOpenedFile = MapHandleFileOutput $currentOpenedFile
	}

	if(-not (IsStringDefined $currentOpenedFile))
	{
		return
	}

	if(IsUnderPath $currentOpenedFile $ISO_CONVERT_PATH)
	{
		$targetIsoFileName = (Get-Item -LiteralPath $currentOpenedFile).Directory.Name + ".iso"
		foreach ($currentPathToCheck in $DOWNLOADED_MOVIES_PATH)
		{
			$media = Get-ChildItem -Recurse -FollowSymlink -LiteralPath $currentPathToCheck -File -Filter $targetIsoFileName
			foreach ($currentMedia in $media)
			{
				TrackActiveFile $media.FullName $trackCsv $timeStamp $additionalContext
			}
		}
		return
	}

	if((IsWatched $currentOpenedFile) -or ((IsNotUnderPath $currentOpenedFile $DOWNLOADED_TV_PATH) -and (IsNotUnderPath $currentOpenedFile $DOWNLOADED_MOVIES_PATH) -and (IsNotUnderPath $currentOpenedFile $DOWNLOADED_PODCASTS_PATH)))
	{
		#ALREADY ARCHIVED
		VerboseLogInfo IGNORING: [$currentOpenedFile]
		return
	}

	if($timeStamp -eq ([System.DateTime]::MinValue))
	{
		$timeStamp = get-date
	}

	$canExclude = $true
	if($trackCsv -eq $TRACKING_CSV)
	{
		$purgeOverride = LoadSubscriptionOverrideVariable (Get-Item -LiteralPath $currentOpenedFile) "PURGE_ON_WATCHED"
		if(($purgeOverride -ne $null) -and $purgeOverride)
		{
			$canExclude = $false
		}
	}

	if($canExclude -and (IsExcludedPath $currentOpenedFile))
	{
		#EXCLUDED
		VerboseLogInfo EXCLUDED DIRECTORY: [$currentOpenedFile]
		return
	}

	if($trackCsv -ne $TRACKING_CSV)
	{
		$currentOpenedFile = [System.IO.Path]::GetFileNameWithoutExtension($currentOpenedFile)
	}

	if(IsValidPath $trackCsv)
	{
		$inputCSV = Import-Csv $trackCsv

		foreach ($currentEntry in $inputCSV)
		{
			if($currentEntry.FileName -eq $currentOpenedFile)
			{
				if(($timeStamp -lt $currentEntry.TimeStamp) -and ($trackCsv -eq $TRACKING_CSV))
				{
					#RE-TRACK
					UnTrackWatchedMedia $currentOpenedFile
					$fileFound = $false
					break
				}
				else
				{
					VerboseLogInfo ALREADY TRACKING: [($trackCsv)] [($currentEntry.FileName)]
					$fileFound = $true
					break
				}
			}
		}
	}

	if(-not $fileFound)
	{
		$additionalLog = ""
		if(IsStringDefined $additionalContext)
		{
			$additionalLog = "[ " + $additionalContext + " ]"
			if($additionalContext -eq "System")
			{
				$additionalLog = "[ " + (BuildSmbAccessString $currentOpenedFile) + " ]"
			}
		}
		$trackTargetFile = [System.IO.Path]::GetFileName($trackCsv)
		if($additionalContext -ne $false)
		{
			LogInfo ADDING: [($trackTargetFile)] --> [($currentOpenedFile)] $additionalLog
		}
		else
		{
			VerboseLogInfo ADDING: [($trackTargetFile)] --> [($currentOpenedFile)] $additionalLog
		}
		$csvEntry = New-Object -TypeName PSObject -Property @{"TimeStamp" = $timeStamp; "FileName" = $currentOpenedFile; }
		AddCsvEntry $csvEntry $trackCsv
	}
}

###################################
#
# FUNCTION ResolveSMBInfo
#
###################################
function ResolveSMBInfo($fileName)
{
	if(FailedStringValidation $fileName) { return }

	$smbInfo = Get-SmbOpenFile | Where-Object { $_.Path -eq $fileName }
	if($smbInfo.Count -ne 1)
	{
		return $null
	}

	if(IsStringDefined($smbInfo.ClientComputerName))
	{
		$clientIP = $smbInfo.ClientComputerName
		if(IsStringDefined $clientIP)
		{
			try
			{
				$hostName = [System.Net.Dns]::GetHostEntry($clientIP).HostName
			}
			catch
			{
				LogWarning Problem resolving [$clientIP]: [($_.ToString())]
			}
			if(IsStringDefined $hostName)
			{
				if($hostName.Contains('.'))
				{
					$hostName = $hostName.Substring(0, $hostName.IndexOf('.'))
				}
				$hostName = $hostName.ToUpper().Trim()
				$clientIP = $hostName
			}
		}
	}
	if(IsStringDefined($smbInfo.ClientUserName))
	{
		$clientName = $smbInfo.ClientUserName
		$clientName = $clientName.ToLower();
		if($clientName.Contains('\'))
		{
			$hostPart = $clientName.Substring(0, $clientName.IndexOf('\'))
			$hostPart = $hostPart.ToUpper()
			$clientName = $clientName.Replace($hostPart, $hostPart, "OrdinalIgnoreCase")
		}
	}

	if(($clientIP -ne $null) -and ($clientName -ne $null))
	{
		return @($clientIP, $clientName)
	}
	else
	{
		return $null
	}
}

###################################
#
# FUNCTION BuildSmbAccessString
#
###################################
function BuildSmbAccessString($fileName)
{
	if(FailedStringValidation $fileName) { return }

	$smbInfo = ResolveSMBInfo $fileName
	if($smbInfo.Count -eq 2)
	{
		return ([String]::Format("{0} - {1}", $smbInfo[0], $smbInfo[1]))
	}
	return "SYSTEM"
}

########################################
#
# FUNCTION GetExecutingProcessesForFile
#
########################################
function GetExecutingProcessesForFile($fileName)
{
    $result = GetExecutingProcessesForMedia $fileName
    if($result.ContainsKey($fileName))
    {
        return $result[$fileName]
    }
    return $null
}

########################################
#
# FUNCTION GetExecutingProcessesForMedia
#
########################################
function GetExecutingProcessesForMedia($fileHint)
{
    $foundMediaHandles = @{}

    $lookForFile = (IsStringDefined $fileHint)
    if($lookForFile -and (IsInvalidPath $fileHint))
    {
        return $foundMediaHandles
    }
	DebugLogInfo GetExecutingProcessesForMedia [$fileHint]

    #EXAMPLE
	#MediaMallServer.exe pid: 2464   type: File          14F0: C:\CinAdmin\DATA\Media\TV\The Late Show with Stephen Colbert\The Late Show with Stephen Colbert - s01e27 - 10_14_2015 (Jack Black, Nick Woodman, Michelle Dorrance).mp4

	$handlePattern = "(?<exe>.+)\s+pid\:\s+\d+\s+type\:\s+File\s+\w+:\s+(?<filename>.+)\s*$"

    $scanDirectories = New-Object -TypeName System.Collections.ArrayList
    $systemWide = $WATCHING_HOSTS.Contains("System") -or $RECORDING_HOSTS.Contains("System")

    if(IsValidPath $DOWNLOADED_TV_PATH)
    {
        $scanDirectories.Add($DOWNLOADED_TV_PATH) > $null
    }
    if(IsValidPath $DOWNLOADED_MOVIES_PATH)
    {
        $scanDirectories.Add($DOWNLOADED_MOVIES_PATH) > $null
    }
    if(IsValidPath $DOWNLOADED_PODCASTS_PATH)
    {
        $scanDirectories.Add($DOWNLOADED_PODCASTS_PATH) > $null
    }

    if($lookForFile)
    {
        $openedSmbFiles = Get-SmbOpenFile | Where-Object { $_.Path -eq $fileHint }
    }
    else
    {
        $openedSmbFiles = Get-SmbOpenFile
    }

    foreach($currentSmbFile in $openedSmbFiles)
    {
        $accessingUser = BuildSmbAccessString $currentSmbFile.Path
        if($accessingUser -ne "System")
        {
            if($systemWide -and -not ($WATCHING_HOSTS.Contains($accessingUser) -or $RECORDING_HOSTS.Contains($accessingUser)))
            {
                $accessingUser = "System"
            }
        }

        if($foundMediaHandles.ContainsKey($currentSmbFile.Path))
        {
            continue
        }

        if(-not $lookForFile)
        {
            foreach($currentScanDirectory in $scanDirectories)
            {
                if(-not (IsUnderPath $currentSmbFile.Path $currentScanDirectory))
                {
                    continue
                }

                $currentOpenedFileExtension = [System.IO.Path]::GetExtension($currentSmbFile.Path)
                if($currentOpenedFileExtension -NotIn $MEDIA_EXTENSIONS)
                {
                    continue
                }

                if(IsWatched $currentSmbFile.Path)
                {
                    continue
                }
                $foundMediaHandles.Add($currentSmbFile.Path, $accessingUser)
            }
        }
        else
        {
            $foundMediaHandles.Add($currentSmbFile.Path, $accessingUser)
            return $foundMediaHandles
        }
    }

	if(-not (IsHandleInstalled))
	{
		return $foundMediaHandles
	}

	$output = New-Object -TypeName System.Collections.ArrayList
	if(-not $lookForFile)
	{
		foreach($currentScanDirectory in $scanDirectories)
		{
			foreach($currentDirOutputLine in  (ExecuteCommand $HANDLE_BINARY "-nobanner" $currentScanDirectory))
			{
				$output.Add($currentDirOutputLine.ToString())  > $null
			}
		}
	}
	else
	{
		foreach($currentDirOutputLine in  (ExecuteCommand $HANDLE_BINARY "-nobanner" ([System.IO.Path]::GetDirectoryName($fileHint))))
		{
			$output.Add($currentDirOutputLine.ToString()) > $null
		}
	}

    foreach ($currentLine in $output)
    {
		if($currentLine -match $handlePattern)
		{
			$currentOpenedFile = $matches["filename"].Trim()
			$originalHandleFileName = $currentOpenedFile
			$currentOpenedFile = MapHandleFileOutput $currentOpenedFile
			if($currentOpenedFile -eq $null)
			{
				LogWarning Could not normalize handle search: [$fileHint] [$originalHandleFileName]
				return $foundMediaHandles
			}
			$currentOpenedFileExtension = [System.IO.Path]::GetExtension($currentOpenedFile)
			$currentExe = $matches["exe"].Trim()

			if($currentExe -eq "System")
			{
				# We'll grab this via Get-SmbOpenFile
				# Not all versions of handle.exe show SMB/SYSTEM accessed files
				continue
			}

			if($foundMediaHandles.ContainsKey($currentOpenedFile))
			{
				continue
			}

			if(-not $lookForFile)
			{
				if($currentOpenedFileExtension -NotIn $MEDIA_EXTENSIONS)
				{
					continue
				}

				if(IsWatched $currentOpenedFile)
				{
					continue
				}

				$foundMediaHandles.Add($currentOpenedFile, $currentExe)
			}
			else
			{
				if($currentOpenedFile -eq $fileHint)
				{
					$foundMediaHandles.Add($currentOpenedFile, $currentExe)
				}
			}
		}
    }

    return $foundMediaHandles
}

################################################
#
# FUNCTION ExtractCurrentPlayOnRecordingProblems
#
################################################
function ExtractCurrentPlayOnRecordingProblems()
{
	if(IsInvalidPath $SQL_LITE)
	{
		LogError SQL Lite not found [$SQL_LITE]. Make sure PlayOn is installed.
		return
	}

	if(IsInvalidPath $RECORDING_DATABASE)
	{
		LogError Recording Database not found [$RECORDING_DATABASE]. Make sure PlayOn is installed.
		return
	}

	if([Environment]::Is64BitProcess)
	{
		LogError This is a 64 bit process. This script needs to be excuted in x86 PowerShell.
		return
	}

	DeleteItem $RECORDING_PROBLEMS_CSV
	Add-Type -LiteralPath $SQL_LITE

	$conn = New-Object -TypeName System.Data.SQLite.SQLiteConnection
	$connectionString = "Data Source=" + $RECORDING_DATABASE
	$conn.ConnectionString = $connectionString
	$conn.Open()

	$command = New-Object -TypeName System.Data.SQLite.SQLiteCommand
	$command.Connection = $conn

	#STATUS 0 is Queued
	#STATUS 1 is Recording
	#STATUS 2 is Completed
	#STATUS 3 is Partial
	#STATUS 4 is Error

	$cutOff = (get-date).AddDays(-10)

	$command.CommandText = "SELECT * FROM recordqueueitems WHERE (Status > 2)"
	$reader = $command.ExecuteReader()
	if(-not ($reader.HasRows))
	{
		VerboseLogInfo No PlayOn recordings extracted.
	}

	while ($reader.Read())
	{
		$name = $reader["Name"]
		$seriesTitle = $reader["SeriesTitle"]
		$status = $reader["Status"]
		$lastUpdated = $reader["Updated"]
		$browsePath = $reader["BrowsePath"]
		$airDate = $reader["AirDate"]

		if($seriesTitle.GetType().ToString() -eq 'System.DBNull')
		{
			$seriesTitle = $null
		}

		$tempDate = [DateTime]::MinValue
		if(-not ([DateTime]::TryParse($lastUpdated, [ref] $tempDate)))
		{
			$lastUpdated = $null
		}
		else
		{
			$lastUpdated = $tempDate
		}

		$tempDate = [DateTime]::MinValue
		if(-not ([DateTime]::TryParse($airDate, [ref] $tempDate)))
		{
			$airDate = $null
		}
		else
		{
			$airDate = $tempDate
		}

		if($name.StartsWith($seriesTitle) -or ($seriesTitle -eq $null))
		{
			$showName = $name
		}
		else
		{
			$showName = [String]::Format("{0} - {1}", $seriesTitle, $name)
		}

		if($lastUpdated -lt $cutOff)
		{
			VerboseLogInfo Skipping Entry: [Show: $showName] [Status: $status] [Air: $airDate] [Updated: $lastUpdated]
			continue
		}

		$queryName = $name
		if($queryName.Contains("'"))
		{
			$queryName = $queryName.Replace("'", "''")
		}

		$command2 = New-Object -TypeName System.Data.SQLite.SQLiteCommand
		$command2.Connection = $conn
		$command2.CommandText = "SELECT * FROM recordqueueitems WHERE (Status < 3) AND (Name = '" + $queryName + "')"

		$reader2 = $command2.ExecuteReader()
		if($reader2.HasRows)
		{
			VerboseLogInfo RECORDING ISSUE IGNORED: [Completed or Active] [Show: $showName] [Status: $status] [Air: $airDate] [Updated: $lastUpdated]
			$reader2.Dispose()
			continue
		}
		$reader2.Dispose()

		if(($lastUpdated -eq $null) -or ((Get-Date) - $lastUpdated).TotalDays -lt 1)
		{
			LogWarning PLAYON RECORDING ISSUE: [Show: $showName] [Status: $status] [Air: $airDate] [Updated: $lastUpdated]
		}
		else
		{
			VerboseLogInfo RECORDING ISSUE: [Show: $showName] [Status: $status] [Air: $airDate] [Updated: $lastUpdated]
		}

		$csvEntry = New-Object -TypeName PSObject -Property @{"Updated" = $lastUpdated; "Status" = $status; "BrowsePath" = $browsePath; "Series" = $seriesTitle; "EpisodeName" = $name; "AirDate" = $airDate; }
		AddCsvEntry $csvEntry $RECORDING_PROBLEMS_CSV
		FindAndDeleteRecordingFile $showName
	}

	$reader.Dispose()
}

###################################
#
# FUNCTION CanMediaHaveAdsRemoved
#
###################################
function CanMediaHaveAdsRemoved($currentOpenedFile)
{
	if(FailedStringValidation $currentOpenedFile) { return $false }

	if(HasFileHasAdsRemoved $currentOpenedFile) { return $false }
	if(-not (ShouldRemoveCommericals $currentOpenedFile)) { return $false }
	if(IsFileCurrentlyOpen $currentOpenedFile) { return $false }
	if(IsFileCurrentlyRecording $currentOpenedFile) { return $false }

	return $true
}

###################################
#
# FUNCTION CanMediaBeRenamed
#
###################################
function CanMediaBeRenamed($currentOpenedFile)
{
	if(FailedStringValidation $currentOpenedFile) { return $false }

	if(HasFileBeenRenamed $currentOpenedFile) { return $false }
	if(IsFileCurrentlyOpen $currentOpenedFile) { return $false }
	if(IsFileCurrentlyRecording $currentOpenedFile) { return $false }

	return $true
}

###################################
#
# FUNCTION IsFileCurrentlyRecording
#
###################################
function IsFileCurrentlyRecording($currentOpenedFile)
{
	return (IsFileCurrentlyRecording $currentOpenedFile $null)
}

###################################
#
# FUNCTION IsFileCurrentlyRecording
#
###################################
function IsFileCurrentlyRecording($currentOpenedFile, $accessingExe)
{
	if(FailedStringValidation $currentOpenedFile) { return $false }

	$currentOpenedFile = MapHandleFileOutput $currentOpenedFile

	if($RECORDING_HOSTS.Count -gt 0)
	{
		if($accessingExe -In $RECORDING_HOSTS)
		{
			return $true
		}

		if((-not (IsStringDefined $accessingExe)) -or ($accessingExe -eq "System"))
		{
			$smbInfo = ResolveSMBInfo $currentOpenedFile
			if($smbInfo.Count -eq 2)
			{
				$accessingExe = "System"
				$currentExe = (($smbInfo[0]) + " - " + ($smbInfo[1]))
				if($currentExe -In $RECORDING_HOSTS)
				{
					return $true
				}
			}
		}
	}

	if(IsValidPath $RECORDING_CSV)
	{
		$currentOpenedFileNameBase = [System.IO.Path]::GetFileNameWithoutExtension($currentOpenedFile)
		$currentlyRecordingFiles = LoadRecordingMedia
		if($currentlyRecordingFiles.ContainsKey($currentOpenedFileNameBase))
		{
			return $true
		}
	}

	#If a file hasn't been processed / renamed yet and is being accessed via SMB, let's assume some other piece of software is recording it.
	if(($accessingExe -eq "System") -and (-not (IsMovie $currentOpenedFile)) -and (-not (IsWatched $currentOpenedFile)) -and (-not (HasFileBeenRenamed $currentOpenedFile)))
	{
		if((IsUnderPath $currentOpenedFile $DOWNLOADED_MOVIES_PATH) -or (IsUnderPath $currentOpenedFile $DOWNLOADED_TV_PATH) -or (IsUnderPath $currentOpenedFile $DOWNLOADED_PODCASTS_PATH))
		{
			LogWarning ASSUMING CURRENTLY RECORDING: [$currentOpenedFile] [$currentExe]
			return $true
		}
	}

	return $false
}

###################################
#
# FUNCTION HasFileBeenRenamed
#
###################################
function HasFileBeenRenamed($currentOpenedFile)
{
	return (HasFileBeenRenamed $currentOpenedFile $null)
}

###################################
#
# FUNCTION HasFileBeenRenamed
#
###################################
function HasFileBeenRenamed($currentOpenedFile, $loadedCache)
{
	if(FailedStringValidation $currentOpenedFile) { return $false }

	if($Force)
	{
		return $false
	}

	if(($loadedCache -eq $null) -or ($loadedCache.Count -eq 0))
	{
		$currentlyRenamedFiles = LoadRenamedMedia
	}
	else
	{
		$currentlyRenamedFiles = $loadedCache
	}

	$baseName = [System.IO.Path]::GetFileNameWithoutExtension($currentOpenedFile)
	if($currentlyRenamedFiles.ContainsKey($baseName))
	{
		return $true
	}

	return $false
}

###################################
#
# FUNCTION HasFileHasAdsRemoved
#
###################################
function HasFileHasAdsRemoved($currentOpenedFile)
{
	if(FailedStringValidation $currentOpenedFile) { return $false }

	$currentlyAdRemovedFiles = LoadAdRemovedMedia

	$baseName = [System.IO.Path]::GetFileNameWithoutExtension($currentOpenedFile)
	if($currentlyAdRemovedFiles.ContainsKey([System.IO.Path]::GetFileNameWithoutExtension($currentOpenedFile)))
	{
		return $true
	}

	#File might have been duplicated
	if($baseName -match $DUPLICATE_PATTERN)
	{
		if($currentlyAdRemovedFiles.ContainsKey([System.IO.Path]::GetFileNameWithoutExtension($baseName)))
		{
			return $true
		}
	}

	return $false
}

##############################
#
# FUNCTION IsCurrentlyPolling
#
##############################
function IsCurrentlyPolling()
{
	if(IsRemoteSession)
	{
		LogWarning "Cannot determine whether polling is occuring in a remote session."
		return $false
	}

	foreach ($currentPsHost in @("powershell", "pwsh"))
	{
		$proc = Get-Process $currentPsHost -ErrorAction Ignore | Where-Object -FilterScript { $_.MainWindowTitle -match $POLLING_WIN_TITLE }
		if($proc -ne $null)
		{
			return $true
		}
	}

	return $false
}

####################################
#
# FUNCTION RemoveSemaphore
#
####################################
function RemoveSemaphore()
{
	if(IsValidPath $CONCURRENCY_FILE)
	{
		LogInfo REMOVING: [($CONCURRENCY_FILE)]
		DeleteItem ($CONCURRENCY_FILE)
	}
}

####################################
#
# FUNCTION LockScriptConcurrency
#
####################################
function LockScriptConcurrency()
{
	if($WHATIF_MODE) { return }

	$scriptName = GetExecutingScriptName ($MyInvocation.ScriptName)
	if($TASK_SCHEDULING)
	{
		LogWarning ENTER LOCK: [$scriptName]
	}

	$currentExecutionTS = Get-Date
	$hasWarned = $false
	$canContinue = $false
	while (-not $canContinue)
	{
		if(IsValidPath $CONCURRENCY_FILE)
		{
			if($ProgressPreference -ne 'SilentlyContinue')
			{
				$previousScript = (Get-Content -LiteralPath $CONCURRENCY_FILE).Trim()
				$previousScript = $previousScript.Replace($SCRIPT_PATH, "").Trim("\")
				$Script:LAST_EXECUTION = (Get-Item -LiteralPath $CONCURRENCY_FILE).LastWriteTime
				$previousScriptExecutionTime = $LAST_EXECUTION.ToString()
				LogWarning Waiting for a previous script to finish... [$previousScript] [STARTED: $previousScriptExecutionTime]
				$hasWarned = $true
			}

			if(((Get-Date) - $currentExecutionTS).TotalMinutes -ge $MAX_CONCURRENCY_WAIT_MINUTES)
			{
				LogWarning "(Is the semaphore file [" ($CONCURRENCY_FILE) "] orphaned? You may need to delete it.)"
				$errorString = "Semaphore appears to be perpetually locked. Exiting [" + $scriptName + "]..."
				throw $errorString
			}
			SleepFor 10
		}
		else
		{
			$scriptName | SafeOutFile -LiteralPath $CONCURRENCY_FILE
			$canContinue = $true
			if($TASK_SCHEDULING)
			{
				LogWarning SECURED SEMAPHORE: [$scriptName]
			}
			if(IsValidPath $CONCURRENCY_FILE)
			{
				$Script:LAST_EXECUTION = (Get-Item -LiteralPath $CONCURRENCY_FILE).LastWriteTime
			}
		}
	}

	if($hasWarned)
	{
		LogWarning ...continuing.
	}
}

####################################
#
# FUNCTION ReleaseScriptConcurrency
#
####################################
function ReleaseScriptConcurrency()
{
	$scriptName = GetExecutingScriptName ($MyInvocation.ScriptName)
	if($TASK_SCHEDULING)
	{
		LogWarning RELEASE LOCK: [$scriptName]
	}

	if(IsValidPath $CONCURRENCY_FILE)
	{
		$semaphoreTs = (Get-Item -LiteralPath $CONCURRENCY_FILE).LastWriteTime
		if($semaphoreTs -ge $LAST_EXECUTION)
		{
			DeleteItem $CONCURRENCY_FILE
			if($TASK_SCHEDULING)
			{
				LogWarning RELEASED SEMAPHORE: [$scriptName]
			}
		}
		else
		{
			LogWarning SEMAPHORE TOO EARLY: [LE: ($LAST_EXECUTION)] [SPH: ($semaphoreTs)]
		}
	}
}

#################################
#
# FUNCTION GetExecutingScriptName
#
#################################
function GetExecutingScriptName($scriptName)
{
	if((-not (IsStringDefined $scriptName)) -or $scriptName.Contains($FUNCTION_SCRIPT))
	{
		$scriptName = $MyInvocation.ScriptName
	}

	if((-not (IsStringDefined $scriptName)) -or $scriptName.Contains($FUNCTION_SCRIPT))
	{
		LogError Not able to get calling script name.
		return
	}

	return $scriptName
}

##############################
#
# FUNCTION LoadTrackedMedia
#
##############################
function LoadTrackedMedia()
{
	return (LoadDictionary $TRACKING_CSV)
}

##############################
#
# FUNCTION LoadRecordingMedia
#
##############################
function LoadRecordingMedia()
{
	return (LoadDictionary $RECORDING_CSV)
}

##############################
#
# FUNCTION LoadRenamedMedia
#
##############################
function LoadRenamedMedia()
{
	return (LoadDictionary $RENAMED_CSV)
}

##############################
#
# FUNCTION LoadAdRemovedMedia
#
##############################
function LoadAdRemovedMedia()
{
	return (LoadDictionary $AD_REMOVE_CSV)
}

##############################
#
# FUNCTION LoadAdRemovedMedia
#
##############################
function LoadSubtitledMedia()
{
	return (LoadDictionary $SUBTITLE_CSV)
}

##############################
#
# FUNCTION LoadDictionary
#
##############################
function LoadDictionary($sourceCsv)
{
	if($sourceCsv -eq $RENAMED_CSV)
	{
		#Case sensitive
		$dictionary = New-Object -TypeName 'System.Collections.Generic.Dictionary[String,String]'
	}
	else
	{
		$dictionary = @{}
	}

	if(IsValidPath $sourceCsv)
	{
		DebugLogInfo LOADING CACHE: [$sourceCsv]
		$inputCSV = Import-Csv $sourceCsv

		foreach ($currentEntry in $inputCSV)
		{
			if($dictionary.ContainsKey($currentEntry.FileName))
			{
				DebugLogInfo IGNORE CACHE DUPE: [$sourceCsv] [($currentEntry.FileName)]
			}
			else
			{
				#DebugLogInfo CACHING: [$sourceCsv] [($currentEntry.FileName)]
				$dictionary.Add($currentEntry.FileName, $currentEntry.TimeStamp)
			}
		}
	}

	return $dictionary
}

##############################
#
# FUNCTION UnTrackRenamedMedia
#
##############################
function UnTrackRenamedMedia($currentOpenedFile)
{
	if(FailedStringValidation $currentOpenedFile) { return }
	if(IsInvalidPath $RENAMED_CSV) { return }

	if(IsInvalidPath $currentOpenedFile)
	{
		$currentOpenedFile = MapHandleFileOutput $currentOpenedFile
	}

	#SEARCH ONLY FOR FILE NAME --- FILE MIGHT HAVE BEEN MOVED TO MAPPING
	$currentOpenedFile = [System.IO.Path]::GetFileNameWithoutExtension($currentOpenedFile)

	RemoveCsvOrTxtEntry $currentOpenedFile $RENAMED_CSV
}

##############################
#
# FUNCTION UnTrackWatchedMedia
#
##############################
function UnTrackWatchedMedia($currentOpenedFile)
{
	if(FailedStringValidation $currentOpenedFile) { return }
	if(IsInvalidPath $TRACKING_CSV) { return }

	if(IsInvalidPath $currentOpenedFile)
	{
		$currentOpenedFile = MapHandleFileOutput $currentOpenedFile
	}

	RemoveCsvOrTxtEntry $currentOpenedFile $TRACKING_CSV
}

##############################
#
# FUNCTION TrackSubtitledMedia
#
##############################
function TrackSubtitledMedia($currentOpenedFile, [System.DateTime] $timeStamp)
{
	TrackActiveFile $currentOpenedFile $SUBTITLE_CSV $timeStamp
}

##############################
#
# FUNCTION TrackAdRemovedMedia
#
##############################
function TrackAdRemovedMedia($currentOpenedFile)
{
	if(IsAudio $currentOpenedFile)
	{
		TrackActiveFile $currentOpenedFile $AD_REMOVE_CSV (get-date) $false
	}
	else
	{
		TrackActiveFile $currentOpenedFile $AD_REMOVE_CSV (get-date)
	}
}

##############################
#
# FUNCTION TrackRenamedMedia
#
##############################
function TrackRenamedMedia($currentOpenedFile)
{
	$renameItem = Get-Item -LiteralPath $currentOpenedFile
	if($renameItem -eq $null) { return }

	$renameOverride = LoadSubscriptionOverrideVariable $renameItem "DEFER_RENAME_PERSIST_HOURS_OVERRIDE"
	if($renameOverride -ne $null)
	{
		$renameOverride = [int] $renameOverride
	}
	else
	{
		$renameOverride = 0
	}

	$hoursOld = ((Get-Date) - $renameItem.CreationTime).TotalHours
	if(($renameOverride -gt 0) -and ($hoursOld -le $renameOverride))
	{
		VerboseLogInfo Deferring rename: [$renameItem.Name] [Until: ($renameItem.CreationTime.AddHours($renameOverride))]
	}
	else
	{
		TrackActiveFile $currentOpenedFile $RENAMED_CSV (get-date)
	}
}

##############################
#
# FUNCTION TrackWatchedMedia
#
##############################
function TrackWatchedMedia($currentOpenedFile, [System.DateTime] $timeStamp, $additionalContext)
{
	if($KODI_INTEGRATION -and ($additionalContext -ne "SESSION"))
	{
		if((IsValidPath $currentOpenedFile) -and (-not (IsAudio $currentOpenedFile)))
		{
			#Kodi sometimes grabs files immediately after recording and causes watched misfires.
			$fileCreated = (Get-Item -LiteralPath $currentOpenedFile).CreationTime
			if(((Get-Date) - $fileCreated).TotalHours -le 1)
			{
				LogWarning Deferring marking newly recorded media as consumed: [$currentOpenedFile]
				return
			}
		}
	}

	TrackActiveFile $currentOpenedFile $TRACKING_CSV $timeStamp $additionalContext
}

##############################
#
# FUNCTION TrackRecordingMedia
#
##############################
function TrackRecordingMedia($currentOpenedFile, $additionalContext)
{
	TrackActiveFile $currentOpenedFile $RECORDING_CSV ([System.DateTime]::MinValue) $additionalContext
}

####################################
#
# FUNCTION FindRecordingFile
#
####################################
function FindRecordingFile($showName)
{
	FindRecordingFile $showName $false
}

####################################
#
# FUNCTION FindRecordingFile
#
####################################
function FindRecordingFile($showName, [bool] $quiet)
{
	$showName = $showName | BeautifyAndDemoteString -ForceQuiet:$quiet | ConvertStringToASCII -ForceQuiet:$quiet | ConvertStringToFileSystem -ForceQuiet:$quiet
	$results = Get-ChildItem -Recurse -FollowSymlink -LiteralPath $DOWNLOADED_TV_PATH -File | Where-Object -FilterScript { ([System.IO.Path]::GetFileNameWithoutExtension($_.Name) -match [RegEx]::Escape($showName)) }

	if($results -eq $null)
	{
		VerboseLogInfo RECORDING NOT FOUND: $showName
		return $null
	}

	$openedMedia = GetExecutingProcessesForMedia

	foreach ($currentPossibleMatch in $results)
	{
		if($openedMedia.ContainsKey($currentPossibleMatch.FullName) -and ($openedMedia[$currentPossibleMatch] -in $RECORDING_HOSTS))
		{
			return $currentPossibleMatch
		}
	}

	if(-not $quiet)
	{
		LogError Could not find recording file: [$showName]
	}

	return $null
}

####################################
#
# FUNCTION FindAndTrackRecordingFile
#
####################################
function FindAndTrackRecordingFile($showName, $additionalContext)
{
	$results = FindRecordingFile $showName

	foreach ($currentResult in $results)
	{
		TrackRecordingMedia $currentResult.FullName $additionalContext
	}
}

#####################################
#
# FUNCTION FindAndDeleteRecordingFile
#
#####################################
function FindAndDeleteRecordingFile($showName)
{
	$results = FindRecordingFile $showName $true

	foreach ($currentResult in $results)
	{
		DeleteItem $currentResult.FullName
	}
}

####################################
#
# FUNCTION CheckForNewSeasons
#
####################################
function CheckForNewSeasons()
{
	#BUILD LIST OF SERIES TO CHECK
	$seriesToCheck = @{}
	$seasonInfoCSV = Import-Csv $CURRENT_SEASONS_CSV
	$tvdbSeries = LoadTvDBDictionary
	foreach ($currentSeries in $tvdbSeries.Keys)
	{
		$currentSeriesId = $tvdbSeries[$currentSeries]
		$seriesSkipped = $false

		if($currentSeriesId -eq 0)
		{
			$seriesSkipped = $true
		}

		if(-not $seriesSkipped)
		{
			$currentSeriesSeasonNumber = 0
			foreach ($currentSeasonInfo in $seasonInfoCSV)
			{
				if($currentSeasonInfo.Id -eq $currentSeriesId)
				{
					if($currentSeasonInfo.Enabled -match "False")
					{
						$seriesSkipped = $true
					}
					else
					{
						$currentSeriesSeasonNumber = $currentSeasonInfo.Season
					}
					break
				}
			}
		}

		if($seriesSkipped)
		{
			DebugLogInfo SKIPPING SEASON CHECK SERIES: $currentSeries [$currentSeriesId]
			continue
		}

		if(-not $seriesToCheck.ContainsKey($currentSeriesId))
		{
			DebugLogInfo ADDING SEASON CHECK SERIES: $currentSeries [$currentSeriesId] [$currentSeriesSeasonNumber]
			$seriesToCheck.Add($currentSeriesId, $currentSeriesSeasonNumber)
		}
	}

	foreach ($currentSeriesId in $seriesToCheck.Keys)
	{
		$currentSeriesSeasonNumber = $seriesToCheck[$currentSeriesId]

		$cacheFile = GetTvDbSeriesCacheFile $currentSeriesId
		DeleteItem $cacheFile

		RefreshTvDbCache $null $currentSeriesId
		$cacheFile = GetTvDbSeriesCacheFile $currentSeriesId

		if(IsInvalidPath $cacheFile)
		{
			if(-not $WHATIF_MODE)
			{
				LogError Cache File Not Found: [$cacheFile]
			}
			continue
		}

		CheckSeasonForSeries $cacheFile $currentSeriesId $currentSeriesSeasonNumber
	}
}

####################################
#
# FUNCTION CheckSeasonForSeries
#
####################################
function CheckSeasonForSeries($tvDbCacheFile, [int] $currentSeriesId, [int] $currentSeriesSeasonNumber)
{
	if(FailedStringValidation $tvDbCacheFile) { return }

	if(IsInvalidPath $tvDbCacheFile)
	{
		LogError TVDB Cache file not found: [$tvDbCacheFile]
	}

	$seriesInfo = (Get-Content -LiteralPath $tvDbCacheFile -Encoding UTF8 | ConvertFrom-Json)
	$showName = $seriesInfo.data.name
	$isEnded = $seriesInfo.data.status.name -eq "Ended"

	DebugLogInfo CHECKING FOR NEW SEASONS: [$showName] [$currentSeriesId] [$tvDbCacheFile] [$currentSeriesSeasonNumber] [$isEnded]

	if($seriesInfo.data.status -eq "Ended")
	{
		LogInfo Series has now ended. Disabling future checks: [$showName] [$currentSeriesId]
		DisableShowSeasonChecks $currentSeriesId
		return
	}

	$tvDbEpisodesCacheFile = GetTvDbEpisodesCacheFile $currentSeriesId
	$episodeInfo = (Get-Content -LiteralPath $tvDbEpisodesCacheFile -Encoding UTF8 | ConvertFrom-Json)

	$sortedEpisodesByAired = ($episodeInfo.data.episodes | Where-Object { $_.aired -ne "" }).aired | Sort-Object -Descending
	$sortedEpisodesBySeason = ($episodeInfo.data.episodes | Where-Object { $_.seasonNumber -ne "" } | Sort-Object { [int]$_.seasonNumber } -Descending).seasonNumber

	if(($sortedEpisodesByAired.Count -le 0) -or ($sortedEpisodesBySeason.Count -le 0))
	{
		LogWarning Not enough information to check for seasons: [$tvDbCacheFile]
		return
	}

	if($sortedEpisodesByAired.GetType().ToString() -eq "System.Object[]")
	{
		$newestShowAired = [System.DateTime]::Parse($sortedEpisodesByAired[0])
	}
	else
	{
		$newestShowAired = [System.DateTime]::Parse($sortedEpisodesByAired)
	}

	if($sortedEpisodesBySeason.GetType().ToString() -eq "System.Object[]")
	{
		$highestShowSeason = [int]::Parse($sortedEpisodesBySeason[0])
	}
	else
	{
		$highestShowSeason = [int]::Parse($sortedEpisodesBySeason)
	}

	#NOTE: ASCENDING BY DEFAULT
	$sortedNewestSeasonEpisodesByAired = ($episodeInfo.data.episodes | Where-Object { ($_.aired -ne "") -and ($_.seasonNumber -eq $highestShowSeason) }).aired | Sort-Object
	if($sortedNewestSeasonEpisodesByAired -eq $null)
	{
		$highestShowSeason--
		$sortedNewestSeasonEpisodesByAired = ($episodeInfo.data.episodes | Where-Object { ($_.aired -ne "") -and ($_.seasonNumber -eq $highestShowSeason) }).aired | Sort-Object
	}

	if($sortedNewestSeasonEpisodesByAired.GetType().ToString() -eq "System.Object[]")
	{
		$oldestEpisodeInNewestSeason = [System.DateTime]::Parse($sortedNewestSeasonEpisodesByAired[0])
	}
	else
	{
		$oldestEpisodeInNewestSeason = [System.DateTime]::Parse($sortedNewestSeasonEpisodesByAired)
	}
	VerboseLogInfo [SERIES: $showName ($currentSeriesId)] [NEWEST: Season: $highestShowSeason Air: ($newestShowAired.Date.ToShortDateString())] [STARTED: $oldestEpisodeInNewestSeason]

	if($currentSeriesSeasonNumber -ge $highestShowSeason)
	{
		return
	}

	$notifyUntil = (Get-Date).AddDays($NEW_SEASON_LEAD_NOTICE_DAYS)
	if($notifyUntil -ge $oldestEpisodeInNewestSeason)
	{
		if($isEnded)
		{
			LogInfo SERIES HAS ENDED: [$showName] [SEASON: $currentSeriesSeasonNumber --> $highestShowSeason]
		}
		elseif($currentSeriesSeasonNumber -gt 0)
		{
			LogInfo NEW SEASON FOUND: [$showName] [SEASON: $currentSeriesSeasonNumber --> $highestShowSeason] [TO AIR: ($oldestEpisodeInNewestSeason.Date.ToShortDateString())]
			DisplayNewSeasonNotification $showName (FormatAirDate $oldestEpisodeInNewestSeason)
		}
		else
		{
			LogInfo MONITORING FOR NEW SEASONS: [$showName]
		}
	}

	if($oldestEpisodeInNewestSeason -le (Get-Date))
	{
		VerboseLogInfo Remembering Latest Season: [$showName] [$currentSeriesSeasonNumber] --> [$highestShowSeason]
		UpdateCurrentShowSeason $currentSeriesId $highestShowSeason
	}
}

##################################
#
# FUNCTION GetLatestShowSeason
#
##################################
function GetLatestShowSeason($showName)
{
	if(-not (UseTvDB))
	{
		LogWarning Not configured to use TVDB.
		return 0
	}

	$seriesId = 0

	$tvdbSeries = LoadTvDBDictionary
	foreach ($currentSeries in $tvdbSeries.Keys)
	{
		if($currentSeries -match [RegEx]::Escape($showName))
		{
			$seriesId = [int]($tvdbSeries[$currentSeries])
			break
		}
	}

	if($seriesId -eq 0)
	{
		LogWarning TVDB information not found for series: [$showName]
		return 0
	}

	$inputCSV = Import-Csv $CURRENT_SEASONS_CSV
	foreach ($currentEntry in $inputCSV)
	{
		$currentSeason = [int]($currentEntry.Season)
		$currentId = [int]($currentEntry.Id)

		if(($currentId -eq $seriesId))
		{
			return $currentSeason
		}
	}

	LogWarning TVDB season information not found for series: [$showName]
	return 0
}

##################################
#
# FUNCTION UpdateCurrentShowSeason
#
##################################
function UpdateCurrentShowSeason([string] $seriesId, [string] $seasonNumber)
{
	$tmpFile = GenerateTmpFileName $CURRENT_SEASONS_CSV

	DeleteItem $tmpFile

	$inputCSV = Import-Csv $CURRENT_SEASONS_CSV

	$swapCsv = $false
	$seriesFound = $false
	foreach ($currentEntry in $inputCSV)
	{
		$csvEntry = $currentEntry
		if(($currentEntry.Id -eq $seriesId) -and ($currentEntry.Season -ne $seasonNumber))
		{
			$csvEntry = New-Object -TypeName PSObject -Property @{"Season" = $seasonNumber; "Id" = $currentEntry.Id; "Enabled" = $currentEntry.Enabled; }
			$seriesFound = $true
			$swapCsv = $true
		}
		AddCsvEntry $csvEntry $tmpFile
	}

	if(-not $seriesFound)
	{
		$csvEntry = New-Object -TypeName PSObject -Property @{"Season" = $seasonNumber; "Id" = $seriesId; "Enabled" = $true; }
		$swapCsv = $true
		AddCsvEntry $csvEntry $tmpFile
	}

	if($swapCsv)
	{
		SwapFileWithTmp $CURRENT_SEASONS_CSV
	}
}

##################################
#
# FUNCTION DisableShowSeasonChecking
#
##################################
function DisableShowSeasonChecks([string] $seriesId)
{
	$tmpFile = GenerateTmpFileName $CURRENT_SEASONS_CSV

	DeleteItem $tmpFile

	$inputCSV = Import-Csv $CURRENT_SEASONS_CSV

	$swapCsv = $false
	$seriesFound = $false
	foreach ($currentEntry in $inputCSV)
	{
		$csvEntry = $currentEntry
		if(($currentEntry.Id -eq $seriesId) -and ($currentEntry.Enabled))
		{
			$csvEntry = New-Object -TypeName PSObject -Property @{"Season" = $currentEntry.Season; "Id" = $currentEntry.Id; "Enabled" = $false; }
			$seriesFound = $true
			$swapCsv = $true
		}
		AddCsvEntry $csvEntry $tmpFile
	}

	if(-not $seriesFound)
	{
		$csvEntry = New-Object -TypeName PSObject -Property @{"Season" = 0; "Id" = $seriesId; "Enabled" = $false; }
		$swapCsv = $true
		AddCsvEntry $csvEntry $tmpFile
	}

	if($swapCsv)
	{
		SwapFileWithTmp $CURRENT_SEASONS_CSV
	}
}

##################################
#
# FUNCTION ExtractMetadataTags
#
##################################
function ExtractMetadataTags($fileName)
{
	$dictionary = @{}
	if(FailedStringValidation ($fileName)) { return $dictionary }

	if(IsInvalidPath $fileName)
	{
		LogWarning File does not exist. Metadata can not be extracted. [$fileName]
		return $dictionary
	}

	$file = Get-Item -LiteralPath $fileName

	if(IsInvalidPath $FFMPEG_BINARY)
	{
		LogWarning FFMPEG_BINARY is not defined. No Metadata can be extracted.
		return $dictionary
	}

	$metadataTargetPath = $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
	$metadataTarget = $metadataTargetPath + "\" + ($file.Name.Replace($file.Extension, ($file.Extension + $METADATA_EXTENSION)))
	CreateDirInPath $metadataTarget

	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList
	$dynamicParameters.Add("-i") > $null
	$dynamicParameters.Add($fileName) > $null
	$dynamicParameters.Add("-f") > $null
	$dynamicParameters.Add("ffmetadata") > $null
	$dynamicParameters.Add($metadataTarget) > $null

	ExecuteFFmpegCommand $metadataTarget $dynamicParameters $null

	if(IsInvalidPath $metadataTarget)
	{
		LogWarning No metadata was extracted. [$metadataTarget]
		return $dictionary
	}

	foreach($currentLine in (Get-Content -LiteralPath $metadataTarget))
	{
		#DebugLogInfo PARSE METADATA LINE: [$currentLine]
		if($currentLine.StartsWith(';') -or $currentLine.EndsWith('\'))
		{
			continue
		}

		if($currentLine.StartsWith('['))
		{
			#GLOBAL only
			break
		}

		if(-not $currentLine.Contains("="))
		{
			#NOTE: Some media might have embedded files like subtitles that will make this very verbose.
			#LogWarning Unexpected metadata tag value pair: [$metadataTarget] [$currentLine]
			continue
		}

		$tag = $currentLine.Substring(0,$currentLine.IndexOf("=")).Trim()
		$value = $currentLine.Substring($currentLine.IndexOf("=")+1).Trim()
		DebugLogInfo METADATA EXTRACTED: [$tag] [$value]
		#Hashes and equals may be escaped
		$value = [RegEx]::Unescape($value)
		$dictionary.Add($tag, $value)
	}

	if(IsFileCurrentlyOpen $metadataTarget)
	{
		SleepFor 5
	}

	if(-not $DEBUG_MODE)
	{
		DeleteItem $metadataTarget
		PurgeEmptyFolder $metadataTargetPath
	}

	return $dictionary
}

##################################
#
# FUNCTION ShouldRemoveCommericals
#
##################################
function ShouldRemoveCommericals($currentMedia)
{
	if(FailedStringValidation ($currentMedia)) { return $false }

	if(-not $Force)
	{
		if(-not (HasFileBeenRenamed $currentMedia)) { return $false }

		if(([System.IO.Path]::GetExtension($currentMedia)) -NotIn $MEDIA_AD_REMOVE_EXTENSIONS)
		{
			VerboseLogInfo SKIPPING AD REMOVAL: [$currentMedia]
			return $false
		}

		$subscriptionOverride = LoadSubscriptionOverrideVariable (Get-Item -LiteralPath $currentMedia) "ACQUISITION_REMOVE_ADS"
		if(($subscriptionOverride -ne $null) -and (-not $subscriptionOverride))
		{
			VerboseLogInfo File has ACQUISITION_REMOVE_ADS override to FALSE: [$currentMedia]
			return $false
		}

		if((IsInvalidPath $COMSKIP_BINARY) -or (IsInvalidPath $FFMPEG_BINARY) -or (IsInvalidPath $FFPROBE_BINARY))
		{
			LogWarning A dependency is missing to remove media ads.
			return $false
		}
	}

	return $true
}

##################################
#
# FUNCTION RemoveMediaAds
#
##################################
function RemoveMediaAds($currentOpenedFile)
{
	if(FailedStringValidation ($currentOpenedFile.FullName)) { return }

	if(-not (ShouldRemoveCommericals ($currentOpenedFile.FullName)))
	{
		LogWarning File cannot have commercials removed: [($currentOpenedFile.FullName)]
		return
	}
	LogInfo REMOVING ADS: [($currentOpenedFile.BaseName)]

	$safeFileName = $currentOpenedFile.BaseName | BeautifyAndDemoteString -ForceQuiet | ConvertStringToASCII -ForceQuiet | ConvertStringToFileSystem -ForceQuiet
	if($safeFileName -eq $currentOpenedFile.BaseName)
	{
		RemoveMediaAdsInternal $currentOpenedFile
	}
	else
	{
		$newFileName = $currentOpenedFile.FullName.Replace($currentOpenedFile.BaseName, $safeFileName)

		RenameFile ($currentOpenedFile.FullName) $newFileName
		TrackAdRemovedMedia $newFileName
		RemoveMediaAdsInternal (Get-Item -LiteralPath $newFileName)
		RenameFile $newFileName ($currentOpenedFile.FullName)
	}
}

###################################
#
# FUNCTION ExportMediaSegment
#
###################################
function ExportMediaSegment($sourceMedia, $outputSegmentFile, $start, $duration,  $sourceMediaSubtitles)
{
	CreateDirInPath $outputSegmentFile
	if(IsValidPath $outputSegmentFile)
	{
		LogWarning Overwriting: [$outputSegmentFile]
		DeleteItem $outputSegmentFile
	}

	if(IsValidPath $sourceMediaSubtitles)
	{
		$exportSrt = $true
	}
	else
	{
		$exportSrt = $false
		DebugLogInfo NOT Exporting segment SRT: [$outputSegmentFile]
	}

	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

	$dynamicParameters.Add("-y") > $null
	$dynamicParameters.Add("-ss") > $null
	$dynamicParameters.Add($start) > $null
	$dynamicParameters.Add("-i") > $null
	$dynamicParameters.Add($sourceMedia.FullName) > $null
	$dynamicParameters.Add("-t") > $null
	$dynamicParameters.Add($duration) > $null
	$dynamicParameters.Add("-map_metadata") > $null
	$dynamicParameters.Add("0") > $null
	$dynamicParameters.Add("-map_chapters") > $null
	$dynamicParameters.Add("-1") > $null
	$dynamicParameters.Add("-c") > $null
	$dynamicParameters.Add("copy") > $null
	$dynamicParameters.Add($outputSegmentFile) > $null

	ExecuteFFmpegCommand $outputSegmentFile $dynamicParameters $null

	if($exportSrt)
	{
		$segmentSrtFile = $outputSegmentFile.Replace($sourceMedia.Extension, ".srt")
		if(IsValidPath $segmentSrtFile)
		{
			LogWarning Overwriting: [$segmentSrtFile]
			DeleteItem $segmentSrtFile
		}

		$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

		$dynamicParameters.Add("-y") > $null
		$dynamicParameters.Add("-ss") > $null
		$dynamicParameters.Add($start) > $null
		$dynamicParameters.Add("-i") > $null
		$dynamicParameters.Add($sourceMedia.FullName) > $null
		$dynamicParameters.Add("-i") > $null
		$dynamicParameters.Add($sourceMediaSubtitles) > $null
		$dynamicParameters.Add("-t") > $null
		$dynamicParameters.Add($duration) > $null
		$dynamicParameters.Add($segmentSrtFile) > $null

		ExecuteFFmpegCommand $segmentSrtFile $dynamicParameters $null
	}
}

##################################
#
# FUNCTION RemoveMediaAdsInternal
#
##################################
function RemoveMediaAdsInternal($currentOpenedFile)
{
	if(FailedStringValidation ($currentOpenedFile.FullName)) { return }

	$subscriptionAdsBookended = LoadSubscriptionOverrideVariable $currentOpenedFile "AD_REMOVE_ADS_BOOKENDED"
	if($subscriptionAdsBookended -eq $null)
	{
		$subscriptionAdsBookended = $false
	}

	$comskipProfile = LoadSubscriptionOverrideVariable $currentOpenedFile "COMSKIP_PROFILE"
	if($comskipProfile -ne $null)
	{
		$comskipProfile = [System.IO.Path]::Combine($COMSKIP_PROFILE_PATH, $comskipProfile)
		if(IsInvalidPath $comskipProfile)
		{
			LogWarning ComSkip profile not found: [($comskipProfile)]
			$comskipProfile = $null
		}
	}

	$baseSegmentDir = $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()

	$outputMedia = $currentOpenedFile.FullName.Replace($currentOpenedFile.Directory.FullName, $baseSegmentDir)
	$outputEdl = $currentOpenedFile.FullName.Replace($currentOpenedFile.Directory.FullName, $AD_REMOVE_LOGS).Replace($currentOpenedFile.Extension, ".edl")

	$adPath = $PURGE_PATH + "\ADS"
	$withComsFile = $currentOpenedFile.FullName.Replace($currentOpenedFile.Extension, ".WithAds" + $currentOpenedFile.Extension)

	# CREATE LOG LOCATION
	if(IsInvalidPath $AD_REMOVE_LOGS)
	{
		CreateDirInPath ($AD_REMOVE_LOGS + "\tmp.txt")
	}

	# DELETE PREVIOUS DATA
	foreach ($currentDependency in @($outputEdl, $outputMedia, $withComsFile))
	{
		if(IsValidPath $currentDependency)
		{
			LogWarning Overwriting: [$currentDependency]
			DeleteItem $currentDependency
		}
	}

	# CALCULATE ORIGINAL LENGTH IF NEEDED
	$sourceTotalDuration = (ReturnDuration $currentOpenedFile.FullName).TotalSeconds

	#First try to export Advertisement chapters as an EDL to see if we can bypass COMSKIP
	$output = ExecuteCommand $FFPROBE_BINARY "-i" $currentOpenedFile.FullName "-print_format" "compact=print_section=0" "-show_entries" "chapter" "-v" "quiet"

	# id=5|time_base=1/1000|start=1294178|start_time=1294.178000|end=1443101|end_time=1443.101000|tag:title=Advertisement
	$adPattern = '(.*\|)+start_time=(?<start_time>\d+\.\d+)\|(.*\|)+end_time=(?<end_time>\d+\.\d+)\|tag:title=Advertisement'
	foreach ($currentOutputLine in $output)
	{
		$responseMatches = [RegEx]::Matches($currentOutputLine, $adPattern)
		if($responseMatches.Success)
		{
			$adStart = $responseMatches[0].Groups["start_time"].Value
			$adEnd = $responseMatches[0].Groups["end_time"].Value

			#First segment should be always also expendable (PlayOn heading), and further logic assumes it.
			# That heading is not marked as an advertisement chapter.
			# However it may not be immediately followed by an ad and could contain the first show segement.
			# The heading itself is about 6 seconds, assume the first segment 30 seconds or less is expendable
			if(IsInvalidPath $outputEdl)
			{
				$adStartDouble = [double]::Parse($adStart)
				if($adStartDouble -eq 0)
				{
					#Do Nothing, handled below
				}
				elseif($adStartDouble -le 30)
				{
					DebugLogInfo Extracted implicit chapter advertisement: [$adStart]
					([String]::Format("0.00`t{0}`t0", $adStart)) | SafeOutFile -LiteralPath $outputEdl
				}
				else
				{
					DebugLogInfo Hardcoded implicit chapter advertisement.
					([String]::Format("0.00`t{0}`t0", "5.989000")) | SafeOutFile -LiteralPath $outputEdl
				}
			}

			DebugLogInfo Extracted chapter advertisement: [$adStart] [$adEnd]
			([String]::Format("{0}`t{1}`t0", $adStart, $adEnd)) | SafeOutFile -LiteralPath $outputEdl
		}
	}

	if(IsInvalidPath $outputEdl)
	{
		# CALCULATE COMMERICAL SEGEMENTS
		VerboseLogInfo Calculating Commercial segments via ComSkip...

		$dynamicParameters = New-Object -TypeName System.Collections.ArrayList
		if($DEBUG_MODE -or $VERBOSE_MODE)
		{
			#USE PROFILE, conflicts with -v
		}
		else
		{
			$dynamicParameters.Add("-q") > $null
		}
		if(($comskipProfile -ne $null) -and (IsValidPath $comskipProfile))
		{
			$dynamicParameters.Add("--ini=" + $comskipProfile) > $null
		}
		$dynamicParameters.Add("--output=" + $AD_REMOVE_LOGS) > $null
		$dynamicParameters.Add($currentOpenedFile.FullName) > $null

		if($WHATIF_MODE)
		{
			LogInfo WHATIF: $COMSKIP_BINARY $dynamicParameters
			return
		}
		else
		{
			ExecuteCommand $COMSKIP_BINARY $dynamicParameters > $null
		}
	}

	if((IsInvalidPath $outputEdl) -and (-not $WHATIF_MODE))
	{
		VerboseLogInfo No ComSkip commercial segements file found: [$outputEdl]
		return
	}

	#EXPORT SUBTITLES
	if(MediaHasSubtitles $currentOpenedFile.FullName)
	{
		$targetSubtitles = $currentOpenedFile.FullName.Replace($currentOpenedFile.Directory.FullName, $AD_REMOVE_LOGS).Replace($currentOpenedFile.Extension, ".srt")
		if(IsValidPath $targetSubtitles)
		{
			LogWarning Overwriting: [$targetSubtitles]
			DeleteItem $targetSubtitles
		}

		$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

		$dynamicParameters.Add("-i") > $null
		$dynamicParameters.Add($currentOpenedFile.FullName) > $null
		$dynamicParameters.Add($targetSubtitles) > $null

		ExecuteFFmpegCommand $targetSubtitles $dynamicParameters $null
	}

	# TRANSLATE TO PROGRAM SEGMENTS, EXPORT EACH ONE
	VerboseLogInfo Extracting segments...
	$commercials = Import-Csv -Delimiter `t -LiteralPath $outputEdl -Header Start, End, tmp

	$segment = 1
	$ad = 1
	$segmentStart = 0.0
	foreach ($currentCommercial in $commercials)
	{
		$startCommercialTime = [double]::Parse($currentCommercial.Start)
		$endCommercialTime = [double]::Parse($currentCommercial.End)
		VerboseLogInfo Commercial [START: $startCommercialTime] [END: $endCommercialTime]

		$skipAd = $false
		if($subscriptionAdsBookended)
		{
			$durationWithWiggle = ($sourceTotalDuration - 5)
			if(($startCommercialTime -ne 0) -and ($endCommercialTime -lt $durationWithWiggle))
			{
				DebugLogInfo Skipping ad, is not bookended: [Number:($ad)] [Start:($startCommercialTime)] [End:($endCommercialTime)]
				$skipAd = $true
			}
		}

		if($AD_REMOVE_DIAGNOSTICS -and (-not $skipAd))
		{
			$adDuration = $endCommercialTime - $startCommercialTime
			$adFile = $currentOpenedFile.FullName.Replace($currentOpenedFile.Directory.FullName, $adPath).Replace($currentOpenedFile.Extension, (".AD" + $ad + $currentOpenedFile.Extension))

			ExportMediaSegment $currentOpenedFile $adFile $startCommercialTime $adDuration $null

			$ad++
		}

		if($startCommercialTime -le $segmentStart)
		{
			$segmentStart = $endCommercialTime
			continue
		}

		$segmentDuration = $startCommercialTime - $segmentStart
		$segmentMediaFile = $outputMedia.Replace($currentOpenedFile.Extension, (".S" + $segment + $currentOpenedFile.Extension))

		ExportMediaSegment $currentOpenedFile $segmentMediaFile $segmentStart $segmentDuration $targetSubtitles

		$segmentStart = $currentCommercial.End
		$segment++
	}

	DebugLogInfo Final Segment [($sourceTotalDuration - $segmentStart)] [$segment]
	if(($sourceTotalDuration - $segmentStart) -gt 5)
	{
		$segmentDuration = $sourceTotalDuration - $segmentStart
		$segmentMediaFile = $outputMedia.Replace($currentOpenedFile.Extension, (".S" + $segment + $currentOpenedFile.Extension))

		ExportMediaSegment $currentOpenedFile $segmentMediaFile $segmentStart $segmentDuration $targetSubtitles
	}

	DebugLogInfo Number of segments: [$segment] ads: [$ad]
	if(($segment -eq 1) -and ($ad -eq 1) -and (-not $WHATIF_MODE))
	{
		LogWarning No show segments detected for media via ComSkip: [($currentOpenedFile.FullName)]
		return
	}

	if(IsValidPath $targetSubtitles)
	{
		PurgeItem $targetSubtitles
	}

	# CONCATENATE PROGRAM SEGEMENTS
	ConcatenateMediaSegments $baseSegmentDir

	$targetMedia = Get-ChildItem -LiteralPath $baseSegmentDir -Filter ("*" + $currentOpenedFile.Extension)
	if($targetMedia.Count -gt 1)
	{
		LogWarning Too much media found: [($targetMedia.Count)]
	}

	if($subscriptionAdsBookended -and ($ad -gt 3))
	{
		LogWarning A large number of ADs were detected and removed: [$ad]
	}

	#EXPORT THUMBNAIL
	$targetThumbnail = $outputMedia.Replace($currentOpenedFile.Extension, ".png")
	if(IsValidPath $targetThumbnail)
	{
		LogWarning Overwriting: [$targetThumbnail]
		DeleteItem $targetThumbnail
	}

	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

	$dynamicParameters.Add("-i") > $null
	$dynamicParameters.Add($currentOpenedFile.FullName) > $null
	$dynamicParameters.Add("-map") > $null
	$dynamicParameters.Add("0:v") > $null
	$dynamicParameters.Add("-map") > $null
	$dynamicParameters.Add("0:V") > $null
	$dynamicParameters.Add("-c") > $null
	$dynamicParameters.Add("copy") > $null
	$dynamicParameters.Add("-update") > $null
	$dynamicParameters.Add("1") > $null
	$dynamicParameters.Add("-vframes") > $null
	$dynamicParameters.Add("1") > $null
	$dynamicParameters.Add($targetThumbnail) > $null

	ExecuteFFmpegCommand $targetThumbnail $dynamicParameters $null

	if(IsValidPath $targetThumbnail)
	{
		# EMBED and SWAP THUMBNAIL
		$withThumbFile = $outputMedia.Replace($currentOpenedFile.Extension, ".WithThumb" + $currentOpenedFile.Extension)
		ConvertMedia $outputMedia $withThumbFile
		if(IsValidPath $withThumbFile)
		{
			PurgeItem $outputMedia
			RenameFile $withThumbFile $outputMedia
		}
	}

	RenameFile ($currentOpenedFile.FullName) $withComsFile
	PurgeItem $withComsFile

	foreach ($currentTargetMedia in $targetMedia)
	{
		MoveFile ($currentTargetMedia.FullName) ($currentOpenedFile.Directory.FullName)
	}

	PurgeEmptyFolder $baseSegmentDir
}

###################################
#
# FUNCTION ShowSubscriptionParameters
#
###################################
function ShowSubscriptionParameters($vars)
{
	foreach ($currentVar in $vars)
	{
		if($currentVar.Name.StartsWith("SUBSCRIPTION") -or $currentVar.Name.StartsWith("ACQUISITION"))
		{
			DebugLogInfo ($currentVar.Name): [($currentVar.Value)]
		}
	}
}

#####################################
#
# FUNCTION HandleSubscriptionNameList
#
#####################################
function HandleSubscriptionNameList()
{
	if((Get-Variable -Name SUBSCRIPTION_NAME -ea silentlycontinue) -eq $null)
	{
		return
	}

	if($SUBSCRIPTION_NAME -ne $null)
	{
		if(($SUBSCRIPTION_NAME.Count -eq 1) -and (IsStringDefined $SUBSCRIPTION_NAME))
		{
			#We're good as-is
		}
		elseif(($SUBSCRIPTION_NAME.Count -gt 1) -and (IsStringDefined $SUBSCRIPTION_NAME[0]))
		{
			Set-Variable -Name SUBSCRIPTION_NAME -Value ($SUBSCRIPTION_NAME[0]) -Scope 1
		}
		else
		{
			Set-Variable -Name SUBSCRIPTION_NAME -Value $null -Scope 1
		}
	}
	else
	{
		Set-Variable -Name SUBSCRIPTION_NAME -Value $null -Scope 1
	}
}

###################################
#
# FUNCTION AcquireMediaContent
#
###################################
function AcquireMediaContent($Subscription, $Uri, $RenameContentTo)
{
	if(FailedStringValidation ($Subscription)) { return }
	if(FailedStringValidation ($Uri)) { return }

	#CLEAR/LOAD SUBSCRIPTION
	. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
	. $Subscription

	HandleSubscriptionNameList
	if(IsStringDefined $SUBSCRIPTION_NAME)
	{
		LogInfo ACQUIRING: [([System.IO.Path]::GetFileName($Subscription))] [$SUBSCRIPTION_NAME] [$Uri]
	}
	else
	{
		LogInfo ACQUIRING: [$Uri]
	}

	if($AdRemoveSubscriptionOverride)
	{
		$ACQUISITION_REMOVE_ADS = (-not $ACQUISITION_REMOVE_ADS)
	}

	if($RenameSubscriptionOverride)
	{
		$ACQUISITION_ALLOW_RENAME = (-not $ACQUISITION_ALLOW_RENAME)
	}

	if($ExtractAudioSubscriptionOverride)
	{
		$ACQUISITION_EXTRACT_AUDIO = (-not $ACQUISITION_EXTRACT_AUDIO)
	}

	ShowSubscriptionParameters (Get-Variable -Scope 0)

	$EPISODE_CONTENT_TEMP_LOCATION = $SUBSCRIPTION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
	if(-not (IsStringDefined $EPISODE_CONTENT_URI))
	{
		$EPISODE_CONTENT_URI = $Uri
	}
	$acquiredMedia = New-Object -TypeName System.Collections.ArrayList
	$baseUri = [System.Uri]::new($Uri).LocalPath.ToString()
	$extension = [System.IO.Path]::GetExtension($baseUri)

	# Make a sound
	Ding

	$executionStatus = $true
	if($SUBSCRIPTION_ACQUIRE_CUSTOM -ne $null)
	{
		foreach ($currentScriptBlock in $SUBSCRIPTION_ACQUIRE_CUSTOM)
		{
			if($DEBUG_MODE)
			{
				DebugLogInfo EXECUTING: [$currentScriptBlock]
				Invoke-Command -NoNewScope -ScriptBlock $currentScriptBlock -ErrorVariable $blockError
			}
			else
			{
				Invoke-Command -NoNewScope -ScriptBlock $currentScriptBlock -ErrorVariable $blockError > $null
			}
		}
		$Uri = $EPISODE_CONTENT_URI
		$executionStatus = ((Get-ChildItem -File -LiteralPath $EPISODE_CONTENT_TEMP_LOCATION) | Where-Object { $_.Extension -In ($MEDIA_EXTENSIONS) } ).Count -gt 0
	}

	if($executionStatus -and ($extension -In $MEDIA_EXTENSIONS))
	{
		$targetFile = [System.IO.Path]::GetFileName($baseUri)
		$targetFile = [System.IO.Path]::Combine($EPISODE_CONTENT_TEMP_LOCATION, $targetFile)
		CreateDirInPath $targetFile

		$webclient = New-Object System.Net.WebClient
		$webclient.Encoding = [System.Text.Encoding]::UTF8

		if($VERBOSE_MODE)
		{
			$webclient.DownloadFile($Uri, $targetFile)
		}
		else
		{
			$webclient.DownloadFile($Uri, $targetFile) > $null
		}

		if($webclient -ne $null)
		{
			$webclient.Dispose()
		}

		$executionStatus = (IsValidPath $targetFile)
	}
	elseif( $executionStatus -and ($Uri -ne $null) -and
		( ($SUBSCRIPTION_ACQUIRE_CUSTOM -eq $null) -or ($ACQUISITION_ADD_PARAMS -ne $null) ) )
	{
		#CASES:
		# $SUBSCRIPTION_ACQUIRE_CUSTOM not set, $ACQUISITION_ADD_PARAMS not set --> use YTDL
		# $SUBSCRIPTION_ACQUIRE_CUSTOM set, $ACQUISITION_ADD_PARAMS not set --> DO NOT USE YTDL
		# $SUBSCRIPTION_ACQUIRE_CUSTOM not set, $ACQUISITION_ADD_PARAMS set --> use YTDL
		# $SUBSCRIPTION_ACQUIRE_CUSTOM set, $ACQUISITION_ADD_PARAMS set --> use YTDL
		$executionStatus = AcquireMediaContentFromYouTubeDL $Uri $EPISODE_CONTENT_TEMP_LOCATION
	}

	#VERIFY MEDIA EXISTS
	$cAcquiredMedia = 0
	$mediaReference = $null
	foreach ($currentDownloadedMedia in (Get-ChildItem -LiteralPath $EPISODE_CONTENT_TEMP_LOCATION -ErrorAction Ignore | Where-Object { ($_.Extension -In $MEDIA_EXTENSIONS) }))
	{
		$cAcquiredMedia++
		if($mediaReference -eq $null)
		{
			$mediaReference = $currentDownloadedMedia.FullName
		}
	}

	if($cAcquiredMedia -le 0)
	{
		LogWarning "No media appears to be acquired."
		return $false
	}

	#WAIT UNTIL HANDLES ARE RELEASED
	foreach ($currentDownloadedMedia in (Get-ChildItem -LiteralPath $EPISODE_CONTENT_TEMP_LOCATION -ErrorAction Ignore | Where-Object { ($_.Extension -In $MEDIA_EXTENSIONS) }))
	{
		if(IsFileCurrentlyOpen $currentDownloadedMedia.FullName)
		{
			SleepFor 5
		}
	}

	#Detect if acquisition might be segmented
	$downloadedMedia = (Get-ChildItem -LiteralPath $EPISODE_CONTENT_TEMP_LOCATION -ErrorAction Ignore | Where-Object { $_.Extension -In $MEDIA_EXTENSIONS })
	if(($downloadedMedia.Count -gt 1))
	{
		$isSegmented = $true
		foreach($currentDownloadedMedia in $downloadedMedia)
		{
			$currentDownloadedMedia.FullName -NotMatch "\.[sS]\d\."
			{
				$isSegmented = $false
				break
			}
		}

		if($isSegmented)
		{
			ConcatenateMediaSegments $EPISODE_CONTENT_TEMP_LOCATION
		}
		else
		{
			LogWarning Multiple media files detected that appear not to be segmented.
			LogWarning '(Did you forget to set $ACQUISITION_OUTPUT_FORMAT to "%(playlist_title)s.S%(playlist_index)s.%(ext)s"?)'
		}
	}

	if(IsStringDefined $RenameContentTo)
	{
		foreach ($currentDownloadedMedia in (Get-ChildItem -LiteralPath $EPISODE_CONTENT_TEMP_LOCATION -ErrorAction Ignore))
		{
			$newName = $EPISODE_CONTENT_TEMP_LOCATION + "\" + ($RenameContentTo | BeautifyAndPromoteString | ConvertStringToFileSystem).Trim() + $currentDownloadedMedia.Extension
			if(IsInvalidPath $newName)
			{
				RenameFile ($currentDownloadedMedia.FullName) $newName
			}
			else
			{
				LogWarning Cannot rename acquisition, deleting: [($currentDownloadedMedia.FullName)]
				PurgeItem $currentDownloadedMedia.FullName
			}
		}
	}
	elseif($SUBSCRIPTION_NAME -ne $null)
	{
		foreach ($currentDownloadedMedia in (Get-ChildItem -LiteralPath $EPISODE_CONTENT_TEMP_LOCATION -ErrorAction Ignore))
		{
			$newName = $null
			if(-not ($currentDownloadedMedia.Name.StartsWith($SUBSCRIPTION_NAME)))
			{
				$newShowName = $SUBSCRIPTION_NAME | BeautifyAndDemoteString | ConvertStringToASCII | ConvertStringToFileSystem
				if(-not ($currentDownloadedMedia.Name.StartsWith($newShowName)))
				{
					$newName = $EPISODE_CONTENT_TEMP_LOCATION + "\" + $newShowName + " - " + $currentDownloadedMedia.Name
				}
			}

			if(IsInvalidPath $newName)
			{
				RenameFile ($currentDownloadedMedia.FullName) $newName
			}
			else
			{
				LogWarning Cannot rename acquisition, deleting: [($currentDownloadedMedia.FullName)]
				PurgeItem $currentDownloadedMedia.FullName
			}
		}
	}

	# Embed thumbnails / subtitles / metadata into source media
	$relevantExtensions = New-Object -TypeName System.Collections.ArrayList
	foreach ($relevantExtensionCollection in @($THUMBNAIL_EXTENSIONS, $SUBTITLE_EXTENSIONS, $METADATA_EXTENSION))
	{
		foreach ($relevantExtension in $relevantExtensionCollection)
		{
			$relevantExtensions.Add($relevantExtension) > $null
		}
	}
	$sourceRelatedFiles = FindRelatedFiles @($mediaReference) ($relevantExtensions.ToArray())
	$sourceMedia = FindRelatedFiles @($mediaReference) $MEDIA_EXTENSIONS
	if($sourceMedia.Count -ne 1)
	{
		LogWarning Invalid number of acquired media files: [($sourceMedia.Count)]
	}
	elseif($sourceRelatedFiles.Count -gt 0)
	{
		$inputMedia = (Get-Item -LiteralPath $sourceMedia)
		$targetMedia = $inputMedia.FullName.Replace($inputMedia.Extension, (".Embedded" + $inputMedia.Extension))

		ConvertMedia ($inputMedia.FullName) $targetMedia

		#WAIT UNTIL HANDLES ARE RELEASED
		foreach ($currentDownloadedMedia in (Get-ChildItem -LiteralPath $EPISODE_CONTENT_TEMP_LOCATION -ErrorAction Ignore))
		{
			$canContinue = $false
			while(-not $canContinue)
			{
				if(IsFileCurrentlyOpen $currentDownloadedMedia.FullName)
				{
					LogWarning Waiting for handle release: [($currentDownloadedMedia.FullName)]
					SleepFor 5
				}
				else
				{
					$canContinue = $true
				}
			}
		}

		if(IsValidPath $targetMedia)
		{
			PurgeItem $inputMedia.FullName
			RenameFile $targetMedia ($inputMedia.FullName)
		}
	}

	if($ACQUISITION_IS_MOVIE)
	{
		foreach ($currentDownloadedMedia in (Get-ChildItem -LiteralPath $EPISODE_CONTENT_TEMP_LOCATION -ErrorAction Ignore))
		{
			MoveFile ($currentDownloadedMedia.FullName) $DOWNLOADED_MOVIES_PATH
			$acquiredMedia.Add([System.IO.Path]::Combine($DOWNLOADED_MOVIES_PATH, $currentDownloadedMedia.Name)) > $null
		}
	}
	else
	{
		foreach ($currentDownloadedMedia in (Get-ChildItem -LiteralPath $EPISODE_CONTENT_TEMP_LOCATION -ErrorAction Ignore))
		{
			if(IsAudio $currentDownloadedMedia.FullName)
			{
				$currentDownloadedMediaTarget = $DOWNLOADED_PODCASTS_PATH
			}
			else
			{
				$currentDownloadedMediaTarget = $DOWNLOADED_TV_PATH
			}
			MoveFile ($currentDownloadedMedia.FullName) $currentDownloadedMediaTarget
			$acquiredMedia.Add([System.IO.Path]::Combine($currentDownloadedMediaTarget, $currentDownloadedMedia.Name)) > $null
		}
	}

	if(-not $ACQUISITION_REMOVE_ADS)
	{
		foreach ($currentDownloadedMedia in $acquiredMedia)
		{
			TrackActiveFile $currentDownloadedMedia $AD_REMOVE_CSV (get-date) $false
		}
	}

	foreach ($currentDownloadedMedia in $acquiredMedia)
	{
		if(IsInvalidPath $currentDownloadedMedia)
		{
			LogWarning Problem acquiring: [$currentDownloadedMedia]
			continue
		}

		$currentDownloadedMediaFile = Get-Item -LiteralPath $currentDownloadedMedia
		if($currentDownloadedMediaFile.Extension -NotIn ($MEDIA_EXTENSIONS))
		{
			continue
		}

		$tmpSeries = ""
		$tmpTitle = ""
		if($RenameContentTo -ne $null)
		{
			$tmpTitle = $RenameContentTo
			if(-not (IsStringDefined $tmpTitle))
			{
				continue
			}
		}
		else
		{
			$renamedFileResults = GenerateSmartRenamedFileName $currentDownloadedMediaFile 0
			if($renamedFileResults -ne $null)
			{
				$tmpSeries = $renamedFileResults[1]
				$tmpTitle = $renamedFileResults[2]
			}

			if(-not (IsStringDefined $tmpTitle))
			{
				$tmpTitle = ExtractBestGuessTitle $currentDownloadedMediaFile
				if(-not (IsStringDefined $tmpTitle))
				{
					continue
				}
			}
		}

		if(IsStringDefined $SUBSCRIPTION_NAME)
		{
			$tmpSeries = $SUBSCRIPTION_NAME
			if($tmpTitle.StartsWith($tmpSeries))
			{
				$tmpTitle = $tmpTitle.SubString($tmpSeries.Length)
			}
			else
			{
				$tmpSeries = $tmpSeries | BeautifyAndDemoteString | ConvertStringToASCII | ConvertStringToFileSystem
				if($tmpTitle.StartsWith($tmpSeries))
				{
					$tmpTitle = $tmpTitle.SubString($tmpSeries.Length)
					$tmpTitle = $tmpTitle | BeautifyAndDemoteString
				}
			}
		}
		RefreshMetadata $currentDownloadedMediaFile $SUBSCRIPTION_NAME $tmpTitle

		if(-not $ACQUISITION_ALLOW_RENAME)
		{
			TrackActiveFile $currentDownloadedMedia $RENAMED_CSV (get-date) $false
		}
	}

	#Extract Audio
	if($ACQUISITION_EXTRACT_AUDIO)
	{
		foreach ($currentDownloadedMedia in $acquiredMedia)
		{
			ExtractMediaAudio $currentDownloadedMedia
		}
	}

	#Clean up
	PurgeEmptyFolder $EPISODE_CONTENT_TEMP_LOCATION
	return $executionStatus
}

############################################
#
# FUNCTION AcquireMediaContentFromYouTubeDL
#
############################################
function AcquireMediaContentFromYouTubeDL($Uri, $TargetFolder)
{
	if(FailedStringValidation ($Uri)) { return }
	if(FailedStringValidation ($TargetFolder)) { return }

	if(IsValidPath $FFMPEG_BINARY)
	{
		$ffmpegLocation = [System.IO.Path]::GetDirectoryName($FFMPEG_BINARY)
	}

	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

	if($ACQUISITION_OUTPUT_FORMAT -ne $null)
	{
		$targetOutput = $TargetFolder + "\" + $ACQUISITION_OUTPUT_FORMAT
		$dynamicParameters.Add("-o") > $null
		$dynamicParameters.Add($targetOutput) > $null
	}

	$dynamicParameters.Add("--fixup") > $null
	$dynamicParameters.Add("detect_or_warn") > $null

	if($ACQUISITION_EXTRACT_METADATA)
	{
		$dynamicParameters.Add("--add-metadata") > $null
	}

	$dynamicParameters.Add("--write-thumbnail") > $null

	if($VERBOSE_MODE)
	{
		$dynamicParameters.Add("--verbose") > $null
	}
	else
	{
		$dynamicParameters.Add("--quiet") > $null
	}

	if($ffmpegLocation -ne $null)
	{
		if((-not $ACQUISITION_IS_AUDIO) -and (IsStringDefined $ACQUISITION_RECODE_VIDEO))
		{
			$dynamicParameters.Add("--recode-video") > $null
			$dynamicParameters.Add($ACQUISITION_RECODE_VIDEO) > $null
		}

		$dynamicParameters.Add("--ffmpeg-location") > $null
		$dynamicParameters.Add($ffmpegLocation) > $null
	}

	if(($ACQUISITION_AUTH_PROVIDER -ne $null) -and ($ACQUISITION_AUTH_PROVIDER -match "Cookies"))
	{
		$cookieFile = LoadPath $ACQUISITION_CREDENTIAL_ID
		if(IsInvalidPath $cookieFile)
		{
			LogError Invalid cookie file: [$cookieFile]
		}
		else
		{
			$dynamicParameters.Add("--cookies") > $null
			$dynamicParameters.Add($cookieFile) > $null
		}
	}
	elseif($ACQUISITION_CREDENTIAL_ID -ne $null)
	{
		$cred = GetCredential $ACQUISITION_CREDENTIAL_ID
		$UserName = $cred[0]
		$Password = $cred[1]

		if((IsStringDefined $UserName) -and (IsStringDefined $Password))
		{
			if($ACQUISITION_AUTH_PROVIDER -ne $null)
			{
				$dynamicParameters.Add("--ap-mso") > $null
				$dynamicParameters.Add($ACQUISITION_AUTH_PROVIDER) > $null

				$dynamicParameters.Add("--ap-username") > $null
				$dynamicParameters.Add($UserName) > $null

				$dynamicParameters.Add("--ap-password") > $null
				$dynamicParameters.Add($Password) > $null
			}
			else
			{
				$dynamicParameters.Add("--username") > $null
				$dynamicParameters.Add($UserName) > $null

				$dynamicParameters.Add("--password") > $null
				$dynamicParameters.Add($Password) > $null
			}
		}
		else
		{
			LogError Problem extracting credentials [$ACQUISITION_CREDENTIAL_ID]
		}
	}

	if($ACQUISITION_SUBTITLES_DAYS -gt 0)
	{
		$dynamicParameters.Add("--sub-format") > $null
		$dynamicParameters.Add("best") > $null

		#NOTE YouTube-DL and YT-DLP have forked this parameter, but appears to be cross compatible
		$dynamicParameters.Add("--write-sub") > $null
		$dynamicParameters.Add("--write-auto-subs") > $null
	}

	if($ACQUISITION_ADD_PARAMS -ne $null)
	{
		$dynamicParameters.Add($ACQUISITION_ADD_PARAMS) > $null
	}

	$dynamicParameters.Add($Uri) > $null

	return (ExecuteYouTubeDLCommand $dynamicParameters)
}

###################################
#
# FUNCTION ExecuteYouTubeDLCommand
#
###################################
function ExecuteYouTubeDLCommand($dynamicParameters)
{
	$commandParameters = New-Object -TypeName System.Collections.ArrayList

	if(-not $YOUTUBE_DL_BINARY.EndsWith(".exe"))
	{
		$commandParameters.Add($PYTHON_BINARY) > $null
	}
	$commandParameters.Add($YOUTUBE_DL_BINARY) > $null

	$scriptFileName = [System.IO.Path]::GetFileName($YOUTUBE_DL_BINARY)

	$errorRecord = $null
	if($WHATIF_MODE)
	{
		LogInfo WHATIF: $commandParameters $dynamicParameters
	}
	else
	{
		try
		{
			$output = ExecuteCommand $commandParameters $dynamicParameters
		}
		catch
		{
			if($_.Exception.ToString().Contains("WARNING: "))
			{
				$warningMessage = $_.Exception.ToString()
				$index = $warningMessage.IndexOf("WARNING: ") + ("WARNING: ".Length)
				$warningMessage = $warningMessage.Substring($index)
				LogWarning [$scriptFileName]: $warningMessage
			}
			else
			{
				$errorRecord = $_
				LogWarning Exception executing: [$commandParameters] [$dynamicParameters]
				LogError [$scriptFileName]: [($_.Exception.ToString())]
			}
		}

		foreach($currentOutputLine in $output)
		{
			if($currentOutputLine.ToString().Contains("WARNING: "))
			{
				$warningMessage = $currentOutputLine.Exception.ToString()
				$index = $warningMessage.IndexOf("WARNING: ") + ("WARNING: ".Length)
				$warningMessage = $warningMessage.Substring($index)
				LogWarning [$scriptFileName]: $warningMessage
				break
			}
		}

		if($errorRecord -ne $null)
		{
			if($Error -ne $null)
			{
				$Error[0] = $errorRecord
			}
			else
			{
				$Error.Add($errorRecord)
			}
			LogError [$scriptFileName]: $errorRecord.Exception.ToString()
		}
		else
		{
			foreach($currentOutputLine in $output)
			{
				DebugLogInfo [$scriptFileName]: $currentOutputLine
			}
		}
	}

	if(($LASTEXITCODE -ne 0) -or ($errorRecord -ne $null))
	{
		return $false
	}

	return $true
}

###################################
#
# FUNCTION ExtractMediaAudio
#
###################################
function ExtractMediaAudio($DownloadedMediaPath)
{
	if(FailedStringValidation ($DownloadedMediaPath)) { return }

	if(-not (IsStringDefined($DOWNLOADED_PODCASTS_PATH)))
	{
		LogError Audio Extraction Path not defined.
		return
	}

	$sourceVideo = $DownloadedMediaPath
	$destinationAudio = [System.IO.Path]::GetFileNameWithoutExtension($sourceVideo)
	$destinationAudio = $destinationAudio + ".mp3"
	$destinationAudio = [System.IO.Path]::Combine($DOWNLOADED_PODCASTS_PATH, $destinationAudio)

	if((Get-ChildItem -LiteralPath $DOWNLOADED_PODCASTS_PATH -Recurse -FollowSymlink -Filter ([System.IO.Path]::GetFileName($destinationAudio))) -ne $null)
	{
		VerboseLogInfo ALREADY EXTRACTED: [$destinationAudio]
		return
	}

	if(IsFileCurrentlyOpen $sourceVideo)
	{
		VerboseLogInfo FILE IS STILL OPEN: [$sourceVideo]
		return
	}

	LogInfo EXTRACT AUDIO: [$sourceVideo] --> [$destinationAudio]
	CreateDirInPath $destinationAudio

	ConvertMedia $sourceVideo $destinationAudio

	if($WHATIF_MODE)
	{
		return
	}
	elseif(IsInvalidPath $destinationAudio)
	{
		LogError Problem extracting audio: [$sourceVideo]
		return
	}

	$currentDownloadedMediaFile = Get-Item -LiteralPath $destinationAudio
	$tmpTitle = $currentDownloadedMediaFile.Name.Replace($currentDownloadedMediaFile.Extension, "").Trim()
	if(($tmpTitle -eq $null) -or ($tmpTitle.Length -eq 0))
	{
		//Skip Metadata
	}
	else
	{
		HandleSubscriptionNameList

		if(($SUBSCRIPTION_NAME -ne $null) -and ($SUBSCRIPTION_NAME.Length -gt 0))
		{
			$tmpTitle = $tmpTitle.Replace($SUBSCRIPTION_NAME, "").Trim(' ', '-')
			RefreshMetadata $currentDownloadedMediaFile $SUBSCRIPTION_NAME $tmpTitle
		}
		else
		{
			$tmpSeries = ExtractShowName $currentDownloadedMediaFile
			if(($tmpSeries -ne $null) -and ($tmpSeries.Length -gt 0))
			{
				$tmpTitle = $tmpTitle.Replace($tmpSeries, "").Trim(' ', '-')
				RefreshMetadata $currentDownloadedMediaFile $tmpSeries $tmpTitle
			}
			else
			{
				RefreshMetadata $currentDownloadedMediaFile "" $tmpTitle
			}
		}
	}

	if(-not $ACQUISITION_ALLOW_RENAME)
	{
		TrackActiveFile $destinationAudio $RENAMED_CSV (get-date) $false
	}
}

###################################
#
# FUNCTION ExtractAndReplaceMediaAudio
#
###################################
function ExtractAndReplaceMediaAudio()
{
	if(IsInvalidPath $DOWNLOADED_PODCASTS_PATH)
	{
		return
	}

	$videos = Get-ChildItem -LiteralPath $DOWNLOADED_PODCASTS_PATH -Recurse -FollowSymlink | Where-Object { $_.Extension -In $MEDIA_EXTENSIONS } | Where-Object { $_.Extension -NotIn $AUDIO_EXTENSIONS }
	foreach ($currentVideo in $videos)
	{
		$sourceVideo = $currentVideo.FullName
		$destinationAudio = [System.IO.Path]::GetFileNameWithoutExtension($sourceVideo)
		$destinationAudio = $destinationAudio + ".mp3"
		$destinationAudio = [System.IO.Path]::Combine($DOWNLOADED_PODCASTS_PATH, $destinationAudio)

		ExtractMediaAudio ($currentVideo.FullName)
		DeleteItem ($currentVideo.FullName)
	}
}

###################################
#
# FUNCTION ListMtpDirectoryRecursively
#
###################################
function ListMtpDirectoryRecursively($RootDirectory)
{
	$RootDirectory = $RootDirectory.TrimEnd("\")

	$shell = New-Object -ComObject Shell.Application

	$items = New-Object -TypeName System.Collections.ArrayList
	$baseShell = $shell.NameSpace($RootDirectory)
	if($baseShell -eq $null)
	{
		LogError Folder not found: [$RootDirectory]
		return
	}

	$folderItems = $shell.NameSpace($RootDirectory).self.GetFolder.Items()
	foreach ($currentFolderItem in $folderItems)
	{
		if($currentFolderItem.IsFolder)
		{
			foreach ($currentSubFolderItem in (ListMtpDirectoryRecursively ($RootDirectory + "\" + $currentFolderItem.Name)))
			{
				$items.Add($currentSubFolderItem) > $null
			}
		}
		else
		{
			$currentFolderItemExtension = $currentFolderItem.Name.SubString($currentFolderItem.Name.Length - 4)
			if($currentFolderItemExtension -in ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
			{
				$items.Add(($RootDirectory + "\" + $currentFolderItem.Name)) > $null
			}
		}
	}

	return $items
}

#############################################
#
# FUNCTION DeleteEmptyMtpDirectoryRecursively
#
#############################################
function DeleteEmptyMtpDirectoryRecursively($RootDirectory, $CopyPlaylists, $isRecursed)
{
	$RootDirectory = $RootDirectory.TrimEnd("\")

	$shell = New-Object -ComObject Shell.Application

	$baseShell = $shell.NameSpace($RootDirectory)
	if($baseShell -eq $null)
	{
		LogError Folder not found: [$RootDirectory]
		return
	}

	$folderItems = $shell.NameSpace($RootDirectory).self.GetFolder.Items()

	if($CopyPlaylists)
	{
		if($folderItems.Count -eq 1)
		{
			foreach ($currentFolderItem in $folderItems)
			{
				$currentFolderItemExtension = $currentFolderItem.Name.SubString($currentFolderItem.Name.Length - 4)
				if($currentFolderItemExtension -eq ".m3u")
				{
					DeleteMtpFile ($RootDirectory + "\" + $currentFolderItem.Name)
				}
			}
			$folderItems = $shell.NameSpace($RootDirectory).self.GetFolder.Items()
		}
	}

	if($folderItems.Count -eq 0)
	{
		$tmpLocation = $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
		mkdir -Path $tmpLocation > $null

		$targetToDelete = $shell.NameSpace($tmpLocation)
		if($targetToDelete -eq $null)
		{
			LogError Folder not found: [$tmpLocation]
			return
		}

		if($isRecursed)
		{
			$copiedTargetFile = $shell.NameSpace($RootDirectory).self.GetFolder
			LogInfo [(FormatFriendlyTransferPath $RootDirectory)] --> [X]

			$targetToDelete.MoveHere($copiedTargetFile)
			DeleteItem $tmpLocation

			$copiedTargetFile = $shell.NameSpace($RootDirectory).self.GetFolder
			if($copiedTargetFile -ne $null)
			{
				LogError Folder still present: [$targetToDelete]
			}
		}
	}
	else
	{
		foreach ($currentFolderItem in $folderItems)
		{
			if($currentFolderItem.IsFolder)
			{
				DeleteEmptyMtpDirectoryRecursively ($RootDirectory + "\" + $currentFolderItem.Name) $CopyPlaylists $true
			}
		}
	}

	return $items
}

###################################
#
# FUNCTION FormatFriendlyTransferPath
#
###################################
function FormatFriendlyTransferPath($source)
{
	if(($MTP_DEVICE_PATH -ne $null) -and ($MTP_DEVICE_NAME -ne $null))
	{
		$source = $source.Replace($MTP_DEVICE_PATH, $MTP_DEVICE_NAME)
	}
	return $source
}

###################################
#
# FUNCTION MtpFileExists
#
###################################
function MtpFileExists($destination)
{
	if(FailedStringValidation ($destination)) { return }

	$shell = New-Object -ComObject Shell.Application
	$fileName = [System.IO.Path]::GetFileName($destination)
	$targetFolder = $destination.Replace($fileName, "")

	$baseShell = $shell.NameSpace($targetFolder)
	if($baseShell -ne $null)
	{
		$copiedTargetFile = $shell.NameSpace($targetFolder).self.GetFolder.Items() | Where-Object { $_.Name -eq ($fileName) }
		if($copiedTargetFile -ne $null)
		{
			VerboseLogInfo Destination Already Exists: [$destination]
			return $true
		}
	}
	return $false
}

###################################
#
# FUNCTION TransferMtpFile
#
###################################
function TransferMtpFile($source, $destination)
{
	if(FailedStringValidation ($source)) { return }
	if(FailedStringValidation ($destination)) { return }

	if(MtpFileExists $destination)
	{
		return
	}

	$shell = New-Object -ComObject Shell.Application
	$sourceFileSystemInfo = Get-Item -LiteralPath $source
	$targetFolder = $destination.Replace($sourceFileSystemInfo.Name, "")

	CreateMtpDirectory($destination)

	$sourceItem = $shell.NameSpace($sourceFileSystemInfo.Directory.FullName).self.GetFolder().Items() | Where-Object { $_.Name -eq ($sourceFileSystemInfo.Name) }
	$shell.NameSpace($targetFolder).self.GetFolder().CopyHere($sourceItem)
	LogInfo [$source] --> [(FormatFriendlyTransferPath $destination)]

	$canContinue = $false
	while (-not $canContinue)
	{
		$baseShell = $shell.NameSpace($targetFolder)
		if($baseShell -ne $null)
		{
			$copiedTargetFile = $baseShell.self.GetFolder.Items() | Where-Object { $_.Name -eq ($sourceFileSystemInfo.Name) }
			if($copiedTargetFile -ne $null)
			{
				$canContinue = $true
			}
			else
			{
				SleepFor 5
			}
		}
	}
}

###################################
#
# FUNCTION CreateMtpDirectory
#
###################################
function CreateMtpDirectory($path)
{
	if(FailedStringValidation ($path)) { return }

	#Trim FileName
	$parentFolderNameOffset = $path.Trim('\').LastIndexOf('\')
	$parentFolderFullName = $path.Substring(0, $parentFolderNameOffset)

	$shell = New-Object -ComObject Shell.Application
	if(($shell.NameSpace($parentFolderFullName).self) -eq $null)
	{
		$originalPath = $parentFolderFullName
		$parentFolderNameOffset = $parentFolderFullName.Trim('\').LastIndexOf('\')
		$parentFolderFullName = $parentFolderFullName.Substring(0, $parentFolderNameOffset)
		$newFolderName = $originalPath.Replace($parentFolderFullName, "").Trim('\')
		if($shell.NameSpace($parentFolderFullName) -eq $null)
		{
			CreateMtpDirectory $originalPath
		}
		VerboseLogInfo Creating MTP Folder: [($parentFolderFullName + "\" + $newFolderName)]
		$shell.NameSpace($parentFolderFullName).NewFolder($newFolderName)
	}
}

###################################
#
# FUNCTION DeleteMtpFile
#
###################################
function DeleteMtpFile($targetToDelete)
{
	$targetParentFolder = $targetToDelete.Substring(0, $targetToDelete.LastIndexOf("\"))
	$targetToDeleteFileName = $targetToDelete.Substring($targetToDelete.LastIndexOf("\") + 1)
	$shell = New-Object -ComObject Shell.Application

	$baseShell = $shell.NameSpace($targetParentFolder)
	if($baseShell -eq $null)
	{
		VerboseLogInfo Folder not found: [$targetParentFolder]
		return
	}

	$copiedTargetFile = $shell.NameSpace($targetParentFolder).self.GetFolder.Items() | Where-Object { $_.Name -eq ($targetToDeleteFileName) }
	if($copiedTargetFile -eq $null)
	{
		VerboseLogInfo File not found: [$targetToDelete]
		return
	}

	$tmpLocation = $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
	mkdir -Path $tmpLocation > $null

	$targetToDelete = $shell.NameSpace($tmpLocation)
	if($targetToDelete -eq $null)
	{
		LogError Folder not found: [$tmpLocation]
		return
	}

	$targetToDelete.MoveHere($copiedTargetFile)
	DeleteItem $tmpLocation

	$copiedTargetFile = $shell.NameSpace($targetParentFolder).self.GetFolder.Items() | Where-Object { $_.Name -eq ($targetToDeleteFileName) }
	if($copiedTargetFile -ne $null)
	{
		LogError File still present: [$targetToDelete]
	}
}

###################################
#
# FUNCTION IsExcludedFromTransfer
#
###################################
function IsExcludedFromTransfer($sourceFile)
{
	return (IsPathInExclusionList $sourceFile $Exclude)
}

###################################
#
# FUNCTION TransferMediaDirectory
#
###################################
function TransferMediaDirectory($baseSourceDirectory, $sourceDirectory, $destinationDirectory, $transferExclusions, $copyPlaylists, $destinationIsMtpDevice)
{
	if(FailedStringValidation ($baseSourceDirectory)) { return }
	if(FailedStringValidation ($sourceDirectory)) { return }
	if(FailedStringValidation ($destinationDirectory)) { return }

	$baseSourceDirectory = (Get-Item -LiteralPath $baseSourceDirectory).FullName

	VerboseLogInfo TRANSFERRING: [$sourceDirectory]

	if(IsWatched $sourceDirectory)
	{
		VerboseLogInfo EXCLUDED PURGED or CONSUMED: [$sourceDirectory]
		return
	}

	# NOTE: For some reason Android's Phonograph App does not recognize .m3u8
	if($copyPlaylists)
	{
		$filesToTransfer = Get-ChildItem -LiteralPath ($sourceDirectory) | Where-Object { ($_.Extension -In ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS + ".m3u")) }
	}
	else
	{
		$filesToTransfer = Get-ChildItem -LiteralPath ($sourceDirectory) | Where-Object { ($_.Extension -In ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS)) }
	}

	if($filesToTransfer.Count -eq 0)
	{
		return
	}

	$cFileNumber = 0
	$transferMediaList = @{}
	foreach ($currentFile in $filesToTransfer)
	{
		if(IsExcludedFromTransfer $currentFile.FullName)
		{
			VerboseLogInfo EXCLUDED: [($currentFile.FullName)]
			continue
		}

		$toSort = $null
		$cFileNumber++

		$seasonResults = ParseSeason $currentFile
		$airDateResults = ExtractShowAirDate $currentFile

		if($currentFile.Name -match '^\s*(?<track>\d+)[\s|-]')
		{
			DebugLogInfo [($currentFile.Name)] [($matches["track"])]

			if(-not ([int]::TryParse(($matches["track"]), [ref] $toSort)))
			{
				LogError Invalid Track Parse: [($currentFile.Name)] [($matches["track"])]
				$toSort = $cFileNumber
			}
		}
		elseif(($seasonResults[0] -ne $null) -or ($seasonResults[2] -ne $null))
		{
			DebugLogInfo [($currentFile.Name)] [($seasonResults[0])] [($seasonResults[2])]

			$seasonNumber = 0
			if($seasonResults[0] -ne $null)
			{
				$seasonNumber = [int]::Parse($seasonResults[0])
			}
			$episodeNumber = 0
			if($seasonResults[2] -ne $null)
			{
				$episodeNumber = [int]::Parse($seasonResults[2])
			}
			$toSort = ($seasonNumber * 100 + $episodeNumber)
		}
		elseif($airDateResults[0] -ne $null)
		{
			DebugLogInfo [($currentFile.Name)] [($airDateResults[0])] [(($airDateResults[0]).Ticks)]
			$toSort = ($airDateResults[0]).Ticks
		}
		else
		{
			DebugLogInfo [($currentFile.Name)] [$cFileNumber]
			$toSort = $cFileNumber
		}

		while ($transferMediaList.ContainsKey($toSort))
		{
			$toSort++
		}

		DebugLogInfo [$toSort] [($currentFile.FullName)]
		$transferMediaList.Add($toSort, $currentFile.FullName) > $null
	}

	foreach ($currentMediaToTransfer in ($transferMediaList.Keys | Sort-Object))
	{
		$source = $transferMediaList[$currentMediaToTransfer]
		$preTranslatedSource = $source
		$preTranslatedFileName = [System.IO.Path]::GetFileName($preTranslatedSource)
		$source = TranslateMediaForTransfer $source
		$postTranslatedFileName = [System.IO.Path]::GetFileName($source)
		$destination = $preTranslatedSource.Replace($sourceDirectory, $destinationDirectory)
		$destination = $destination.Replace($preTranslatedFileName, $postTranslatedFileName)

		if(($destinationIsMtpDevice -and (MtpFileExists $destination)) -or (IsValidPath $destination))
		{
			DebugLogInfo File already transferred: [$destination]
		}
		else
		{
			TransferMediaFile $source $destination $destinationIsMtpDevice
		}

		if(($preTranslatedSource -ne $source) -and (IsUnderPath $source $CONVERSION_TMP))
		{
			if($DEBUG_MODE)
			{
				DeleteItem $source
			}
			else
			{
				PermanentlyDeleteItem $source
			}
		}
	}
}

####################################
#
# FUNCTION TranslateMediaForTransfer
#
####################################
function TranslateMediaForTransfer($source)
{
	if(-not $StripExtendedChars)
	{
		return $source
	}

	if(IsInvalidPath $source)
	{
		LogWarning Media not found, cannot translate: [$source]
		return $source
	}

	$originalName = (Get-Item -LiteralPath $source).Name
	$originalLocation = (Get-Item -LiteralPath $source).Directory.FullName
	$strippedName = $originalName | BeautifyAndDemoteString | ConvertStringToASCII | ConvertStringToFileSystem -ForceQuiet

	if($strippedName -ne $originalName)
	{
		$tmpLocation =  $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
		$newStrippedSource = $source.Replace($originalLocation, $tmpLocation)
		$newStrippedSource = $newStrippedSource.Replace($originalName, $strippedName)

		CreateDirInPath $newStrippedSource
		Copy-Item -LiteralPath $source -Destination $newStrippedSource -Force
	}
	else
	{
		return $source
	}

	return $newStrippedSource
}

###################################
#
# FUNCTION TransferMediaFile
#
###################################
function TransferMediaFile($source, $destination, $destinationIsMtpDevice)
{
	if(FailedStringValidation ($source)) { return }
	if(FailedStringValidation ($destination)) { return }

	$preTranslatedSource = $source
	$source = TranslateMediaForTransfer $source

	$destination = $destination.Replace('/', '\')
	if($destinationIsMtpDevice)
	{
		$destination = $destination.Trim("\")
	}
	else
	{
		$destination = $destination.Replace("\\", "\")
	}

	$start = Get-Date
	if($destinationIsMtpDevice)
	{
		CreateMtpDirectory $destination
		TransferMtpFile $source $destination
	}
	else
	{
		if(IsValidPath $destination)
		{
			DebugLogInfo File already has been transferred: [$destination]
			return
		}

		if($StripExtendedChars -and (IsUnderPath $source $CONVERSION_TMP))
		{
			#Check Metadata title
			$metaData = ExtractMetadataTags $source
			if(IsStringDefined ($metaData["title"]))
			{
				$strippedTitle = ($metaData["title"]) | BeautifyAndDemoteString
				if($metaData["title"] -ne $strippedTitle)
				{
					RefreshMetadata (Get-Item -LiteralPath $source) (ExtractShowName (Get-Item -LiteralPath $source)) ($strippedTitle)
				}
			}
		}

		CreateDirInPath $destination
		Copy-Item -LiteralPath $source -Destination $destination -Force
		LogInfo [$source] --> [(FormatFriendlyTransferPath $destination)]

		if(((Get-Date)-$start).TotalSeconds -le 1)
		{
			#Ensure timstamp spreading for legacy devices' ordering
			SleepFor 1
		}
	}

	if(($preTranslatedSource -ne $source) -and (IsUnderPath $source $CONVERSION_TMP))
	{
		DeleteItem $source
	}

	if((-not $IsMtpDevice) -and (IsInvalidPath $destination))
	{
		LogWarning Problem transferring file: [$destination]
	}
}

#######################################
#
# FUNCTION IsVariableDefinedInScope
#
#######################################
function IsVariableDefinedInScope($VariableName, $TargetScope)
{
	foreach ($currentVariable in $TargetScope)
	{
		if($currentVariable.Name -eq $VariableName)
		{
			return $true
		}
	}

	return $false
}

##########################################
#
# FUNCTION AnalyzeBlockForExecutionSuccess
#
##########################################
function AnalyzeBlockForExecutionSuccess($ScriptBlock, $ScopeVariables, $IsLastAttemptedEpisodeExtraction, $Subscription)
{
	if($WHATIF_MODE)
	{
		return $false
	}

	foreach ($currentScriptBlock in $ScriptBlock)
	{
		$scriptCommands = [RegEx]::Split($currentScriptBlock.ToString(), '\n')

		$validBlockCommands = New-Object -TypeName System.Collections.ArrayList
		foreach ($currentScriptBlockCommand in $scriptCommands)
		{
			if($currentScriptBlockCommand.Contains('#'))
			{
				$currentScriptBlockCommand = $currentScriptBlockCommand.Substring(0, $currentScriptBlockCommand.IndexOf('#'))
			}
			$currentScriptBlockCommand = $currentScriptBlockCommand.Trim()

			if(-not (IsStringDefined $currentScriptBlockCommand))
			{
				continue
			}

			$validBlockCommands.Add($currentScriptBlockCommand) > $null
		}

		$cCommands = 0
		foreach ($currentScriptBlockCommand in $validBlockCommands)
		{
			$cCommands++

			#DebugLogInfo ANALYZING BLOCK COMMAND [$cCommands] [$currentScriptBlockCommand]
			if(-not ($currentScriptBlockCommand.EndsWith(';')) -and ($cCommands -lt $validBlockCommands.Count))
			{
				LogWarning ScriptBlock command line not properly terminated with a semicolon. Skipping episode extraction...
				return $false
			}
		}
	}

	return $true
}

##########################################
#
# FUNCTION FlattenToDictionary
#
##########################################
function FlattenToDictionary($collection)
{
	$resultingDictionary = @{}

	if($collection -eq $null)
	{
		return $resultingDictionary
	}

	$isDictionary = $false
	$toEnumerate = $collection

	if($collection.GetType().ToString() -eq "System.Collections.Hashtable")
	{
		$isDictionary = $true
		$toEnumerate = $collection.Keys
	}

	foreach ($subItem in $toEnumerate)
	{
		if(($subItem.Count -ne $null) -and ($subItem.Count -gt 1))
		{
			$subCollection = FlattenToDictionary $subItem

			#SubCollection is always a dictionary
			if($subCollection.GetType().ToString() -ne "System.Collections.Hashtable")
			{
				LogError Problem with sub
				return
			}

			foreach ($currentSubSubItem in $subCollection.Keys)
			{
				if($currentSubSubItem.GetType().ToString() -ne "System.String")
				{
					LogWarning Cannot Flatten Item to dictionary: [($currentSubSubItem.GetType().ToString())]
					continue
				}

				if(($subCollection[$currentSubSubItem] -ne $null) -and ((($subCollection[$currentSubSubItem]).GetType().ToString() -ne "System.Management.Automation.ScriptBlock") -and (($subCollection[$currentSubSubItem]).GetType().ToString() -ne "System.Boolean")))
				{
					LogWarning Cannot Flatten Item to dictionary: [(($subCollection[$currentSubSubItem]).GetType().ToString())]
					continue
				}

				if($resultingDictionary.ContainsKey($currentSubSubItem.ToString()))
				{
					LogError Subscription has duplicate pattern defined: [($currentSubSubItem.ToString())]
					continue
				}

				$resultingDictionary.Add($currentSubSubItem.ToString(), $subCollection[$currentSubSubItem]) > $null
			}
		}
		else
		{
			$dictionaryValue = $null
			if($isDictionary)
			{
				$dictionaryValue = $collection[$subItem]
			}

			if($subItem.GetType().ToString() -ne "System.String")
			{
				LogWarning Cannot Flatten Item to dictionary: [($subItem.GetType().ToString())]
				continue
			}

			if(($dictionaryValue -ne $null) -and (($dictionaryValue.GetType().ToString() -ne "System.Management.Automation.ScriptBlock") -and ($dictionaryValue.GetType().ToString() -ne "System.Boolean")))
			{
				LogWarning Cannot Flatten Item to dictionary: [($dictionaryValue.GetType().ToString())]
				continue
			}

			if($resultingDictionary.ContainsKey($subItem.ToString()))
			{
				LogError Subscription has duplicate pattern defined: [($subItem.ToString())]
				continue
			}

			$resultingDictionary.Add($subItem.ToString(), $dictionaryValue) > $null
		}
	}
	return $resultingDictionary
}

############################################
#
# FUNCTION DisplayPlayOnSubscriptionProblems
#
############################################
function DisplayPlayOnSubscriptionProblems()
{
	if(-not $PLAYON_WARN_SUBSCRIPTION_PROBLEMS)
	{
		return
	}

	if(-not (IsPlayOnInstalled))
	{
		LogWarning PlayOn is not installed or configured.
		return
	}

	DeleteItem $RECORDING_PROBLEMS_CSV

	$block = "{ . " + $FUNCTION_SCRIPT + "; DisplayPlayOnSubscriptionProblemsInternal; ExtractCurrentPlayOnRecordingProblems }"

	#This needs to run in x86 due to SQL LITE dependency

	if(-not ([Environment]::Is64BitProcess))
	{
		ExecuteCommand $POWERSHELL_FILE "-command" $block
	}
	else
	{
		if(IsInvalidPath $POWERSHELL_32_FILE)
		{
			LogError "PowerShell executable not detected ['$POWERSHELL_32_FILE']."
			return
		}

		ExecuteCommand $POWERSHELL_32_FILE "-command" $block
	}
}

####################################################
#
# FUNCTION DisplayPlayOnSubscriptionProblemsInternal
#
####################################################
function DisplayPlayOnSubscriptionProblemsInternal()
{
	if(IsInvalidPath $SQL_LITE)
	{
		LogError SQL Lite not found [$SQL_LITE]. Make sure PlayOn is installed.
		return
	}

	if(IsInvalidPath $RECORDING_DATABASE)
	{
		LogError Recording Database not found [$RECORDING_DATABASE]. Make sure PlayOn is installed.
		return
	}

	Add-Type -LiteralPath $SQL_LITE

	$conn = New-Object -TypeName System.Data.SQLite.SQLiteConnection
	$connectionString = "Data Source=" + $RECORDING_DATABASE
	$conn.ConnectionString = $connectionString
	$conn.Open()

	$command = New-Object -TypeName System.Data.SQLite.SQLiteCommand
	$command.Connection = $conn

	#STATUS 0 is Succeeded
	#STATUS 1 is Error

	$command.CommandText = "SELECT * FROM subscriptions WHERE (status > 0)"
	$reader = $command.ExecuteReader()
	if(-not ($reader.HasRows))
	{
		VerboseLogInfo No PlayOn recordings extracted.
	}

	while ($reader.Read())
	{
		$name = $reader["Name"]
		$status = $reader["Status"]
		$lastUpdated = $reader["Updated"]
		$lastChecked = $reader["Checked"]

		$tempDate = [DateTime]::MinValue
		if(-not ([DateTime]::TryParse($lastUpdated, [ref] $tempDate)))
		{
			$lastUpdated = $null
		}
		else
		{
			$lastUpdated = $tempDate
		}

		$tempDate = [DateTime]::MinValue
		if(-not ([DateTime]::TryParse($lastChecked, [ref] $tempDate)))
		{
			$lastChecked = $null
		}
		else
		{
			$lastChecked = $tempDate
		}

		LogWarning SUBSCRIPTION ISSUE: [$name] [Status: $status] [Updated: $lastUpdated] [Checked: $lastChecked]
		DisplaySubscriptionProblemNotification $name
	}
}

############################################
#
# FUNCTION LoadSubscriptionOverrideVariable
#
############################################
function LoadSubscriptionOverrideVariable([System.IO.FileSystemInfo] $show, $variableName)
{
	if(FailedStringValidation ($variableName)) { return }

	if($SERIES_SUBSCRIPTION_MAPPING -eq $null)
	{
		$tmpDictionary = @{}
		$tmpDictionary.Add("*", $SUBSCRIPTION_INSTALL_PATH + "\defaults.ps1")

		foreach ($currentSubscription in (Get-ChildItem -LiteralPath $SUBSCRIPTION_INSTALL_PATH -Filter "*.ps1"))
		{
			DebugLogInfo Checking Subscription: [$currentSubscription]

			. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
			. ($currentSubscription.FullName)

			foreach ($currentSubscriptionName in $SUBSCRIPTION_NAME)
			{
				if(($currentSubscriptionName -eq $null) -or ($currentSubscriptionName.Length -le 0))
				{
					continue
				}

				if($currentSubscriptionName.Contains(":"))
				{
					$currentSubscriptionName = $currentSubscriptionName.Replace(":", "_")
				}

				if($tmpDictionary.ContainsKey($currentSubscriptionName))
				{
					LogWarning Duplicate '[$SUBSCRIPTION_NAME]' defined: [$currentSubscriptionName] [($currentSubscription.FullName)]
					continue
				}
				$tmpDictionary.Add($currentSubscriptionName, $currentSubscription.FullName)
				DebugLogInfo Adding Subscription Cache: [($currentSubscriptionName)] [($currentSubscription.FullName)]
				$Script:SERIES_SUBSCRIPTION_MAPPING = $tmpDictionary
			}
		}

		# RESET STATE
		if(IsVariableDefinedInScope $variableName (Get-Variable -Scope 0))
		{
			if($WHATIF_MODE)
			{
				(Get-Variable -Name $variableName).Value = $null
			}
			else
			{
				Remove-Variable -Name $variableName -Scope 0
			}
		}
	}

	$tmpSeries = ExtractShowName $show
	if((($tmpSeries -ne $null) -or ($tmpSeries.Length -gt 0)) -and (-not $SERIES_SUBSCRIPTION_MAPPING.ContainsKey($tmpSeries)))
	{
		#Attempt to match series via TVDB network
		$tvdbSeries = LoadTvDBDictionary
		if($tvdbSeries.ContainsKey($tmpSeries))
		{
			$tvDbId = $tvdbSeries[$tmpSeries]
			if($tvDbId -ne 0)
			{
				$seriesUri = [String]::Format("{0}/series/{1}/extended", $TVDB_SERVER, $tvDbId)
				$cacheFile = GetTvDbSeriesCacheFile $tvDbId
				#This can banner spam
				$oldAckStatus = $ACKNOWLEDGED_TVDB
				$Script:ACKNOWLEDGED_TVDB = $true
				if(IsInvalidPath $cacheFile)
				{
					RefreshTvDbCacheFile $cacheFile $seriesUri $show $tvDbId
				}
				$seriesInfo = Get-Content -LiteralPath $cacheFile -Encoding UTF8 | ConvertFrom-Json
				$mappedSubscriptionFile = $null
				if(-not (IsStringDefined ($seriesInfo.data.latestNetwork.name)))
				{
					DeleteItem $cacheFile
					RefreshTvDbCacheFile $cacheFile $seriesUri $show $tvDbId
					$seriesInfo = Get-Content -LiteralPath $cacheFile -Encoding UTF8 | ConvertFrom-Json
				}
				$Script:ACKNOWLEDGED_TVDB = $oldAckStatus

				if(($seriesInfo.data.Count -eq 1) -and (IsStringDefined ($seriesInfo.data.latestNetwork.name)))
				{
					$lastNetworkName = $seriesInfo.data.latestNetwork.name
					if($TVDB_NETWORK_SUBSCRIPTION_MAPPING.Contains($lastNetworkName))
					{
						$mappedSubscriptionFile = LoadPath ($TVDB_NETWORK_SUBSCRIPTION_MAPPING[$lastNetworkName])

						$tmpDictionary = $Script:SERIES_SUBSCRIPTION_MAPPING
						$tmpDictionary.Add($tmpSeries, $mappedSubscriptionFile)
						DebugLogInfo Adding Subscription Cache: [($tmpSeries)] [($mappedSubscriptionFile)]
						$Script:SERIES_SUBSCRIPTION_MAPPING = $tmpDictionary
					}
				}
				elseif($seriesInfo.data.companies -ne $null)
				{
					foreach($currentCompany in $seriesInfo.data.companies)
					{
						if((IsStringDefined $currentCompany.name) -and ($currentCompany.name -ne 'Syndication'))
						{
							if($TVDB_NETWORK_SUBSCRIPTION_MAPPING.Contains($currentCompany.name))
							{
								$mappedSubscriptionFile = LoadPath ($TVDB_NETWORK_SUBSCRIPTION_MAPPING[$currentCompany.name])

								$tmpDictionary = $Script:SERIES_SUBSCRIPTION_MAPPING
								$tmpDictionary.Add($tmpSeries, $mappedSubscriptionFile)
								DebugLogInfo Adding Subscription Cache: [($tmpSeries)] [($mappedSubscriptionFile)]
								$Script:SERIES_SUBSCRIPTION_MAPPING = $tmpDictionary

								break
							}
						}
					}
				}
				else
				{
					$tmpDictionary = $Script:SERIES_SUBSCRIPTION_MAPPING
					$tmpDictionary.Add($tmpSeries, $null)
					DebugLogInfo Adding Subscription Cache: [($tmpSeries)] [($mappedSubscriptionFile)]
					$Script:SERIES_SUBSCRIPTION_MAPPING = $tmpDictionary
				}
			}
		}
	}

	if(($tmpSeries -eq $null) -or ($tmpSeries.Length -le 0) -or (-not $SERIES_SUBSCRIPTION_MAPPING.ContainsKey($tmpSeries)))
	{
		VerboseLogInfo Source [($show.FullName)] has no mapped series name. Using Defaults...
		$tmpSeries = "*"
	}

	$extractedVariable = $null
	if(($SERIES_SUBSCRIPTION_MAPPING -ne $null) -and ($SERIES_SUBSCRIPTION_MAPPING.ContainsKey($tmpSeries)))
	{
		$mappedSubscriptionFile = $SERIES_SUBSCRIPTION_MAPPING[$tmpSeries]
		if($mappedSubscriptionFile -eq $null)
		{
			return $null
		}

		. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
		. $mappedSubscriptionFile

		VerboseLogInfo MATCHED SHOW TO SUBSCRIPTION: [($show.FullName)] --> [($mappedSubscriptionFile)]

		if($WHATIF_MODE)
		{
			if(IsVariableDefinedInScope $variableName (Get-Variable -Scope 0))
			{
				if((Get-Variable -Name $variableName -Scope 0).Value -eq $null)
				{
					return $null
				}
				$extractedVariable = (Get-Variable -Scope 0 -Name $variableName).Value
			}
		}
		elseif(-not (IsVariableDefinedInScope $variableName (Get-Variable -Scope 0)))
		{
			return $null
		}
		else
		{
			$extractedVariable = (Get-Variable -Scope 0 -Name $variableName).Value
		}
		VerboseLogInfo RETURNING MATCHED SUBSCRIPTION VAR: [($variableName)] --> [($extractedVariable)]

		if((-not $WHATIF_MODE) -and ($extractedVariable -eq $null))
		{
			LogWarning Override Variable [($variableName)] in subscription [($mappedSubscriptionFile)] is explicitly set to null, which is indistinguishable from not being set at all.
		}

		return $extractedVariable
	}

	LogError No mapped subscription found for media: [($show.FullName)]
}

##########################################
#
# FUNCTION SplitJson
#
##########################################
function SplitJson($results)
{
	if(FailedStringValidation ($results)) { return }

	$splitPatterns = New-Object -TypeName System.Collections.ArrayList
	$splitMatches = [RegEx]::Matches($results, '(?<replaceTarget>,\s*")')
	foreach ($currentSplitMatch in $splitMatches)
	{
		if((-not $currentSplitMatch.Success) -or ($splitPatterns.Contains($currentSplitMatch.Value)))
		{
			continue
		}

		$splitPatterns.Add($currentSplitMatch.Value) > $null
		$targetSplitPattern = $currentSplitMatch.Value.Replace($currentSplitMatch.Groups["replaceTarget"].Value, (',' + [Environment]::NewLine + '"'))
		$results = $results.Replace(($currentSplitMatch.Value), $targetSplitPattern)
	}
	return $results
}

##########################################
#
# FUNCTION SplitXmlHtml
#
##########################################
function SplitXmlHtml($results)
{
	if(FailedStringValidation ($results)) { return }

	$splitPatterns = New-Object -TypeName System.Collections.ArrayList
	$splitMatches = [RegEx]::Matches($results, '(?<replaceTarget>>\s*<)')
	foreach ($currentSplitMatch in $splitMatches)
	{
		if((-not $currentSplitMatch.Success) -or ($splitPatterns.Contains($currentSplitMatch.Value)))
		{
			continue
		}

		$splitPatterns.Add($currentSplitMatch.Value) > $null
		$targetSplitPattern = $currentSplitMatch.Value.Replace($currentSplitMatch.Groups["replaceTarget"].Value, ('>' + [Environment]::NewLine + '<'))
		$results = $results.Replace(($currentSplitMatch.Value), $targetSplitPattern)
	}
	return $results
}

##########################################
#
# FUNCTION PrepareSubscriptionUriLines
#
##########################################
function PrepareSubscriptionUriLines($splitSource, $results)
{
	$realResults = [System.Text.StringBuilder]::new()
	foreach($currentResults in $results)
	{
		$realResults.AppendLine($currentResults.ToString()) > $null
	}
	$results = $realResults.ToString().Trim()

	if(-not $splitSource)
	{
		VerboseLogInfo TOTAL LINES: [($results.Count)]
		return $results
	}

	$resultLines = [RegEx]::Split($results, '\n')
	$beforeParseLines = $resultLines.Count

	$splitJSON = $false
	if($results[0] -eq '{')
	{
		#####
		#JSON
		#####

		$results = SplitJson $results
		$splitJSON = $true
	}
	elseif($results[0] -eq '<')
	{
		#################
		#HTML / XML / RSS
		#Accomodate <!CDATA[]> to not split from parent tags
		#################

		$splitPatterns = New-Object -TypeName System.Collections.ArrayList
		$splitMatches = [RegEx]::Matches($results, "[^\]](?<replaceTarget>>\s*<)[^!]")
		foreach ($currentSplitMatch in $splitMatches)
		{
			if($splitPatterns.Contains($currentSplitMatch.Value))
			{
				continue
			}

			$splitPatterns.Add($currentSplitMatch.Value) > $null
			$targetSplitPattern = $currentSplitMatch.Value.Replace($currentSplitMatch.Groups["replaceTarget"].Value, ('>' + [Environment]::NewLine + '<'))
			$results = $results.Replace(($currentSplitMatch.Value), $targetSplitPattern)
			#DebugLogInfo [($currentSplitMatch.Value)] --> [$targetSplitPattern]
		}
	}

	$resultLines = [RegEx]::Split($results, '\n')

	$newResultLines = New-Object -TypeName System.Collections.ArrayList
	foreach ($currentLine in $resultLines)
	{
		if(($currentLine.Length -le $SPLIT_LINE_THRESHOLD) -or ($splitJSON))
		{
			$newResultLines.Add($currentLine.Trim()) > $null
		}
		else
		{
			#This is likely embedded JSON in HTML and has not been split already.
			$splittedLine = [RegEx]::Split((SplitJson $currentLine), '\n')
			foreach ($currentSubLine in $splittedLine)
			{
				if($currentSubLine.Length -eq $currentLine.Length)
				{
					break
				}
				$newResultLines.Add($currentSubLine.Trim()) > $null
			}

			$splittedLine = [RegEx]::Split((SplitXmlHtml $currentLine), '\n')
			foreach ($currentSubLine in $splittedLine)
			{
				$newResultLines.Add($currentSubLine.Trim()) > $null
			}
		}
	}

	VerboseLogInfo SPLIT SOURCE LINES [($beforeParseLines)] --> [($newResultLines.Count)]
	return $newResultLines
}

##########################################
#
# FUNCTION CheckMediaSubscription
#
##########################################
function CheckMediaSubscription($Subscription)
{
	CheckMediaSubscription $Subscription $null
}

##########################################
#
# FUNCTION CheckMediaSubscription
#
##########################################
function CheckMediaSubscription($Subscription, $redirectionUri)
{
	if(FailedStringValidation ($Subscription)) { return }

	VerboseLogInfo Checking Subscription: [($Subscription)]

	#Twice to include $baselineScopeVars
	$baselineScopeVars = Get-Variable -Scope 0
	$baselineScopeVars = Get-Variable -Scope 0
	$subscriptionHistoryCsv = $SUBSCRIPTION_TMP + "\" + (Get-Item $Subscription).BaseName + ".csv"
	$lastSubAcquire = [System.DateTime]::MinValue
	$epPubDateUtilized = $false
	if(IsValidPath $subscriptionHistoryCsv)
	{
		$lastSubAcquire = (Get-Item -LiteralPath $subscriptionHistoryCsv).LastWriteTime
		#Topical shows often publish shortly after air date and claim that it happened in the past. So, let's
		# always check subscriptions for new content published at least in the last 24 hours.
		$lastSubAcquire = $lastSubAcquire.AddDays(-1)
	}

	#CLEAR/LOAD SUBSCRIPTION
	. $SUBSCRIPTION_INSTALL_PATH\defaults.ps1
	. $Subscription

	if(-not ($SUBSCRIPTION_POLLABLE -or $Force -or (IsStringDefined $redirectionUri)))
	{
		VerboseLogInfo Subscription is not pollable. Please use the 'Force' parameter if you really want to do this. [([System.IO.Path]::GetFileName($Subscription))]
		return
	}

	if($Subscription -match "\\defaults.ps1")
	{
		LogError You can not use the defaults subscription for checking new subscriptions.
		return
	}

	HandleSubscriptionNameList

	if(IsStringDefined $redirectionUri)
	{
		$SUBSCRIPTION_URI = $redirectionUri
	}

	if($SUBSCRIPTION_URI.Count -le 0)
	{
		LogError No Subscription URI defined or set: [([System.IO.Path]::GetFileName($Subscription))]
		return
	}

	if($SUBSCRIPTION_ACQUIRE_ACTION -eq $null)
	{
		LogError No Subscription postprocessing action defined or set: [([System.IO.Path]::GetFileName($Subscription))]
		return
	}

	if($DEBUG_MODE)
	{
		$SUBSCRIPTION_DIAGNOSTICS = $true
	}

	if(($SUBSCRIPTION_EXTRACTORS -eq $null) -or $SUBSCRIPTION_EXTRACTORS.Count -eq 0)
	{
		LogError Subscription does not have a extractor set defined.
		return
	}
	$extractorPatterns = FlattenToDictionary $SUBSCRIPTION_EXTRACTORS
	$matchedExtractorPatterns = New-Object -TypeName System.Collections.ArrayList

	ShowSubscriptionParameters (Get-Variable -Scope 0)

	#########################
	#  INLINE LOGIC:	#
	#########################

	#APPLY EXTRACTORS TO A LINE IN THIS SCOPE
	$ExtractMatchesScriptBlock = {
		$matchesDictionary = @{}

		foreach ($currentExtractor in $extractorPatterns.Keys)
		{
			if(-not (IsStringDefined $currentExtractor))
			{
				LogError Subscription has invalid extractor defined: [$currentExtractor]
				continue
			}

			if($currentLine -notmatch $currentExtractor)
			{
				continue
			}

			if(-not $matchedExtractorPatterns.Contains($currentExtractor))
			{
				$matchedExtractorPatterns.Add($currentExtractor) > $null
			}

			$extractorHasConditions = $true
			$extractorCondition = $extractorPatterns[$currentExtractor]
			if($extractorCondition -eq $null)
			{
				$extractorHasConditions = $false
			}

			if($extractorHasConditions -and ($extractorCondition.GetType().ToString() -ne "System.Management.Automation.ScriptBlock"))
			{
				LogWarning EXTRACTOR CONDITION NOT DEFINED CORRECTLY: [($extractorCondition.ToString())]
				continue
			}

			if($extractorHasConditions)
			{
				$beforeVarState = $null
				$EXTRACTOR_MATCH_OVERRIDE = $null
				$beforeVarState = Get-Variable -Scope 0

				try
				{
					$extractorConditionResult = Invoke-Command -NoNewScope -ScriptBlock $extractorCondition
				}
				catch
				{
					LogError "Error while executing block in " $Subscription ":"
					LogError Block was: [($extractorCondition)]
					LogError Error was: [($_.ToString())]
					return
				}

				if($beforeVarState -ne $null)
				{
					$afterVarState = Get-Variable -Scope 0
					$beforeVarStateDict = @{}
					foreach ($currentlyDefinedVariable in $beforeVarState)
					{
						$beforeVarStateDict.Add($currentlyDefinedVariable.Name, $currentlyDefinedVariable.Value)
					}

					foreach ($currentlyDefinedVariable in $afterVarState)
					{
						if(($currentlyDefinedVariable.Name.ToString() -eq "foreach") -or ($currentlyDefinedVariable.Name -eq "extractorConditionResult"))
						{
							continue
						}

						if($currentlyDefinedVariable.Value -ne $null)
						{
							if(($currentlyDefinedVariable.Value.Count -gt 1) -or ($currentlyDefinedVariable.Value.GetType().ToString() -eq "System.Collections.ArrayList") -or $currentlyDefinedVariable.Value.GetType().ToString().Contains("Collection") -or $currentlyDefinedVariable.Value.GetType().ToString().Contains("Enumerator"))
							{
								continue
							}
						}

						if($beforeVarStateDict.ContainsKey($currentlyDefinedVariable.Name))
						{
							if($beforeVarStateDict[$currentlyDefinedVariable.Name] -ne $currentlyDefinedVariable.Value)
							{
								if($currentlyDefinedVariable.Value -eq $null)
								{
									DebugLogInfo ([String]::Format("REMOVE CONDITIONAL VARIABLE FROM SCOPE [L: {0}] [{1}] --> [{2}]", $cLineNumber, $currentlyDefinedVariable.Name, $currentlyDefinedVariable.Value))
									Remove-Variable -Name ($currentlyDefinedVariable.Name) -Scope 0
									if($variablesToRemoveOnReset.Contains($currentlyDefinedVariable.Name))
									{
										$variablesToRemoveOnReset.Remove($currentlyDefinedVariable.Name) > $null
									}
								}
								else
								{
									DebugLogInfo ([String]::Format("VARIABLE MODIFIED IN SCOPE [L: {0}] [{1}] --> [{2}]", $cLineNumber, $currentlyDefinedVariable.Name, $currentlyDefinedVariable.Value))
								}
							}
						}
						else
						{
							DebugLogInfo ([String]::Format("ADD CONDITIONAL VARIABLE TO SCOPE [L: {0}] [{1}] --> [{2}]", $cLineNumber, $currentlyDefinedVariable.Name, $currentlyDefinedVariable.Value))
							$matchName = $currentlyDefinedVariable.Name
							$matchValue = $currentlyDefinedVariable.Value
							Invoke-Command -NoNewScope -ScriptBlock $SaveMatchInScopeScriptBlock
						}
					}
				}

				if((-not $extractorConditionResult) -and ($EXTRACTOR_MATCH_OVERRIDE -eq $null))
				{
					DebugLogInfo EXTRACTOR CONDITION UNMET: [L: $cLineNumber] [($extractorCondition.ToString())] [SKIPPING: ($currentExtractor)]
					continue
				}
			}

			if($EXTRACTOR_MATCH_OVERRIDE -ne $null)
			{
				DebugLogInfo MATCH OVERRIDE: [$currentLine] --> [$extractorCondition]
				$originalMatch = $currentSubscriptionMatches
				$currentSubscriptionMatches = $EXTRACTOR_MATCH_OVERRIDE
			}
			else
			{
				$originalMatch = $null
				$currentSubscriptionMatches = [RegEx]::Matches($currentLine, $currentExtractor, [System.Text.RegularExpressions.RegExOptions]::ExplicitCapture)
			}

			foreach ($currentSubscriptionMatch in $currentSubscriptionMatches)
			{
				foreach ($currentSubscriptionMatchGroup in $currentSubscriptionMatch.Groups)
				{
					if([Int]::TryParse($currentSubscriptionMatchGroup.Name, [ref] $null))
					{
						#Implicit, not captured groups, do not save as variables
						continue
					}

					if(-not $currentSubscriptionMatchGroup.Success)
					{
						LogWarning UNMATCHED GROUP DETECTED: [([System.IO.Path]::GetFileName($Subscription))] [L: $cLineNumber] [($currentSubscriptionMatchGroup.Name)]
						continue
					}

					if(($originalMatch -ne $null) -and ($originalMatch.Groups.Count -ne 0))
					{
						$matchIndex = $originalMatch.Groups[0].Index
					}
					else
					{
						$matchIndex = $currentSubscriptionMatchGroup.Index
					}

					#DebugLogInfo MATCH [I: ($matchIndex)] [($currentSubscriptionMatchGroup.Name)] [($currentSubscriptionMatchGroup.Value)]

					if(-not ($matchesDictionary.ContainsKey($matchIndex)))
					{
						$matchesDictionary.Add($matchIndex, (New-Object -TypeName System.Collections.ArrayList))
					}
					($matchesDictionary[($matchIndex)]).Add($currentSubscriptionMatchGroup) > $null
				}
			}
		}

		#Sort
		$matchResults = New-Object -TypeName System.Collections.ArrayList
		foreach ($currentMatchResultIndex in ($matchesDictionary.Keys | Sort-Object))
		{
			foreach ($currentMatchResultIndexEntryInstance in ($matchesDictionary[$currentMatchResultIndex]))
			{
				$matchResults.Add($currentMatchResultIndexEntryInstance) > $null
			}
		}

		$matchesDictionary = $null
	} #END ExtractMatchesScriptBlock

	# REMOVE EPISODE STATE VARIABLES IN THIS SCOPE
	$RemoveEpisodeStateInScopeScriptBlock =	{
		if(-not $WHATIF_MODE)
		{
			foreach ($addedVariable in $variablesToRemoveOnReset)
			{
				if(-not (IsVariableDefinedInScope $addedVariable  (Get-Variable -Scope 0)))
				{
					continue
				}

				Remove-Variable -Name $addedVariable -Scope 0
			}
		}
		InitializeVariablesToRemove $variablesToRemoveOnReset
	} #END RemoveEpisodeStateInScopeScriptBlock

	# SAVE SUBSCRIPTION MATCH IN THIS SCOPE
	$SaveMatchInScopeScriptBlock =	{
		if((IsStringDefined $matchName) -and (-not $WHATIF_MODE))
		{
			DebugLogInfo ([String]::Format("ADD VARIABLE TO SCOPE [L: {0}] [{1}] --> [{2}]", $cLineNumber, $matchName, $matchValue))

			Set-Variable -Name ($matchName.Trim()) -Value ($matchValue.Trim()) -Scope 0
			if(-not $variablesToRemoveOnReset.Contains($matchName))
			{
				$variablesToRemoveOnReset.Add($matchName) > $null
			}
		}
	} #END SaveMatchInScopeScriptBlock

	# EXTRACT EPISODE FROM SUBSCRIPTION IN THIS SCOPE
	$ExtractEpisodeScriptBlock =	{
		#We saw this extraction before, we might be done for an episode
		$savedErrors = $null
		$canAttemptExtraction = AnalyzeBlockForExecutionSuccess $SUBSCRIPTION_ACQUIRE_ACTION (Get-Variable -Scope 0) $isLastAttemptedEpisodeExtraction $Subscription

		#NOTE: See definition of InitializeVariablesToRemove for default array size
		if($isLastAttemptedEpisodeExtraction -and ($variablesToRemoveOnReset.Count -le $cInitalVariableState))
		{
			VerboseLogInfo ([String]::Format("No Last Episode from Subscription: [{0}] [L: {1}]", ([System.IO.Path]::GetFileName($Subscription)), $cLineNumber))
			$canAttemptExtraction = $false
		}
		elseif((-not $canAttemptExtraction) -and (-not $isLastAttemptedEpisodeExtraction))
		{
			VerboseLogInfo ([String]::Format("Problem Extracting Episode from Subscription: [{0}] [L: {1}]", ([System.IO.Path]::GetFileName($Subscription)), $cLineNumber))
		}

		if(-not ($canAttemptExtraction -or $isLastAttemptedEpisodeExtraction))
		{
			Invoke-Command -NoNewScope -ScriptBlock $SaveMatchInScopeScriptBlock
		}
		elseif($canAttemptExtraction)
		{
			if($Error -ne $null)
			{
				$Error.Clear()
			}
			foreach ($currentScriptBlock in $SUBSCRIPTION_ACQUIRE_ACTION)
			{
				if($DEBUG_MODE)
				{
					DebugLogInfo EXECUTING: [$currentScriptBlock]
					Invoke-Command -NoNewScope -ScriptBlock $currentScriptBlock -ErrorVariable $blockError
				}
				else
				{
					Invoke-Command -NoNewScope -ScriptBlock $currentScriptBlock -ErrorVariable $blockError > $null
				}
			}
			$savedErrors = $blockError

			if(IsStringDefined $EPISODE_PUBLISH_DATE)
			{
				$currentEpisodePublishedDate = ParseAirDate $EPISODE_PUBLISH_DATE
				$epPubDateUtilized = $true
			}

			if($EPISODE_EXCLUSION)
			{
				$cEpisodesMatched++
				if(IsStringDefined($EPISODE_CONTENT_RENAME_TO))
				{
					VerboseLogInfo ([String]::Format("EXCLUDING EPISODE: [{0}] [{1}]", $EPISODE_CONTENT_URI, $EPISODE_CONTENT_RENAME_TO))
				}
				else
				{
					VerboseLogInfo ([String]::Format("EXCLUDING EPISODE: [{0}]", $EPISODE_CONTENT_URI))
				}
			}
			elseif((-not $Force) -and (IsStringDefined $EPISODE_PUBLISH_DATE) -and ($lastSubAcquire -gt $currentEpisodePublishedDate))
			{
				VerboseLogInfo Current episode published date [$currentEpisodePublishedDate] is before last time this subscription found content [$lastSubAcquire]. Stopping...
				$abortSubscription = $true
				return
			}
			elseif(-not ((IsStringDefined $EPISODE_CONTENT_URI) -or (IsStringDefined $EPISODE_REDIRECT_URI)))
			{
				LogWarning ([String]::Format("Not enough information to extract episode: [{0}] [L: {1}]", $Subscription, $cLineNumber))
			}
			else
			{
				$cEpisodesMatched++

				if(IsStringDefined($EPISODE_CONTENT_RENAME_TO))
				{
					if(-not ($EPISODE_CONTENT_RENAME_TO.StartsWith($SUBSCRIPTION_NAME)) -and (IsStringDefined($SUBSCRIPTION_NAME)))
					{
						$tmpSeriesName = $SUBSCRIPTION_NAME.Trim() | BeautifyAndDemoteString | ConvertStringToASCII | ConvertStringToFileSystem -ForceQuiet
						$EPISODE_CONTENT_RENAME_TO = $tmpSeriesName + " - " + $EPISODE_CONTENT_RENAME_TO.Trim()
					}

					$EPISODE_CONTENT_RENAME_TO = $EPISODE_CONTENT_RENAME_TO.Trim()
					$EPISODE_CONTENT_RENAME_TO = [System.Net.WebUtility]::HtmlDecode($EPISODE_CONTENT_RENAME_TO)

					if($EPISODE_CONTENT_RENAME_TO.Contains("\U") -or $EPISODE_CONTENT_RENAME_TO.Contains("\u"))
					{
						$EPISODE_CONTENT_RENAME_TO = [Regex]::Replace($EPISODE_CONTENT_RENAME_TO, "\\[Uu]([0-9A-Fa-f]{4})", {[char]::ToString([Convert]::ToInt32($args[0].Groups[1].Value, 16))})
					}

					if($EPISODE_CONTENT_RENAME_TO.Contains('\'))
					{
						$EPISODE_CONTENT_RENAME_TO = [RegEx]::Unescape($EPISODE_CONTENT_RENAME_TO)
					}

					$EPISODE_CONTENT_RENAME_TO = $EPISODE_CONTENT_RENAME_TO.Replace("  ", " ")

					VerboseLogInfo ([String]::Format("RENAME EPISODE TO: [{0}]", $EPISODE_CONTENT_RENAME_TO))
				}

				if(IsStringDefined($EPISODE_CONTENT_URI))
				{
					if($EPISODE_CONTENT_URI.Contains('\'))
					{
						$EPISODE_CONTENT_URI = [RegEx]::Unescape($EPISODE_CONTENT_URI);
					}

					if(-not (IsStringDefined $EPISODE_CONTENT_ID))
					{
						$EPISODE_CONTENT_ID = $EPISODE_CONTENT_URI
					}

					if($seenEpisodes.Contains($EPISODE_CONTENT_ID.ToLower()))
					{
						VerboseLogInfo ([String]::Format("ALREADY ACQUIRED EPISODE: [{0}] [{1}]", $EPISODE_CONTENT_URI, $EPISODE_CONTENT_ID))
					}
					else
					{
						VerboseLogInfo ([String]::Format("ACQUIRE EPISODE: [{0}] [{1}]", $EPISODE_CONTENT_URI, $EPISODE_CONTENT_ID))

						$csvEntry = New-Object -TypeName PSObject -Property @{	"TimeStamp" = (get-date);
							"Title"                                                           = $EPISODE_CONTENT_RENAME_TO;
							"Content"                                                         = $EPISODE_CONTENT_URI;
							"Id"                                                              = $EPISODE_CONTENT_ID;
						}

						if($BuildCache)
						{
							LogInfo Adding Subscription Entry [([System.IO.Path]::GetFileName($Subscription))] [$EPISODE_CONTENT_URI]
							AddCsvEntry $csvEntry $subscriptionHistoryCsv
							$seenEpisodes.Add($EPISODE_CONTENT_ID.ToLower()) > $null
						}
						else
						{
							$acquireResult = AcquireMediaContent $Subscription $EPISODE_CONTENT_URI $EPISODE_CONTENT_RENAME_TO
							if($acquireResult -or $Force)
							{
								if($acquireResult)
								{
									$cEpisodesExtracted++
								}
								VerboseLogInfo Adding Subscription Entry [([System.IO.Path]::GetFileName($Subscription))] [$EPISODE_CONTENT_URI]
								AddCsvEntry $csvEntry $subscriptionHistoryCsv
							}
							else
							{
								VerboseLogInfo Skipping Subscription Entry [([System.IO.Path]::GetFileName($Subscription))] [$acquireResult]
							}
							$seenEpisodes.Add($EPISODE_CONTENT_ID.ToLower()) > $null
						}
					}
				}

				if(IsStringDefined($EPISODE_REDIRECT_URI))
				{
					if(-not (IsStringDefined $SUBSCRIPTION_REDIRECT))
					{
						LogWarning ([String]::Format("No redirection subscription defined [{0}]. Can not redirect to: [{1}]", ([System.IO.Path]::GetFileName($Subscription)), $EPISODE_REDIRECT_URI))
					}
					elseif(IsInvalidPath $SUBSCRIPTION_REDIRECT)
					{
						LogWarning ([String]::Format("Invalid redirection subscription [{0}] [{1}]. Can not redirect to: [{2}]", ([System.IO.Path]::GetFileName($Subscription)), $SUBSCRIPTION_REDIRECT, $EPISODE_REDIRECT_URI))
					}
					else
					{
						VerboseLogInfo ([String]::Format("SUBSCRIPTION REDIRECT: [{0}] --> [{1}]", $EPISODE_REDIRECT_URI, $SUBSCRIPTION_REDIRECT))

						#Why is this needed?
						$tmpUri = $EPISODE_REDIRECT_URI
						Invoke-Command -NoNewScope -ScriptBlock $RemoveEpisodeStateInScopeScriptBlock
						CheckMediaSubscription $SUBSCRIPTION_REDIRECT $tmpUri
					}
				}
			}

			Invoke-Command -NoNewScope -ScriptBlock $RemoveEpisodeStateInScopeScriptBlock

			Invoke-Command -NoNewScope -ScriptBlock $SaveMatchInScopeScriptBlock

			if( -not ((IsStringDefined($EPISODE_REDIRECT_URI)) -or (IsStringDefined($EPISODE_CONTENT_URI)) -or (IsStringDefined($EPISODE_CONTENT_RENAME_TO)) -or (IsStringDefined($EPISODE_EXCLUSION))) )
			{
				#Not only an error was hit in the script block, but no work was done.
				foreach($currentError in $savedErrors)
				{
					LogWarning "Error while executing block in" $Subscription ":"
					LogWarning Block was: [($SUBSCRIPTION_ACQUIRE_ACTION.ToString())]
					throw $currentError
				}
			}
		}
	} #END ExtractEpisodeScriptBlock

	#########################
	#  END INLINE LOGIC	#
	#########################

	$seenEpisodes = New-Object -TypeName System.Collections.ArrayList
	if(IsValidPath $subscriptionHistoryCsv)
	{
		$subscriptionHistory = Import-Csv $subscriptionHistoryCsv
		foreach ($subscriptionEntry in $subscriptionHistory)
		{
			if(-not (IsStringDefined $subscriptionEntry.Id.ToString().ToLower()))
			{
				LogWarning Corrupt content detected: [$subscriptionHistoryCsv]
				continue
			}

			if(-not $seenEpisodes.Contains($subscriptionEntry.Id.ToString().ToLower()))
			{
				$seenEpisodes.Add($subscriptionEntry.Id.ToString().ToLower()) > $null
			}
		}
	}

	foreach ($currentSubscriptionUri in $SUBSCRIPTION_URI)
	{
		if(-not (IsStringDefined $currentSubscriptionUri))
		{
			LogWarning Invalid Subscription URI: [$currentSubscriptionUri]
			continue
		}

		if(-not $currentSubscriptionUri.ToLower().StartsWith("http"))
		{
			$results = $currentSubscriptionUri
		}
		else
		{
			$results = RobustGetUrlContent $currentSubscriptionUri $SUBSCRIPTION_REQUEST_HEADERS
		}

		if($results -eq $null)
		{
			if(-not $SUBSCRIPTION_IGNORE_DOWNLOAD_URL_ERRORS)
			{
				LogError No Data found: [([System.IO.Path]::GetFileName($Subscription))] [$currentSubscriptionUri]
			}
			return
		}

		$resultLines = PrepareSubscriptionUriLines $SUBSCRIPTION_SPLIT_SOURCE $results
		if(($resultLines.Count) -le 1)
		{
			LogWarning The source content did not split: [([System.IO.Path]::GetFileName($Subscription))]
		}

		$diagnosticFileRoot = ".\" + [System.IO.Path]::GetFileNameWithoutExtension($Subscription)

		$diagnosticSubscriptionSourceLog = $diagnosticFileRoot + ".Source.log"
		if($SUBSCRIPTION_DIAGNOSTICS)
		{
			if($redirectionUri -eq $null)
			{
				DeleteItem $diagnosticSubscriptionSourceLog
			}
			"**********************************" | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
			"* " + $SUBSCRIPTION_URI + " *" | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
			"* " + (Get-Date) + " *" | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
			"**********************************" | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog

			if($SUBSCRIPTION_REQUEST_HEADERS.Count -gt 0)
			{
				"* REQUEST ************************" | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
				foreach ($currentHeader in $SUBSCRIPTION_REQUEST_HEADERS.Keys)
				{
					([String]::Format("* {0} : [{1}]", $currentHeader, $SUBSCRIPTION_REQUEST_HEADERS[$currentHeader])) | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
				}
				"**********************************" | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
			}

			"* RESPONSE ***********************" | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
			foreach ($currentHeader in $webclient.ResponseHeaders.Keys)
			{
				([String]::Format("* {0} : [{1}]", $currentHeader, $webclient.ResponseHeaders[$currentHeader])) | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
			}
			"**********************************" | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
		}

		$cLineNumber = 0
		$cEpisodesMatched = 0
		$cEpisodesExtracted = 0

		$variablesToRemoveOnReset = New-Object -TypeName System.Collections.ArrayList
		InitializeVariablesToRemove $variablesToRemoveOnReset
		$cInitalVariableState = $variablesToRemoveOnReset.Count

		if(IsStringDefined($SUBSCRIPTION_EPISODE_BEGIN))
		{
			$episodeParserEngaged = $false
		}
		else
		{
			$episodeParserEngaged = $true
		}

		try
		{
			foreach ($currentLine in $resultLines)
			{
				$currentLine = $currentLine.Trim()
				if($currentLine.Length -eq 0)
				{
					continue
				}

				$cLineNumber++

				$previousParserEngagedState = $episodeParserEngaged
				if((IsStringDefined($SUBSCRIPTION_EPISODE_BEGIN)) -and ($currentLine -match $SUBSCRIPTION_EPISODE_BEGIN) -and (-not $episodeParserEngaged))
				{
					$episodeParserEngaged = $true
				}

				if((IsStringDefined($SUBSCRIPTION_EPISODE_END)) -and ($currentLine -match $SUBSCRIPTION_EPISODE_END) -and ($episodeParserEngaged))
				{
					$episodeParserEngaged = $false
				}

				if(-not $episodeParserEngaged)
				{
					if($SUBSCRIPTION_DIAGNOSTICS)
					{
						([String]::Format("[-L: {0}] [{1:D5}] {2}", $cLineNumber, $currentLine.Length, $currentLine)) | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
					}
					continue
				}
				else
				{
					if($SUBSCRIPTION_DIAGNOSTICS)
					{
						([String]::Format("[+L: {0}] [{1:D5}] {2}", $cLineNumber, $currentLine.Length, $currentLine)) | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
					}
				}

				if($previousParserEngagedState -ne $episodeParserEngaged)
				{
					$matchName = $null
					$matchValue = $null
					if($variablesToRemoveOnReset.Count -gt $cInitalVariableState)
					{
						Invoke-Command -NoNewScope -ScriptBlock $ExtractEpisodeScriptBlock
						if($abortSubscription)
						{
							return
						}
					}
				}

				#Populates variable $matchResults
				Invoke-Command -NoNewScope -ScriptBlock $ExtractMatchesScriptBlock

				$cCurrentMatch = 0
				foreach ($currentSubscriptionMatch in $matchResults)
				{
					$cCurrentMatch++
					$matchName = $currentSubscriptionMatch.Name
					$matchValue = $currentSubscriptionMatch.Value

					#DebugLogInfo ([String]::Format("[MATCH {0} of {1}] [{2}] [{3}]", $cCurrentMatch, $currentSubscriptionMatches.Count, $matchName, $matchValue))

					if(-not (IsVariableDefinedInScope $matchName  (Get-Variable -Scope 0)))
					{
						Invoke-Command -NoNewScope -ScriptBlock $SaveMatchInScopeScriptBlock
					}
					else
					{
						Invoke-Command -NoNewScope -ScriptBlock $ExtractEpisodeScriptBlock
						if($abortSubscription)
						{
							return
						}
					}
				} #END EXTRATOR MATCHES
			} #END LINE PARSE

			$isLastAttemptedEpisodeExtraction = $true
			Invoke-Command -NoNewScope -ScriptBlock $ExtractEpisodeScriptBlock
			if($abortSubscription)
			{
				return
			}
		}
		finally
		{
			if($SUBSCRIPTION_DIAGNOSTICS)
			{
				$currentDumpLineNumber = 0
				foreach ($currentLine in $resultLines)
				{
					$currentDumpLineNumber++
					if($currentDumpLineNumber -gt $cLineNumber)
					{
						([String]::Format("[#L: {0}] [{1:D5}] {2}", $currentDumpLineNumber, $currentLine.Length, $currentLine)) | SafeOutFile -LiteralPath $diagnosticSubscriptionSourceLog
					}
				}
			}
		}

		if(($cEpisodesMatched -eq 0) -and (-not $WHATIF_MODE) -and (-not $SUBSCRIPTION_DIAGNOSTICS))
		{
			LogWarning No episode content was extracted for subscription [([System.IO.Path]::GetFileName($Subscription))]. You may need to update it...
		}

		VerboseLogInfo ([String]::Format("SUBSCRIPTION [{0}]: [MATCHED: {1}] [EXTRACTED: {2}]", ([System.IO.Path]::GetFileName($Subscription)), $cEpisodesMatched, $cEpisodesExtracted))
		if($VERBOSE_MODE -and (-not $epPubDateUtilized))
		{
			LogWarning Subscription is not publish date optimized: [([System.IO.Path]::GetFileName($Subscription))]
		}

		if($cEpisodesMatched -gt 0)
		{
			foreach($currentExtractor in $extractorPatterns.Keys)
			{
				if(-not $matchedExtractorPatterns.Contains($currentExtractor))
				{
					LogWarning Extractor Pattern not matched: [([System.IO.Path]::GetFileName($Subscription))] [$currentExtractor]
				}
			}
		}
	} #END URI
}

#######################################
#
# FUNCTION RobustGetUrlContent
#
#######################################
function RobustGetUrlContent($uri, $headers)
{
	VerboseLogInfo QUERYING: [$uri]

	try
	{
		$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

		#NOTE: Argument '--verbose' causes output problems when parsing
		if($DEBUG_MODE)
		{
			$traceName = $uri.Replace("http://www.", [String]::Empty)
			$traceName = $traceName.Replace("http://", [String]::Empty)
			$traceName = $traceName.Replace("https://", [String]::Empty)
			$traceName = $traceName.Replace("www.", [String]::Empty)
			foreach ($currentInvalidChar in [System.IO.Path]::GetInvalidFileNameChars())
			{
				$traceName = $traceName.Replace($currentInvalidChar, '_')
			}
			$dynamicParameters.Add("--trace-ascii") > $null
			$dynamicParameters.Add(".\" + $traceName + ".trace.txt") > $null
		}

		$userAgentFound = $false
		foreach($currentHeader in $headers.Keys)
		{
			$headerString = $currentHeader + ": " + ($headers[$currentHeader])
			$dynamicParameters.Add("--header") > $null
			$dynamicParameters.Add($headerString) > $null
			VerboseLogInfo Adding Request Header [$headerString]

			if($currentHeader -eq "User-Agent")
			{
				$userAgentFound = $true
			}
		}

		if(-not $userAgentFound)
		{
			$dynamicParameters.Add("--user-agent") > $null
			$dynamicParameters.Add("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:127.0) Gecko/20100101 Firefox/127.0") > $null
		}

		$dynamicParameters.Add("--no-progress-meter") > $null
		$dynamicParameters.Add("--location") > $null
		$dynamicParameters.Add("--suppress-connect-headers") > $null
		$dynamicParameters.Add($uri) > $null

		#NOTE: cURL has natively shipped since Windows 10/11
		$results = ExecuteCommand "curl" $dynamicParameters
	}
	catch
	{
		if(-not $SUBSCRIPTION_IGNORE_DOWNLOAD_URL_ERRORS)
		{
			SleepFor 5
			LogError $_
		}
		else
		{
			LogWarning Web Error Encoutered: [($_.Exception.ToString())]
			$Error.Clear()
		}
		$results = $null
	}
	finally
	{
		$ProgressPreference = $backupProgressPreference
	}

	return $results
}

#######################################
#
# FUNCTION ReturnOrderedMatchesForLine
#
#######################################
function ReturnOrderedMatchesForLine($Extractors, $InputLine)
{
	$currentLine = $InputLine
	$matchesDictionary = @{}

	if(($Extractors -eq $null) -or $Extractors.Count -eq 0)
	{
		LogError Subscription does not have a extractor set defined.
		return (New-Object -TypeName System.Collections.ArrayList)
	}

	$extractorPatterns = FlattenToDictionary $Extractors

	foreach ($currentExtractor in $extractorPatterns.Keys)
	{
		$originalMatch = $null

		if(-not (IsStringDefined $currentExtractor))
		{
			LogError Subscription has invalid extractor defined: [$currentExtractor]
			continue
		}

		if($currentLine -notmatch $currentExtractor)
		{
			continue
		}

		$EXTRACTOR_MATCH_OVERRIDE = $null

		$extractorCondition = $extractorPatterns[$currentExtractor]
		if($extractorCondition -eq $null)
		{
			$extractorCondition = { $true }
		}

		if($extractorCondition.GetType().ToString() -eq "System.Management.Automation.ScriptBlock")
		{
			if(-not (Invoke-Command -NoNewScope -ScriptBlock $extractorCondition) -and ($EXTRACTOR_MATCH_OVERRIDE -eq $null))
			{
				DebugLogInfo EXTRACTOR CONDITION UNMET: [($extractorCondition.ToString())] [$currentLine]
				continue
			}
		}
		else
		{
			LogWarning EXTRACTOR CONDITION NOT DEFINED CORRECTLY: [($extractorCondition.ToString())]
			continue
		}

		if($EXTRACTOR_MATCH_OVERRIDE -ne $null)
		{
			DebugLogInfo MATCH OVERRIDE: [$currentLine] --> [$extractorCondition]
			$originalMatch = $currentSubscriptionMatches
			$currentSubscriptionMatches = $EXTRACTOR_MATCH_OVERRIDE
		}
		else
		{
			$currentSubscriptionMatches = [RegEx]::Matches($currentLine, $currentExtractor, [System.Text.RegularExpressions.RegExOptions]::ExplicitCapture)
		}

		foreach ($currentSubscriptionMatch in $currentSubscriptionMatches)
		{
			foreach ($currentSubscriptionMatchGroup in $currentSubscriptionMatch.Groups)
			{
				if([Int]::TryParse($currentSubscriptionMatchGroup.Name, [ref] $null))
				{
					#Implicit, not captured groups, do not save as variables
					continue
				}

				if(-not $currentSubscriptionMatchGroup.Success)
				{
					DebugLogInfo UNMATCHED GROUP DETECTED: [($currentSubscriptionMatchGroup.Name)]
					continue
				}

				if(($originalMatch -ne $null) -and ($originalMatch.Groups.Count -ne 0))
				{
					$matchIndex = $originalMatch.Groups[0].Index
				}
				else
				{
					$matchIndex = $currentSubscriptionMatchGroup.Index
				}

				#DebugLogInfo MATCH [I: ($matchIndex)] [($currentSubscriptionMatchGroup.Name)] [($currentSubscriptionMatchGroup.Value)]

				if(-not ($matchesDictionary.ContainsKey($matchIndex)))
				{
					$matchesDictionary.Add($matchIndex, (New-Object -TypeName System.Collections.ArrayList))
				}
				($matchesDictionary[($matchIndex)]).Add($currentSubscriptionMatchGroup) > $null
			}
		}
	}

	$matchResults = New-Object -TypeName System.Collections.ArrayList
	foreach ($currentMatchResultIndex in ($matchesDictionary.Keys | Sort-Object))
	{
		foreach ($currentMatchResultIndexEntryInstance in ($matchesDictionary[$currentMatchResultIndex]))
		{
			$matchResults.Add($currentMatchResultIndexEntryInstance) > $null
		}
	}

	return $matchResults
}

######################################
#
# FUNCTION InitializeVariablesToRemove
#
######################################
function InitializeVariablesToRemove($array)
{
	if($array -eq $null)
	{
		$array = New-Object -TypeName System.Collections.ArrayList
	}

	$array.Clear()

	$array.Add("EPISODE_CONTENT_URI")  > $null
	$array.Add("EPISODE_CONTENT_ID")  > $null
	$array.Add("EPISODE_REDIRECT_URI")  > $null
	$array.Add("EPISODE_CONTENT_RENAME_TO")  > $null
	$array.Add("EPISODE_EXCLUSION")  > $null
	$array.Add("EPISODE_PUBLISH_DATE")  > $null
}

###################################
#
# FUNCTION ConcatenateMediaSegments
#
###################################
function ConcatenateMediaSegments($rootFolder)
{
	if(FailedStringValidation ($rootFolder)) { return }

	if(IsInvalidPath $rootFolder)
	{
		LogWarning Cannot Concatenate media for an invalid location: [$rootFolder]
		return
	}

	$segmentReference = FindReferenceSegmentFile $rootFolder $MEDIA_EXTENSIONS
	if($segmentReference -eq $null)
	{
		LogError Reference not found in folder: [$rootFolder]
		return
	}

	#Sometimes files have extended characters in them that breaks references for FFMPEG
	foreach ($currentChar in $segmentReference.FullName.ToCharArray())
	{
		if($currentChar -gt 127)
		{
			foreach ($currentItem in (Get-ChildItem -LiteralPath $rootFolder))
			{
				if($currentItem.FullName.Contains($currentChar))
				{
					$newName = $currentItem.FullName.Replace($currentChar.ToString(), '')
					RenameFile $currentItem.FullName $newName
				}
			}

			$segmentReference = FindReferenceSegmentFile $rootFolder $MEDIA_EXTENSIONS
			if($segmentReference -eq $null)
			{
				LogError Reference not found in folder: [$rootFolder]
				return
			}
		}
	}

	$targetOutput = $segmentReference.FullName.Replace(".S1.", ".")
	if($targetOutput -eq $segmentReference.FullName)
	{
		$targetOutput = $segmentReference.FullName.Replace($segmentReference.Extension, ".CONCAT" + $segmentReference.Extension)
	}

	#Purge unneeded thumbnails
	$selectedThumb = FindReferenceSegmentFile $rootFolder $THUMBNAIL_EXTENSIONS
	if($selectedThumb -ne $null)
	{
		Get-ChildItem -LiteralPath $rootFolder | Where-Object { ($_.Extension -in $THUMBNAIL_EXTENSIONS) -and ($_.Name -ne $selectedThumb.Name) } | Remove-Item
	}

	#Detect subtitles
	$subtitleFiles = FindRelatedFiles @($segmentReference.FullName) $SUBTITLE_EXTENSIONS
	$concatSubtitles = $false
	if($subtitleFiles.Count -gt 0)
	{
		$concatSubtitles = $true
	}

	#EXPORT METADATA (will be embedded later)
	$targetMetadata = $targetOutput.Replace($segmentReference.Extension, $METADATA_EXTENSION)
	if(IsValidPath $targetMetadata)
	{
		LogWarning Overwriting: [$targetMetadata]
		DeleteItem $targetMetadata
	}

	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

	$dynamicParameters.Add("-i") > $null
	$dynamicParameters.Add($segmentReference.FullName) > $null
	$dynamicParameters.Add("-f") > $null
	$dynamicParameters.Add("ffmetadata") > $null
	$dynamicParameters.Add($targetMetadata) > $null

	ExecuteFFmpegCommand $targetMetadata $dynamicParameters $null

	$segmentInput = $targetOutput.Replace($segmentReference.Extension, $SEGMENTS_EXTENSION)
	if(IsValidPath $segmentInput)
	{
		LogWarning Overwriting: [$segmentInput]
		DeleteItem $segmentInput
	}

	$cSegment = 1
	$futureFilesToDelete = New-Object -TypeName System.Collections.ArrayList
	foreach ($currentSegment in (Get-ChildItem -LiteralPath $rootFolder -Filter ("*" + $segmentReference.Extension) | Sort-Object -Property CreationTime))
	{
		$segmentMediaFile = $currentSegment.FullName
		DebugLogInfo Segment: [$segmentMediaFile]

		$segmentSubtitleFile = $null
		if($concatSubtitles)
		{
			foreach ($currentSubtitleFile in $subtitleFiles)
			{
				if($currentSubtitleFile.Contains($currentSegment.BaseName))
				{
					$segmentSubtitleFile = $currentSubtitleFile
					DebugLogInfo Subtitle for segment: [$segmentSubtitleFile]
					break
				}
			}

			if($segmentSubtitleFile -eq $null)
			{
				LogError Subtitle for segment not found: [($currentSegment.BaseName)]
				return
			}
		}

		$segmentMediaFilePadded = $segmentMediaFile.Replace($currentSegment.Extension, ".Pad" + $currentSegment.Extension)
		if(IsValidPath $segmentMediaFilePadded)
		{
			LogWarning Overwriting: [$segmentMediaFilePadded]
			DeleteItem $segmentMediaFilePadded
		}

		$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

		$segmentHasSubtitles = ($segmentSubtitleFile -ne $null) -and (IsValidPath $segmentSubtitleFile) -and ((Get-Item -LiteralPath $segmentSubtitleFile).Length -gt 0)

		$dynamicParameters.Add("-y") > $null
		$dynamicParameters.Add("-i") > $null
		$dynamicParameters.Add($segmentMediaFile) > $null
		if($segmentHasSubtitles)
		{
			$dynamicParameters.Add("-i") > $null
			$dynamicParameters.Add($segmentSubtitleFile) > $null
		}
		$dynamicParameters.Add("-af") > $null
		$dynamicParameters.Add("apad") > $null
		$dynamicParameters.Add("-c:v") > $null
		$dynamicParameters.Add("copy") > $null
		if($segmentSubtitleFile -ne $null)
		{
			$dynamicParameters.Add("-c:s") > $null
			$dynamicParameters.Add("mov_text") > $null
		}
		$dynamicParameters.Add("-shortest") > $null
		$dynamicParameters.Add("-avoid_negative_ts") > $null
		$dynamicParameters.Add("make_zero") > $null
		$dynamicParameters.Add("-fflags") > $null
		$dynamicParameters.Add("+genpts") > $null
		$dynamicParameters.Add("-map_metadata") > $null
		$dynamicParameters.Add("0") > $null
		$dynamicParameters.Add($segmentMediaFilePadded) > $null

		$filesToDelete = New-Object -TypeName System.Collections.ArrayList
		$filesToDelete.Add($segmentMediaFile) > $null
		$futureFilesToDelete.Add($segmentMediaFilePadded) > $null
		if($segmentSubtitleFile -ne $null)
		{
			$filesToDelete.Add($segmentSubtitleFile) > $null
		}

		ExecuteFFmpegCommand $segmentMediaFilePadded $dynamicParameters $filesToDelete

		$segmentMediaFilePadded = $segmentMediaFilePadded.Replace("'", "'\''")
		"file '" + $segmentMediaFilePadded + "'" | SafeOutFile -LiteralPath $segmentInput
		$cSegment++
	}

	if(($cSegment -eq 1) -and (-not $WHATIF_MODE))
	{
		LogError No show segments detected for media: [($targetOutput)]
		return
	}

	if((IsInvalidPath $segmentInput) -and (-not $WHATIF_MODE))
	{
		LogError Input segments file not found: [$segmentInput]
		return
	}

	# CONCATENATE PROGRAM SEGEMENTS
	ConvertMedia $segmentInput $targetOutput
	if(IsValidPath $targetOutput)
	{
		foreach ($fileToDelete in $futureFilesToDelete)
		{
			DeleteItem $fileToDelete
		}
	}
	else
	{
		LogError Target Media was not concatenated: [$targetOutput]
		foreach ($fileToDelete in @($segmentInput, $targetMetadata))
		{
			DeleteItem $fileToDelete
		}
	}

	$originalName = $targetOutput
	$originalName = $originalName.Replace(".CONCAT.", ".")
	$originalName = $originalName.Replace(".S1.", ".")
	$originalName = $originalName.Replace(".S01.", ".")
	if(($targetOutput -ne $originalName) -and (IsInvalidPath $originalName))
	{
		RenameFile $targetOutput $originalName
	}

	if(IsInvalidPath $originalName)
	{
		return
	}
}

#################################
#
# FUNCTION CalculateWatchedFolder
#
#################################
function CalculateWatchedFolder($fileName)
{
	$targetWatchedDirectory = DeriveWatchedPath $fileName
	$showNameMatched = $false

	#LOOK UP KNOWN DICTIONARY OF TARGETS
	$file = Get-Item -LiteralPath $fileName

	if(IsUnderPath $fileName $DOWNLOADED_MOVIES_PATH)
	{
		return $targetWatchedDirectory
	}

	$newShowName = ExtractShowName $file

	#TRY DYNAMICALY DETERMINING SHOW NAME BY NAME
	if((-not $showNameMatched) -and ($newShowName -ne $null))
	{
		$override = MatchFolderOverride $newShowName $CONSUMED_PATH_OVERRIDE
		if($override -ne $null)
		{
			$showNameMatched = $true;
			$targetWatchedDirectory = $override
		}
	}

	#TOPICAL AND DAILY SHOWS DEFAULT TO PURGE FOLDER
	$isTopicalShow = IsTopicalShow $file
	$isDailyShow = IsDailyShow $file
	if((-not $showNameMatched) -and ($isTopicalShow -or $isDailyShow))
	{
		$targetWatchedDirectory = $PURGE_PATH
		$showNameMatched = $true
	}

	#DYNAMICALLY ATTEMPT TO FIND TARGET BASED ON SHOW NAME
	if((-not $showNameMatched) -and ($newShowName -ne $null))
	{
		$targetWatchedDirectory = [System.IO.Path]::Combine($targetWatchedDirectory, $newShowName)
	}

	#DETECT SEASON FOR MORE TARGETED DIRECTORY
	if(($targetWatchedDirectory -ne (DeriveWatchedPath $targetWatchedDirectory)) -and ((IsInvalidPath $targetWatchedDirectory) -or (IsSeasonedFolder $file $targetWatchedDirectory)))
	{
		$seasonName = ExtractShowSeason $file
		if($seasonName -ne $null)
		{
			if((IsAudio $file.FullName) -and ($newShowName -ne $null))
			{
				$targetWatchedDirectory = [System.IO.Path]::Combine($targetWatchedDirectory, $newShowName + " - " + $seasonName)
			}
			else
			{
				$targetWatchedDirectory = [System.IO.Path]::Combine($targetWatchedDirectory, $seasonName)
			}
		}
	}

	VerboseLogInfo [$file] [Show: $newShowName] [Season: $seasonName] [Topical: $isTopicalShow] [Target: $targetWatchedDirectory]
	return $targetWatchedDirectory
}

#####################################
#
# FUNCTION IsKodiConfigured
#
#####################################
function IsKodiConfigured()
{
	if(-not $KODI_INTEGRATION)
	{
		return $false
	}

	if($KODI_SERVERS.Count -le 0)
	{
		LogError Kodi is not configured.
		return $false
	}

	return $true
}

#####################################
#
# FUNCTION IsPlexConfigured
#
#####################################
function IsPlexConfigured()
{
	if(-not $PLEX_INTEGRATION)
	{
		return $false
	}

	if(-not (IsStringDefined($PLEX_CREDENTIALS)))
	{
		LogError Plex is not configured.
		return $false
	}

	return $true
}

#####################################
#
# FUNCTION MarkLibraryMediaWatched
#
#####################################
function MarkLibraryMediaWatched()
{
	MarkKodiMediaWatched
	MarkPlexMediaWatched
}

#####################################
#
# FUNCTION MarkPlexMediaWatched
#
#####################################
function MarkPlexMediaWatched()
{
	$watchedTargets = New-Object -TypeName System.Collections.ArrayList
	foreach ($currentTarget in @($DOWNLOADED_TV_PATH, $DOWNLOADED_MOVIES_PATH))
	{
		$currentWatchedPath = DeriveWatchedPath $currentTarget
		if(IsValidPath $currentWatchedPath)
		{
			$watchedTargets.Add($currentWatchedPath) > $null
		}
	}

	$cCurrentProgress = 0
	foreach ($currentWatchedTarget in $watchedTargets)
	{
		$cCurrentProgress++
		OutputProgressBar ("Marking Plex Library Consumed [$currentWatchedTarget]...") ($cCurrentProgress / $watchedTargets.Count)

		$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

		$dynamicParameters.Add("-t") > $null
		$dynamicParameters.Add($currentWatchedTarget) > $null

		ExecutePlexApiCommand $PLEX_WATCHED_SCRIPT $dynamicParameters
	}
}

#####################################
#
# FUNCTION MarkPlexMediaUnwatched
#
#####################################
function MarkPlexMediaUnwatched($path)
{
	if(FailedStringValidation ($path)) { return }

	if(IsInvalidPath $path)
	{
		LogError Invalid Path: [$path]
	}

	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList
	$dynamicParameters.Add("-o") > $null
	$dynamicParameters.Add("-t") > $null
	$dynamicParameters.Add($path) > $null

	ExecutePlexApiCommand $PLEX_WATCHED_SCRIPT $dynamicParameters
}

#####################################
#
# FUNCTION TrackPlexWatchedMedia
#
#####################################
function TrackPlexWatchedMedia()
{
	if(-not (IsPlexConfigured))
	{
		return
	}

	if("Plex Media Server.exe" -NotIn $WATCHING_HOSTS)
	{
		return
	}

	if((-not $Force) -and (IsValidPath $PLEX_OPENED_CSV) -and (IsValidPath $LAST_TRACKED))
	{
		if((Get-Item -LiteralPath $LAST_TRACKED).LastWriteTime -gt (Get-Item -LiteralPath $PLEX_OPENED_CSV).LastWriteTime)
		{
			DebugLogInfo No work to be done right now to detect plex watchers.
			return
		}
	}

	VerboseLogInfo Scanning Plex Consumed State...
	if(IsInvalidPath $LAST_TRACKED)
	{
		$currentTrackedWindow = (Get-Date).AddHours($TRACK_AGE_HOURS * -1)
	}
	else
	{
		$currentTrackedWindow = (Get-Item -LiteralPath $LAST_TRACKED).LastWriteTime
	}

	if(IsValidPath $PLEX_OPENED_CSV)
	{
		$seenMedia = New-Object -TypeName System.Collections.ArrayList
		$inputCSV = Import-Csv $PLEX_OPENED_CSV
		foreach ($currentCsvEntry in $inputCSV)
		{
			$openedMedia = $currentCsvEntry.SessionOpenMediaFile
			$openedBy = $currentCsvEntry.SessionUserName
			$isPlexAdmin = [System.Boolean]::Parse($currentCsvEntry.UserNameIsPlexAdmin)
			$sessionState = $currentCsvEntry.SessionClientState
			$plexTimestamp = [System.DateTime]::Parse($currentCsvEntry.DateTime)

			if($seenMedia.Contains($openedMedia) -or ($currentTrackedWindow -gt $plexTimestamp) -or ($sessionState -ne "playing") -or ([System.IO.Path]::GetExtension($openedMedia) -NotIn $MEDIA_EXTENSIONS))
			{
				#DebugLogInfo SKIPPING PLEX OPENED ENTRY: [$plexTimestamp] [$openedBy] [$openedMedia] [$isPlexAdmin] [$sessionState]
				continue
			}

			$seenMedia.Add($openedMedia) > $null
			if(IsValidPath $openedMedia)
			{
				if(-not $isPlexAdmin)
				{
					LogInfo GUEST CONSUMING: [("PLEX - " + $openedBy)] [$openedMedia]
					$guestWatchedOverride = LoadSubscriptionOverrideVariable (Get-Item -LiteralPath $openedMedia) "GUEST_WATCHERS"
					if(($guestWatchedOverride -ne $null) -and $guestWatchedOverride.Contains($openedBy))
					{
						TrackWatchedMedia $openedMedia ($plexTimestamp) ("PLEX - " + $openedBy)
					}
				}
				else
				{
					DebugLogInfo FOUND CONSUMED: [("PLEX - " + $openedBy)] [$openedMedia]
					TrackWatchedMedia $openedMedia ($plexTimestamp) ("PLEX - " + $openedBy)
				}
			}
		}
	}
}

#####################################
#
# FUNCTION ExecutePlexApiCommand
#
#####################################
function ExecutePlexApiCommand($scriptName, [System.Collections.ArrayList] $dynamicParameters)
{
	if(FailedStringValidation ($scriptName)) { return }

	if(-not (IsPlexConfigured))
	{
		return
	}

	if((-not (IsStringDefined $PYTHON_BINARY)) -or (-not (IsStringDefined $PLEX_API)))
	{
		LogError Plex API not correctly configred: [$PLEX_API]
		return
	}

	if(IsInvalidPath $PLEX_API)
	{
		LogError Plex API not correctly configred: [$PLEX_API]
		return
	}

	if(IsInvalidPath $PYTHON_BINARY)
	{
		LogError Python not correctly configred: [$PYTHON_BINARY]
		return
	}

	$dynamicParameters.Add("-w") > $null
	$dynamicParameters.Add($PLEX_API) > $null

	if($DEBUG_MODE)
	{
		$dynamicParameters.Add("-v") > $null
	}

	$extractedCred = GetCredential $PLEX_CREDENTIALS
	if($extractedCred -eq $null)
	{
		return
	}

	if(-not (IsStringDefined $PLEX_SERVERNAME))
	{
		$PLEX_SERVERNAME = "localhost"
	}
	$dynamicParameters.Add("-s") > $null
	$dynamicParameters.Add($PLEX_SERVERNAME) > $null

	if($extractedCred[0].Trim() -eq "localhost")
	{
		DebugLogInfo "Passing Token to PLEX"
		[Environment]::SetEnvironmentVariable("PLEXAPI_AUTH_SERVER_TOKEN", $extractedCred[1])
	}
	else
	{
		DebugLogInfo "Passing Username and Password to PLEX"
		[Environment]::SetEnvironmentVariable("PLEXAPI_AUTH_MYPLEX_USERNAME", $extractedCred[0])
		[Environment]::SetEnvironmentVariable("PLEXAPI_AUTH_MYPLEX_PASSWORD", $extractedCred[1])
	}

	$logFile = [System.IO.Path]::Combine($DATA_PATH, "Plex.log")
	[Environment]::SetEnvironmentVariable("PLEXAPI_LOG_PATH", $logFile)
	[Environment]::SetEnvironmentVariable("PLEXAPI_LOG_ROTATE_BYTES", (1024 * 1024 * $LOG_ROLLOVER_MB))

	if($DEBUG_MODE)
	{
		[Environment]::SetEnvironmentVariable("PLEXAPI_LOG_LEVEL", "DEBUG")
	}
	else
	{
		[Environment]::SetEnvironmentVariable("PLEXAPI_LOG_LEVEL", "INFO")
	}

	[Environment]::SetEnvironmentVariable("PLEXAPI_ENABLE_FAST_CONNECT", "false")
	[Environment]::SetEnvironmentVariable("PYTHONIOENCODING", "UTF-8")

	$scriptFileName = [System.IO.Path]::GetFileName($scriptName)

	if($WHATIF_MODE)
	{
		LogInfo WHATIF: $PYTHON_BINARY $scriptName $dynamicParameters
	}
	else
	{
		try
		{
			$output = ExecuteCommand $PYTHON_BINARY $scriptName $dynamicParameters
		}
		catch
		{
			if($_.Exception.Message.Contains("connect"))
			{
				LogWarning Unable To Connect to Plex.
			}
			elseif( ($scriptFileName -eq "Mark-PlexMediaWatched.py") -and ($_.Exception.Message.Contains("unwatchedLibraryItem.isPlayed")) -or ($_.Exception.Message.Contains("unwatchedLibraryItem.markPlayed")) )
			{
				#Ignore, seems to trigger for some reason.
			}
			else
			{
				LogWarning Exception executing: [$scriptFileName] [$dynamicParameters]
				HandleError $_
			}
		}

		foreach($currentOutputLine in $output)
		{
			LogInfo [$scriptFileName]: $currentOutputLine
		}
	}
}

###########################################
#
# FUNCTION AuditPlexCurrentlyWatchingMedia
#
###########################################
function AuditPlexCurrentlyWatchingMedia()
{
	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

	$dynamicParameters.Add("-o") > $null
	$dynamicParameters.Add($PLEX_AUDIT_CSV) > $null

	ExecutePlexApiCommand $PLEX_AUDIT_SCRIPT $dynamicParameters
}

###########################################
#
# FUNCTION ExportPlexCurrentlyWatchingMedia
#
###########################################
function ExportPlexCurrentlyWatchingMedia()
{
	$dynamicParameters = New-Object -TypeName System.Collections.ArrayList

	$dynamicParameters.Add("-o") > $null
	$dynamicParameters.Add($PLEX_OPENED_CSV) > $null

	ExecutePlexApiCommand $PLEX_OPENED_SCRIPT $dynamicParameters
}

$KODI_FOLDER_TRASLATE_ORDERED = New-Object -TypeName System.Collections.ArrayList
#####################################
#
# FUNCTION TranslateKodiPath
#
#####################################
function TranslateKodiPath($path)
{
	if(FailedStringValidation ($path)) { return }
	$translatedPath = $path

	if(-not $KODI_INTEGRATION)
	{
		LogError "Kodi not configured!"
		return
	}

	$tmpKodiTrandlatedFolders = $KODI_FOLDER_TRASLATE_ORDERED
	if($KODI_FOLDER_TRANSLATE.Count -ne $tmpKodiTrandlatedFolders.Count)
	{
		$tmpKodiTrandlatedFolders.Clear()
		foreach ($currentFolderTranslation in ($KODI_FOLDER_TRANSLATE.Values | Sort-Object -Descending -Property Length))
		{
			foreach ($currentKey in $KODI_FOLDER_TRANSLATE.Keys)
			{
				if($KODI_FOLDER_TRANSLATE[$currentKey] -eq $currentFolderTranslation)
				{
					$tmpKodiTrandlatedFolders.Add($currentKey) > $null
				}
			}
		}
	}
	$Script:KODI_FOLDER_TRASLATE_ORDERED = $tmpKodiTrandlatedFolders

	foreach ($currentFolderTranslation in $KODI_FOLDER_TRASLATE_ORDERED)
	{
		$translationTarget = $KODI_FOLDER_TRANSLATE[$currentFolderTranslation]
		$translatedPath = $translatedPath.Replace($currentFolderTranslation, $translationTarget)
	}

	$translatedPath = $translatedPath.Replace('/', '\')

	return $translatedPath
}

#####################################
#
# FUNCTION ExecuteKodiApi
#
#####################################
function ExecuteKodiApi($currentKodiUri, $jsonRequest)
{
	$debugLogFile = "KodiJsonApi.log"

	$webclient = New-Object System.Net.WebClient
	$webclient.Encoding = [System.Text.Encoding]::UTF8
	$extractedCred = GetCredential ($KODI_SERVERS[$currentKodiUri])
	if($extractedCred -eq $null)
	{
		return
	}
	$webclient.Credentials = New-Object System.Net.NetworkCredential(($extractedCred[0]), ($extractedCred[1]))

	if(	$jsonRequest.Contains("VideoLibrary.Scan") -or
		$jsonRequest.Contains("VideoLibrary.RemoveMovie") -or
		$jsonRequest.Contains("VideoLibrary.RemoveEpisode") -or
		$jsonRequest.Contains("VideoLibrary.SetMovieDetails") -or
		$jsonRequest.Contains("Files.SetFileDetails") )
	{
		$requestUri = $jsonRequest
		VerboseLogInfo POSTING: [$currentKodiUri] [$requestUri]
		$postData = [System.Text.Encoding]::UTF8.GetBytes($jsonRequest)
	}
	else
	{
		$requestUri = [String]::Format("{0}?request={1}", $currentKodiUri, $jsonRequest)
		VerboseLogInfo QUERYING: [$currentKodiUri] [$requestUri]
	}

	if($DEBUG_MODE)
	{
		"**********************************" | SafeOutFile -LiteralPath $debugLogFile
		"* " + (Get-Date) + " *" | SafeOutFile -LiteralPath $debugLogFile
		"* " + $requestUri + " *" | SafeOutFile -LiteralPath $debugLogFile
		"**********************************" | SafeOutFile -LiteralPath $debugLogFile
	}

	try
	{
		if(	$jsonRequest.Contains("VideoLibrary.Scan") -or
			$jsonRequest.Contains("VideoLibrary.RemoveMovie") -or
			$jsonRequest.Contains("VideoLibrary.RemoveEpisode") -or
			$jsonRequest.Contains("VideoLibrary.SetMovieDetails") -or
			$jsonRequest.Contains("Files.SetFileDetails") )
	{
			$response = $webclient.UploadData($currentKodiUri, "POST", $postData)
			$results = [System.Text.Encoding]::UTF8.GetString($response)
		}
		else
		{
			$results = $webclient.DownloadString($requestUri)
		}
	}
	catch [System.Net.WebException]
	{
		if($WHATIF_MODE)
		{
			$Error.Clear()
		}

		if($_.ToString().Contains("Unable to connect to the remote server") -or $_.ToString().Contains("connected host has failed to respond") -or $_.ToString().Contains("actively refused it") -or $_.ToString().Contains("no such host is known"))
		{
			VerboseLogInfo Server appears unavailable or Kodi not accessible: [$currentKodiUri]
		}
		else
		{
			LogWarning Problem calling JSON request: [$currentKodiUri] [($_.ToString())]
		}
		if($DEBUG_MODE)
		{
			$_.ToString() | SafeOutFile -LiteralPath $debugLogFile
		}
		return
	}

	if($DEBUG_MODE)
	{
		foreach($currentLine in $results)
		{
			$currentLine | SafeOutFile -LiteralPath $debugLogFile
		}
	}

	$resultsJson = $results | ConvertFrom-Json
	if($resultsJson.error -ne $null)
	{
		LogError JSON ERROR RESPONSE [$currentKodiUri]: [($resultsJson.error.message)] [Request: ($jsonRequest)]
		return
	}

	return $resultsJson
}

#####################################
#
# FUNCTION MarkKodiMediaWatched
#
#####################################
function MarkKodiMediaWatched()
{
	MarkKodiMediaWatchState "1"
}

#####################################
#
# FUNCTION MarkKodiMediaUnwatched
#
#####################################
function MarkKodiMediaUnwatched()
{
	MarkKodiMediaWatchState "0"
}

#####################################
#
# FUNCTION MarkKodiMediaUnwatched
#
#####################################
function MarkKodiMediaUnwatched([string] $path)
{
	MarkKodiMediaWatchState "0" $path
}

#####################################
#
# FUNCTION MarkKodiMediaWatchState
#
#####################################
function MarkKodiMediaWatchState($state, [string] $path)
{
	if(-not (IsKodiConfigured))
	{
		return
	}

	$currentlyTrackedMedia = LoadTrackedMedia

	$state = $state.ToString()
	if($state -eq "0")
	{
		$queryState = "1"
	}
	if($state -eq "1")
	{
		$queryState = "0"
	}

	$cCurrentProgress = 0
	foreach ($currentKodiUri in $KODI_SERVERS.Keys)
	{
		$cCurrentProgress++
		if($state -eq "0")
		{
			OutputProgressBar ("Marking Kodi Media Unconsumed [$currentKodiUri]...") ($cCurrentProgress / $KODI_SERVERS.Keys.Count)
		}
		else
		{
			OutputProgressBar ("Marking Kodi Media Consumed [$currentKodiUri]...") ($cCurrentProgress / $KODI_SERVERS.Keys.Count)
		}

		foreach ($currentKodiMethod in @("VideoLibrary.GetMovies", "VideoLibrary.GetEpisodes"))
		{
			$jsonRequest = '{"jsonrpc": "2.0", "method": "' + $currentKodiMethod + '", "params": { "filter": {"field": "playcount", "operator": "is", "value": "' + $queryState + '"}, "properties" : ["playcount", "file"], "sort": { "order": "ascending", "method": "label", "ignorearticle": true } }, "id": 1}'

			$resultsJson = ExecuteKodiApi $currentKodiUri $jsonRequest
			if($resultsJson -eq $null)
			{
				continue
			}

			if($currentKodiMethod.Contains("Movies"))
			{
				$kodiMediaResults = $resultsJson.result.movies
			}
			else
			{
				$kodiMediaResults = $resultsJson.result.episodes
			}
			VerboseLogInfo RESULT COUNT [($kodiMediaResults.Count)]

			foreach ($currentJsonResult in $kodiMediaResults)
			{
				$kodiFilesToProcess = @{}
				if($currentJsonResult.file.StartsWith("stack://"))
				{
					if($path -ne $null)
					{
						#Explicitly passing in a path does not apply to stacks --- it shows up unbounded
						continue
					}
					MarkKodiStackWatchState $currentJsonResult.movieid $currentKodiUri $state

					$currentStack = $currentJsonResult.file.Replace("stack://", "")
					$currentStack = $currentStack.Trim()
					foreach ($currentStackItem in $currentStack.Split(' , '))
					{
						$kodiFilesToProcess.Add($currentStackItem.Trim(), 0) > $null
					}
				}
				else
				{
					$kodiFilesToProcess.Add($currentJsonResult.file.Trim(), $currentJsonResult) > $null
				}

				foreach ($currentKodiFile in $kodiFilesToProcess.Keys)
				{
					$currentTranslatedKodiFile = TranslateKodiPath $currentKodiFile

					if(IsInvalidPath $currentTranslatedKodiFile)
					{
						continue
					}

					if(IsExcludedPath $currentTranslatedKodiFile)
					{
						continue
					}

					if(($path -ne $null) -and -not (IsUnderPath $currentTranslatedKodiFile $path))
					{
						continue
					}

					if($currentlyTrackedMedia.ContainsKey($currentTranslatedKodiFile))
					{
						continue
					}

					DebugLogInfo CONSIDERING KODI RESULT [($currentKodiUri)] [($currentTranslatedKodiFile)] [Playcount: ($currentJsonResult.playcount)]

					if((($state -eq "1") -and (IsWatched $currentTranslatedKodiFile)) -or (($state -eq "0") -and -not (IsWatched $currentTranslatedKodiFile)))
					{
						if(IsFileCurrentlyOpen $currentTranslatedKodiFile)
						{
							continue
						}
						MarkKodiFileWatchState $currentKodiFile $currentKodiUri $state
					}
				}
			}
		}
	}
}

#####################################
#
# FUNCTION RemoveMissingKodiItems
#
#####################################
function RemoveMissingKodiItems()
{
	if(-not (IsKodiConfigured))
	{
		return
	}

	$cCurrentProgress = 0
	foreach ($currentKodiUri in $KODI_SERVERS.Keys)
	{
		$cCurrentProgress++
		OutputProgressBar ("Removing Missing Kodi Items [$currentKodiUri]...") ($cCurrentProgress / $KODI_SERVERS.Keys.Count)
		VerboseLogInfo Marking Kodi Content Consumed [$currentKodiUri] ...

		foreach ($currentKodiMethod in @("VideoLibrary.GetMovies", "VideoLibrary.GetEpisodes"))
		{
			$jsonRequest = '{"jsonrpc": "2.0", "method": "' + $currentKodiMethod + '", "params": { "properties" : ["playcount", "file"], "sort": { "order": "ascending", "method": "label", "ignorearticle": true } }, "id": 1}'

			$resultsJson = ExecuteKodiApi $currentKodiUri $jsonRequest
			if($resultsJson -eq $null)
			{
				continue
			}

			if($currentKodiMethod.Contains("Movies"))
			{
				$kodiMediaResults = $resultsJson.result.movies
			}
			else
			{
				$kodiMediaResults = $resultsJson.result.episodes
			}
			VerboseLogInfo RESULT COUNT [($kodiMediaResults.Count)]

			foreach ($currentJsonResult in $kodiMediaResults)
			{
				$kodiFilesToProcess = @{}
				if($currentJsonResult.file.StartsWith("stack://"))
				{
					# Ignore removing stacks for now.
					continue
				}
				$kodiFilesToProcess.Add($currentJsonResult.file.Trim(), $currentJsonResult) > $null

				foreach ($currentKodiFile in $kodiFilesToProcess.Keys)
				{
					$currentTranslatedKodiFile = TranslateKodiPath $currentKodiFile
					DebugLogInfo CONSIDERING KODI RESULT [($currentKodiUri)] [($currentTranslatedKodiFile)] [Playcount: ($currentJsonResult.playcount)]

					if(IsExcludedPath $currentTranslatedKodiFile)
					{
						continue
					}

					$canRemove = $false
					if(IsInvalidPath $currentTranslatedKodiFile)
					{
						$canRemove = $true
					}
					else
					{
						$itemToCheck = (Get-Item -LiteralPath $currentTranslatedKodiFile)
						$currentTranslatedKodiFileBaseName = [System.IO.Path]::GetFileNameWithoutExtension($currentTranslatedKodiFile)
						$fsCaseSenstiveCheck = (Get-ChildItem -File -LiteralPath ($itemToCheck.Directory.FullName) | Where-Object { $_.BaseName -ceq $currentTranslatedKodiFileBaseName })
						if($fsCaseSenstiveCheck -eq $null)
						{
							$canRemove = $true
						}
					}

					if($canRemove)
					{
						$resultJson = $kodiFilesToProcess[$currentKodiFile]
						$libraryId = "0"
						if($resultJson.episodeid -ne $null)
						{
							$libraryId = $resultJson.episodeid
						}
						if($resultJson.movieid -ne $null)
						{
							$libraryId = $resultJson.movieid
						}
						RemoveKodiLibraryItem $libraryId $currentTranslatedKodiFile $currentKodiUri
					}
				}
			}
		}
	}
}

##################################
#
# FUNCTION RemoveKodiLibraryItem
#
##################################
function RemoveKodiLibraryItem([String] $id, [String] $translatedKodiFilePath, [String] $kodiUri)
{
	LogInfo "REMOVE FROM KODI:" [$translatedKodiFilePath] [$kodiUri]

	if(IsMovie $translatedKodiFilePath)
	{
		$jsonRequest = '{"jsonrpc": "2.0", "method": "VideoLibrary.RemoveMovie", "params": { "movieid": ' + $id + ' }, "id": 1}'
	}
	else
	{
		$jsonRequest = '{"jsonrpc": "2.0", "method": "VideoLibrary.RemoveEpisode", "params": { "episodeid": ' + $id + ' }, "id": 1}'
	}
	ExecuteKodiApi $kodiUri $jsonRequest > $null
}

##################################
#
# FUNCTION MarkKodiStackWatchState
#
##################################
function MarkKodiStackWatchState([String] $id, [String] $kodiUri, [string] $state)
{
	if($id -eq "0")
	{
		$stateString = "Unwatched"
	}
	else
	{
		$stateString = "Watched"
	}
	LogInfo SET KODI STACK STATE: [$stateString] [$kodiUri] [$id]

	$jsonRequest = '{"jsonrpc": "2.0", "method": "VideoLibrary.SetMovieDetails", "params": { "movieid": ' + $id + ', "playcount": ' + $state + ' }, "id": "libMovies"}'
	ExecuteKodiApi $kodiUri $jsonRequest > $null
}

#################################
#
# FUNCTION MarkKodiFileWatchState
#
#################################
function MarkKodiFileWatchState([String] $target, [String] $kodiUri, [string] $state)
{
	if($id -eq "0")
	{
		$stateString = "Unwatched"
	}
	else
	{
		$stateString = "Watched"
	}
	LogInfo SET KODI STATE: [$stateString] [$kodiUri] [$target]

	$jsonRequest = '{"jsonrpc": "2.0", "method": "Files.SetFileDetails", "params": { "file": "' + $target + '", "playcount": ' + $state + ', "media": "video" }, "id": "libMovies"}'
	ExecuteKodiApi $kodiUri $jsonRequest > $null
}

#######################################
#
# FUNCTION OrganizeWatchedEpisodicMedia
#
#######################################
function OrganizeWatchedEpisodicMedia()
{
	if((IsInvalidPath (DeriveWatchedPath $DOWNLOADED_TV_PATH)) -and (IsInvalidPath (DeriveWatchedPath $DOWNLOADED_MOVIES_PATH)) -and (IsInvalidPath (DeriveWatchedPath $DOWNLOADED_PODCASTS_PATH)))
	{
		LogVerbose No valid consumed paths to organize.
		return
	}

	if(IsInvalidPath $TRACKING_CSV)
	{
		VerboseLogInfo No media is currently being tracked.
		return
	}

	$tmpFile = GenerateTmpFileName $TRACKING_CSV
	DeleteItem $tmpFile

	$inputCSV = Import-Csv $TRACKING_CSV

	$swapCsv = $false
	$cCurrentProgress = 0
	foreach ($currentEntry in $inputCSV)
	{
		$cCurrentProgress++
		OutputProgressBar ("Moving Consumed Media...") ($cCurrentProgress / ($inputCSV.Count + 1))

		$fileName = $currentEntry.FileName
		$timeStamp = [System.DateTime]::Parse($currentEntry.TimeStamp)

		if((IsInvalidPath $fileName) -or (IsExcludedPath $fileName) -or (IsWatched $fileName))
		{
			#NO LONGER NEED TO TRACK --- MOVED OR DELETED
			LogInfo FORGETTING TRACK: [$fileName]
			$swapCsv = $true
			continue
		}
		$file = Get-Item -LiteralPath $fileName

		if((Get-ChildItem -LiteralPath $fileName).Extension -NotIn $MEDIA_EXTENSIONS)
		{
			LogInfo EXCLUDED FILE EXTENSION: [$fileName]
			$swapCsv = $true
			continue
		}

		AddCsvEntry $currentEntry $tmpFile

		$hoursToTrack = LoadSubscriptionOverrideVariable $file "TRACK_AGE_HOURS_OVERRIDE"
		if($hoursToTrack -eq $null)
		{
			$hoursToTrack = $TRACK_AGE_HOURS
		}

		$totalHours = ((Get-Date) - $timeStamp).TotalHours
		if($totalHours -le $hoursToTrack)
		{
			#NOT READY TO MARK AS WATCHED
			$formattedTotalHours = [String]::Format("{0:N2}", $totalHours)
			$formatedHoursToGo = [String]::Format("{0:N2}", ($hoursToTrack - $totalHours))
			VerboseLogInfo TRACKING: $fileName [TrackedHours: $formattedTotalHours] [HoursUntilWatched: $formatedHoursToGo]
			continue
		}

		#MOVE SHOW
		$subscriptionPurgeOverride = LoadSubscriptionOverrideVariable $file "PURGE_ON_WATCHED"
		if(($subscriptionPurgeOverride -ne $null) -and $subscriptionPurgeOverride)
		{
			$targetWatchedDirectory = $PURGE_PATH
		}
		else
		{
			$targetWatchedDirectory = CalculateWatchedFolder $fileName
		}

		if($targetWatchedDirectory -eq (DeriveWatchedPath $targetWatchedDirectory) -and -not (IsMovie $fileName))
		{
			LogWarning Consumed media appears unsorted: [($file.FullName.Replace($file.Directory.FullName, $targetWatchedDirectory))]
		}

		MoveMediaAndRelatedFiles $fileName $targetWatchedDirectory
	}

	if($swapCsv)
	{
		SwapFileWithTmp $TRACKING_CSV
	}
	DeleteItem $tmpFile
}

###################################
#
# FUNCTION RenameMediaFile
#
###################################
function RenameMediaFile($originalFileFullPath, $newFileFullPath)
{
	if(FailedStringValidation $originalFileFullPath) { return }
	if(FailedStringValidation $newFileFullPath) { return }

	if(-not (CanMediaBeRenamed $originalFileFullPath))
	{
		return
	}

	$originalFile = Get-Item -LiteralPath $originalFileFullPath
	RenameFile ($originalFile.FullName) $newFileFullPath

	if(IsInvalidPath $DOWNLOADED_PODCASTS_PATH)
	{
		return
	}

	if(IsAudio $originalFile.FullName)
	{
		return
	}

	$currentOriginalAudioCopies = Get-ChildItem -LiteralPath $DOWNLOADED_PODCASTS_PATH -Filter ($originalFile.BaseName + "*") | Where-Object { -not (IsWatched $_.FullPath) }
	foreach ($currentOriginalAudioCopy in $currentOriginalAudioCopies)
	{
		if($currentOriginalAudioCopy.FullName -eq $newFileFullPath)
		{
			continue
		}

		if(-not (CanMediaBeRenamed ($currentOriginalAudioCopy.FullName)))
		{
			continue
		}

		$newAudioCopy = [System.IO.Path]::GetFileNameWithoutExtension($newFileFullPath) + $currentOriginalAudioCopy.Extension
		$newAudioCopy = [System.IO.Path]::Combine($currentOriginalAudioCopy.Directory.FullName, $newAudioCopy)
		RenameFile ($currentOriginalAudioCopy.FullName) $newAudioCopy
	}
}

###################################
#
# FUNCTION MoveMediaAndRelatedFiles
#
###################################
function MoveMediaAndRelatedFiles($watchedMedia, $targetFolder)
{
	if(FailedStringValidation ($watchedMedia)) { return }
	if(FailedStringValidation ($targetFolder)) { return }

	$searchPattern = [System.IO.Path]::GetFileNameWithoutExtension($watchedMedia) + ".*"
	$isAudio = IsAudio $watchedMedia

	if($isAudio -and (IsStringDefined($DOWNLOADED_TV_PATH)))
	{
		foreach ($currentRelatedFile in (Get-ChildItem -Recurse -FollowSymlink -LiteralPath $DOWNLOADED_TV_PATH -Filter $searchPattern))
		{
			if((IsUnderPath $currentRelatedFile.FullName $targetFolder) -or (IsUnderPath $currentRelatedFile.FullName $PURGE_PATH) -or (IsFileCurrentlyOpen $currentRelatedFile.FullName))
			{
				continue
			}
			MoveFile ($currentRelatedFile.FullName) $targetFolder
			$Script:CURRENT_SCRIPT_CLEANUP_NEEDED = $true
		}
	}

	if((-not $isAudio) -and (IsStringDefined($DOWNLOADED_PODCASTS_PATH)))
	{
		foreach ($currentRelatedFile in (Get-ChildItem -Recurse -FollowSymlink -LiteralPath $DOWNLOADED_PODCASTS_PATH -Filter $searchPattern))
		{
			if((IsUnderPath $currentRelatedFile.FullName $targetFolder) -or (IsUnderPath $currentRelatedFile.FullName $PURGE_PATH) -or (IsFileCurrentlyOpen $currentRelatedFile.FullName))
			{
				continue
			}
			MoveFile ($currentRelatedFile.FullName) $targetFolder
			$Script:CURRENT_SCRIPT_CLEANUP_NEEDED = $true
		}
	}

	foreach ($currentSubtitleExtension in $SUBTITLE_EXTENSIONS)
	{
		$potentialSubtitleFile = $watchedMedia.Replace(([System.IO.Path]::GetExtension($watchedMedia)), $currentSubtitleExtension)
		if(IsValidPath $potentialSubtitleFile)
		{
			MoveFile $potentialSubtitleFile $targetFolder
			$Script:CURRENT_SCRIPT_CLEANUP_NEEDED = $true
		}
	}

	if(IsValidPath $watchedMedia)
	{
		if(-not (IsFileCurrentlyOpen $watchedMedia))
		{
			MoveFile $watchedMedia $targetFolder
			$Script:CURRENT_SCRIPT_CLEANUP_NEEDED = $true
		}
	}
}

##################################
#
# FUNCTION MatchFolderOverride
#
##################################
function MatchFolderOverride($showToMatch, $overrideList)
{
	if(FailedStringValidation ($showToMatch)) { return }

	if($overrideList.ContainsKey($showToMatch))
	{
		return ($overrideList[$showToMatch])
	}

	return $null
}

##################################
#
# FUNCTION OrganizeDownloadedMedia
#
##################################
function OrganizeDownloadedMedia($RootPath)
{
	$RootPath = LoadPath $RootPath
	if((IsNotUnderPath $RootPath $DOWNLOADED_TV_PATH) -and (IsNotUnderPath $RootPath $DOWNLOADED_PODCASTS_PATH))
	{
		LogWarning Invalid root path to organize: [$RootPath]
		return
	}
	OrganizeDownloadedEpisodicMedia $RootPath
}

##################################
#
# FUNCTION OrganizeDownloadedEpisodicMedia
#
##################################
function OrganizeDownloadedEpisodicMedia($RootPath)
{
	$showCountDictionary = @{}
	$filesToConsider = $null
	$rootOrganizationPath = $null

	if(($RootPath -eq $null) -or ($RootPath.Length -le 0))
	{
		$rootOrganizationPath = $DOWNLOADED_TV_PATH
		$filesToConsider = $DOWNLOADED_TV_PATH
	}
	elseif(IsInvalidPath $RootPath)
	{
		LogWarning No valid downloaded path is defined [$RootPath].
		return
	}
	elseif($RootPath -eq $DOWNLOADED_TV_PATH)
	{
		$rootOrganizationPath = $DOWNLOADED_TV_PATH
		$filesToConsider = $DOWNLOADED_TV_PATH
	}
	elseif($RootPath -eq $DOWNLOADED_PODCASTS_PATH)
	{
		$rootOrganizationPath = $DOWNLOADED_PODCASTS_PATH
		$filesToConsider = $DOWNLOADED_PODCASTS_PATH
	}
	else
	{
		if($Force -and (IsWatched $RootPath))
		{
			if((Get-Item -LiteralPath $RootPath).Attributes -Match 'Directory')
			{
				$rootOrganizationPath = (Get-Item -LiteralPath $RootPath).Parent.FullName
			}
			else
			{
				$rootOrganizationPath = DeriveWatchedPath $RootPath
			}
			$filesToConsider = $RootPath
		}
		elseif((IsUnderPath $RootPath $DOWNLOADED_TV_PATH) -and (IsNotUnderPath $RootPath $DOWNLOADED_PODCASTS_PATH))
		{
			$rootOrganizationPath = $DOWNLOADED_TV_PATH
			$filesToConsider = $RootPath
		}
		elseif((IsUnderPath $RootPath $DOWNLOADED_PODCASTS_PATH))
		{
			$rootOrganizationPath = $DOWNLOADED_PODCASTS_PATH
			$filesToConsider = $RootPath
		}
		else
		{
			if((Get-Item -LiteralPath $RootPath).Attributes -Match 'Directory')
			{
				$rootOrganizationPath = (Get-Item -LiteralPath $RootPath).Parent.FullName
			}
			else
			{
				$rootOrganizationPath = (Get-Item -LiteralPath $RootPath).Directory.FullName
			}
			$filesToConsider = $RootPath
		}
	}

	if($rootOrganizationPath -eq $null)
	{
		LogError No root path could be calculated.
		return
	}

	if($filesToConsider -eq $null)
	{
		LogError No target files could be determined.
		return
	}

	VerboseLogInfo Root Organizational Path: [$rootOrganizationPath]

	$currentlyTrackedMedia = LoadTrackedMedia
	$currentlyRecordingMedia = LoadRecordingMedia

	if($Force)
	{
		$currentlyRenamedMedia = @{}
		$currentAdRemovedMedia = @{}
	}
	else
	{
		$currentlyRenamedMedia = LoadRenamedMedia
		$currentAdRemovedMedia = LoadAdRemovedMedia
	}

	if($EMBED_SUBTITLES)
	{
		foreach ($possibleProblem in (Get-ChildItem -LiteralPath $filesToConsider -Recurse -FollowSymlink -File | Where-Object { $_.Extension -In $SUBTITLE_EXTENSIONS }))
		{
			LogWarning Orphaned subtitles not embedded: [($possibleProblem.FullName)]
		}
	}

	if($EMBED_THUMBNAIL)
	{
		foreach ($possibleProblem in (Get-ChildItem -LiteralPath $filesToConsider -Recurse -FollowSymlink -File | Where-Object { $_.Extension -In $THUMBNAIL_EXTENSIONS }))
		{
			LogWarning Orphaned thumbnail not embedded: [($possibleProblem.FullName)]
		}
	}

	if(-not $Force)
	{
		foreach ($possibleProblem in (Get-ChildItem -LiteralPath $filesToConsider -Recurse -FollowSymlink -File | Where-Object { $_.BaseName -Match "(\.s\d)|(\.nosubs)|(\.pad)|(\.embedded)|(\.concat)|(\.oldsubs)|(\.ad)|(\.segments)|(\.metadata)" }))
		{
			LogWarning Possible incomplete media detected: [($possibleProblem.FullName)]
		}

		foreach ($possibleProblem in (Get-ChildItem -LiteralPath $filesToConsider -Recurse -FollowSymlink -File | Where-Object { $_.BaseName.ToLower() -match $DUPLICATE_PATTERN }))
		{
			LogWarning Possible duplicate detected: [($possibleProblem.FullName)]
		}
	}

	$newShows = Get-ChildItem -LiteralPath $filesToConsider -Recurse -FollowSymlink -File
	$cCurrentProgress = 0

	foreach ($newShow in $newShows)
	{
		$cCurrentProgress++
		OutputProgressBar ("Organizing Media [$filesToConsider]...") ($cCurrentProgress / $newShows.Count)
		$targetToWatchDirectory = $null

		if($newShow.Extension -NotIn ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
		{
			VerboseLogInfo EXCLUDED FILE EXTENSION: [($newShow.FullName)]
			continue
		}

		$isAudio = IsAudio $newShow.FullName
		if((-not $Force) -and ($rootOrganizationPath -ne $DOWNLOADED_PODCASTS_PATH) -and $isAudio -and (IsUnderPath ($newShow.FullName) $DOWNLOADED_PODCASTS_PATH))
		{
			VerboseLogInfo EXCLUDED AUDIO: [($newShow.FullName)]
			continue
		}

		if((-not $Force) -and (IsExcludedPath $newShow.FullName))
		{
			VerboseLogInfo EXCLUDED DIR: [($newShow.FullName)]
			continue
		}

		if((-not $Force) -and (IsMovie $newShow.FullName))
		{
			VerboseLogInfo EXCLUDED MOVIE: [($newShow.FullName)]
			continue
		}

		$isWatched = IsWatched $newShow.FullName
		if((-not $Force) -and $isWatched)
		{
			VerboseLogInfo EXCLUDED CONSUMED: [($newShow.FullName)]
			continue
		}

		if($currentlyRecordingMedia.ContainsKey($newShow.BaseName))
		{
			VerboseLogInfo EXCLUDED RECORDING SHOW: [($newShow.FullName)]
			continue
		}

		if(IsInvalidPath ($newShow.FullName))
		{
			LogWarning Media no longer found. Skipping... [($newShow.FullName)]
			continue
		}

		#REMEMBER ORIGINAL FILE
		$originalFile = $newShow

		#RENAME SHOW
		$hasBeenRenamed = HasFileBeenRenamed $newShow.FullName $currentlyRenamedMedia
		if(-not $hasBeenRenamed)
		{
			if($Force -or ((UseTvDB) -and (IsValidPath $newShow.FullName)))
			{
				$tvDbId = SearchTvDbForSeries $newShow $false
				if($tvDbId -ne $null)
				{
					$targetRenamedShow = SmartRefreshMediaInfo $newShow $tvDbId

					if(($originalFile.FullName -cne $targetRenamedShow) -and (IsValidPath $targetRenamedShow))
					{
						$newShow = Get-Item -LiteralPath $targetRenamedShow
						$hasBeenRenamed = $true
						if(-not $Force)
						{
							$currentlyRenamedMedia = LoadRenamedMedia
						}
					}
				}
				elseif((-not $isAudio) -and -not (IsFileCurrentlyOpen ($newShow.FullName)))
				{
					MoveIfProbablyMovie $newShow
				}
			}
		}

		$showNameMatched = $false;
		$newShowName = ExtractShowName $newShow

		if($Force -and $isWatched)
		{
			$targetToWatchDirectory = $newShow.Directory.FullName
			$showNameMatched = $true;
		}
		else
		{
			$targetToWatchDirectory = $rootOrganizationPath
		}

		#TRY DYNAMICALY DETERMINING SHOW NAME BY NAME
		if((-not $showNameMatched) -and ($newShowName -ne $null))
		{
			$override = MatchFolderOverride $newShowName $ORGANIZE_PATH_OVERRIDE
			if($override -ne $null)
			{
				$showNameMatched = $true;
				$targetToWatchDirectory = $override
			}
		}

		if(-not $showNameMatched)
		{
			$isArchived = (IsStringDefined $ARCHIVE_FOLDER_NAME) -and ($newShow.Directory.FullName.EndsWith($ARCHIVE_FOLDER_NAME))
			if($isArchived)
			{
				$targetToWatchDirectory = $newShow.Directory.FullName
			}
			elseif($newShowName -ne $null)
			{
				$aggregateEpisode = $true
				if($EPISODES_AGGREGATE_THRESHOLD -gt 1)
				{
					if($newShowName -ne $null)
					{
						$numberOfShows = GetToWatchShowCount $newShowName $showCountDictionary $rootOrganizationPath
						if($numberOfShows -lt $EPISODES_AGGREGATE_THRESHOLD)
						{
							$aggregateEpisode = $false
						}
					}
				}

				if($aggregateEpisode)
				{
					$targetToWatchDirectory = [System.IO.Path]::Combine($rootOrganizationPath, $newShowName)
				}
			}
		}

		# Do not move on force refresh of watched
		if($isWatched -and $Force)
		{
			continue
		}

		if($targetToWatchDirectory -eq $null)
		{
			continue
		}

		$newShowAirDate = ExtractShowAirDate $newShow
		$newShowAirDate = $newShowAirDate[0]

		if(-not $isArchived)
		{
			if(IsStringDefined $ARCHIVE_FOLDER_NAME)
			{
				$archiveAgeOverride = LoadSubscriptionOverrideVariable $newShow "ARCHIVE_AGE_DAYS_OVERRIDE"
				if(($archiveAgeOverride -ne $null) -and (((get-date).Date - $newShowAirDate).TotalDays -gt $archiveAgeOverride))
				{
					$targetToWatchDirectory = [System.IO.Path]::Combine($targetToWatchDirectory, $ARCHIVE_FOLDER_NAME)
				}
				else
				{
					#MOVE OLDER DAILY SHOWS TO ARCHIVE
					$isTopicalShow = IsTopicalShow $newShow
					if($isTopicalShow -and ($targetToWatchDirectory -ne $rootOrganizationPath) -and (((get-date).Date - $newShowAirDate).TotalDays -gt $ARCHIVE_AGE_DAYS) -and (IsNotUnderPath ($newShow.FullName) ($DOWNLOADED_PODCASTS_PATH)))
					{
						$targetToWatchDirectory = [System.IO.Path]::Combine($targetToWatchDirectory, $ARCHIVE_FOLDER_NAME)
					}
				}
			}

			#MOVE DAILY SHOWS TO DEDICATED FOLDER
			$isDailyShow = IsDailyShow $newShow
			if($isDailyShow -and (IsStringDefined $DAILY_FOLDER_NAME))
			{
				$targetToWatchDirectory = [System.IO.Path]::Combine($rootOrganizationPath, $DAILY_FOLDER_NAME)
			}

			#DETECT SEASON FOR MORE TARGETED DIRECTORY
			$seasonName = ExtractShowSeason $newShow
			$createSeasonedFolder = $false
			if(($seasonName -ne $null) -and ($targetToWatchDirectory -ne $rootOrganizationPath))
			{
				$createSeasonedFolder = IsSeasonedFolder $newShow $targetToWatchDirectory
			}

			if($createSeasonedFolder)
			{
				if($isAudio -and ($newShowName -ne $null))
				{
					$targetToWatchDirectory = [System.IO.Path]::Combine($targetToWatchDirectory, $newShowName + " - " + $seasonName)
				}
				else
				{
					$targetToWatchDirectory = [System.IO.Path]::Combine($targetToWatchDirectory, $seasonName)
				}
			}
		}

		VerboseLogInfo [($newShow.FullName)] [Show: $newShowName] [Season: $seasonName] [Air: ($newShowAirDate.ToShortDateString())] [Topical: $isTopicalShow] [Target Path: ($targetToWatchDirectory)]

		#MOVE SHOW
		if($newShow.Directory.FullName -ne $targetToWatchDirectory)
		{
			$targetMovedShow = [System.IO.Path]::Combine($targetToWatchDirectory, $newShow.Name)
			$canContinue = $false
			for ($cTries = 5; $cTries -gt 0; $cTries--)
			{
				if(IsFileCurrentlyOpen ($newShow.FullName))
				{
					SleepFor 1
				}
				else
				{
					$canContinue = $true
					break
				}
			}
			if($canContinue)
			{
				MoveFile $newShow.FullName $targetToWatchDirectory
				$Script:CURRENT_SCRIPT_CLEANUP_NEEDED = $true
				if(IsValidPath $targetMovedShow)
				{
					$newShow = Get-Item -LiteralPath $targetMovedShow
				}
			}
			else
			{
				continue
			}
		}

		if(IsInvalidPath $newShow.FullName)
		{
			continue
		}

		#IF SHOW ALREADY WAS RENAMED, REMEMBER
		if($hasBeenRenamed -and (IsValidPath $newShow.FullName) -and -not $currentlyRenamedMedia.ContainsKey($newShow.BaseName))
		{
			TrackRenamedMedia ($newShow.FullName)
		}

		#IF SHOW WAS ALREADY TRACKED BEFORE, CONTINUE TO TRACK
		if($currentlyTrackedMedia.ContainsKey($originalFile.FullName) -and ($originalFile.FullName -ne $newShow.FullName))
		{
			$timeStamp = [System.DateTime]::Parse($currentlyTrackedMedia[($originalFile.FullName)])
			TrackWatchedMedia $newShow.FullName $timeStamp
		}

		#IF SHOW ALREADY HAD ADS REMOVED, REMEMBER
		if($currentAdRemovedMedia.ContainsKey($originalFile.BaseName) -and ($originalFile.FullName -ne $newShow.FullName))
		{
			TrackAdRemovedMedia ($newShow.FullName)
		}

		#DID WE ALREADY WATCH THIS SHOW?
		if((-not $Force) -and ($newShow.Extension -In $MEDIA_EXTENSIONS))
		{
			$futureWatchedTarget = CalculateWatchedFolder ($newShow.FullName)
			if((IsValidPath $futureWatchedTarget) -and (IsNotUnderPath $futureWatchedTarget $PURGE_PATH))
			{
				$possibleWatchedShow = [System.IO.Path]::GetFileNameWithoutExtension($newShow.FullName) + ".*"
				if((Get-ChildItem -LiteralPath $futureWatchedTarget -Filter $possibleWatchedShow).Count -gt 0)
				{
					LogWarning SHOW APPEARS ALREADY CONSUMED: [($newShow.FullName)]
				}
			}
		}
	}
}

######################################
#
# FUNCTION OrganizeMediaAlbumDirectory
#
######################################
function OrganizeMediaAlbumDirectory($targetDirectory)
{
	if(IsInvalidPath $targetDirectory) { return }

	$directories =  Get-ChildItem -LiteralPath $targetDirectory -Recurse -FollowSymlink -Directory | Sort-Object
	if($directories.Count -eq 0)
	{
		$directories =  Get-Item -LiteralPath $targetDirectory
	}
	foreach($currentOpenedDirectory in $directories)
	{
		$currentPlayList = $currentOpenedDirectory.FullName + "\" + $currentOpenedDirectory.Name + ".m3u"
		$currentInfoFile = ".\" + $currentOpenedDirectory.Name + ".txt"
		$totalAlbumLength = [Timespan]::Zero

		if(IsValidPath $currentPlayList)
		{
			LogWarning REFRESHING: [$currentPlayList]
			DeleteItem $currentPlayList
		}

		if(IsValidPath $currentInfoFile)
		{
			LogWarning REFRESHING: [$currentInfoFile]
			DeleteItem $currentInfoFile
		}

		#Write playlist header
		if(IsInvalidPath $currentPlayList)
		{
			CreateDirInPath $currentPlayList
			"#EXTM3U" | SafeOutFile -LiteralPath $currentPlayList
		}

		#Write info header
		if($VERBOSE_MODE -and (IsInvalidPath $currentInfoFile))
		{
			CreateDirInPath $currentOpenedDirectory
			$currentAlbumInfoLine = [String]::Format("Album Mix {0} - Playlist {1}", $currentOpenedDirectory.Name, $currentPlayList)
			$currentAlbumInfoLine | SafeOutFile -LiteralPath $currentInfoFile
			[String]::new('-', $currentAlbumInfoLine.Length) | SafeOutFile -LiteralPath $currentInfoFile
			"" | SafeOutFile -LiteralPath $currentInfoFile
		}

		$files = Get-ChildItem -Path $currentOpenedDirectory -File | Sort-Object
		$cCurrentProgress = 0
		foreach($currentOpenedFile in $files)
		{
			$cCurrentProgress++
			OutputProgressBar ("Generating Playlist Album: [" + $currentOpenedDirectory.Name + ".m3u" + "]...") ($cCurrentProgress/$files.Count)

			if($currentOpenedFile.Name -eq "desktop.ini")
			{
				DeleteItem ($currentOpenedFile.FullName)
				continue
			}

			if(-not (IsAudio $currentOpenedFile.FullName))
			{
				VerboseLogInfo Not audio: [($currentOpenedFile.FullName)]
				continue
			}

			if($currentOpenedFile.Extension -ne ".mp3")
			{
				ConvertMediaToMpX ($currentOpenedFile.FullName)
				$convertedFile = $currentOpenedFile.FullName.Replace($currentOpenedFile.Extension, ".mp3")
				if(IsValidPath $convertedFile)
				{
					$currentOpenedFile = Get-Item -Path $convertedFile
				}
				else
				{
					continue
				}
			}

			$metadata = ExtractMetadataTags($currentOpenedFile.FullName)
			$newMetadata = ExtractMetadataTags($currentOpenedFile.FullName)

			$trackTitle = $metadata["title"]
			if($trackTitle -eq $null) { $trackTitle = [String]::Empty }

			$albumName = $metadata["Album"]
			if($albumName -eq $null) { $albumName = [String]::Empty }

			$albumArtist = $metadata["album_artist"]
			if($albumArtist -eq $null) { $albumArtist = [String]::Empty }

			$contributing_Artist = $metadata["artist"]
			if($contributing_Artist -eq $null) { $contributing_Artist = [String]::Empty }

			$trackNumber = $metadata["Track"]
			if($trackNumber -eq $null) { $trackNumber = [String]::Empty }

			$trackLength = ReturnDuration ($currentOpenedFile.FullName)

			#Update total album length
			$totalAlbumLength = $totalAlbumLength + $trackLength

			#Extract track from filename
			$parsedTrackNumberPattern = "(?<track>\d+)(?:\s|-|_)+.+"
			if($currentOpenedFile.BaseName -match $parsedTrackNumberPattern)
			{
				$parsedTrackNumber = $matches["track"]

				if($trackNumber.TrimStart("0") -ne $parsedTrackNumber.TrimStart("0"))
				{
					$newMetadata["Track"] = $parsedTrackNumber
				}
			}
			else
			{
				LogWarning Not explicitly ordered: [($currentOpenedFile.Name)]
			}

			if($currentOpenedFile.Name.Contains("Unknown Track"))
			{
				if((IsStringDefined $trackTitle) -and (-not $trackTitle.Contains("Unknown Track")))
				{
					$newFileName = $trackTitle | BeautifyAndDemoteString -ForceQuiet | ConvertStringToASCII -ForceQuiet | ConvertStringToFileSystem -ForceQuiet
					$newFileName = $currentOpenedFile.FullName -replace "Unknown Track \d+",$newFileName
					RenameFile ($currentOpenedFile.FullName) $newFileName
					if(IsValidPath $newFileName)
					{
						$currentOpenedFile = Get-Item -Path $newFileName
					}
					else
					{
						LogError Problem renaming: [($currentOpenedFile.FullName)]
						continue
					}
				}
				else
				{
					LogWarning Fix untitled track: [($currentOpenedFile.Name)]
				}
			}

			#Promote Album name from directory name if different from file metadata
			if($albumName -ne $currentOpenedFile.Directory.Name)
			{
				$newMetadata["Album"] = $currentOpenedFile.Directory.Name
			}

			#Software will only group if the artist is the same for a mix. Use contributing_artist instead for this case, even if it
			# squashes the real contributing artist
			if( (-not $albumArtist.Contains("Various artists")) -and (-not $albumArtist.Contains("Soundtrack")) )
			{
				if(IsStringDefined $albumArtist)
				{
					$newMetadata["artist"] = $albumArtist
				}
				$newMetadata["album_artist"] = "Various artists"
			}

			#Scrub unneeded Zune / Windows Media Player / Amazon Info
			foreach($currentTag in @(	"id3v2_priv.ZuneMediaID",
										"id3v2_priv.WM/WMContentID",
										"id3v2_priv.WM/MediaClassSecondaryID",
										"id3v2_priv.ZuneAlbumMediaID",
										"id3v2_priv.WM/Provider",
										"id3v2_priv.WM/MediaClassPrimaryID",
										"id3v2_priv.WM/WMCollectionGroupID",
										"id3v2_priv.WM/UniqueFileIdentifier",
										"id3v2_priv.WM/WMCollectionID",
										"id3v2_priv.ZuneAlbumArtistMediaID",
										"id3v2_priv.ZuneCollectionID",
										"id3v2_priv.www.amazon.com"
									)	)
			{
				if($newMetadata.ContainsKey($currentTag))
				{
					$newMetadata.Remove($currentTag)
				}
			}

			#Do we need to refresh metadata?
			$modifyMetadata = $false
			if($metadata.Count -ne $newMetadata.Count)
			{
				$modifyMetadata = $true
			}
			else
			{
				foreach($currentSourceTag in $newMetadata.Keys)
				{
					if($metadata.ContainsKey($currentSourceTag) -and ($metadata[$currentSourceTag] -cne $newMetadata[$currentSourceTag]))
					{
						$modifyMetadata = $true
					}
				}
			}

			if($modifyMetadata)
			{
				$metadataTargetPath = $CONVERSION_TMP + "\" + [System.IO.Path]::GetRandomFileName()
				$metadataTarget = $metadataTargetPath + "\" + ($currentOpenedFile.Name.Replace($currentOpenedFile.Extension, ($currentOpenedFile.Extension + $METADATA_EXTENSION)))
				$targetMedia = $metadataTargetPath + "\" + $currentOpenedFile.Name
				CreateDirInPath $metadataTarget

				";FFMETADATA1" | SafeOutFile -LiteralPath $metadataTarget
				foreach($currentSourceTag in $newMetadata.Keys)
				{
					$metaString = $currentSourceTag + "=" + ($newMetadata[$currentSourceTag])
					$metaString | SafeOutFile -LiteralPath $metadataTarget
				}

				if(IsInvalidPath $metadataTarget)
				{
					LogError Metadata file not persisted: [$metadataTarget]
					return
				}

				ConvertMedia ($currentOpenedFile.FullName) $targetMedia

				if(IsInvalidPath $targetMedia)
				{
					LogError Metadata not copied to: [$targetMedia]
					return
				}

				PurgeItem $currentOpenedFile.FullName

				MoveFile $targetMedia ($currentOpenedFile.Directory.FullName)
				if(IsValidPath $targetMedia)
				{
					LogWarning Original Conversion Media still exists: [$targetMedia]
				}
			}

			$currentOpenedFile.Name | SafeOutFile -LiteralPath $currentPlayList
			if($VERBOSE_MODE)
			{
				[String]::Format("{0} \ By {1} \ {2}", $currentOpenedFile.Name, $albumArtist, $trackLength) | SafeOutFile -LiteralPath $currentInfoFile
			}
			LogInfo PLAYLIST ALBUM ITEM: [($currentOpenedFile.Directory.Name)] [($currentOpenedFile.BaseName)] [$contributing_Artist] [([String]::Format("{0:mm\:ss}", $trackLength))]
		}

		if(IsValidPath $currentInfoFile)
		{
			"" | SafeOutFile -LiteralPath $currentInfoFile
			"TOTAL LENGTH: " + $totalAlbumLength | SafeOutFile -LiteralPath $currentInfoFile
			LogInfo "TOTAL LENGTH: " [($currentOpenedDirectory.Name)] [$totalAlbumLength]
		}
	}
}

#################################
#
# FUNCTION ShouldTriggerScript
#
#################################
function ShouldTriggerScript([System.DateTime] $LastTriggered, [int] $Frequency, [System.DateTime] $now)
{
	if($LastTriggered -eq [System.DateTime]::MinValue)
	{
		return $false
	}

	if($LastTriggered -gt $now)
	{
		return $false
	}

	if(($Frequency -eq $null) -or ($Frequency -le 0))
	{
		return $false
	}

	$totalMinutesPassed = [System.Math]::Round(($now - $LastTriggered).TotalMinutes)
	return ($totalMinutesPassed -ge $Frequency)
}

#################################
#
# FUNCTION GetNextExecutionTime
#
#################################
function GetNextExecutionTime([System.DateTime] $last, [int] $interval)
{
	if($last -eq [System.DateTime]::MinValue)
	{
		return $null
	}

	if($interval -le 0)
	{
		LogError Invalid polling interval: [$interval]
		return
	}

	$now = get-date
	if(($now - $last).TotalSeconds -le 1)
	{
		$last = $now
	}
	$next = $last

	while ($now -ge $next)
	{
		$next = $next.AddMinutes($interval)
	}

	return $next
}

#################################
#
# FUNCTION ExecutePollExecutable
#
#################################
function ExecutePollExecutable($currentScriptExtension)
{
	$Error.Clear()

	if($currentScriptExtension.Contains(' '))
	{
		$executable = $currentScriptExtension.Substring(0, $currentScriptExtension.IndexOf(" "))
	}
	else
	{
		$executable = $currentScriptExtension
	}

	if(IsInvalidPath $executable)
	{
		LogWarning Polling executable not found: [$executable]. Skipping...
		return
	}

	$start = get-date
	try
	{
		$executable = Get-Item -LiteralPath $executable

		if($executable.Directory.FullName -eq $SUBSCRIPTION_FOLDER)
		{
			LockScriptConcurrency
			CheckMediaSubscription $currentScriptExtension
			$end = get-date
			LogCommandExecution [CHECK SUBSCRIPTION: $currentScriptExtension] [($end - $start)]
		}
		elseif($executable.Extension -eq ".ps1")
		{
			ExecutePowerShellScript $currentScriptExtension
			#ExecuteCommand $currentScriptExtension -WhatIf:($WHATIF_MODE) -Verbose:($VERBOSE_MODE) -Debug:($DEBUG_MODE)
		}
		elseif($executable.Extension -eq ".py")
		{
			LockScriptConcurrency
			ExecuteCommand $PYTHON_BINARY $currentScriptExtension
		}
		else
		{
			# ASSUME EXE
			ExecuteCommand $currentScriptExtension > $null
		}
	}
	catch
	{
		$end = get-date
		LogWarning [EXECUTION ERROR: $currentScriptExtension] [($end - $start)]
		LogCommandExecution [EXECUTION ERROR: $currentScriptExtension] [($end - $start)]
		if($SHOW_ERROR_EXCEPTIONS)
		{
			HandleError $_
		}
		else
		{
			LogError $_
		}
	}
	finally
	{
		if(IsValidPath $CONCURRENCY_FILE)
		{
			$lastExecution = (Get-Item -LiteralPath $CONCURRENCY_FILE).LastWriteTime
			if($lastExecution -ge $start)
			{
				ReleaseScriptConcurrency
			}
		}
	}
}

#################################
#
# FUNCTION StartMediaPolling
#
#################################
function StartMediaPolling()
{
	$DEBUG_POLLING = $false

	if(($POLLING_SCRIPTS -eq $null) -or ($POLLING_SCRIPTS.Count -eq 0))
	{
		LogError "No correct script extensions are configured."
		return
	}

	# Test to see if we can set the Window Title, which is needed so other scripts see we're polling.
	#  Windows Terminal's tabs can hide
	$processFound = $false
	$oldTitle = (Get-Host).UI.RawUI.WindowTitle
	(Get-Host).UI.RawUI.WindowTitle = $PROJECT_NAME + " Test"
	SleepFor 3

	foreach ($currentPsHost in @("powershell", "pwsh"))
	{
		$proc = Get-Process $currentPsHost -ErrorAction Ignore | Where-Object -FilterScript { $_.MainWindowTitle -match ($PROJECT_NAME + " Test") }
		if($proc -ne $null)
		{
			$processFound = $true
		}
	}
	(Get-Host).UI.RawUI.WindowTitle = $oldTitle
	if(-not $processFound)
	{
		LogError "Cannot set window title. Are you running this under a Windows Terminal tab?"
		return
	}

	#LOAD SCRIPTS
	$scriptExtensions = @{}
	$scriptExtensionsLastExecuted = @{}
	$scriptExtensionsNextExecuted = @{}

	foreach ($currentExtension in $POLLING_SCRIPTS.Keys)
	{
		$currentExtensionScript = LoadPath $currentExtension
		$currentExtensionFrequency = $POLLING_SCRIPTS[$currentExtension]

		if(IsInvalidPath $currentExtensionScript)
		{
			LogWarning Script Extension not found: [$currentExtensionScript]
			$scriptExtensionsLastExecuted.Add($currentExtensionScript, [System.DateTime]::MinValue)
			$scriptExtensionsNextExecuted.Add($currentExtensionScript, $null)
			continue
		}

		$ErrorActionPreference = 'SilentlyContinue'
		$tmpSpan = ([TimeSpan] $currentExtensionFrequency)
		$ErrorActionPreference = 'Continue'

		if(($tmpSpan -eq $null) -or ($tmpSpan -eq [System.DateTime]::MinValue))
		{
			LogWarning Invalid Script Extension Frequency: [$currentExtensionFrequency]
			$scriptExtensionsLastExecuted.Add($currentExtensionScript, [System.DateTime]::MinValue)
			$scriptExtensionsNextExecuted.Add($currentExtensionScript, $null)
			continue
		}

		$scriptExtensions.Add($currentExtensionScript, $tmpSpan)

		#INITIAL SCHEDULE
		switch ($currentExtensionScript)
		{
			{ $_ -in $TRACK_SCRIPT, $ORGANIZE_SCRIPT }
			{
				#SNAP ON THE HOUR
				$last_track = (get-date).Date.AddHours((get-date).Hour)
				$last_track = GetNextExecutionTime $last_track ($tmpSpan.TotalMinutes)
				$last_track = $last_track.AddMinutes($tmpSpan.TotalMinutes * -1)
				$scriptExtensionsLastExecuted.Add($currentExtensionScript, $last_track)
				$scriptExtensionsNextExecuted.Add($currentExtensionScript, (GetNextExecutionTime $last_track $tmpSpan.TotalMinutes))
			}
			{ $_ -in $NOTIFY_SCRIPT, $AUDIT_SCRIPT }
			{
				#SNAP TO 3AM Sunday
				$last_notify = (get-date).Date.AddHours(3)
				while ($last_notify.DayOfWeek -NotMatch "Sunday")
				{
					$last_notify = $last_notify.AddDays(-1)
				}
				$last_notify = GetNextExecutionTime $last_notify ($tmpSpan.TotalMinutes)
				$last_notify = $last_notify.AddMinutes($tmpSpan.TotalMinutes * -1)
				$scriptExtensionsLastExecuted.Add($currentExtensionScript, $last_notify)
				$next_notify = (GetNextExecutionTime $last_notify $tmpSpan.TotalMinutes)
				$scriptExtensionsNextExecuted.Add($currentExtensionScript, $next_notify)
			}
			default
			{
				$scriptExtensionsLastExecuted.Add($currentExtensionScript, (get-date))
				$scriptExtensionsNextExecuted.Add($currentExtensionScript, (get-date).Add($tmpSpan))
			}
		}

		if($DEBUG_POLLING -or $VERBOSE_MODE)
		{
			LogInfo LOADED SCRIPT: [$currentExtensionScript] [FREQ: $tmpSpan] [LAST: ($scriptExtensionsLastExecuted[$currentExtensionScript])] [NEXT: ($scriptExtensionsNextExecuted[$currentExtensionScript])]
		}
	}

	#CHECK DEPENDENCIES

	if(-not (IsHandleInstalled))
	{
		$scriptExtensionsLastExecuted[$TRACK_SCRIPT] = [System.DateTime]::MinValue
	}

	if(-not (UseTvDB))
	{
		LogWarning TVDB incorrectly configured.
		$scriptExtensionsLastExecuted[$NOTIFY_SCRIPT] = [System.DateTime]::MinValue
	}

	LogInfo $POLLING_START

	$shouldLoop = $true
	$nextScript = "Undefined"
	$nextEvent = "Undefined"
	while ($shouldLoop)
	{
		$now = get-date
		$lastScriptExecution = ""

		#EXECUTE EXTENSIONS
		foreach ($currentScriptExtension in $scriptExtensions.Keys)
		{
			$extensionFrequencyMinutes = ($scriptExtensions[$currentScriptExtension]).TotalMinutes
			if(ShouldTriggerScript $scriptExtensionsLastExecuted[$currentScriptExtension] $extensionFrequencyMinutes $now)
			{
				ExecutePollExecutable $currentScriptExtension
				$lastScriptExecution = $currentScriptExtension

				($scriptExtensionsLastExecuted[$currentScriptExtension]).AddMinutes(($scriptExtensions[$currentScriptExtension]).TotalMinutes) > $null
				$scriptExtensionsLastExecuted[$currentScriptExtension] = (get-date)
				$scriptExtensionsNextExecuted[$currentScriptExtension] = GetNextExecutionTime ($scriptExtensionsLastExecuted[$currentScriptExtension]) $extensionFrequencyMinutes
			}
		}

		if($VERBOSE_MODE -or $DEBUG_POLLING)
		{
			foreach ($currentScriptExtension in $scriptExtensions.Keys)
			{
				if($scriptExtensionsNextExecuted[$currentScriptExtension] -eq $null)
				{
					$nextExecution = "DISABLED"
				}
				else
				{
					$nextExecution = [String]::Format("{0:N2}", ($scriptExtensionsNextExecuted[$currentScriptExtension] - $now).TotalMinutes)
				}
				DebugLogInfo "NEXT: [$currentScriptExtension] ($nextExecution minutes)"
			}
		}

		$nextEvent = $now.AddDays(1)
		$nextScript = $null
		foreach ($currentScriptExtension in $scriptExtensions.Keys)
		{
			$currentEvent = $scriptExtensionsNextExecuted[$currentScriptExtension]
			if($currentEvent -eq $null)
			{
				continue
			}

			if($nextEvent -gt $currentEvent)
			{
				$nextEvent = $currentEvent
				$nextScript = $currentScriptExtension
			}
		}

		# DYANMIC DIAGNOSTICS
		if(IsValidPath $CONFIG_FILE)
		{
			. $CONFIG_FILE
		}

		$sleepSeconds = [System.Math]::Round(($nextEvent - (get-date)).TotalSeconds)
		if($sleepSeconds -le 0)
		{
			$sleepSeconds = 5
		}

		if($DEBUG_POLLING)
		{
			$sleepSecondsString = [String]::Format("{0:N0}", $sleepSeconds)
			LogInfo "NEXT SCRIPT: [$nextScript] in $sleepSecondsString seconds."
		}

		if($TASK_SCHEDULING)
		{
			$DEBUG_POLLING = $true

			LogInfo Last Executions:
			foreach ($currentScriptSchedule in $scriptExtensionsLastExecuted.Keys)
			{
				LogInfo ($currentScriptSchedule) [($scriptExtensionsLastExecuted[$currentScriptSchedule])]
			}

			LogInfo Next Executions:
			foreach ($currentScriptSchedule in $scriptExtensionsNextExecuted.Keys)
			{
				LogInfo ($currentScriptSchedule) [($scriptExtensionsNextExecuted[$currentScriptSchedule])]
			}
		}
		else
		{
			$DEBUG_POLLING = $false
		}

		if(-not $WHATIF_MODE)
		{
			if($SCRIPT_PATH -ne $null)
			{
				$displayScript = $nextScript.Replace($SCRIPT_PATH, "").Trim('\')
				$lastScriptExecution = $lastScriptExecution.Replace($SCRIPT_PATH, "").Trim('\')
			}
			else
			{
				$displayScript = $nextScript
			}
			$status = $POLLING_WIN_TITLE
			if(IsStringDefined $lastScriptExecution)
			{
				$status = $status + " [LAST: " + $lastScriptExecution + " at " + ($now.ToLongTimeString()) + "]"
			}
			if($nextEvent -ne "Undefined")
			{
				$status = $status + " [NEXT: " + $displayScript + " at " + ($nextEvent.ToLongTimeString()) + "]"
			}
			(Get-Host).UI.RawUI.WindowTitle = $status
		}

		if($WHATIF_MODE)
		{
			$shouldLoop = $false
		}

		$pollInterval = 30
		while($sleepSeconds -gt 0)
		{
			if(IsValidPath $TERMINATION_REQUEST)
			{
				LogInfo Poll Termination Request detected.
				DeleteItem ($TERMINATION_REQUEST)
				(Get-Host).UI.RawUI.WindowTitle = ($PROJECT_NAME + " TERMINATED")
				return
			}
			elseif($sleepSeconds -gt $pollInterval)
			{
				$sleepSeconds = $sleepSeconds - $pollInterval
				SleepFor $pollInterval
			}
			else
			{
				SleepFor $sleepSeconds
				$sleepSeconds = 0
			}
		}
	}
}