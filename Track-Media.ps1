[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Help,
    [Parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true, Position=0)]
    [Alias('FullName')]
    [String[]] $Path,
    [switch] $Purge,
    [switch] $Remove,
    [switch] $ClearSemaphore,
    [switch] $Force = $false
)

#########
#
# MAIN
#
#########

Begin
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1
	trap { ReleaseScriptConcurrency; break }

	if($ClearSemaphore)
	{
		RemoveSemaphore
		exit
	}
	LockScriptConcurrency

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

	$seenFiles = @{}

	if(-not (HasAdministratorRights))
	{
		LogWarning "This script is not running as administrator. Handle information for media services will not be found."
	}
}
Process
{
	try
	{
		if($Help)
		{
			return
		}

		switch($MyInvocation.InvocationName.ToLower().Trim())
		{
			{ $_ -in  	"resetmed",
						"reset-mediastate",
						"reset-consumedmediastate",
						"reset-watchedmedia",
						"reset-watchedmediastate",
						"reset-listenedmediastate" }
			{
				$Remove = $true
			}
		}

		if(($Path -eq $null) -and $Remove)
		{
			LogError "Remove parameter requires the $Path parameter."
			return
		}

		if(($Path -eq $null) -and $Purge)
		{
			LogError "Purge parameter requires the $Path parameter."
			return
		}

		if($Remove -and $Purge)
		{
			LogError "You cannot specify both the $Remove and $Path parameters together."
			return
		}

		if(($Path -eq $null) -or ($Path.Length -eq 0))
		{
			ExportPlexCurrentlyWatchingMedia
			TrackPlexWatchedMedia
			ProcessCurrentOpenedMedia
		}
		else
		{
			$resetWatchedPaths = New-Object -TypeName System.Collections.ArrayList
			$pathsToClean = New-Object -TypeName System.Collections.ArrayList
			foreach($Target in $Path)
			{
				$Target = LoadPath $Target
				if(IsInvalidPath $Target) { continue }

				if((IsWatched $Target) -and (IsNotUnderPath $Target $PURGE_PATH))
				{
					if((Get-Item -LiteralPath $Target).Attributes -Match 'Directory')
					{
						$parentDir = (Get-Item -LiteralPath $Target).Parent.FullName
					}
					else
					{
						$parentDir = (Get-Item -LiteralPath $Target).Directory.Parent.FullName
					}

					if((IsStringDefined $parentDir) -and (-not $pathsToClean.Contains($parentDir)))
					{
						$pathsToClean.Add($parentDir) > $null
					}
				}

				$files =  Get-ChildItem -LiteralPath $Target -Recurse -FollowSymlink -File
				foreach($currentOpenedFile in $files)
				{
					$currentOpenedFile = Get-Item -LiteralPath (TranslatePathToSymLink ($currentOpenedFile.FullName))
					$canExclude = $true
					$purgeOverride = LoadSubscriptionOverrideVariable $currentOpenedFile "PURGE_ON_WATCHED"
					if((($purgeOverride -ne $null) -and $purgeOverride) -or $Purge)
					{
						$canExclude = $false
					}

					if($canExclude -and (IsExcludedPath $currentOpenedFile.FullName))
					{
						LogWarning EXCLUDED FILE: [($currentOpenedFile.FullName)]
						continue
					}

					if(IsWatched $currentOpenedFile.FullName)
					{
						if(-not $Remove)
						{
							LogWarning SKIPPING CONSUMED PATH: [($currentOpenedFile.FullName)]
							continue
						}
						else
						{
							if($currentOpenedFile.Extension -NotIn ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
							{
								VerboseLogInfo EXCLUDED FILE EXTENSION: [($currentOpenedFile.FullName)]
								continue
							}

							$originalTarget = $currentOpenedFile.Directory.FullName
							if((IsAudio $currentOpenedFile.FullName) -and (IsStringDefined $DOWNLOADED_PODCASTS_PATH))
							{
								$originalTarget = $originalTarget.Replace($PURGE_PATH, $DOWNLOADED_PODCASTS_PATH)
								$originalTarget = $originalTarget.Replace((DeriveWatchedPath $DOWNLOADED_PODCASTS_PATH), $DOWNLOADED_PODCASTS_PATH)
							}
							else
							{
								$originalTarget = $originalTarget.Replace($PURGE_PATH, $DOWNLOADED_TV_PATH)
								$originalTarget = $originalTarget.Replace((DeriveWatchedPath $DOWNLOADED_TV_PATH), $DOWNLOADED_TV_PATH)
								$originalTarget = $originalTarget.Replace((DeriveWatchedPath $DOWNLOADED_MOVIES_PATH), $DOWNLOADED_MOVIES_PATH)
							}

							if($currentOpenedFile.Directory.FullName.Contains($PURGE_PATH))
							{
								$showName = ExtractShowName $currentOpenedFile
								if(IsStringDefined $showName)
								{
									$originalTarget = [System.IO.Path]::Combine($originalTarget, $showName)
								}
							}

							MoveFile $currentOpenedFile.FullName $originalTarget
							if((-not $resetWatchedPaths.Contains($originalTarget)) -and ($originalTarget -ne $DOWNLOADED_TV_PATH) -and ($originalTarget -ne $DOWNLOADED_MOVIES_PATH) -and ($originalTarget -ne $DOWNLOADED_PODCASTS_PATH))
							{
								$resetWatchedPaths.Add($originalTarget) > $null
							}
							$originalTarget = [System.IO.Path]::Combine($originalTarget, $currentOpenedFile.Name)
							TrackAdRemovedMedia $originalTarget
							UnTrackWatchedMedia $originalTarget
							continue
						}
					}

					if($currentOpenedFile.Extension -NotIn $MEDIA_EXTENSIONS)
					{
						LogWarning EXCLUDED FILE EXTENSION: [($currentOpenedFile.FullName)]
						continue
					}

					if((IsNotUnderPath $currentOpenedFile.Directory.FullName $DOWNLOADED_TV_PATH) -and (IsNotUnderPath $currentOpenedFile.Directory.FullName $DOWNLOADED_PODCASTS_PATH) -and (IsNotUnderPath $currentOpenedFile.Directory.FullName $DOWNLOADED_MOVIES_PATH))
					{
						LogWarning NOT IN DOWNLOADED PATH: [($currentOpenedFile.FullName)]
						continue
					}

					if($seenFiles.ContainsKey($currentOpenedFile.FullName))
					{
						continue
					}
					else
					{
						$seenFiles.Add($currentOpenedFile.FullName, $null)
					}

					if($Remove)
					{
						LogInfo NO LONGER TRACKING: [($currentOpenedFile.FullName)]
						UnTrackWatchedMedia $currentOpenedFile.FullName
					}
					else
					{
						if($Purge)
						{
							PurgeItem $currentOpenedFile.FullName $false
						}
						else
						{
							#Explicitly passed in files marked as watched should be immediately processed, not tracked.
							TrackWatchedMedia $currentOpenedFile.FullName ((Get-Date).AddHours($TRACK_AGE_HOURS * -1)) ("SESSION")
						}
					}
				}
			}
		}
	}
	catch
	{
		ReleaseScriptConcurrency
		throw
	}
}
End
{
	if(IsValidPath $LAST_TRACKED)
	{
		DeleteItem $LAST_TRACKED
	}
	GetExecutingScriptName | SafeOutFile -LiteralPath $LAST_TRACKED

	if($resetWatchedPaths.Count -gt 0)
	{
		foreach($currentPath in $pathsToClean)
		{
			LogInfo REMOVING EMPTY SUBDIRS: [$currentPath]
			PurgeEmptyDirectories $currentPath $false
		}

		if(IsKodiConfigured)
		{
			RemoveMissingKodiItems
		}

		if(IsPlexConfigured)
		{
			RefreshPlex
			foreach($currentResetWatchedPath in $resetWatchedPaths)
			{
				MarkPlexMediaUnwatched $currentResetWatchedPath
			}
		}
	}

	ReleaseScriptConcurrency
	PauseIfClicked
}