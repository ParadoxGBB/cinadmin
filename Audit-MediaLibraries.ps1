[CmdletBinding(SupportsShouldProcess=$true)]
param(
    [switch] $Duplicates,
    [switch] $EpisodeContinuity,
    [switch] $EpisodeNames,
    [switch] $Kodi,
    [switch] $Movies,
    [switch] $Plex,
    [switch] $Sizes,
    [switch] $SeasonDirectories,
    [switch] $Help,
    [switch] $Force = $false,
    [Parameter(ValueFromPipeline=$true, ValueFromPipelineByPropertyName=$true, Position=0)]
    [Alias('FullName')]
    [String[]] $Path
)

#########
#
# MAIN
#
#########

Begin
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1
	trap { ReleaseScriptConcurrency; break }

	LockScriptConcurrency

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

    if(	$Duplicates -or
		$EpisodeContinuity -or
		$EpisodeNames -or
		$Kodi -or
		$Movies -or
		$Plex -or
		$Sizes -or
		$SeasonDirectories )
	{
		$AUDIT_DUPLICATES         = $false
		$AUDIT_EPISODE_NAMES      = $false
		$AUDIT_SEASON_DIRECTORIES = $false
		$AUDIT_SIZES              = $false
		$AUDIT_EPISODE_CONTINUITY = $false
		$AUDIT_MOVIES             = $false
		$AUDIT_PLEX               = $false
		$AUDIT_KODI               = $false
	}

	if($Duplicates)
	{
		$AUDIT_DUPLICATES       = $true
	}
	if($EpisodeContinuity)
	{
		$AUDIT_EPISODE_CONTINUITY = $true
	}
	if($EpisodeNames)
	{
		$AUDIT_EPISODE_NAMES    = $true
	}
	if($Kodi)
	{
		$AUDIT_KODI             = $true
	}
	if($Movies)
	{
		$AUDIT_MOVIES           = $true
	}
	if($Plex)
	{
		$AUDIT_PLEX             = $true
	}
	if($Sizes)
	{
		$AUDIT_SIZES            = $true
	}
	if($SeasonDirectories)
	{
		$AUDIT_SEASON_DIRECTORIES = $true
	}

	$start = Get-Date
	$seenFiles = New-Object -TypeName System.Collections.ArrayList

	if(IsNotUnderPath (DeriveWatchedPath $DOWNLOADED_TV_PATH) $DOWNLOADED_TV_PATH)
	{
		LogWarning Invalid consumed / downloaded heirarchy: [$DOWNLOADED_TV_PATH]
	}

	if(IsNotUnderPath (DeriveWatchedPath $DOWNLOADED_MOVIES_PATH) $DOWNLOADED_MOVIES_PATH)
	{
		LogWarning Invalid consumed / downloaded heirarchy: [$DOWNLOADED_MOVIES_PATH]
	}

	if(IsNotUnderPath (DeriveWatchedPath $DOWNLOADED_PODCASTS_PATH) $DOWNLOADED_PODCASTS_PATH)
	{
		LogWarning Invalid consumed / downloaded heirarchy: [$DOWNLOADED_PODCASTS_PATH]
	}

	if($ProgressPreference -eq 'SilentlyContinue')
	{
		LogWarning "Auditing Media Libraries... (This can take a while, so can delay other scripts from executing)."
	}
}
Process
{
	try
	{
		if($Help)
		{
			return
		}

		#AUDIT POTENTIAL DUPLICATE NAMES
		$seenFiles.Clear()
		if($AUDIT_DUPLICATES)
		{
			$pathsToUse = @($DOWNLOADED_TV_PATH, $DOWNLOADED_MOVIES_PATH, $DOWNLOADED_PODCASTS_PATH)
			if($Path.Count -gt 0)
			{
				$pathsToUse = $Path
			}

			foreach($currentPathToCheck in $pathsToUse)
			{
				if(IsInvalidPath $currentPathToCheck) { continue }
				$cCurrentProgress = 0
				$media = Get-ChildItem -Recurse -LiteralPath $currentPathToCheck -File -FollowSymlink
				foreach($currentMedia in $media)
				{
					$cCurrentProgress++
					OutputProgressBar ("Auditing duplicates [$currentPathToCheck]...") ($cCurrentProgress/$media.Count)

					if($seenFiles.Contains($currentMedia.FullName.ToLower()))
					{
						continue
					}
					else
					{
						$seenFiles.Add($currentMedia.FullName.ToLower()) > $null
					}
					VerboseLogInfo Considering: [($currentMedia.FullName)]

					if(-not $Force)
					{
						if($currentMedia.Extension -NotIn ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
						{
							continue
						}

						if(IsPathInExclusionList $currentMedia.FullName $AUDIT_EXCLUSIONS)
						{
							DebugLogInfo EXCLUDED: [($currentMedia.FullName)]
							continue
						}
					}
					VerboseLogInfo Considering: [($currentMedia.FullName)]

					#CHECK FOR DUPES
					if($file.BaseName -match $DUPLICATE_PATTERN)
					{
						LogWarning Duplicate Detected: [($currentMedia.FullName)]
					}
					if($currentMedia.BaseName -match "\.\d+$")
					{
						$dupeBaseName = $currentMedia.BaseName.Replace($matches.Values[0], "")
						$dupeBaseName = $currentMedia.FullName.Replace($currentMedia.BaseName, $dupeBaseName)
						if(IsValidPath $dupeBaseName)
						{
							LogWarning Duplicate Detected: [($currentMedia.FullName)]
						}
					}
				}
			}
		}

		#AUDIT OBSOLETE NAMES
		$seenFiles.Clear()
		if($AUDIT_EPISODE_NAMES)
		{
			$pathsToUse = @($DOWNLOADED_TV_PATH, $DOWNLOADED_PODCASTS_PATH)
			if($Path.Count -gt 0)
			{
				$pathsToUse = $Path
			}

			foreach($currentPathToCheck in $pathsToUse)
			{
				if(IsInvalidPath $currentPathToCheck) { continue }
				$cCurrentProgress = 0
				$media = Get-ChildItem -Recurse -LiteralPath $currentPathToCheck -File -FollowSymlink
				foreach($currentMedia in $media)
				{
					$cCurrentProgress++
					OutputProgressBar ("Auditing episodic names [$currentPathToCheck]...") ($cCurrentProgress/$media.Count)

					if($seenFiles.Contains($currentMedia.FullName.ToLower()))
					{
						continue
					}
					else
					{
						$seenFiles.Add($currentMedia.FullName.ToLower()) > $null
					}

					if(-not $Force)
					{
						if($currentMedia.Extension -NotIn ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
						{
							continue
						}

						if(IsPathInExclusionList $currentMedia.FullName $AUDIT_EXCLUSIONS)
						{
							DebugLogInfo EXCLUDED: [($currentMedia.FullName)]
							continue
						}
					}
					VerboseLogInfo Considering: [($currentMedia.FullName)]

					$newFileResults = GenerateSmartRenamedFileName $currentMedia 0
					if(($newFileResults -eq $null) -or ($newFileResults.Count -eq 0))
					{
						$newFile = $null
					}
					else
					{
						$newFile = $newFileResults[0]
					}

					if(($newFile -ne $null) -and ($newFile -ne $currentMedia.FullName))
					{
						LogWarning Obsolete file name: [($currentMedia.FullName)] --> [$newFile]...
						continue
					}

					#
					# AUDIT TITLE IN METADATA
					#
					$metaData = ExtractMetadataTags $currentMedia.FullName
					$showName = ExtractShowName $currentMedia
					$metadataTitle = $metaData["title"]
					# Oddly, sometimes a single space is expanded to two when extracting?
					if(IsStringDefined $metadataTitle)
					{
						$metadataTitle = $metadataTitle.Replace("  ", " ")
					}
					$viableMetadataTitles = New-Object -TypeName System.Collections.ArrayList

					if(-not (IsStringDefined $metadataTitle))
					{
						LogWarning No Metadata title: [($currentMedia.FullName)]
					}

					if(IsAudio $currentMedia.FullName)
					{
						$seriesMetaDataProperty = "artist"
					}
					else
					{
						$seriesMetaDataProperty = "show"
					}
					$seriesMetadata = $metaData[$seriesMetaDataProperty]

					if(-not (IsStringDefined $seriesMetadata))
					{
						LogWarning No Metadata series "(artist/show)": [($currentMedia.FullName)]
						continue
					}

					if($seriesMetadata.Contains('_'))
					{
						LogWarning Translated Characters in artist or show: [($currentMedia.FullName)] [$seriesMetadata]
						continue
					}

					$tmpTitleFromFileName = $currentMedia.BaseName
					if($tmpTitleFromFileName.StartsWith($showName))
					{
						$tmpTitleFromFileName = $tmpTitleFromFileName.SubString($showName.Length).Trim(" ", "-")
					}

					# Allow stripped Title in file and metadata for watched / archived shows for now
					# however some titles might have both defined due to style
					#   E.G. Mr. Robot - S01E01 - eps1.0_hellofriend.mov.mp4
					if($metadataTitle.Contains('_') -xor $tmpTitleFromFileName.Contains('_'))
					{
						LogWarning Translated Characters in Title or Name: [($currentMedia.FullName)] [$metadataTitle]
						continue
					}

					#Metadata should be everything other than the show name by default
					if(IsStringDefined $showName)
					{
						$projectedMetadata = $currentMedia.BaseName
						$projectedMetadata = $projectedMetadata.SubString($showName.Length)
						$projectedMetadata = $projectedMetadata.Trim('-', ' ')
						$viableMetadataTitles.Add($projectedMetadata) > $null

						#Allow 3 digit episode number for legacy reasons
						if($metadataTitle -match '\dE\d{3}\s')
						{
							$seasonEpisodeFromFileName = (ParseSeasonViaString $currentMedia.BaseName)[1]
							$seasonEpisodeFromFileNameExtended = $seasonEpisodeFromFileName.Replace("E", "E0")
							$projectedMetadata = $projectedMetadata.Replace($seasonEpisodeFromFileName, $seasonEpisodeFromFileNameExtended)
							$viableMetadataTitles.Add($projectedMetadata) > $null
						}
					}

					#Allow just title with no air date / episode
					$projectedMetadata = ExtractBestGuessTitle $currentMedia
					$viableMetadataTitles.Add($projectedMetadata) > $null

					# Audit
					$metaDataOk = $false
					if($metadataTitle -In $viableMetadataTitles.ToArray())
					{
						$metaDataOk = $true
					}

					foreach($currentViableMetadataTitle in $viableMetadataTitles)
					{
						if(($currentViableMetadataTitle | BeautifyAndDemoteString) -eq ($metadataTitle | BeautifyAndDemoteString))
						{
							$metaDataOk = $true
						}
					}

					if(-not $metaDataOk)
					{
						LogWarning Title Possibly Incorrect in Metadata: [($currentMedia.FullName)] [META: $metadataTitle]
						foreach($currentViableMetadata in $viableMetadataTitles)
						{
							LogWarning VIABLE METADATA: [$currentViableMetadata]
						}
					}
				}
			}
		}

		#AUDIT OBSOLETE DIRECTORY NAMES
		$seenFiles.Clear()
		if($AUDIT_SEASON_DIRECTORIES)
		{
			$pathsToUse = @($DOWNLOADED_TV_PATH, $DOWNLOADED_PODCASTS_PATH)
			if($Path.Count -gt 0)
			{
				$pathsToUse = $Path
			}

			foreach($currentPathToCheck in $pathsToUse)
			{
				if(IsInvalidPath $currentPathToCheck) { continue }
				$cCurrentProgress = 0
				$media = Get-ChildItem -Recurse -LiteralPath $currentPathToCheck -Directory -FollowSymlink
				foreach($currentMedia in $media)
				{
					$cCurrentProgress++
					OutputProgressBar ("Auditing directories [$currentPathToCheck]...") ($cCurrentProgress/$media.Count)

					if($seenFiles.Contains($currentMedia.FullName.ToLower()))
					{
						continue
					}
					else
					{
						$seenFiles.Add($currentMedia.FullName.ToLower()) > $null
					}

					if(-not $Force)
					{
						if(IsPathInExclusionList $currentMedia.FullName $AUDIT_EXCLUSIONS)
						{
							DebugLogInfo EXCLUDED: [($currentMedia.FullName)]
							continue
						}
					}
					VerboseLogInfo Considering: [($currentMedia.FullName)]

					if((IsUnderPath $currentMedia.Name $DOWNLOADED_TV_PATH) -and $currentMedia.Name.StartsWith("S"))
					{
						$tmpInput = $currentMedia.Name.Substring(1)
						$tmpInt = 0
						$validInt= [int]::TryParse($tmpInput, [ref] $tmpInt)
						if($validInt -and ($tmpInt -lt 10) -and ($currentMedia.Name.Length -le 2))
						{
							$newName = $currentMedia.FullName.Replace(("\"+ $currentMedia.Name), [String]::Format("\S{0:D2}", $tmpInt))
							LogWarning Obsolete directory name: [($currentMedia.FullName)] --> [($newName)]
						}
					}

					if((IsUnderPath $currentMedia.FullName $DOWNLOADED_PODCASTS_PATH) -and ($currentMedia.Name -match "S\d+"))
					{
						$parentName = $currentMedia.Parent.Name
						$parentName = [RegEx]::Escape($parentName)
						if(-not ($currentMedia.Name.Contains($currentMedia.Parent.Name)))
						{
							LogWarning Possible directory name missing parent reference: [($currentMedia.FullName)]
						}
						elseif(-not ($currentMedia.Name -Match ($parentName + "\s-\sS\d{2,}")))
						{
							LogWarning Possible incorrect season directory name: [($currentMedia.FullName)]
						}
					}
				}
			}
		}

		#AUDIT FILE SIZES
		$seenFiles.Clear()
		if($AUDIT_SIZES)
		{
			$pathsToUse = @($DOWNLOADED_TV_PATH, $DOWNLOADED_MOVIES_PATH, $DOWNLOADED_PODCASTS_PATH)
			if($Path.Count -gt 0)
			{
				$pathsToUse = $Path
			}

			foreach($currentPathToCheck in $pathsToUse)
			{
				if(IsInvalidPath $currentPathToCheck) { continue }
				$cCurrentProgress = 0
				$media = Get-ChildItem -Recurse -LiteralPath $currentPathToCheck -Directory -FollowSymlink
				foreach($currentMedia in $media)
				{
					$cCurrentProgress++
					OutputProgressBar ("Auditing file sizes [$currentPathToCheck]...") ($cCurrentProgress/$media.Count)

					if($seenFiles.Contains($currentMedia.FullName.ToLower()))
					{
						continue
					}
					else
					{
						$seenFiles.Add($currentMedia.FullName.ToLower()) > $null
					}

					if(IsPathInExclusionList $currentMedia.FullName $AUDIT_EXCLUSIONS)
					{
						DebugLogInfo EXCLUDED: [($currentMedia.FullName)]
						continue
					}

					if(($currentMedia.Extension -In $MEDIA_EXTENSIONS) -and $currentMedia.Length -le (5*1024))
					{
						LogWarning Media is less than 5 kilobytes: [($currentMedia.FullName)]
					}
				}
			}
		}

		#AUDIT EPISODE CONTINUITY
		$seenFiles.Clear()
		if($AUDIT_EPISODE_CONTINUITY)
		{
			$pathsToUse = @($DOWNLOADED_TV_PATH, $DOWNLOADED_PODCASTS_PATH)
			if($Path.Count -gt 0)
			{
				$pathsToUse = $Path
			}

			$continuityDictionary = @{}
			foreach($currentPathToCheck in $pathsToUse)
			{
				$continuityDictionary.Clear() #Track per path

				if(IsInvalidPath $currentPathToCheck) { continue }
				$cCurrentProgress = 0
				$media = Get-ChildItem -Recurse -LiteralPath $currentPathToCheck -File -FollowSymlink
				foreach($currentMedia in $media)
				{
					$cCurrentProgress++
					OutputProgressBar ("Auditing episode continuity [$currentPathToCheck]...") ($cCurrentProgress/$media.Count)

					if($seenFiles.Contains($currentMedia.FullName.ToLower()))
					{
						continue
					}
					else
					{
						$seenFiles.Add($currentMedia.FullName.ToLower()) > $null
					}

					if(-not $Force)
					{
						if($currentMedia.Extension -NotIn ($MEDIA_EXTENSIONS))
						{
							continue
						}

						if(IsPathInExclusionList $currentMedia.FullName $AUDIT_EXCLUSIONS)
						{
							DebugLogInfo EXCLUDED: [($currentMedia.FullName)]
							continue
						}

						if(IsDailyShow $currentMedia)
						{
							continue
						}

						if(IsTopicalShow $currentMedia)
						{
							continue
						}
					}
					VerboseLogInfo Considering: [($currentMedia.FullName)]

					$seasonResults = ParseSeason $currentMedia
					if(($seasonResults -eq $null) -or ($seasonResults[0] -eq $null) -or ($seasonResults[2] -eq $null))
					{
						continue
					}

					$showName = ExtractShowName $currentMedia
					if(-not (IsStringDefined $showName))
					{
						continue
					}

					$seasonNumber = $seasonResults[0].TrimStart("0")
					if(-not (IsStringDefined $seasonNumber))
					{
						$seasonNumber = "0"
					}
					$episodeNumber = $seasonResults[2].TrimStart("0")
					if(-not (IsStringDefined $episodeNumber))
					{
						$episodeNumber = "0"
					}
					$seasonNumber = [String]::Format("{0:D3}", [Int]::Parse($seasonNumber))
					$episodeNumber = [String]::Format("{0:D4}", [Int]::Parse($episodeNumber))

					if($seasonNumber -eq "000" -or ($episodeNumber -eq "0000"))
					{
						continue
					}

					$seasonKey = [String]::Format("{0} - {1}", $showName, $seasonNumber)
					if($continuityDictionary.ContainsKey($seasonKey))
					{
						if(($continuityDictionary[$seasonKey]).ContainsKey($episodeNumber))
						{
							$collidingFile = ($continuityDictionary[$seasonKey])[$episodeNumber]
							LogWarning Duplicate Season / Episode detected: [($currentMedia.FullName)] [($collidingFile)]
						}
						else
						{
							($continuityDictionary[$seasonKey]).Add($episodeNumber, $currentMedia.FullName) > $null
						}
					}
					else
					{
						$continuityDictionary.Add($seasonKey, @{}) > $null
						($continuityDictionary[$seasonKey]).Add($episodeNumber, $currentMedia.FullName) > $null
					}
				}

				foreach($currentSeason in ($continuityDictionary.Keys | Sort-Object))
				{
					$episodes = ($continuityDictionary[$currentSeason]).Keys | Sort-Object
					$startAt = $episodes[0]
					$cEpisodes = $episodes.Count
					VerboseLogInfo Scanning Season [$currentSeason] [$startAt] [$cEpisodes]
					for($i = 1; ($i -le $cEpisodes); $i++)
					{
						$episodeNumber = [String]::Format("{0:D4}", [Int]::Parse($i))
						if(-not ($continuityDictionary[$currentSeason]).ContainsKey($episodeNumber))
						{
							LogWarning Missing Episode! [$currentSeason] [$episodeNumber]
						}
					}
				}
			}
		}

		#AUDIT MOVIES
		$seenFiles.Clear()
		if($AUDIT_MOVIES)
		{
			$pathsToUse = @($DOWNLOADED_MOVIES_PATH)
			if($Path.Count -gt 0)
			{
				$pathsToUse = $Path
			}

			foreach($currentPathToCheck in $pathsToUse)
			{
				if(IsInvalidPath $currentPathToCheck) { continue }
				$cCurrentProgress = 0
				$media = Get-ChildItem -Recurse -LiteralPath $currentPathToCheck -File -FollowSymlink
				foreach($currentMedia in $media)
				{
					$cCurrentProgress++
					OutputProgressBar ("Auditing movies [$currentPathToCheck]...") ($cCurrentProgress/$media.Count)

					if($seenFiles.Contains($currentMedia.FullName.ToLower()))
					{
						continue
					}
					else
					{
						$seenFiles.Add($currentMedia.FullName.ToLower()) > $null
					}

					if(-not $Force)
					{
						if($currentMedia.Extension -NotIn ($MEDIA_EXTENSIONS + $SUBTITLE_EXTENSIONS))
						{
							continue
						}

						if($currentMedia.Extension -NotIn (".mkv", ".mp4", ".avi", ".mpg", ".m4v"))
						{
							continue
						}

						if(IsPathInExclusionList $currentMedia.FullName $AUDIT_EXCLUSIONS)
						{
							DebugLogInfo EXCLUDED: [($currentMedia.FullName)]
							continue
						}
					}
					VerboseLogInfo Considering: [($currentMedia.FullName)]

					$movieTitlefromName = $currentMedia.BaseName
					$movieTitlefromName = ($movieTitlefromName -replace "\s+\(\d\d\d\d\)$").Trim()
					$movieTitlefromName = ($movieTitlefromName -replace "^(\d+\s*-\s)").Trim()

					$movieTitlefromMeta = ""
					$movieYearfromMeta = ""
					$movieSubtitlefromMeta = ""

					$metaData = ExtractMetadataTags $currentMedia.FullName
					if($metaData.ContainsKey("title"))
					{
						$movieTitlefromMeta = ($metaData["title"]).Trim()
					}
					if($metaData.ContainsKey("year"))
					{
						$movieYearfromMeta = ($metaData["year"]).Trim()
					}
					if($metaData.ContainsKey("year"))
					{
						$movieSubtitlefromMeta = ($metaData["subtitle"]).Trim()
					}
					DebugLogInfo Movie [($currentMedia.FullName)] [$movieTitlefromName] [$movieTitlefromMeta]

					if(IsStringDefined $movieTitlefromMeta)
					{
						if(($convertedTitleFromFile | BeautifyAndDemoteString) -ne ($convertedTitleFromMeta | BeautifyAndDemoteString))
						{
							LogWarning Movie Title Possibly Incorrect in Metadata: [($currentMedia.FullName)] [File: $movieTitlefromName] [META: $movieTitlefromMeta]
						}
					}

					if(IsStringDefined $movieYearfromMeta)
					{
						if($movieYearfromMeta.Length -ne 4)
						{
							LogWarning Movie Year Possibly Incorrect in Metadata: [($currentMedia.FullName)] [$movieYearfromMeta]
						}
					}

					if(IsStringDefined $movieSubtitlefromMeta)
					{
						LogWarning Movie Subtitle in Metadata: [($currentMedia.FullName)] [$movieSubtitlefromMeta]
					}
				}
			}
		}

		#AUDIT PLEX
		$seenFiles.Clear()
		if($AUDIT_PLEX -and (IsPlexConfigured))
		{
			AuditPlexCurrentlyWatchingMedia
			if(IsInvalidPath $PLEX_AUDIT_CSV)
			{
				LogWarning Plex audit not found: [$PLEX_AUDIT_CSV]
			}

			OutputProgressBar ("Auditing Plex Libraries...") (1/2)

			$seenPlexFiles = New-Object -TypeName System.Collections.ArrayList
			foreach($currentPlexItem in (Import-Csv -LiteralPath $PLEX_AUDIT_CSV))
			{
				$plexItemFullPath = $currentPlexItem.FullPath
				$plexItemMediaIndex = $currentPlexItem.MediaIndex
				$plexItemPartIndex = $currentPlexItem.PartIndex
				$plexItemWatched = $currentPlexItem.Watched
				$plexItemLibrary = $currentPlexItem.LibraryTitle

				if(IsInvalidPath $plexItemFullPath)
				{
					$originalEntry = $plexItemFullPath
					$plexItemFullPath = MapHandleFileOutput $plexItemFullPath
					if(IsInvalidPath $plexItemFullPath)
					{
						LogWarning PLEX ITEM NOT FOUND LOCALLY: [$originalEntry] [$plexItemLibrary]
						continue
					}
				}

				if(-not $Force)
				{
					if(IsPathInExclusionList $plexItemFullPath $AUDIT_EXCLUSIONS)
					{
						DebugLogInfo EXCLUDED: [($plexItemFullPath)]
						continue
					}
				}

				$plexItemIdentifier = [String]::Format("{0}:{1}", $plexItemMediaIndex, $plexItemPartIndex)
				if(($plexItemMediaIndex -gt 0) -or ($plexItemPartIndex -gt 0))
				{
					if(-not (IsUnderPath $plexItemFullPath $PURGE_PATH))
					{
						LogWarning PLEX ITEM INDEX MAY BE HIDDEN IN STACK: [$plexItemFullPath] [$plexItemLibrary] [$plexItemIdentifier]
					}
				}

				if(-not (IsAudio $plexItemFullPath))
				{
					if($plexItemWatched -eq "")
					{
						LogWarning PLEX ITEM HAS INVALID CONSUMED STATE: [$plexItemFullPath] [$plexItemLibrary] [$plexItemWatched]
						continue
					}

					$plexItemIsWatched = [System.Boolean]::Parse($plexItemWatched)
					if((IsWatched $plexItemFullPath) -and -not (IsUnderPath $plexItemFullPath $PURGE_PATH))
					{
						if(-not $plexItemIsWatched)
						{
							LogWarning PLEX ITEM HAS INCORRECT UNCONSUMED STATE: [$plexItemFullPath] [$plexItemLibrary] [$plexItemWatched]
						}
					}
					elseif((IsUnderPath $plexItemFullPath $DOWNLOADED_TV_PATH) -or (IsUnderPath $plexItemFullPath $DOWNLOADED_MOVIES_PATH))
					{
						if($plexItemIsWatched)
						{
							LogWarning PLEX ITEM HAS INCORRECT CONSUMED STATE: [$plexItemFullPath] [$plexItemLibrary] [$plexItemWatched]
						}
					}
				}

				if($seenPlexFiles.Contains($plexItemFullPath.ToLower()))
				{
					continue
				}
				else
				{
					$seenPlexFiles.Add($plexItemFullPath.ToLower()) > $null
				}
			}

			$pathsToUse = @($DOWNLOADED_TV_PATH, $DOWNLOADED_MOVIES_PATH, $DOWNLOADED_PODCASTS_PATH)
			if($Path.Count -gt 0)
			{
				$pathsToUse = $Path
			}

			foreach($currentPathToCheck in $pathsToUse)
			{
				if(IsInvalidPath $currentPathToCheck) { continue }
				$cCurrentProgress = 0
				$media = Get-ChildItem -Recurse -LiteralPath $currentPathToCheck -File -FollowSymlink
				foreach($currentMedia in $media)
				{
					$cCurrentProgress++
					OutputProgressBar ("Auditing Plex Libraries [$currentPathToCheck]...") ($cCurrentProgress/$media.Count)

					if($seenFiles.Contains($currentMedia.FullName.ToLower()))
					{
						continue
					}
					else
					{
						$seenFiles.Add($currentMedia.FullName.ToLower()) > $null
					}

					if(-not $Force)
					{
						if($currentMedia.Extension -NotIn ($MEDIA_EXTENSIONS))
						{
							continue
						}

						if($currentMedia.Extension -In (".iso", ".img"))
						{
							continue
						}
					}
					VerboseLogInfo Considering: [($currentMedia.FullName)]

					if(IsPathInExclusionList $currentMedia.FullName $AUDIT_EXCLUSIONS)
					{
						DebugLogInfo EXCLUDED: [($currentMedia.FullName)]
						continue
					}

					if($seenPlexFiles.Contains($currentMedia.FullName.ToLower()))
					{
						continue
					}
					else
					{
						LogWarning ITEM NOT FOUND IN PLEX: [($currentMedia.FullName)]
					}
				}
			}
		}

		#AUDIT KODI
		$seenFiles.Clear()
		if($AUDIT_KODI -and (IsKodiConfigured))
		{
			$cServerIndex = 0
			foreach($currentKodiUri in $KODI_SERVERS.Keys)
			{
				$cServerIndex++
				OutputProgressBar ("Auditing Kodi Libraries [$currentKodiUri]...") ($cServerIndex/$KODI_SERVERS.Count)

				$seenKodiFiles = New-Object -TypeName System.Collections.ArrayList
				$serverDown = $false

				foreach($currentKodiMethod in @("VideoLibrary.GetMovies", "VideoLibrary.GetEpisodes"))
				{
					if($serverDown)
					{
						continue
					}

					$jsonRequest = '{"jsonrpc": "2.0", "method": "' + $currentKodiMethod + '", "params": { "properties" : ["playcount", "file"], "sort": { "order": "ascending", "method": "label", "ignorearticle": true } }, "id": 1}'

					$webclient = New-Object System.Net.WebClient
					$webclient.Encoding = [System.Text.Encoding]::UTF8
					$extractedCred = GetCredential ($KODI_SERVERS[$currentKodiUri])
					if($extractedCred -eq $null)
					{
						continue
					}
					$webclient.Credentials = New-Object System.Net.NetworkCredential(($extractedCred[0]), ($extractedCred[1]))

					$requestUri = [String]::Format("{0}?request={1}",$currentKodiUri,$jsonRequest)
					DebugLogInfo QUERYING: [$requestUri]

					try
					{
						$results = $webclient.DownloadString($requestUri)
					}
					catch [System.Net.WebException]
					{
						if($WHATIF_MODE)
						{
							$Error.Clear()
						}

						if($_.ToString().Contains("Unable to connect to the remote server") -or $_.ToString().Contains("connected host has failed to respond") -or $_.ToString().Contains("actively refused it"))
						{
							LogWarning Server appears unavailable or Kodi not accessible: [$currentKodiUri]
							$serverDown = $true
						}
						else
						{
							LogWarning Problem setting Kodi state: [$currentKodiUri] [($_.ToString())]
						}
						continue
					}

					$resultsJson = $results | ConvertFrom-Json
					DebugLogInfo RESPONSE: [$resultsJson]
					if($resultsJson.error -ne $null)
					{
						LogError JSON ERROR RESPONSE [($response)] [Request: ($jsonRequest)]
						continue
					}

					if($currentKodiMethod.Contains("Movies"))
					{
						$kodiMediaResults = $resultsJson.result.movies
					}
					else
					{
						$kodiMediaResults = $resultsJson.result.episodes
					}

					VerboseLogInfo RESULT COUNT [($kodiMediaResults.Count)]
					foreach($currentJsonResult in $kodiMediaResults)
					{
						$kodiFilesToProcess = New-Object -TypeName System.Collections.ArrayList
						if($currentJsonResult.file.StartsWith("stack://"))
						{
							$currentStack = $currentJsonResult.file.Replace("stack://", "")
							$currentStack = $currentStack.Trim()
							foreach($currentStackItem in $currentStack.Split(' , '))
							{
								$kodiFilesToProcess.Add($currentStackItem.Trim()) > $null
							}
						}
						else
						{
							$kodiFilesToProcess.Add($currentJsonResult.file.Trim()) > $null
						}

						foreach($currentKodiFile in $kodiFilesToProcess)
						{
							$currentTranslatedKodiFile = TranslateKodiPath $currentKodiFile
							DebugLogInfo CONSIDERING KODI RESULT [($currentKodiUri)] [($currentTranslatedKodiFile)] [Playcount: ($currentJsonResult.playcount)]

							if(-not $Force)
							{
								if(IsPathInExclusionList $currentTranslatedKodiFile.ToLower() $AUDIT_EXCLUSIONS)
								{
									DebugLogInfo EXCLUDED: [($currentTranslatedKodiFile.ToLower())]
									continue
								}
							}

							if($seenKodiFiles.Contains($currentTranslatedKodiFile.ToLower()))
							{
								LogWarning Duplicate Kodi Entry found: [($currentKodiUri)] [($currentTranslatedKodiFile)]
							}
							else
							{
								$seenKodiFiles.Add($currentTranslatedKodiFile.ToLower()) > $null
							}
						}
					}
				} #EACH EPISODES / MOVIES LIST

				if($serverDown)
				{
					continue
				}

				$pathsToUse = @($DOWNLOADED_TV_PATH, $DOWNLOADED_MOVIES_PATH)
				if($Path.Count -gt 0)
				{
					$pathsToUse = $Path
				}

				foreach($currentPathToCheck in $pathsToUse)
				{
					if(IsInvalidPath $currentPathToCheck) { continue }
					$cCurrentProgress = 0
					$media = Get-ChildItem -Recurse -LiteralPath $currentPathToCheck -File -FollowSymlink
					foreach($currentMedia in $media)
					{
						$cCurrentProgress++
						OutputProgressBar ("Auditing Kodi Libraries [$currentPathToCheck]...") ($cCurrentProgress/$media.Count)

						if($seenFiles.Contains($currentMedia.FullName.ToLower()))
						{
							continue
						}
						else
						{
							$seenFiles.Add($currentMedia.FullName.ToLower()) > $null
						}

						if(-not $Force)
						{
							if($currentMedia.Extension -NotIn ($MEDIA_EXTENSIONS))
							{
								continue
							}

							if(IsAudio $currentMedia.FullName)
							{
								continue
							}

							if(IsPathInExclusionList $currentMedia.FullName $AUDIT_EXCLUSIONS)
							{
								DebugLogInfo EXCLUDED: [($currentMedia.FullName)]
								continue
							}
						}
						VerboseLogInfo Considering: [($currentMedia.FullName)]

						if(-not $seenKodiFiles.Contains($currentMedia.FullName.ToLower()))
						{
							LogWarning Kodi Entry not found: [($currentKodiUri)] [($currentMedia.FullName)]
						}
					}
				}
			} #EACH KODI SERVER
		}
	}
	catch
	{
		ReleaseScriptConcurrency
		throw
	}
}
End
{
	try
	{
		if($Help)
		{
			return
		}

		$executionTime = (Get-Date) - $start
		VerboseLogInfo Audit Time: [$executionTime]

	}
	finally
	{
		ReleaseScriptConcurrency
		PauseIfClicked
	}
}