@ECHO OFF

::SELECT TARGET POWERSHELL HOST
IF EXIST "%ProgramFiles%\PowerShell\7\pwsh.exe" (
  SET PS1_HOST="%ProgramFiles%\PowerShell\7\pwsh.exe"
) ELSE (
  SET PS1_HOST="%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe"
)

::DETERMINE IF SCRIPT WAS CLICKED
SETLOCAL enabledelayedexpansion
SET testl=%cmdcmdline:"=%
SET testr=!testl:%~nx0=!
IF NOT "%testl%" == "%testr%" (
  SET CMD_CLICKED="-Clicked"
)

::DETECT PREVIOUS INSTALL
IF EXIST "%~dp0\Shortcuts\CinAdmin Session (Win).lnk" (
  ECHO Past installation detected. Assuming we're installing for remote access.
  SET PAST_INSTALLED="-Remote"
  GOTO INSTALL
)

::ELEVATE SESSION IF NEEDED FOR LOCAL INSTALL
OPENFILES > nul
IF %ERRORLEVEL% EQU 1 (
  ECHO You are NOT an Administrator. Spawning a new elevated session...
  IF EXIST "%temp%\getadmin.vbs" ( DEL "%temp%\getadmin.vbs" )
  ECHO SET UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
  ECHO UAC.ShellExecute "cmd.exe", "/c ""%~s0""", "", "runas", 1 >> "%temp%\getadmin.vbs"
  "%temp%\getadmin.vbs"
  IF EXIST "%temp%\getadmin.vbs" ( DEL "%temp%\getadmin.vbs" )
  EXIT /B 0
)

:INSTALL
PUSHD "%~dp0"
%PS1_HOST% -NoLogo -Command "%CD%\Install-CinAdmin.ps1" %CMD_CLICKED% %PAST_INSTALLED%
