[CmdletBinding(SupportsShouldProcess=$true)]
param(
	[switch] $Help,
	[switch] $Force = $false,
	[switch] $AdRemoveSubscriptionOverride,
	[switch] $ExtractAudioSubscriptionOverride,
	[switch] $RenameSubscriptionOverride,
	[string] $RenameTo,
	[string] $Subscription,
	[Parameter(Position=0)]
	[string] $Uri
)

try
{
	$SCRIPT_PATH = Split-Path -parent $PSCommandPath
	. $SCRIPT_PATH\CinAdmin.Functions.ps1

	LockScriptConcurrency

	#########
	#
	# MAIN
	#
	#########

	if($Help)
	{
		ShowSyntax
		PauseIfClicked
		return
	}
	ShowConfiguration
	TestConfiguration

	if(IsValidPath $Uri)
	{
		$fileItems = (Get-ChildItem $Uri -File -Recurse -FollowSymlink)

		foreach($fileItem in $fileItems)
		{
			FlashMessage "Green" $fileItem.FullName

			LogInfo "Duration [" ( ReturnDuration $fileItem.FullName ) "]"

			LogInfo "Is extension in configured list [" ($fileItem.Extension -In ($MEDIA_EXTENSIONS)) "]"
			LogInfo "Is currently tracking [" ((LoadTrackedMedia).ContainsKey($fileItem.FullName)) "]"

			LogInfo "IsExcludedPath [" (IsExcludedPath $fileItem.FullName) "]"
			LogInfo "IsFileCurrentlyOpen [" (IsFileCurrentlyOpen $fileItem.FullName) "]"
			LogInfo "IsFileCurrentlyRecording [" (IsFileCurrentlyRecording $fileItem.FullName) "]"

			LogInfo "HasFileBeenRenamed [" (HasFileBeenRenamed $fileItem.FullName) "]"
			LogInfo "HasFileHasAdsRemoved [" (HasFileHasAdsRemoved $fileItem.FullName) "]"
			LogInfo "MediaHasSubtitles [" (MediaHasSubtitles $fileItem.FullName) "]"

			LogInfo "IsAudio [" (IsAudio $fileItem.FullName) "]"

			$isWatched = (IsWatched $fileItem.FullName)
			LogInfo "IsWatched [" $isWatched "]"
			if(-not $isWatched)
			{
				LogInfo "CalculateWatchedFolder [" (CalculateWatchedFolder $fileItem.FullName) "]"
			}

			$isMovie = (IsMovie $fileItem.FullName)
			LogInfo "IsMovie [" $isMovie  "]"
			if(-not $isMovie)
			{
				LogInfo "IsTopicalShow [" (IsTopicalShow $fileItem) "]"
				LogInfo "IsDailyShow [" (IsDailyShow $fileItem) "]"
				LogInfo "ExtractShowAirDate [" (ExtractShowAirDate $fileItem) "]"
				LogInfo "ExtractShowSeason [" (ExtractShowSeason $fileItem) "]"

				$tvdbId = (SearchTvDbForSeries $fileItem $false)
				LogInfo "TVDB ID [" $tvdbId "]"
				LogInfo "TVDB Series [" (GetTvDbSeriesCacheFile $tvdbId) "]"
				LogInfo "TVDB Episodes [" (GetTvDbEpisodesCacheFile $tvdbId) "]"

				$showName = (ExtractShowName $fileItem)
				LogInfo "ExtractShowName [" $showName "]"

				if(($SERIES_SUBSCRIPTION_MAPPING -eq $null) -or (-not ($SERIES_SUBSCRIPTION_MAPPING.ContainsKey($showName))))
				{
					LoadSubscriptionOverrideVariable $fileItem "ACQUISITION_REMOVE_ADS" > $null
				}

				if((IsStringDefined $showName) -and $SERIES_SUBSCRIPTION_MAPPING.ContainsKey($showName))
				{
					LogInfo "Mapped Subscription Settings [" ($SERIES_SUBSCRIPTION_MAPPING[$showName]) "]"
				}
				else
				{
					LogInfo "Mapped Subscription Settings [ DEFAULT ]"
				}

				LogInfo "ExtractBestGuessTitle [" (ExtractBestGuessTitle $fileItem) "]"
			}

			if($Force)
			{
				LogInfo "Metadata:"
				ExtractMetadataTags ($fileItem.FullName) | Format-List
			}
		}
		return
	}

	if($AdRemoveSubscriptionOverride -or $ExtractAudioSubscriptionOverride -or $RenameSubscriptionOverride)
	{
		#Handled Inside Functions
	}

	if(-not (IsStringDefined $Subscription))
	{
		$Subscription = $SUBSCRIPTION_INSTALL_PATH + "\defaults.ps1"
		LogWarning Using: [$Subscription]
	}

	if(IsInvalidPath $Subscription)
	{
		LogError Subscription not found: [$Subscription]
		return
	}

	if(IsInvalidPath $YOUTUBE_DL_BINARY)
	{
		LogError YouTube-Dl not found. You need these this tool to acquire media.
		return
	}

	if(-not ($YOUTUBE_DL_BINARY.EndsWith(".exe")) -and (IsInvalidPath $PYTHON_BINARY))
	{
		LogError YouTube-Dl is not a standalone executable and Python is not found. You need these tools to acquire media.
		return
	}

	if(($Uri -eq $null) -or ($Uri.Length -eq 0))
	{
		LogError "Uri parameter is required."
		return
	}

	if(-not $Force)
	{
		$subscriptionHistoryCsv = $SUBSCRIPTION_TMP + "\" + (Get-Item $Subscription).BaseName + ".csv"
		if(IsValidPath $subscriptionHistoryCsv)
		{
			$subscriptionHistory = Import-Csv $subscriptionHistoryCsv
			foreach($subscriptionEntry in $subscriptionHistory)
			{
				if($subscriptionHistory.Content -eq $Uri.ToLower())
				{
					LogWarning You have already downloaded this content. Use the Force parameter to override.
					return
				}
			}
		}
	}

	AcquireMediaContent $Subscription $Uri $RenameTo > $null
}
finally
{
	PurgeEmptyDirectories $CONVERSION_TMP $true
	ReleaseScriptConcurrency
	PauseIfClicked
}